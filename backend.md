# 后端开发规范
## 目录架构
    .scxz-backend：平台项目名称 
        .destroyer-appl：微服务模块
            .destroyer-core：核心模块
            .destroyer-user：客户用户中心（端口：9001）
            .destroyer-assess：评测中心（端口：9002）
            .destroyer-system：系统核心中心（端口：9099）
            .destroyer-xxx：xxx中心
            .
            .
        .destroyer-common：公共模块
        .destroyer-GatetWay：路由网关（端口：9000）

## 接口规范
### 接口结构
    http://ip:9000/api/destroyer/微服务中心名称/接口名称/接口方法
### swagger地址
    http://ip:微服务端口/doc.html
    [例]客户用户中心swagger地址：http://ip:9001/doc.html


### 接口示例
    以量表基础信息（scaleBaseInfo）为例：
        新增数据：http://ip:9000/sczx/assess/api/scaleBaseInfo/create
        根据ID删除：http://ip:9000/sczx/assess/api/scaleBaseInfo/deleteById
        查询列表：http://ip:9000/sczx/assess/api/scaleBaseInfo/list
        查询分页列表：http://ip:9000/sczx/assess/api/scaleBaseInfo/page
        修改数据：http://ip:9000/sczx/assess/api/scaleBaseInfo/edit

### 请求方法及参数
    [强制]项目所有接口请求方式保存一致
    method：POST

    [强制]请求参数需符合面向对象思想，均采用JSON对象格式传输
    新增：
        {
            "name": "",
            "sex": "",
            ...
        }

    删除：
        {
            "id": "",
            "idList": []
        }

    查询：
        {
            "name": "",
            "sex": "",
            ...
        }

    
    修改：
        {
            "pk": "",
            "name": "",
            "sex": "",
            ...
        }

### 接口响应结构
    {
        "code": 200,
        "msg": "操作成功",
        "success": true,
        "data": []
    }
### 接口响应code
    200：成功
    400：业务异常、缺少必要的请求参数等等
    401：请求未授权
    500：服务器异常  
### docker部署
    1.配置
[参考docker.md](./docker.md)
    
    2.打包命令
      1）单独打包 如：mvn clean package install -pl destroyer-gateway  -am
        mvn clean package install -pl destroyer-appl/destroyer-system  -am docker:build
      2) 整体   ：mvn clean package docker:build 
   