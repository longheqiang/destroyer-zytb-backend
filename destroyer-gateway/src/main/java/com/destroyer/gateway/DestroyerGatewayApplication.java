package com.destroyer.gateway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class DestroyerGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(DestroyerGatewayApplication.class, args);
        log.info("=======================GatetWay启动成功=======================");

    }

}
