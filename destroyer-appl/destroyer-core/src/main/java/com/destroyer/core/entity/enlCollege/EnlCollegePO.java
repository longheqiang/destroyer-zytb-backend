package com.destroyer.core.entity.enlCollege;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.enlCollege.bo.EnlCollegeBO;
import com.destroyer.core.entity.enlCollege.vo.EnlCollegeVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：招生院校(简单对象)
 * 说明：招生院校(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "enl_college", autoResultMap = true)
@ApiModel("招生院校(简单对象)")
public class EnlCollegePO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public EnlCollegePO(EnlCollegeBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 院校标识符
     */
    @ApiModelProperty(value = "院校标识符")
    private String collegeid;


    /**
     * 招生单位代码
     */
    @ApiModelProperty(value = "招生单位代码")
    private String collegecode;


    /**
     * 院校名称
     */
    @ApiModelProperty(value = "院校名称")
    private String collegename;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public EnlCollegeVO parseVO() {
        EnlCollegeVO rsp = new EnlCollegeVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}