package com.destroyer.core.entity.userInfo;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.userInfo.bo.UserInfoBO;
import com.destroyer.core.entity.userInfo.vo.UserInfoVO;
import cn.hutool.core.bean.BeanUtil;
import com.destroyer.core.entity.userRole.vo.UserRoleVO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * 标题：用户信息(简单对象)
 * 说明：用户信息(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-2-26
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "user_info", autoResultMap = true)
@ApiModel("用户信息(简单对象)")
public class UserInfoPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public UserInfoPO(UserInfoBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 用户代码
     */
    @ApiModelProperty(value = "用户代码")
    private String usercode;


    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String username;


    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称")
    private String nickname;


    /** 系统角色;超级管理员:admin,普通用户：normal */
    @ApiModelProperty(value = "系统角色;超级管理员:admin,普通用户：normal")
    private String sysRole ;


    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roleId;


    /**
     * 拼音
     */
    @ApiModelProperty(value = "拼音")
    private String pinyin;


    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;


    /**
     * 用户类型
     */
    @ApiModelProperty(value = "用户类型")
    private String usertype;


    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;


    /**
     * 电话
     */
    @ApiModelProperty(value = "电话")
    private String mobile;


    /**
     * QQ号
     */
    @ApiModelProperty(value = "QQ号")
    private String qq;


    /**
     * 微信号
     */
    @ApiModelProperty(value = "微信号")
    private String weixin;


    /**
     * 主题
     */
    @ApiModelProperty(value = "主题")
    private String theme;


    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    private String avatar;


    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;


    /**
     * 最近登录时间
     */
    @ApiModelProperty(value = "最近登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastlogintime;


    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段")
    private String jsonExt;


    /**
     * 备用1
     */
    @ApiModelProperty(value = "备用1")
    private String opt1;


    /**
     * 备用2
     */
    @ApiModelProperty(value = "备用2")
    private String opt2;


    /**
     * 备用3
     */
    @ApiModelProperty(value = "备用3")
    private String opt3;


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String procode;


    /**
     * 用户状态;正常:normal,禁用:disabled
     */
    @ApiModelProperty(value = "用户状态;正常:normal,禁用:disabled")
    private String userState;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public UserInfoVO parseVO() {
        UserInfoVO rsp = new UserInfoVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }

    /**
     * 解析为响应视图对象(自定义)
     *
     * @return VO对象
     */
    public UserInfoVO parseVO(Map<Long, UserRoleVO> userRoleMap) {
        UserInfoVO rsp = this.parseVO();
        if(null != userRoleMap && userRoleMap.size() > 0){
            rsp.setRole(userRoleMap.get(rsp.getRoleId()));
        }
        return rsp;
    }
}