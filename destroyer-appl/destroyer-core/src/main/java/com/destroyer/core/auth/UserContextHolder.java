package com.destroyer.core.auth;


import java.io.Serializable;

/**
 * 标题：用户上下文
 * 说明：用户上下文，用于保持线程中的用户信息
 * 时间：2023/3/20
 * 作者：admin
 */
public class UserContextHolder implements Serializable {

    private static final long serialVersionUID = -2395755360433248239L;

    //创建ThreadLocal保存User对象
    public static ThreadLocal<Object> holder = new ThreadLocal<>();

    /**
     * 移除线程中当前用户信息
     */
    public static void remove(){
        holder.remove();
    }
}
