package com.destroyer.core.entity.enlMajorplan.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.core.entity.enlMajorplan.EnlMajorplanPO;

/**
 * 标题：专业录取计划(查询业务对象)
 * 说明：专业录取计划(查询业务对象),常用于查询入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("专业录取计划(查询业务对象)")
public class EnlMajorplanQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 年份
     */
    @ApiModelProperty(value = "年份")
    private Integer annum;


    /**
     * 批次代码
     */
    @ApiModelProperty(value = "批次代码")
    private String pcdm;


    /**
     * 批次名称
     */
    @ApiModelProperty(value = "批次名称")
    private String pcmc;


    /**
     * 科类代码
     */
    @ApiModelProperty(value = "科类代码")
    private String kldm;


    /**
     * 科类名称
     */
    @ApiModelProperty(value = "科类名称")
    private String klmc;


    /**
     * 院校代码
     */
    @ApiModelProperty(value = "院校代码")
    private String collegecode;


    /**
     * 院校名称
     */
    @ApiModelProperty(value = "院校名称")
    private String collegename;


    /**
     * 院校计划人数
     */
    @ApiModelProperty(value = "院校计划人数")
    private Integer collegeplan;


    /**
     * 专业代码
     */
    @ApiModelProperty(value = "专业代码")
    private String majorcode;


    /**
     * 专业名称
     */
    @ApiModelProperty(value = "专业名称")
    private String majorname;


    /**
     * 专业备注
     */
    @ApiModelProperty(value = "专业备注")
    private String majorcmt;


    /**
     * 学制
     */
    @ApiModelProperty(value = "学制")
    private Integer needyears;


    /**
     * 学费
     */
    @ApiModelProperty(value = "学费")
    private Integer fee;


    /**
     * 选课要求
     */
    @ApiModelProperty(value = "选课要求")
    private String subjectneed;


    /**
     * 专业计划人数
     */
    @ApiModelProperty(value = "专业计划人数")
    private Integer majorplan;


    /**
     * 国标专业代码
     */
    @ApiModelProperty(value = "国标专业代码")
    private String stdmajorcode;


    /**
     * 前年匹配标识符
     */
    @ApiModelProperty(value = "前年匹配标识符")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long lastyearmatchid;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<EnlMajorplanPO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<EnlMajorplanPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(StringUtils.isNotBlank(this.getProno()), EnlMajorplanPO::getProno, this.getProno());
        rsp.eq(null != this.getAnnum(), EnlMajorplanPO::getAnnum, this.getAnnum());
        rsp.like(StringUtils.isNotBlank(this.getPcdm()), EnlMajorplanPO::getPcdm, this.getPcdm());
        rsp.like(StringUtils.isNotBlank(this.getPcmc()), EnlMajorplanPO::getPcmc, this.getPcmc());
        rsp.like(StringUtils.isNotBlank(this.getKldm()), EnlMajorplanPO::getKldm, this.getKldm());
        rsp.like(StringUtils.isNotBlank(this.getKlmc()), EnlMajorplanPO::getKlmc, this.getKlmc());
        rsp.like(StringUtils.isNotBlank(this.getCollegecode()), EnlMajorplanPO::getCollegecode, this.getCollegecode());
        rsp.like(StringUtils.isNotBlank(this.getCollegename()), EnlMajorplanPO::getCollegename, this.getCollegename());
        rsp.eq(null != this.getCollegeplan(), EnlMajorplanPO::getCollegeplan, this.getCollegeplan());
        rsp.like(StringUtils.isNotBlank(this.getMajorcode()), EnlMajorplanPO::getMajorcode, this.getMajorcode());
        rsp.like(StringUtils.isNotBlank(this.getMajorname()), EnlMajorplanPO::getMajorname, this.getMajorname());
        rsp.like(StringUtils.isNotBlank(this.getMajorcmt()), EnlMajorplanPO::getMajorcmt, this.getMajorcmt());
        rsp.eq(null != this.getNeedyears(), EnlMajorplanPO::getNeedyears, this.getNeedyears());
        rsp.eq(null != this.getFee(), EnlMajorplanPO::getFee, this.getFee());
        rsp.like(StringUtils.isNotBlank(this.getSubjectneed()), EnlMajorplanPO::getSubjectneed, this.getSubjectneed());
        rsp.eq(null != this.getMajorplan(), EnlMajorplanPO::getMajorplan, this.getMajorplan());
        rsp.like(StringUtils.isNotBlank(this.getStdmajorcode()), EnlMajorplanPO::getStdmajorcode, this.getStdmajorcode());
        rsp.eq(null != this.getLastyearmatchid(), EnlMajorplanPO::getLastyearmatchid, this.getLastyearmatchid());
        return rsp;
    }

}