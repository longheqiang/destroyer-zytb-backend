package com.destroyer.core.entity.userMenu.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.destroyer.core.entity.userMenu.UserMenuPO;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;

/**
 * 标题：系统菜单(查询业务对象)
 * 说明：系统菜单(查询业务对象),常用于查询入参
 * 时间：2024-2-27
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("系统菜单(查询业务对象)")
public class UserMenuQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long pid;


    /**
     * 菜单类型;管理后台:background,客户端:client
     */
    @ApiModelProperty(value = "菜单类型;管理后台:background,客户端:client")
    private String menuType;


    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String name;


    /**
     * 目录地址
     */
    @ApiModelProperty(value = "目录地址")
    private String path;


    /**
     * 部件
     */
    @ApiModelProperty(value = "部件")
    private String component;


    /**
     * 重定向子路由
     */
    @ApiModelProperty(value = "重定向子路由")
    private String redirect;


    /**
     * 扩展元数据
     */
    @ApiModelProperty(value = "扩展元数据")
    private String meta;


    /**
     * 顺序
     */
    @ApiModelProperty(value = "顺序")
    private Integer ord;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<UserMenuPO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<UserMenuPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(null != this.getPid(), UserMenuPO::getPid, this.getPid());
        rsp.like(StringUtils.isNotBlank(this.getMenuType()), UserMenuPO::getMenuType, this.getMenuType());
        rsp.like(StringUtils.isNotBlank(this.getName()), UserMenuPO::getName, this.getName());
        rsp.like(StringUtils.isNotBlank(this.getPath()), UserMenuPO::getPath, this.getPath());
        rsp.like(StringUtils.isNotBlank(this.getComponent()), UserMenuPO::getComponent, this.getComponent());
        rsp.like(StringUtils.isNotBlank(this.getRedirect()), UserMenuPO::getRedirect, this.getRedirect());
        rsp.like(StringUtils.isNotBlank(this.getMeta()), UserMenuPO::getMeta, this.getMeta());
        rsp.eq(null != this.getOrd(), UserMenuPO::getOrd, this.getOrd());
        return rsp;
    }

}