package com.destroyer.core.config.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.stereotype.Service;

/**
 * 标题：RediesKeyExpirationMessageListenerServiceImpl
 * 说明：redies KEY 过期消息回调
 * 时间：2023/3/17
 * 作者：admin
 */
@Service
@Slf4j
public class RediesKeyExpirationMessageListenerServiceImpl implements RediesKeyExpirationMessageListenerService {
    @Override
    public void onMessage(Message message) {
        String expireKey = message.toString();
        log.info("redis 响应====>" + expireKey);
        //异步调用处理方法
        Thread thread = new Thread(new Runnable() {
            public synchronized void run() {

            }
        });
        thread.start();
    }
}
