package com.destroyer.core.entity.wechat.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标题：WechatPrepayWithRequestPaymentVO
 * 说明：微信发起支付参数视图对象
 * 时间：2024/3/14
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("微信发起支付参数视图对象")
public class WechatPrepayWithRequestPaymentVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "appId")
    private String appId;

    @ApiModelProperty(value = "时间戳，标准北京时间")
    private String timestamp;

    @ApiModelProperty(value = "随机字符串，不长于32位。")
    private String nonceStr;

    @ApiModelProperty(value = "小程序下单接口返回的prepay_id参数值，提交格式如：prepay_id=***")
    private String packageVal;

    @ApiModelProperty(value = "签名类型，默认为RSA，仅支持RSA")
    private String signType;

    @ApiModelProperty(value = "签名")
    private String paySign;
}
