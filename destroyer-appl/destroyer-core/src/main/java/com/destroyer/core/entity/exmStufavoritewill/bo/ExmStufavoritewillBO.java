package com.destroyer.core.entity.exmStufavoritewill.bo;

import com.destroyer.core.entity.exmStufavoritewill.ExmStufavoritewillPO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

 /**
 * 标题：学生收藏志愿(通用业务对象)
 * 说明：学生收藏志愿(通用业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("学生收藏志愿(通用业务对象)")
public class ExmStufavoritewillBO extends ExmStufavoritewillPO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}