package com.destroyer.core.entity.userRoleMenu.bo;

import com.destroyer.core.entity.userRoleMenu.UserRoleMenuPO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

 /**
 * 标题：用户角色菜单权限(通用业务对象)
 * 说明：用户角色菜单权限(通用业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-2-27
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("用户角色菜单权限(通用业务对象)")
public class UserRoleMenuBO extends UserRoleMenuPO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}