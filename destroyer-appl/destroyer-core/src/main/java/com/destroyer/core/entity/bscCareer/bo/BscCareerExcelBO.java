package com.destroyer.core.entity.bscCareer.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：职业库(Excel业务对象)
 * 说明：职业库(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-7
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("职业库(Excel业务对象)")
public class BscCareerExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 父标识符
     */
    @ApiModelProperty(value = "父标识符")
    @ExcelProperty("父标识符")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long pid;

    /**
     * 职业代码
     */
    @ApiModelProperty(value = "职业代码")
    @ExcelProperty("职业代码")
    private String code;

    /**
     * 父代码
     */
    @ApiModelProperty(value = "父代码")
    @ExcelProperty("父代码")
    private String pcode;

    /**
     * 职业名
     */
    @ApiModelProperty(value = "职业名")
    @ExcelProperty("职业名")
    private String name;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    @ExcelProperty("备注")
    private String cmt;


    /**
     * 改造Excel模版
     *
     * @return
     */
    public BscCareerExcelBO buildExportModule() {
        this.pid = 0L;
        this.code = "职业代码";
        this.pcode = "父代码";
        this.name = "职业名";
        this.cmt = "备注";
        return this;
    }
}