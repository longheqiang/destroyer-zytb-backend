package com.destroyer.core.entity.exmStudentConsRecord.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：用户消费记录(Excel业务对象)
 * 说明：用户消费记录(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-7
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户消费记录(Excel业务对象)")
public class ExmStudentConsRecordExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;



    /** 学生id */
    @ApiModelProperty(value = "学生id")
    @ExcelProperty("学生id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuid;

    /** 金额 */
    @ApiModelProperty(value = "金额")
    @ExcelProperty("金额")
    private BigDecimal amount;

    /** 消费类型1：消费，-1：充值 */
    @ApiModelProperty(value = "消费类型1：消费，-1：充值")
    @ExcelProperty("消费类型1：消费，-1：充值")
    private Integer consType;









    /**
     * 改造Excel模版
     * @return
     */
    public ExmStudentConsRecordExcelBO buildExportModule(){
        this.stuid = 0L;
        this.amount = new BigDecimal(0.0);
        this.consType = 0;
        return this;
    }
}