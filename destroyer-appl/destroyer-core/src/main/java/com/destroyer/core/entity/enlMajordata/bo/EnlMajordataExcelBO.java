package com.destroyer.core.entity.enlMajordata.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：专业录取数据表(Excel业务对象)
 * 说明：专业录取数据表(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-7
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("专业录取数据表(Excel业务对象)")
public class EnlMajordataExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;



    /** 省份代码 */
    @ApiModelProperty(value = "省份代码")
    @ExcelProperty("省份代码")
    private String prono;

    /** 年份 */
    @ApiModelProperty(value = "年份")
    @ExcelProperty("年份")
    private Integer annum;

    /** 批次代码 */
    @ApiModelProperty(value = "批次代码")
    @ExcelProperty("批次代码")
    private String pcdm;

    /** 批次名称 */
    @ApiModelProperty(value = "批次名称")
    @ExcelProperty("批次名称")
    private String pcmc;

    /** 科类代码 */
    @ApiModelProperty(value = "科类代码")
    @ExcelProperty("科类代码")
    private String kldm;

    /** 科类名称 */
    @ApiModelProperty(value = "科类名称")
    @ExcelProperty("科类名称")
    private String klmc;

    /** 院校代码 */
    @ApiModelProperty(value = "院校代码")
    @ExcelProperty("院校代码")
    private String collegecode;

    /** 院校名 */
    @ApiModelProperty(value = "院校名")
    @ExcelProperty("院校名")
    private String collegename;

    /** 专业代码 */
    @ApiModelProperty(value = "专业代码")
    @ExcelProperty("专业代码")
    private String majorcode;

    /** 专业名 */
    @ApiModelProperty(value = "专业名")
    @ExcelProperty("专业名")
    private String majorname;

    /** 专业备注 */
    @ApiModelProperty(value = "专业备注")
    @ExcelProperty("专业备注")
    private String majorcmt;

    /** 选科需求 */
    @ApiModelProperty(value = "选科需求")
    @ExcelProperty("选科需求")
    private String subjectneeds;

    /** 国标专业代码 */
    @ApiModelProperty(value = "国标专业代码")
    @ExcelProperty("国标专业代码")
    private String stdmajorno;

    /** 国标专业名 */
    @ApiModelProperty(value = "国标专业名")
    @ExcelProperty("国标专业名")
    private String stdmajorname;

    /** 计划录取 */
    @ApiModelProperty(value = "计划录取")
    @ExcelProperty("计划录取")
    private Integer plannum;

    /** 实际录取 */
    @ApiModelProperty(value = "实际录取")
    @ExcelProperty("实际录取")
    private Integer realnum;

    /** 最低分 */
    @ApiModelProperty(value = "最低分")
    @ExcelProperty("最低分")
    private BigDecimal minscore;

    /** 最低分位次 */
    @ApiModelProperty(value = "最低分位次")
    @ExcelProperty("最低分位次")
    private Integer minscoreranking;

    /** 平均分 */
    @ApiModelProperty(value = "平均分")
    @ExcelProperty("平均分")
    private BigDecimal avgscore;

    /** 平均分位次 */
    @ApiModelProperty(value = "平均分位次")
    @ExcelProperty("平均分位次")
    private Integer avgscoreranking;

    /** 最高分 */
    @ApiModelProperty(value = "最高分")
    @ExcelProperty("最高分")
    private BigDecimal maxscore;

    /** 最高分位次 */
    @ApiModelProperty(value = "最高分位次")
    @ExcelProperty("最高分位次")
    private Integer maxscoreranking;

    /** 备用1 */
    @ApiModelProperty(value = "备用1")
    @ExcelProperty("备用1")
    private String opt1;

    /** 备用2 */
    @ApiModelProperty(value = "备用2")
    @ExcelProperty("备用2")
    private String opt2;

    /** 备用3 */
    @ApiModelProperty(value = "备用3")
    @ExcelProperty("备用3")
    private String opt3;

    /** 备用4 */
    @ApiModelProperty(value = "备用4")
    @ExcelProperty("备用4")
    private String opt4;

    /** 备用5 */
    @ApiModelProperty(value = "备用5")
    @ExcelProperty("备用5")
    private String opt5;









    /**
     * 改造Excel模版
     * @return
     */
    public EnlMajordataExcelBO buildExportModule(){
        this.prono = "省份代码";
        this.annum = 0;
        this.pcdm = "批次代码";
        this.pcmc = "批次名称";
        this.kldm = "科类代码";
        this.klmc = "科类名称";
        this.collegecode = "院校代码";
        this.collegename = "院校名";
        this.majorcode = "专业代码";
        this.majorname = "专业名";
        this.majorcmt = "专业备注";
        this.subjectneeds = "选科需求";
        this.stdmajorno = "国标专业代码";
        this.stdmajorname = "国标专业名";
        this.plannum = 0;
        this.realnum = 0;
        this.minscore = new BigDecimal(0.0);
        this.minscoreranking = 0;
        this.avgscore = new BigDecimal(0.0);
        this.avgscoreranking = 0;
        this.maxscore = new BigDecimal(0.0);
        this.maxscoreranking = 0;
        this.opt1 = "备用1";
        this.opt2 = "备用2";
        this.opt3 = "备用3";
        this.opt4 = "备用4";
        this.opt5 = "备用5";
        return this;
    }
}