package com.destroyer.core.entity.sysFile;

import com.baomidou.mybatisplus.annotation.TableName;
import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.sysFile.bo.SysFileBO;
import com.destroyer.core.entity.sysFile.vo.SysFileVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * 标题：系统附件信息(简单对象)
 * 说明：系统附件信息(简单对象),属性与表字段一一对应，包括类型
 * 时间：2023-10-11
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "sys_file", autoResultMap = true)
@ApiModel("系统附件信息(简单对象)")
public class SysFilePO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public SysFilePO(SysFileBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 附件名称
     */
    @ApiModelProperty(value = "附件名称")
    private String name;


    /**
     * 附件类型;文档：doc，图片：imag，音频：audio，视频：video，压缩文件：cmp，其他：other
     */
    @ApiModelProperty(value = "附件类型;文档：doc，图片：imag，音频：audio，视频：video，压缩文件：cmp，其他：other")
    private String fileType;


    /**
     * URL
     */
    @ApiModelProperty(value = "URL")
    private String url;


    /** 大小 */
    @ApiModelProperty(value = "大小")
    private String fileSize ;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public SysFileVO parseVO() {
        SysFileVO rsp = new SysFileVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}