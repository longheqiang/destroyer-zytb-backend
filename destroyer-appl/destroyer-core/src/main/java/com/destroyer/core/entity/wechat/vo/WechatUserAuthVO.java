package com.destroyer.core.entity.wechat.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标题：WechatUserAuthVO
 * 说明：微信用户授权视图对象
 * 时间：2024/3/4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("微信用户授权视图对象")
public class WechatUserAuthVO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * session_key
     */
    @ApiModelProperty(value = "session_key")
    private String sessionKey;

    /**
     * unionid
     */
    @ApiModelProperty(value = "unionid")
    private String unionid;


    /**
     * openid
     */
    @ApiModelProperty(value = "openid")
    private String openid;

}
