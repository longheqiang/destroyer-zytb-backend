package com.destroyer.core.entity.enlCollege.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.core.entity.enlCollege.EnlCollegePO;

/**
 * 标题：招生院校(查询业务对象)
 * 说明：招生院校(查询业务对象),常用于查询入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("招生院校(查询业务对象)")
public class EnlCollegeQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 院校标识符
     */
    @ApiModelProperty(value = "院校标识符")
    private String collegeid;


    /**
     * 招生单位代码
     */
    @ApiModelProperty(value = "招生单位代码")
    private String collegecode;


    /**
     * 院校名称
     */
    @ApiModelProperty(value = "院校名称")
    private String collegename;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<EnlCollegePO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<EnlCollegePO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(StringUtils.isNotBlank(this.getProno()), EnlCollegePO::getProno, this.getProno());
        rsp.like(StringUtils.isNotBlank(this.getCollegeid()), EnlCollegePO::getCollegeid, this.getCollegeid());
        rsp.like(StringUtils.isNotBlank(this.getCollegecode()), EnlCollegePO::getCollegecode, this.getCollegecode());
        rsp.like(StringUtils.isNotBlank(this.getCollegename()), EnlCollegePO::getCollegename, this.getCollegename());
        return rsp;
    }

}