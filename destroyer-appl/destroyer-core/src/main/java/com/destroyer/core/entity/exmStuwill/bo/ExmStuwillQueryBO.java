package com.destroyer.core.entity.exmStuwill.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.core.entity.exmStuwill.ExmStuwillPO;

/**
 * 标题：学生志愿(查询业务对象)
 * 说明：学生志愿(查询业务对象),常用于查询入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("学生志愿(查询业务对象)")
public class ExmStuwillQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 学生id
     */
    @ApiModelProperty(value = "学生id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuid;


    /**
     * 高考年份
     */
    @ApiModelProperty(value = "高考年份")
    private Integer gkyear;


    /**
     * 批次代码
     */
    @ApiModelProperty(value = "批次代码")
    private String pcdm;


    /**
     * 批次名称
     */
    @ApiModelProperty(value = "批次名称")
    private String pcmc;


    /**
     * 科类代码
     */
    @ApiModelProperty(value = "科类代码")
    private String kldm;


    /**
     * 科类名称
     */
    @ApiModelProperty(value = "科类名称")
    private String klmc;


    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer theorder;


    /**
     * 院校id
     */
    @ApiModelProperty(value = "院校id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long collegeid;


    /**
     * 院校代码
     */
    @ApiModelProperty(value = "院校代码")
    private String collegeno;


    /**
     * 院校名称
     */
    @ApiModelProperty(value = "院校名称")
    private String collegename;


    /**
     * 专业代码
     */
    @ApiModelProperty(value = "专业代码")
    private String majorno;


    /**
     * 专业名
     */
    @ApiModelProperty(value = "专业名")
    private String majorname;


    /**
     * 前年数据标识符
     */
    @ApiModelProperty(value = "前年数据标识符")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long lastyeardataid;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<ExmStuwillPO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<ExmStuwillPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.like(StringUtils.isNotBlank(this.getProno()), ExmStuwillPO::getProno, this.getProno());
        rsp.eq(null != this.getStuid(), ExmStuwillPO::getStuid, this.getStuid());
        rsp.eq(null != this.getGkyear(), ExmStuwillPO::getGkyear, this.getGkyear());
        rsp.like(StringUtils.isNotBlank(this.getPcdm()), ExmStuwillPO::getPcdm, this.getPcdm());
        rsp.like(StringUtils.isNotBlank(this.getPcmc()), ExmStuwillPO::getPcmc, this.getPcmc());
        rsp.like(StringUtils.isNotBlank(this.getKldm()), ExmStuwillPO::getKldm, this.getKldm());
        rsp.like(StringUtils.isNotBlank(this.getKlmc()), ExmStuwillPO::getKlmc, this.getKlmc());
        rsp.eq(null != this.getTheorder(), ExmStuwillPO::getTheorder, this.getTheorder());
        rsp.eq(null != this.getCollegeid(), ExmStuwillPO::getCollegeid, this.getCollegeid());
        rsp.like(StringUtils.isNotBlank(this.getCollegeno()), ExmStuwillPO::getCollegeno, this.getCollegeno());
        rsp.like(StringUtils.isNotBlank(this.getCollegename()), ExmStuwillPO::getCollegename, this.getCollegename());
        rsp.like(StringUtils.isNotBlank(this.getMajorno()), ExmStuwillPO::getMajorno, this.getMajorno());
        rsp.like(StringUtils.isNotBlank(this.getMajorname()), ExmStuwillPO::getMajorname, this.getMajorname());
        rsp.eq(null != this.getLastyeardataid(), ExmStuwillPO::getLastyeardataid, this.getLastyeardataid());
        return rsp;
    }

}