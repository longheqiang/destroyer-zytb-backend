package com.destroyer.core.entity.bscYear.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：年份(Excel业务对象)
 * 说明：年份(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-7
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("年份(Excel业务对象)")
public class BscYearExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;



    /** 年份 */
    @ApiModelProperty(value = "年份")
    @ExcelProperty("年份")
    private Integer annum;









    /**
     * 改造Excel模版
     * @return
     */
    public BscYearExcelBO buildExportModule(){
        this.annum = 0;
        return this;
    }
}