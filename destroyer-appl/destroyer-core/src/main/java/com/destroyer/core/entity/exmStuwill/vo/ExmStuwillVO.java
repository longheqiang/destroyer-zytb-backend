package com.destroyer.core.entity.exmStuwill.vo;

import com.destroyer.core.entity.exmStuwill.ExmStuwillPO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.List;

 /**
 * 标题：学生志愿(基础响应视图对象)
 * 说明：学生志愿(基础响应视图对象),作为前端、其他服务调用时的响应出参
 * 时间：2024-3-1
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("学生志愿(基础响应视图对象)")
public class ExmStuwillVO extends ExmStuwillPO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}