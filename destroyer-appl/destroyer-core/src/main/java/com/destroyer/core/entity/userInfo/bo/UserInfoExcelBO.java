package com.destroyer.core.entity.userInfo.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：用户信息(Excel业务对象)
 * 说明：用户信息(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-7
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户信息(Excel业务对象)")
public class UserInfoExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;



    /** 用户代码 */
    @ApiModelProperty(value = "用户代码")
    @ExcelProperty("用户代码")
    private String usercode;

    /** 用户名 */
    @ApiModelProperty(value = "用户名")
    @ExcelProperty("用户名")
    private String username;

    /** 昵称 */
    @ApiModelProperty(value = "昵称")
    @ExcelProperty("昵称")
    private String nickname;

    /** 系统角色;超级管理员:admin,普通用户：normal */
    @ApiModelProperty(value = "系统角色;超级管理员:admin,普通用户：normal")
    @ExcelProperty("系统角色;超级管理员:admin,普通用户：normal")
    private String sysRole;

    /** 角色ID */
    @ApiModelProperty(value = "角色ID")
    @ExcelProperty("角色ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roleId;

    /** 拼音 */
    @ApiModelProperty(value = "拼音")
    @ExcelProperty("拼音")
    private String pinyin;

    /** 密码 */
    @ApiModelProperty(value = "密码")
    @ExcelProperty("密码")
    private String password;

    /** 用户类型 */
    @ApiModelProperty(value = "用户类型")
    @ExcelProperty("用户类型")
    private String usertype;

    /** 邮箱 */
    @ApiModelProperty(value = "邮箱")
    @ExcelProperty("邮箱")
    private String email;

    /** 电话 */
    @ApiModelProperty(value = "电话")
    @ExcelProperty("电话")
    private String mobile;

    /** QQ号 */
    @ApiModelProperty(value = "QQ号")
    @ExcelProperty("QQ号")
    private String qq;

    /** 微信号 */
    @ApiModelProperty(value = "微信号")
    @ExcelProperty("微信号")
    private String weixin;

    /** 主题 */
    @ApiModelProperty(value = "主题")
    @ExcelProperty("主题")
    private String theme;

    /** 头像 */
    @ApiModelProperty(value = "头像")
    @ExcelProperty("头像")
    private String avatar;

    /** 描述 */
    @ApiModelProperty(value = "描述")
    @ExcelProperty("描述")
    private String description;

    /** 最近登录时间 */
    @ApiModelProperty(value = "最近登录时间")
    @ExcelProperty("最近登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastlogintime;

    /** 扩展字段 */
    @ApiModelProperty(value = "扩展字段")
    @ExcelProperty("扩展字段")
    private String jsonExt;

    /** 备用1 */
    @ApiModelProperty(value = "备用1")
    @ExcelProperty("备用1")
    private String opt1;

    /** 备用2 */
    @ApiModelProperty(value = "备用2")
    @ExcelProperty("备用2")
    private String opt2;

    /** 备用3 */
    @ApiModelProperty(value = "备用3")
    @ExcelProperty("备用3")
    private String opt3;

    /** 省份代码 */
    @ApiModelProperty(value = "省份代码")
    @ExcelProperty("省份代码")
    private String procode;

    /** 用户状态;正常:normal,禁用:disabled */
    @ApiModelProperty(value = "用户状态;正常:normal,禁用:disabled")
    @ExcelProperty("用户状态;正常:normal,禁用:disabled")
    private String userState;









    /**
     * 改造Excel模版
     * @return
     */
    public UserInfoExcelBO buildExportModule(){
        this.usercode = "用户代码";
        this.username = "用户名";
        this.nickname = "昵称";
        this.sysRole = "系统角色;超级管理员:admin,普通用户：normal";
        this.roleId = 0L;
        this.pinyin = "拼音";
        this.password = "密码";
        this.usertype = "用户类型";
        this.email = "邮箱";
        this.mobile = "电话";
        this.qq = "QQ号";
        this.weixin = "微信号";
        this.theme = "主题";
        this.avatar = "头像";
        this.description = "描述";
        this.lastlogintime = DateUtils.getCurrentDateYYYYMMDDHHMMSS();
        this.jsonExt = "扩展字段";
        this.opt1 = "备用1";
        this.opt2 = "备用2";
        this.opt3 = "备用3";
        this.procode = "省份代码";
        this.userState = "用户状态;正常:normal,禁用:disabled";
        return this;
    }
}