package com.destroyer.core.service.impl.sysFileRel;

import com.destroyer.core.entity.sysFileRel.bo.SysFileRelDeleteByBizBO;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelQueryBO;
import com.destroyer.core.entity.sysFileRel.vo.SysFileRelVO;
import com.destroyer.core.feign.api.ISysFileRelFeignClient;
import com.destroyer.common.entity.system.Result;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.service.sysFileRel.ISysFileRelFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 标题：SysFileRelFeignServiceImpl
 * 说明：
 * 时间：2023/10/19
 * 作者：admin
 */
@Service
public class SysFileRelFeignServiceImpl implements ISysFileRelFeignService {


    @Autowired
    private ISysFileRelFeignClient iSysFileRelFeignClient;


    /**
     * 根据业务ID和业务类型分组查询map集合API
     * @param req
     * @return
     */
    @Override
    public boolean DeleteByBizTypeAndBizId(SysFileRelDeleteByBizBO req) {
        boolean rsp = false;
        Result<Boolean> result = iSysFileRelFeignClient.deleteByBizTypeAndBizId(req);
        if(result.isSuccess()){
            rsp = result.getData();
        }else {
            throw new ServiceException(ResultEnum.FAILURE, result.getMsg());
        }
        return rsp;
    }



    /**
     * 根据业务ID和业务类型分组查询map集合API
     * @param req
     * @return
     */
    @Override
    public Map<Long, Map<String, List<SysFileRelVO>>> mapGroupByBizIdAndBizType(SysFileRelQueryBO req) {
        Map<Long, Map<String, List<SysFileRelVO>>> rsp = new HashMap<>();
        Result<Map<Long, Map<String, List<SysFileRelVO>>>> result = iSysFileRelFeignClient.mapGroupByBizIdAndBizType(req);
        if(result.isSuccess()){
            rsp = result.getData();
        }else {
            throw new ServiceException(ResultEnum.FAILURE, result.getMsg());
        }
        return rsp;
    }


}
