package com.destroyer.core.entity.exmStuwill.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：学生志愿(Excel业务对象)
 * 说明：学生志愿(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-7
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("学生志愿(Excel业务对象)")
public class ExmStuwillExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;



    /** 省份代码 */
    @ApiModelProperty(value = "省份代码")
    @ExcelProperty("省份代码")
    private String prono;

    /** 学生id */
    @ApiModelProperty(value = "学生id")
    @ExcelProperty("学生id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuid;

    /** 高考年份 */
    @ApiModelProperty(value = "高考年份")
    @ExcelProperty("高考年份")
    private Integer gkyear;

    /** 批次代码 */
    @ApiModelProperty(value = "批次代码")
    @ExcelProperty("批次代码")
    private String pcdm;

    /** 批次名称 */
    @ApiModelProperty(value = "批次名称")
    @ExcelProperty("批次名称")
    private String pcmc;

    /** 科类代码 */
    @ApiModelProperty(value = "科类代码")
    @ExcelProperty("科类代码")
    private String kldm;

    /** 科类名称 */
    @ApiModelProperty(value = "科类名称")
    @ExcelProperty("科类名称")
    private String klmc;

    /** 排序 */
    @ApiModelProperty(value = "排序")
    @ExcelProperty("排序")
    private Integer theorder;

    /** 院校id */
    @ApiModelProperty(value = "院校id")
    @ExcelProperty("院校id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long collegeid;

    /** 院校代码 */
    @ApiModelProperty(value = "院校代码")
    @ExcelProperty("院校代码")
    private String collegeno;

    /** 院校名称 */
    @ApiModelProperty(value = "院校名称")
    @ExcelProperty("院校名称")
    private String collegename;

    /** 专业代码 */
    @ApiModelProperty(value = "专业代码")
    @ExcelProperty("专业代码")
    private String majorno;

    /** 专业名 */
    @ApiModelProperty(value = "专业名")
    @ExcelProperty("专业名")
    private String majorname;

    /** 前年数据标识符 */
    @ApiModelProperty(value = "前年数据标识符")
    @ExcelProperty("前年数据标识符")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long lastyeardataid;









    /**
     * 改造Excel模版
     * @return
     */
    public ExmStuwillExcelBO buildExportModule(){
        this.prono = "省份代码";
        this.stuid = 0L;
        this.gkyear = 0;
        this.pcdm = "批次代码";
        this.pcmc = "批次名称";
        this.kldm = "科类代码";
        this.klmc = "科类名称";
        this.theorder = 0;
        this.collegeid = 0L;
        this.collegeno = "院校代码";
        this.collegename = "院校名称";
        this.majorno = "专业代码";
        this.majorname = "专业名";
        this.lastyeardataid = 0L;
        return this;
    }
}