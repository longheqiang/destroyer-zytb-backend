package com.destroyer.core.wrapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 打包器
 *
 * @param <T>
 * @param <V>
 */
public abstract class BaseWrapper<T, V> {
    public abstract V entityVO(T t);

    public List<V> listVO(List<T> list) {
        return list.stream()
                .filter(Objects::nonNull)
                .map(this::entityVO)
                .collect(Collectors.toList());
    }
}
