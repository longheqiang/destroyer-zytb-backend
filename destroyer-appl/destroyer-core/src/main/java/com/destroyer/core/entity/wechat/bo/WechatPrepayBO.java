package com.destroyer.core.entity.wechat.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标题：WechatPrepayBO
 * 说明：微信支付下单业务对象
 * 时间：2024/3/12
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("微信支付下单业务对象")
public class WechatPrepayBO implements Serializable {
    private static final long serialVersionUID = 1L;



    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号")
    private String orderNo;

    /**
     * 总金额 说明：订单总金额，单位为分
     */
    @ApiModelProperty(value = "总金额 说明：订单总金额，单位为分")
    private Integer total;



    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;

}
