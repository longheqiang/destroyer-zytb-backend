package com.destroyer.core.config.myBatisPlus;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 标题：MyBatisPlusConfig
 * 说明：MP 配置类
 * 时间：2023/8/1
 * 作者：admin
 */
@Configuration
@Component
public class MyBatisPlusConfig {
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        /*创建mp拦截器*/
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        /*创建分页插件*/
        PaginationInnerInterceptor pagInterceptor = new PaginationInnerInterceptor();
        /*设置请求的页面大于最大页容量后的请求操作，true回调第一页，false继续翻页，默认翻页*/
        pagInterceptor.setOverflow(false);
        /*设置单页分页的条数限制*/
        pagInterceptor.setMaxLimit(500L);
        /*设置数据库类型*/
        pagInterceptor.setDbType(DbType.MYSQL);
        /*将分页拦截器添加到mp拦截器中*/
        interceptor.addInnerInterceptor(pagInterceptor);
        return interceptor;

    }


}
