package com.destroyer.core.entity.test;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * Excel 导入数据库 沙具信息。
 */
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Data
@AllArgsConstructor
public class TestExcelBO implements Serializable {

    public void getExample(){

    }
    public TestExcelBO(boolean isGetExample) {
      if(isGetExample){
          this.index = 1;
          this.businessId = "100000466492";
          this.companyCode = "1204";
          this.companyText = "1204-泸州老窖怀旧酒类营销有限公司";
          this.startFrom = "2023/9/1";
          this.endAt = "2023/9/1";
          this.city = "283";
          this.cityText = "德州";
          this.stayPeriod = 1;
          this.orderType = 1;
          this.orderFrom = "线上订单";
          this.supplierName = "胜意之旅CPS";
          this.stayNo = "HT2309010001246NV3";
          this.commentTxt = "";
      }
    }

    public TestExcelBO() {
    }

    @ExcelProperty("序号")
    private Integer index;

    @ExcelProperty("单据号")
    private String businessId;

    @ExcelProperty("公司代码")
    private String companyCode;

    @ExcelProperty("公司名称")
    private String companyText;


    @ExcelProperty("开始日期")
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @JsonFormat(pattern = "yyyy/MM/dd", timezone = "GMT+8")
    private String startFrom;

    @ExcelProperty("结束日期")
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    @JsonFormat(pattern = "yyyy/MM/dd", timezone = "GMT+8")
    private String endAt;



    @ExcelProperty("城市编号")
    private String city;


    @ExcelProperty("城市")
    private String cityText;



    @ExcelProperty("入住天数")
    private Integer stayPeriod;

    @ExcelProperty("拆分后天数")
    private Integer stayPeriodSplit;

    @ExcelProperty("退改标识")
    private Integer orderType;

    @ExcelProperty("订单来源")
    private String orderFrom;


    @ExcelProperty("供应商")
    private String supplierName;

    @ExcelProperty("差旅订单号")
    private String stayNo;




    @ExcelProperty("报销人")
    private String applicantUser;


    @ExcelProperty("填报人")
    private String createUser;


    @ExcelProperty("备注")
    private String commentTxt;



}
