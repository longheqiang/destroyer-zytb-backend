package com.destroyer.core.entity.sysFile.vo;

import com.destroyer.core.entity.sysFile.SysFilePO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 标题：系统附件信息(基础响应视图对象)
 * 说明：系统附件信息(基础响应视图对象),作为前端、其他服务调用时的响应出参
 * 时间：2023-10-11
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("系统附件信息(基础响应视图对象)")
public class SysFileVO extends SysFilePO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}