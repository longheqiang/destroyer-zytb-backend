package com.destroyer.core.entity.wechat.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标题：WechatTransactionVO
 * 说明：微信交易视图对象
 * 时间：2024/3/14
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("微信交易视图对象")
public class WechatTransactionVO implements Serializable {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty("APPID")
    private String spAppid;

    @ApiModelProperty("APPID")
    private String subAppid;

    @ApiModelProperty("商家ID")
    private String spMchid;

    @ApiModelProperty("商家ID")
    private String subMchid;

    @ApiModelProperty("银行类型")
    private String bankType;

    @ApiModelProperty("业务订单号")
    private String outTradeNo;

    @ApiModelProperty("总金额")
    private Integer total;

    @ApiModelProperty("成功时间")
    private String successTime;

}
