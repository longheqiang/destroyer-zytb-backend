package com.destroyer.core.entity.bscProvince.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：省份表(Excel业务对象)
 * 说明：省份表(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-7
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("省份表(Excel业务对象)")
public class BscProvinceExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;



    /** 省份代码 */
    @ApiModelProperty(value = "省份代码")
    @ExcelProperty("省份代码")
    private String prono;

    /** 全称 */
    @ApiModelProperty(value = "全称")
    @ExcelProperty("全称")
    private String proname;

    /** 简称 */
    @ApiModelProperty(value = "简称")
    @ExcelProperty("简称")
    private String proshortname;

    /** 缩写 */
    @ApiModelProperty(value = "缩写")
    @ExcelProperty("缩写")
    private String proabbr;









    /**
     * 改造Excel模版
     * @return
     */
    public BscProvinceExcelBO buildExportModule(){
        this.prono = "省份代码";
        this.proname = "全称";
        this.proshortname = "简称";
        this.proabbr = "缩写";
        return this;
    }
}