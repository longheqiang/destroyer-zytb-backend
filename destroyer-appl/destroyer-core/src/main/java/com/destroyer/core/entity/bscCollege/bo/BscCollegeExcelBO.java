package com.destroyer.core.entity.bscCollege.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：院校库(Excel业务对象)
 * 说明：院校库(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-7
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("院校库(Excel业务对象)")
public class BscCollegeExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;



    /** 省份代码 */
    @ApiModelProperty(value = "省份代码")
    @ExcelProperty("省份代码")
    private String prono;

    /** 省份 */
    @ApiModelProperty(value = "省份")
    @ExcelProperty("省份")
    private String proshortname;

    /** 城市 */
    @ApiModelProperty(value = "城市")
    @ExcelProperty("城市")
    private String city;

    /** 国标代码 */
    @ApiModelProperty(value = "国标代码")
    @ExcelProperty("国标代码")
    private String collegeuid;

    /** 院校名称 */
    @ApiModelProperty(value = "院校名称")
    @ExcelProperty("院校名称")
    private String collegename;

    /** 隶属于 */
    @ApiModelProperty(value = "隶属于")
    @ExcelProperty("隶属于")
    private String belongto;

    /** 类型 */
    @ApiModelProperty(value = "类型")
    @ExcelProperty("类型")
    private String categories;

    /** 办学层次 */
    @ApiModelProperty(value = "办学层次")
    @ExcelProperty("办学层次")
    private String edulevel;

    /** 办学性质 */
    @ApiModelProperty(value = "办学性质")
    @ExcelProperty("办学性质")
    private String naturetype;

    /** 校徽 */
    @ApiModelProperty(value = "校徽")
    @ExcelProperty("校徽")
    private String logofile;

    /** 特征 */
    @ApiModelProperty(value = "特征")
    @ExcelProperty("特征")
    private String features;

    /** 邮箱 */
    @ApiModelProperty(value = "邮箱")
    @ExcelProperty("邮箱")
    private String email;

    /** 地址 */
    @ApiModelProperty(value = "地址")
    @ExcelProperty("地址")
    private String address;

    /** 网址 */
    @ApiModelProperty(value = "网址")
    @ExcelProperty("网址")
    private String url;

    /** 简介 */
    @ApiModelProperty(value = "简介")
    @ExcelProperty("简介")
    private String intro;

    /** 短简介 */
    @ApiModelProperty(value = "短简介")
    @ExcelProperty("短简介")
    private String sintro;

    /** 排名 */
    @ApiModelProperty(value = "排名")
    @ExcelProperty("排名")
    private Integer ranking;

    /** WSL排名 */
    @ApiModelProperty(value = "WSL排名")
    @ExcelProperty("WSL排名")
    private Integer rankingofwsl;

    /** 软科排名 */
    @ApiModelProperty(value = "软科排名")
    @ExcelProperty("软科排名")
    private Integer rankingofrk;

    /** 校友会 */
    @ApiModelProperty(value = "校友会")
    @ExcelProperty("校友会")
    private Integer rankingofxyh;

    /** QS排名 */
    @ApiModelProperty(value = "QS排名")
    @ExcelProperty("QS排名")
    private Integer rankingofqs;

    /** US排名 */
    @ApiModelProperty(value = "US排名")
    @ExcelProperty("US排名")
    private Integer rankingofusnews;

    /** 易度排名 */
    @ApiModelProperty(value = "易度排名")
    @ExcelProperty("易度排名")
    private Integer rankingofedu;

    /** 推荐指数 */
    @ApiModelProperty(value = "推荐指数")
    @ExcelProperty("推荐指数")
    private BigDecimal comscore;









    /**
     * 改造Excel模版
     * @return
     */
    public BscCollegeExcelBO buildExportModule(){
        this.prono = "省份代码";
        this.proshortname = "省份";
        this.city = "城市";
        this.collegeuid = "国标代码";
        this.collegename = "院校名称";
        this.belongto = "隶属于";
        this.categories = "类型";
        this.edulevel = "办学层次";
        this.naturetype = "办学性质";
        this.logofile = "校徽";
        this.features = "特征";
        this.email = "邮箱";
        this.address = "地址";
        this.url = "网址";
        this.intro = "简介";
        this.sintro = "短简介";
        this.ranking = 0;
        this.rankingofwsl = 0;
        this.rankingofrk = 0;
        this.rankingofxyh = 0;
        this.rankingofqs = 0;
        this.rankingofusnews = 0;
        this.rankingofedu = 0;
        this.comscore = new BigDecimal(0.0);
        return this;
    }
}