package com.destroyer.core.entity.sysDic.vo;

import com.destroyer.core.entity.sysDic.SysDicPO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标题：系统字典(基础响应视图对象)
 * 说明：系统字典(基础响应视图对象),作为前端、其他服务调用时的响应出参
 * 时间：2023-9-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("系统字典(基础响应视图对象)")
public class SysDicVO extends SysDicPO implements Serializable {
    private static final long serialVersionUID = 1L;




}