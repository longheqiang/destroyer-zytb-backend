package com.destroyer.core.controller;


import com.alibaba.nacos.api.naming.pojo.ServiceInfo;
import com.destroyer.common.entity.system.Result;
import com.destroyer.common.util.DateUtils;
import com.destroyer.core.entity.base.ServiceInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * 标题：ApiController
 * 说明：
 * 时间：2023/9/7
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api")
@Api(value = "ApiController", tags = "当前服务信息")
public class ApiController {


    @Value("${app.build.time}")
    private String buildTime;

    @Value("${app.version}")
    private String version;

    @Value("${spring.application.cname}")
    private String applicationCname;

    @Value("${spring.application.desc}")
    private String applicationDesc;

    private final Environment environment;

    public ApiController(Environment environment) {
        this.environment = environment;
    }



    @GetMapping(value = "/getServerInfo")
    @ApiOperation("获取当前服务器的信息")
    public Result<ServiceInfoVO> getServerInfo() {
        ServiceInfoVO rsp = ServiceInfoVO.builder()
                .buildTime(DateUtils.utcToGM8(buildTime))
                .name(applicationCname)
                .description(applicationDesc)
                .profile(Arrays.toString(environment.getActiveProfiles()))
                .version(version)
                .build();
        return Result.success(rsp);
    }


}
