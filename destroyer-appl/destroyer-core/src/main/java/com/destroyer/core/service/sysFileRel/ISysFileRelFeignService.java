package com.destroyer.core.service.sysFileRel;

import com.destroyer.core.entity.sysFileRel.bo.SysFileRelDeleteByBizBO;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelQueryBO;
import com.destroyer.core.entity.sysFileRel.vo.SysFileRelVO;

import java.util.List;
import java.util.Map;

/**
 * 标题：ISysFileRelFeignService
 * 说明：
 * 时间：2023/10/19
 * 作者：admin
 */
public interface ISysFileRelFeignService {

    /**
     * 删除数据API(根据业务ID和业务类型)
     * @param req
     * @return
     */
    boolean DeleteByBizTypeAndBizId(SysFileRelDeleteByBizBO req);


    /**
     * 根据业务ID和业务类型分组查询map集合API
     * @param req
     * @return
     */
    Map<Long, Map<String, List<SysFileRelVO>>> mapGroupByBizIdAndBizType(SysFileRelQueryBO req);
}
