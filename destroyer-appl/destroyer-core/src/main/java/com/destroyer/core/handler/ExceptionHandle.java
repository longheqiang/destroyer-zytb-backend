
package com.destroyer.core.handler;


import com.destroyer.common.entity.system.Result;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServerDataException;
import com.destroyer.common.exception.ServerParamException;
import com.destroyer.common.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.PrintWriter;
import java.io.StringWriter;

@RestControllerAdvice
@Slf4j
public class ExceptionHandle {
    public ExceptionHandle() {
    }

    /**
     * 拦截全局基础统一异常
     * @param e
     * @return
     */
    @ExceptionHandler({ServiceException.class})
    @ResponseBody
    public Result handle(ServiceException e) {
        log.error("===============================ERROR===============================");
        log.error(e.getMessage());
        log.error(errorInfoToString(e));
        log.error("===============================ERROR===============================");
        return Result.fail(e.getResult(),e.getMessage());
    }

    /**
     * 拦截全局参数统一异常
     * @param e
     * @return
     */
    @ExceptionHandler({ServerParamException.class})
    @ResponseBody
    public Result handle(ServerParamException e) {
        log.error("===============================PARAM-ERROR===============================");
        log.error(e.getMessage());
        log.error(errorInfoToString(e));
        log.error("===============================PARAM-ERROR===============================");
        return Result.fail(e.getResult(),e.getMessage());
    }

    /**
     * 拦截全局数据统一异常
     * @param e
     * @return
     */
    @ExceptionHandler({ServerDataException.class})
    @ResponseBody
    public Result handle(ServerDataException e) {
        log.error("===============================DATA-ERROR===============================");
        log.error(e.getMessage());
        log.error(errorInfoToString(e));
        log.error("===============================DATA-ERROR===============================");
        return Result.fail(e.getResult(),e.getMessage());
    }
    /**
     * 拦截其他服务500异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result exceptionHandler(Exception e) {
        log.error("===============================ERROR===============================");
        log.error(e.getMessage());
        log.error(errorInfoToString(e));
        log.error("===============================ERROR===============================");
        return Result.fail(ResultEnum.INTERNAL_SERVER_ERROR);
    }

    /**
     * 拦截参数校验框架异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public Result exceptionHandler(MethodArgumentNotValidException e) {
        String msg = "";
        if (e.getBindingResult() != null
                && e.getBindingResult().getAllErrors() != null
                && e.getBindingResult().getAllErrors().size() > 0) {
            msg = e.getBindingResult().getAllErrors().get(0).getDefaultMessage();
        }

        log.error("===============================ERROR===============================");
        log.error(msg);
        log.error(errorInfoToString(e));
        log.error("===============================ERROR===============================");
        return Result.fail(ResultEnum.INTERNAL_SERVER_ERROR,e.getMessage());

    }
    /**
     * Exception出错的栈信息转成字符串
     * 用于打印到日志中
     */
    private String errorInfoToString(Throwable e) {
        //try-with-resource语法糖 处理机制
        try(StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw)){
            e.printStackTrace(pw);
            pw.flush();
            sw.flush();
            return sw.toString();
        }catch (Exception ignored){
            throw new RuntimeException(ignored.getMessage(),ignored);
        }
    }
}
