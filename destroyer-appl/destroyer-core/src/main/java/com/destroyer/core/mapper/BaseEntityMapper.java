package com.destroyer.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.destroyer.core.entity.base.BaseEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 标题：BaseEntityMapper
 * 说明：解决MP报：can not find lambda cache for this entity [com.destroyer.core.entity.base.BaseEntity]]
 * 时间：2023/11/10
 * 作者：admin
 */
@Mapper
public interface BaseEntityMapper extends BaseMapper<BaseEntity> {
}
