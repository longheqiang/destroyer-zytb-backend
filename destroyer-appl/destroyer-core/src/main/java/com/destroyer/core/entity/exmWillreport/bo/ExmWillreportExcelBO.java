package com.destroyer.core.entity.exmWillreport.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：分析报告(Excel业务对象)
 * 说明：分析报告(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-7
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("分析报告(Excel业务对象)")
public class ExmWillreportExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;



    /** 省份代码 */
    @ApiModelProperty(value = "省份代码")
    @ExcelProperty("省份代码")
    private String prono;

    /** 学生id */
    @ApiModelProperty(value = "学生id")
    @ExcelProperty("学生id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuid;

    /** 高考年份 */
    @ApiModelProperty(value = "高考年份")
    @ExcelProperty("高考年份")
    private Integer gkyear;

    /** 包括内容JSON格式 */
    @ApiModelProperty(value = "包括内容JSON格式")
    @ExcelProperty("包括内容JSON格式")
    private String report;









    /**
     * 改造Excel模版
     * @return
     */
    public ExmWillreportExcelBO buildExportModule(){
        this.prono = "省份代码";
        this.stuid = 0L;
        this.gkyear = 0;
        this.report = "包括内容JSON格式";
        return this;
    }
}