package com.destroyer.core.entity.bscCareer.bo;

import com.destroyer.core.entity.bscCareer.BscCareerPO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

 /**
 * 标题：职业库(通用业务对象)
 * 说明：职业库(通用业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("职业库(通用业务对象)")
public class BscCareerBO extends BscCareerPO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}