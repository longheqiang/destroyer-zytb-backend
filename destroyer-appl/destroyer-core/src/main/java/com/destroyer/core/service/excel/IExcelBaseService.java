package com.destroyer.core.service.excel;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 标题：IExcelBaseService
 * 说明：
 * 时间：2024/3/7
 * 作者：admin
 */
public interface IExcelBaseService {
    /**
     * 导出模板
     *
     * @param response
     * @return
     */
    boolean exportExcelModule(HttpServletResponse response) throws IOException;

    /**
     * 导入数据
     * @param req
     * @return
     */
    boolean importExcelData(MultipartFile req) throws IOException;
}
