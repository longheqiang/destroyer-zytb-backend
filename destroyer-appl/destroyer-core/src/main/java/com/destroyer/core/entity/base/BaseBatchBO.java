package com.destroyer.core.entity.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 标题：BaseCreateBatchBO
 * 说明：基础批量新增业务对象
 * 时间：2023/9/7
 * 作者：admin
 */
@Data
@ApiModel("基础批量业务对象")
public class BaseBatchBO<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**主数据集 */
    @ApiModelProperty(value = "主数据集")
    private List<T> list;


    /**
     * 向集合内新增元素
     * @param item
     */
    public void add(T item){
        if(null == this.list){
            this.list = new ArrayList<>();
        }
        this.list.add(item);
    }

    /**
     * 向集合内新增元素
     * @param list
     */
    public void addAll(List<T> list){
        if(null == this.list){
            this.list = new ArrayList<>();
        }
        this.list.addAll(list);
    }


    /**
     * 清空元素
=     */
    public void clear(){
        this.list = new ArrayList<>();
    }
}
