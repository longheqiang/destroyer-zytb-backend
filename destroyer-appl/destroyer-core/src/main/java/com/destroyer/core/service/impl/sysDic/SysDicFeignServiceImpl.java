package com.destroyer.core.service.impl.sysDic;

import com.destroyer.core.entity.sysDic.bo.SysDicQueryBO;
import com.destroyer.core.feign.api.ISysDicFeignClient;
import com.destroyer.common.entity.system.Result;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.service.sysDic.ISysDicFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 标题：系统字典服务
 * 说明：系统字典服务
 * 时间：2023/9/8
 * 作者：admin
 */
@Service
public class SysDicFeignServiceImpl implements ISysDicFeignService {



    /**
     * 字典服务
     */
    @Autowired
    private ISysDicFeignClient iSysDicFeignClient;




    /**
     * 根据类型集合查询字典map
     * @param req
     * @return
     */
    @Override
    public Map<String, Map<String, String>> getMapByTypeList(String[] req) {
        Map<String, Map<String, String>> rsp = new HashMap<>();
        SysDicQueryBO sysDicQueryBO = new SysDicQueryBO();
        List<String> typeList = Arrays.asList(req);
        sysDicQueryBO.setTypeList(typeList);
        Result<Map<String, Map<String, String>>> result = iSysDicFeignClient.queryMapGroupByType(sysDicQueryBO);
        if(result.isSuccess()){
            rsp = result.getData();
        }else {
            throw new ServiceException(ResultEnum.FAILURE, result.getMsg());
        }
        return rsp;
    }



}
