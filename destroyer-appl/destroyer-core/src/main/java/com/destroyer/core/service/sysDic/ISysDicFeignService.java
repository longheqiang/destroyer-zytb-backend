package com.destroyer.core.service.sysDic;

import java.util.Map;

/**
 * 标题：系统字典服务
 * 说明：系统字典服务
 * 时间：2023/9/4
 * 作者：admin
 */
public interface ISysDicFeignService {

    /**
     * 根据类型集合查询字典map
     * @param req
     * @return
     */
    Map<String, Map<String, String>> getMapByTypeList(String[] req);
}
