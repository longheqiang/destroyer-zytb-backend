package com.destroyer.core.entity.base;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ServiceInfoVO {
    private String name;
    private String version;
    private String description;
    private String buildTime;
    private String profile;


}
