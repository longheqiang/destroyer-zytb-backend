package com.destroyer.core.entity.bscMajors;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.bscMajors.bo.BscMajorsBO;
import com.destroyer.core.entity.bscMajors.vo.BscMajorsVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：专业库(简单对象)
 * 说明：专业库(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "bsc_majors", autoResultMap = true)
@ApiModel("专业库(简单对象)")
public class BscMajorsPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public BscMajorsPO(BscMajorsBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 父标识符
     */
    @ApiModelProperty(value = "父标识符")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long pid;


    /**
     * 专业名
     */
    @ApiModelProperty(value = "专业名")
    private String name;


    /**
     * 专业代码
     */
    @ApiModelProperty(value = "专业代码")
    private String code;


    /**
     * 层次
     */
    @ApiModelProperty(value = "层次")
    private String edulevel;


    /**
     * 子专业数量
     */
    @ApiModelProperty(value = "子专业数量")
    private Integer childnum;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public BscMajorsVO parseVO() {
        BscMajorsVO rsp = new BscMajorsVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}