package com.destroyer.core.entity.bscProvince.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.core.entity.bscProvince.BscProvincePO;

/**
 * 标题：省份表(查询业务对象)
 * 说明：省份表(查询业务对象),常用于查询入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("省份表(查询业务对象)")
public class BscProvinceQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 全称
     */
    @ApiModelProperty(value = "全称")
    private String proname;


    /**
     * 简称
     */
    @ApiModelProperty(value = "简称")
    private String proshortname;


    /**
     * 缩写
     */
    @ApiModelProperty(value = "缩写")
    private String proabbr;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<BscProvincePO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<BscProvincePO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(StringUtils.isNotBlank(this.getProno()), BscProvincePO::getProno, this.getProno());
        rsp.like(StringUtils.isNotBlank(this.getProname()), BscProvincePO::getProname, this.getProname());
        rsp.like(StringUtils.isNotBlank(this.getProshortname()), BscProvincePO::getProshortname, this.getProshortname());
        rsp.like(StringUtils.isNotBlank(this.getProabbr()), BscProvincePO::getProabbr, this.getProabbr());
        return rsp;
    }

}