package com.destroyer.core.entity.exmOrder.vo;

import com.destroyer.core.entity.exmOrder.ExmOrderPO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.List;

 /**
 * 标题：消费订单(基础响应视图对象)
 * 说明：消费订单(基础响应视图对象),作为前端、其他服务调用时的响应出参
 * 时间：2024-3-13
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("消费订单(基础响应视图对象)")
public class ExmOrderVO extends ExmOrderPO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}