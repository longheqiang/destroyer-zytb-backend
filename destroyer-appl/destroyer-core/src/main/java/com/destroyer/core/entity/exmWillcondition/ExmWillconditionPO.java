package com.destroyer.core.entity.exmWillcondition;

import com.alibaba.fastjson.JSON;
import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.exmWillcondition.bo.ExmWillconditionBO;
import com.destroyer.core.entity.exmWillcondition.vo.ExmWillconditionVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：学生志愿条件(简单对象)
 * 说明：学生志愿条件(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "exm_willcondition", autoResultMap = true)
@ApiModel("学生志愿条件(简单对象)")
public class ExmWillconditionPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public ExmWillconditionPO(ExmWillconditionBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 学生id
     */
    @ApiModelProperty(value = "学生id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuid;


    /**
     * 高考年份
     */
    @ApiModelProperty(value = "高考年份")
    private Integer gkyear;


    /**
     * 条件json
     */
    @ApiModelProperty(value = "条件json")
    private String conditions;


    /**
     * 是否可用
     */
    @ApiModelProperty(value = "是否可用")
    private Boolean isenabled;


    /**
     * 顺序
     */
    @ApiModelProperty(value = "顺序")
    private Integer theorder;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public ExmWillconditionVO parseVO() {
        ExmWillconditionVO rsp = new ExmWillconditionVO();
        BeanUtils.copyProperties(this, rsp);
        if(StringUtils.isNotBlank(rsp.getConditions())){
            rsp.setConditionsObj(JSON.parseObject(rsp.getConditions()));
        }
        return rsp;
    }
}