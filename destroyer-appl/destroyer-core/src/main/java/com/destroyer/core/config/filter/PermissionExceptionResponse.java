package com.destroyer.core.config.filter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.destroyer.common.entity.system.Result;
import com.destroyer.common.entity.system.ResultEnum;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 标题：PermissionExceptionResponse
 * 说明：安全认证异常、不通过统一返回
 * 时间：2023/3/20
 * 作者：admin
 */
public class PermissionExceptionResponse {
    public  static void rsp(HttpServletResponse response) throws IOException {
        String error = JSON.toJSONString(Result.fail(ResultEnum.UN_AUTHORIZED), SerializerFeature.WriteNullStringAsEmpty);
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json; charset=utf-8");
        response.getWriter().write(error);
    }
}
