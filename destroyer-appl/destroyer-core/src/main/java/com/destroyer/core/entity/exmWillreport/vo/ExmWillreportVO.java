package com.destroyer.core.entity.exmWillreport.vo;

import com.alibaba.fastjson.JSONObject;
import com.destroyer.core.entity.exmWillreport.ExmWillreportPO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.List;

 /**
 * 标题：分析报告(基础响应视图对象)
 * 说明：分析报告(基础响应视图对象),作为前端、其他服务调用时的响应出参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("分析报告(基础响应视图对象)")
public class ExmWillreportVO extends ExmWillreportPO implements Serializable {
    private static final long serialVersionUID = 1L;

     /**
      * 包括内容JSON对象
      */
     @ApiModelProperty(value = "包括内容JSON对象")
     private JSONObject reportObj;
}