package com.destroyer.core.entity.enlMajordata.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.core.entity.enlMajordata.EnlMajordataPO;

/**
 * 标题：专业录取数据表(查询业务对象)
 * 说明：专业录取数据表(查询业务对象),常用于查询入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("专业录取数据表(查询业务对象)")
public class EnlMajordataQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 年份
     */
    @ApiModelProperty(value = "年份")
    private Integer annum;


    /**
     * 批次代码
     */
    @ApiModelProperty(value = "批次代码")
    private String pcdm;


    /**
     * 批次名称
     */
    @ApiModelProperty(value = "批次名称")
    private String pcmc;


    /**
     * 科类代码
     */
    @ApiModelProperty(value = "科类代码")
    private String kldm;


    /**
     * 科类名称
     */
    @ApiModelProperty(value = "科类名称")
    private String klmc;


    /**
     * 院校代码
     */
    @ApiModelProperty(value = "院校代码")
    private String collegecode;


    /**
     * 院校名
     */
    @ApiModelProperty(value = "院校名")
    private String collegename;


    /**
     * 专业代码
     */
    @ApiModelProperty(value = "专业代码")
    private String majorcode;


    /**
     * 专业名
     */
    @ApiModelProperty(value = "专业名")
    private String majorname;


    /**
     * 专业备注
     */
    @ApiModelProperty(value = "专业备注")
    private String majorcmt;


    /**
     * 选科需求
     */
    @ApiModelProperty(value = "选科需求")
    private String subjectneeds;


    /**
     * 国标专业代码
     */
    @ApiModelProperty(value = "国标专业代码")
    private String stdmajorno;


    /**
     * 国标专业名
     */
    @ApiModelProperty(value = "国标专业名")
    private String stdmajorname;


    /**
     * 计划录取
     */
    @ApiModelProperty(value = "计划录取")
    private Integer plannum;


    /**
     * 实际录取
     */
    @ApiModelProperty(value = "实际录取")
    private Integer realnum;


    /**
     * 最低分
     */
    @ApiModelProperty(value = "最低分")
    private BigDecimal minscore;


    /**
     * 最低分位次
     */
    @ApiModelProperty(value = "最低分位次")
    private Integer minscoreranking;


    /**
     * 平均分
     */
    @ApiModelProperty(value = "平均分")
    private BigDecimal avgscore;


    /**
     * 平均分位次
     */
    @ApiModelProperty(value = "平均分位次")
    private Integer avgscoreranking;


    /**
     * 最高分
     */
    @ApiModelProperty(value = "最高分")
    private BigDecimal maxscore;


    /**
     * 最高分位次
     */
    @ApiModelProperty(value = "最高分位次")
    private Integer maxscoreranking;


    /**
     * 备用1
     */
    @ApiModelProperty(value = "备用1")
    private String opt1;


    /**
     * 备用2
     */
    @ApiModelProperty(value = "备用2")
    private String opt2;


    /**
     * 备用3
     */
    @ApiModelProperty(value = "备用3")
    private String opt3;


    /**
     * 备用4
     */
    @ApiModelProperty(value = "备用4")
    private String opt4;


    /**
     * 备用5
     */
    @ApiModelProperty(value = "备用5")
    private String opt5;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<EnlMajordataPO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<EnlMajordataPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(StringUtils.isNotBlank(this.getProno()), EnlMajordataPO::getProno, this.getProno());
        rsp.eq(null != this.getAnnum(), EnlMajordataPO::getAnnum, this.getAnnum());
        rsp.like(StringUtils.isNotBlank(this.getPcdm()), EnlMajordataPO::getPcdm, this.getPcdm());
        rsp.like(StringUtils.isNotBlank(this.getPcmc()), EnlMajordataPO::getPcmc, this.getPcmc());
        rsp.like(StringUtils.isNotBlank(this.getKldm()), EnlMajordataPO::getKldm, this.getKldm());
        rsp.like(StringUtils.isNotBlank(this.getKlmc()), EnlMajordataPO::getKlmc, this.getKlmc());
        rsp.like(StringUtils.isNotBlank(this.getCollegecode()), EnlMajordataPO::getCollegecode, this.getCollegecode());
        rsp.like(StringUtils.isNotBlank(this.getCollegename()), EnlMajordataPO::getCollegename, this.getCollegename());
        rsp.like(StringUtils.isNotBlank(this.getMajorcode()), EnlMajordataPO::getMajorcode, this.getMajorcode());
        rsp.like(StringUtils.isNotBlank(this.getMajorname()), EnlMajordataPO::getMajorname, this.getMajorname());
        rsp.like(StringUtils.isNotBlank(this.getMajorcmt()), EnlMajordataPO::getMajorcmt, this.getMajorcmt());
        rsp.like(StringUtils.isNotBlank(this.getSubjectneeds()), EnlMajordataPO::getSubjectneeds, this.getSubjectneeds());
        rsp.like(StringUtils.isNotBlank(this.getStdmajorno()), EnlMajordataPO::getStdmajorno, this.getStdmajorno());
        rsp.like(StringUtils.isNotBlank(this.getStdmajorname()), EnlMajordataPO::getStdmajorname, this.getStdmajorname());
        rsp.eq(null != this.getPlannum(), EnlMajordataPO::getPlannum, this.getPlannum());
        rsp.eq(null != this.getRealnum(), EnlMajordataPO::getRealnum, this.getRealnum());
        rsp.eq(null != this.getMinscore(), EnlMajordataPO::getMinscore, this.getMinscore());
        rsp.eq(null != this.getMinscoreranking(), EnlMajordataPO::getMinscoreranking, this.getMinscoreranking());
        rsp.eq(null != this.getAvgscore(), EnlMajordataPO::getAvgscore, this.getAvgscore());
        rsp.eq(null != this.getAvgscoreranking(), EnlMajordataPO::getAvgscoreranking, this.getAvgscoreranking());
        rsp.eq(null != this.getMaxscore(), EnlMajordataPO::getMaxscore, this.getMaxscore());
        rsp.eq(null != this.getMaxscoreranking(), EnlMajordataPO::getMaxscoreranking, this.getMaxscoreranking());
        rsp.like(StringUtils.isNotBlank(this.getOpt1()), EnlMajordataPO::getOpt1, this.getOpt1());
        rsp.like(StringUtils.isNotBlank(this.getOpt2()), EnlMajordataPO::getOpt2, this.getOpt2());
        rsp.like(StringUtils.isNotBlank(this.getOpt3()), EnlMajordataPO::getOpt3, this.getOpt3());
        rsp.like(StringUtils.isNotBlank(this.getOpt4()), EnlMajordataPO::getOpt4, this.getOpt4());
        rsp.like(StringUtils.isNotBlank(this.getOpt5()), EnlMajordataPO::getOpt5, this.getOpt5());
        return rsp;
    }

}