package com.destroyer.core.config.myBatisPlus;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.destroyer.core.util.UserUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 标题：MyMetaObjectHandler
 * 说明：mp 自定义元数据填充器，已实现通用字段自动填充
 * 时间：2023/8/2
 * 作者：admin
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * 插入元对象字段填充（用于插入时对公共字段的填充）
     *
     * @param metaObject 元对象
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("======新增自动化填充字段======");
        this.setFieldValByName("createTime",new Date(),metaObject);
        this.setFieldValByName("updateTime",new Date(),metaObject);
        Long userId = UserUtils.getUserId();
        if(null == userId){
            userId = 0L;
        }
        this.setFieldValByName("createUser",userId,metaObject);
        this.setFieldValByName("updateUser",userId,metaObject);

    }


    /**
     * 更新元对象字段填充（用于更新时对公共字段的填充）
     *
     * @param metaObject 元对象
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("======更新自动化填充字段======");
        this.setFieldValByName("updateTime",new Date(),metaObject);
        Long userId = UserUtils.getUserId();
        if(null == userId){
            userId = 0L;
        }
        this.setFieldValByName("updateUser",userId,metaObject);


    }
}
