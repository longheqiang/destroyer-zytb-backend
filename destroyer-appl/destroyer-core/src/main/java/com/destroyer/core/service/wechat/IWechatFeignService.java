package com.destroyer.core.service.wechat;

import com.destroyer.core.entity.wechat.bo.WechatPrepayBO;
import com.destroyer.core.entity.wechat.bo.WechatUserCodeBO;
import com.destroyer.core.entity.wechat.vo.WechatPrepayWithRequestPaymentVO;
import com.destroyer.core.entity.wechat.vo.WechatTransactionVO;
import com.destroyer.core.entity.wechat.vo.WechatUserAuthVO;
import com.destroyer.core.entity.wechat.vo.WechatUserPhoneNumberVO;

/**
 * 标题：IWechatFeignService
 * 说明：
 * 时间：2024/3/5
 * 作者：admin
 */
public interface IWechatFeignService {

    /**
     * 获取微信授权token
     * @return
     */
    String getAccessToken();


    /**
     * 通过code获取用户OpenId
     * @param req
     * @return
     */
    WechatUserAuthVO getUserOpenIdByCode(WechatUserCodeBO req);





    /**
     * 通过code获取用户手机号
     * @param req
     * @return
     */
    WechatUserPhoneNumberVO getPhoneNumber(WechatUserCodeBO req);


    /**
     * 微信建立支付预订单
     * @param req
     * @return
     */
    String prepay(WechatPrepayBO req);


    /**
     * 下单并生成调起支付的参数
     * @param req
     * @return
     */
    WechatPrepayWithRequestPaymentVO prepayWithRequestPayment(WechatPrepayBO req);



    /**
     * 通过预支付ID生成调起支付的参数
     * @param req
     * @return
     */
    WechatPrepayWithRequestPaymentVO prepayWithRequestPaymentByPrepayId(String req);

    /**
     * 支付成功回调业务接口
     * @param req 业务订单号
     * @return
     */
    Boolean wechatPaymentSuccessNotify(WechatTransactionVO req);
}
