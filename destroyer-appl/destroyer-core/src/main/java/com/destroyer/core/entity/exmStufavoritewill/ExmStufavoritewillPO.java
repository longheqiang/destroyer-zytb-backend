package com.destroyer.core.entity.exmStufavoritewill;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.exmStufavoritewill.bo.ExmStufavoritewillBO;
import com.destroyer.core.entity.exmStufavoritewill.vo.ExmStufavoritewillVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：学生收藏志愿(简单对象)
 * 说明：学生收藏志愿(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "exm_stufavoritewill", autoResultMap = true)
@ApiModel("学生收藏志愿(简单对象)")
public class ExmStufavoritewillPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public ExmStufavoritewillPO(ExmStufavoritewillBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 学生id
     */
    @ApiModelProperty(value = "学生id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuid;


    /**
     * 高考年份
     */
    @ApiModelProperty(value = "高考年份")
    private Integer gkyear;


    /**
     * 批次代码
     */
    @ApiModelProperty(value = "批次代码")
    private String pcdm;


    /**
     * 批次名称
     */
    @ApiModelProperty(value = "批次名称")
    private String pcmc;


    /**
     * 科类代码
     */
    @ApiModelProperty(value = "科类代码")
    private String kldm;


    /**
     * 科类名称
     */
    @ApiModelProperty(value = "科类名称")
    private String klmc;


    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer theorder;


    /**
     * 院校id
     */
    @ApiModelProperty(value = "院校id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long collegeid;


    /**
     * 院校代码
     */
    @ApiModelProperty(value = "院校代码")
    private String collegeno;


    /**
     * 院校名称
     */
    @ApiModelProperty(value = "院校名称")
    private String collegename;


    /**
     * 专业代码
     */
    @ApiModelProperty(value = "专业代码")
    private String majorno;


    /**
     * 专业名
     */
    @ApiModelProperty(value = "专业名")
    private String majorname;


    /**
     * 前年数据标识符
     */
    @ApiModelProperty(value = "前年数据标识符")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long lastyeardataid;


    /**
     * 星级打分
     */
    @ApiModelProperty(value = "星级打分")
    private Integer rating;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public ExmStufavoritewillVO parseVO() {
        ExmStufavoritewillVO rsp = new ExmStufavoritewillVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}