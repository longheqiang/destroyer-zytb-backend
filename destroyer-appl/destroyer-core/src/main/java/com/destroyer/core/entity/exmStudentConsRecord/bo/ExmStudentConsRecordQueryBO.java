package com.destroyer.core.entity.exmStudentConsRecord.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.core.entity.exmStudentConsRecord.ExmStudentConsRecordPO;

/**
 * 标题：用户消费记录(查询业务对象)
 * 说明：用户消费记录(查询业务对象),常用于查询入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("用户消费记录(查询业务对象)")
public class ExmStudentConsRecordQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 学生id
     */
    @ApiModelProperty(value = "学生id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuid;


    /**
     * 金额
     */
    @ApiModelProperty(value = "金额")
    private BigDecimal amount;


    /**
     * 消费类型1：消费，-1：充值
     */
    @ApiModelProperty(value = "消费类型1：消费，-1：充值")
    private Integer consType;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<ExmStudentConsRecordPO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<ExmStudentConsRecordPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(null != this.getStuid(), ExmStudentConsRecordPO::getStuid, this.getStuid());
        rsp.eq(null != this.getAmount(), ExmStudentConsRecordPO::getAmount, this.getAmount());
        rsp.eq(null != this.getConsType(), ExmStudentConsRecordPO::getConsType, this.getConsType());
        return rsp;
    }

}