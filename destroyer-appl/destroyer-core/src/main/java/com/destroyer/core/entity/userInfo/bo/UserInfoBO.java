package com.destroyer.core.entity.userInfo.bo;

import com.destroyer.core.entity.userInfo.UserInfoPO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

 /**
 * 标题：用户信息(通用业务对象)
 * 说明：用户信息(通用业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-2-26
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("用户信息(通用业务对象)")
public class UserInfoBO extends UserInfoPO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}