package com.destroyer.core.entity.bscCollege.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.core.entity.bscCollege.BscCollegePO;

/**
 * 标题：院校库(查询业务对象)
 * 说明：院校库(查询业务对象),常用于查询入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("院校库(查询业务对象)")
public class BscCollegeQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 省份
     */
    @ApiModelProperty(value = "省份")
    private String proshortname;


    /**
     * 城市
     */
    @ApiModelProperty(value = "城市")
    private String city;


    /**
     * 国标代码
     */
    @ApiModelProperty(value = "国标代码")
    private String collegeuid;


    /**
     * 院校名称
     */
    @ApiModelProperty(value = "院校名称")
    private String collegename;


    /**
     * 隶属于
     */
    @ApiModelProperty(value = "隶属于")
    private String belongto;


    /**
     * 类型
     */
    @ApiModelProperty(value = "类型")
    private String categories;


    /**
     * 办学层次
     */
    @ApiModelProperty(value = "办学层次")
    private String edulevel;


    /**
     * 办学性质
     */
    @ApiModelProperty(value = "办学性质")
    private String naturetype;


    /**
     * 校徽
     */
    @ApiModelProperty(value = "校徽")
    private String logofile;


    /**
     * 特征
     */
    @ApiModelProperty(value = "特征")
    private String features;


    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;


    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    private String address;


    /**
     * 网址
     */
    @ApiModelProperty(value = "网址")
    private String url;


    /**
     * 简介
     */
    @ApiModelProperty(value = "简介")
    private String intro;


    /**
     * 短简介
     */
    @ApiModelProperty(value = "短简介")
    private String sintro;


    /**
     * 排名
     */
    @ApiModelProperty(value = "排名")
    private Integer ranking;


    /**
     * WSL排名
     */
    @ApiModelProperty(value = "WSL排名")
    private Integer rankingofwsl;


    /**
     * 软科排名
     */
    @ApiModelProperty(value = "软科排名")
    private Integer rankingofrk;


    /**
     * 校友会
     */
    @ApiModelProperty(value = "校友会")
    private Integer rankingofxyh;


    /**
     * QS排名
     */
    @ApiModelProperty(value = "QS排名")
    private Integer rankingofqs;


    /**
     * US排名
     */
    @ApiModelProperty(value = "US排名")
    private Integer rankingofusnews;


    /**
     * 易度排名
     */
    @ApiModelProperty(value = "易度排名")
    private Integer rankingofedu;


    /**
     * 推荐指数
     */
    @ApiModelProperty(value = "推荐指数")
    private BigDecimal comscore;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<BscCollegePO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<BscCollegePO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(StringUtils.isNotBlank(this.getProno()), BscCollegePO::getProno, this.getProno());
        rsp.like(StringUtils.isNotBlank(this.getProshortname()), BscCollegePO::getProshortname, this.getProshortname());
        rsp.like(StringUtils.isNotBlank(this.getCity()), BscCollegePO::getCity, this.getCity());
        rsp.like(StringUtils.isNotBlank(this.getCollegeuid()), BscCollegePO::getCollegeuid, this.getCollegeuid());
        rsp.like(StringUtils.isNotBlank(this.getCollegename()), BscCollegePO::getCollegename, this.getCollegename());
        rsp.like(StringUtils.isNotBlank(this.getBelongto()), BscCollegePO::getBelongto, this.getBelongto());
        rsp.like(StringUtils.isNotBlank(this.getCategories()), BscCollegePO::getCategories, this.getCategories());
        rsp.like(StringUtils.isNotBlank(this.getEdulevel()), BscCollegePO::getEdulevel, this.getEdulevel());
        rsp.like(StringUtils.isNotBlank(this.getNaturetype()), BscCollegePO::getNaturetype, this.getNaturetype());
        rsp.like(StringUtils.isNotBlank(this.getLogofile()), BscCollegePO::getLogofile, this.getLogofile());
        rsp.like(StringUtils.isNotBlank(this.getFeatures()), BscCollegePO::getFeatures, this.getFeatures());
        rsp.like(StringUtils.isNotBlank(this.getEmail()), BscCollegePO::getEmail, this.getEmail());
        rsp.like(StringUtils.isNotBlank(this.getAddress()), BscCollegePO::getAddress, this.getAddress());
        rsp.like(StringUtils.isNotBlank(this.getUrl()), BscCollegePO::getUrl, this.getUrl());
        rsp.like(StringUtils.isNotBlank(this.getIntro()), BscCollegePO::getIntro, this.getIntro());
        rsp.like(StringUtils.isNotBlank(this.getSintro()), BscCollegePO::getSintro, this.getSintro());
        rsp.eq(null != this.getRanking(), BscCollegePO::getRanking, this.getRanking());
        rsp.eq(null != this.getRankingofwsl(), BscCollegePO::getRankingofwsl, this.getRankingofwsl());
        rsp.eq(null != this.getRankingofrk(), BscCollegePO::getRankingofrk, this.getRankingofrk());
        rsp.eq(null != this.getRankingofxyh(), BscCollegePO::getRankingofxyh, this.getRankingofxyh());
        rsp.eq(null != this.getRankingofqs(), BscCollegePO::getRankingofqs, this.getRankingofqs());
        rsp.eq(null != this.getRankingofusnews(), BscCollegePO::getRankingofusnews, this.getRankingofusnews());
        rsp.eq(null != this.getRankingofedu(), BscCollegePO::getRankingofedu, this.getRankingofedu());
        rsp.eq(null != this.getComscore(), BscCollegePO::getComscore, this.getComscore());
        return rsp;
    }

}