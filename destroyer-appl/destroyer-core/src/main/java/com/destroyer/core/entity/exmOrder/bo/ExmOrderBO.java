package com.destroyer.core.entity.exmOrder.bo;

import com.destroyer.core.entity.exmOrder.ExmOrderPO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

 /**
 * 标题：消费订单(通用业务对象)
 * 说明：消费订单(通用业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-13
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("消费订单(通用业务对象)")
public class ExmOrderBO extends ExmOrderPO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}