package com.destroyer.core.config.oss;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class AlyOssConfig {


    /**
     * LTAIran6cH2VaP3P
     * hCcjQ4XJVJhCuyCryp6RczivKk91I9
     */

    private String accessKey = "LTAIran6cH2VaP3P";
    private String secretKey = "hCcjQ4XJVJhCuyCryp6RczivKk91I9";
    private String region = "oss-cn-hangzhou";// 常用Region参考： https://help.aliyun.com/document_detail/140601.html
    private String bucket = "destroyer-sign";
    private String endpoint = "https://" + region + ".aliyuncs.com";

    private String imgDomain = "https://img-sign.zxzyn.com/";

    @Bean
    public OSS OssClient() {
        return new OSSClientBuilder().build(endpoint, accessKey, secretKey);
    }
}
