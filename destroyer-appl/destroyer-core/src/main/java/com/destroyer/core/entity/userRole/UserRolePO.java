package com.destroyer.core.entity.userRole;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.userRole.bo.UserRoleBO;
import com.destroyer.core.entity.userRole.vo.UserRoleVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：用户角色(简单对象)
 * 说明：用户角色(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-2-27
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "user_role", autoResultMap = true)
@ApiModel("用户角色(简单对象)")
public class UserRolePO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public UserRolePO(UserRoleBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    private String name;


    /**
     * 角色别名
     */
    @ApiModelProperty(value = "角色别名")
    private String alias;


    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long pid;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public UserRoleVO parseVO() {
        UserRoleVO rsp = new UserRoleVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}