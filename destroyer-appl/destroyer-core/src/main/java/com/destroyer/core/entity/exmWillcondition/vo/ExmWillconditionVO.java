package com.destroyer.core.entity.exmWillcondition.vo;

import com.alibaba.fastjson.JSONObject;
import com.destroyer.core.entity.exmWillcondition.ExmWillconditionPO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.List;

 /**
 * 标题：学生志愿条件(基础响应视图对象)
 * 说明：学生志愿条件(基础响应视图对象),作为前端、其他服务调用时的响应出参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("学生志愿条件(基础响应视图对象)")
public class ExmWillconditionVO extends ExmWillconditionPO implements Serializable {
    private static final long serialVersionUID = 1L;

     /**
      * 条件json对象
      */
     @ApiModelProperty(value = "条件json对象")
     private JSONObject conditionsObj;
}