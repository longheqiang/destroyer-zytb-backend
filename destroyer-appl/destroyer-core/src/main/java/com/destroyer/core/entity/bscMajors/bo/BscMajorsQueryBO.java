package com.destroyer.core.entity.bscMajors.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.core.entity.bscMajors.BscMajorsPO;

/**
 * 标题：专业库(查询业务对象)
 * 说明：专业库(查询业务对象),常用于查询入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("专业库(查询业务对象)")
public class BscMajorsQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 父标识符
     */
    @ApiModelProperty(value = "父标识符")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long pid;


    /**
     * 专业名
     */
    @ApiModelProperty(value = "专业名")
    private String name;


    /**
     * 专业代码
     */
    @ApiModelProperty(value = "专业代码")
    private String code;


    /**
     * 层次
     */
    @ApiModelProperty(value = "层次")
    private String edulevel;


    /**
     * 子专业数量
     */
    @ApiModelProperty(value = "子专业数量")
    private Integer childnum;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<BscMajorsPO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<BscMajorsPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(null != this.getPid(), BscMajorsPO::getPid, this.getPid());
        rsp.like(StringUtils.isNotBlank(this.getName()), BscMajorsPO::getName, this.getName());
        rsp.like(StringUtils.isNotBlank(this.getCode()), BscMajorsPO::getCode, this.getCode());
        rsp.like(StringUtils.isNotBlank(this.getEdulevel()), BscMajorsPO::getEdulevel, this.getEdulevel());
        rsp.eq(null != this.getChildnum(), BscMajorsPO::getChildnum, this.getChildnum());
        return rsp;
    }

}