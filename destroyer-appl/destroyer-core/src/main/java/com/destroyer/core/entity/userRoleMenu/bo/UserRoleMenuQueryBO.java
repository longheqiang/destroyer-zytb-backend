package com.destroyer.core.entity.userRoleMenu.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.destroyer.core.entity.userRoleMenu.UserRoleMenuPO;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;

/**
 * 标题：用户角色菜单权限(查询业务对象)
 * 说明：用户角色菜单权限(查询业务对象),常用于查询入参
 * 时间：2024-2-27
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("用户角色菜单权限(查询业务对象)")
public class UserRoleMenuQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roleId;


    /**
     * 菜单ID
     */
    @ApiModelProperty(value = "菜单ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long menuId;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<UserRoleMenuPO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<UserRoleMenuPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(null != this.getRoleId(), UserRoleMenuPO::getRoleId, this.getRoleId());
        rsp.eq(null != this.getMenuId(), UserRoleMenuPO::getMenuId, this.getMenuId());
        return rsp;
    }

}