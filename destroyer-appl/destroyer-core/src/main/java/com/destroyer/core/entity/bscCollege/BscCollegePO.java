package com.destroyer.core.entity.bscCollege;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.bscCollege.bo.BscCollegeBO;
import com.destroyer.core.entity.bscCollege.vo.BscCollegeVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：院校库(简单对象)
 * 说明：院校库(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "bsc_college", autoResultMap = true)
@ApiModel("院校库(简单对象)")
public class BscCollegePO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public BscCollegePO(BscCollegeBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 省份
     */
    @ApiModelProperty(value = "省份")
    private String proshortname;


    /**
     * 城市
     */
    @ApiModelProperty(value = "城市")
    private String city;


    /**
     * 国标代码
     */
    @ApiModelProperty(value = "国标代码")
    private String collegeuid;


    /**
     * 院校名称
     */
    @ApiModelProperty(value = "院校名称")
    private String collegename;


    /**
     * 隶属于
     */
    @ApiModelProperty(value = "隶属于")
    private String belongto;


    /**
     * 类型
     */
    @ApiModelProperty(value = "类型")
    private String categories;


    /**
     * 办学层次
     */
    @ApiModelProperty(value = "办学层次")
    private String edulevel;


    /**
     * 办学性质
     */
    @ApiModelProperty(value = "办学性质")
    private String naturetype;


    /**
     * 校徽
     */
    @ApiModelProperty(value = "校徽")
    private String logofile;


    /**
     * 特征
     */
    @ApiModelProperty(value = "特征")
    private String features;


    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;


    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    private String address;


    /**
     * 网址
     */
    @ApiModelProperty(value = "网址")
    private String url;


    /**
     * 简介
     */
    @ApiModelProperty(value = "简介")
    private String intro;


    /**
     * 短简介
     */
    @ApiModelProperty(value = "短简介")
    private String sintro;


    /**
     * 排名
     */
    @ApiModelProperty(value = "排名")
    private Integer ranking;


    /**
     * WSL排名
     */
    @ApiModelProperty(value = "WSL排名")
    private Integer rankingofwsl;


    /**
     * 软科排名
     */
    @ApiModelProperty(value = "软科排名")
    private Integer rankingofrk;


    /**
     * 校友会
     */
    @ApiModelProperty(value = "校友会")
    private Integer rankingofxyh;


    /**
     * QS排名
     */
    @ApiModelProperty(value = "QS排名")
    private Integer rankingofqs;


    /**
     * US排名
     */
    @ApiModelProperty(value = "US排名")
    private Integer rankingofusnews;


    /**
     * 易度排名
     */
    @ApiModelProperty(value = "易度排名")
    private Integer rankingofedu;


    /**
     * 推荐指数
     */
    @ApiModelProperty(value = "推荐指数")
    private BigDecimal comscore;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public BscCollegeVO parseVO() {
        BscCollegeVO rsp = new BscCollegeVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}