package com.destroyer.core.entity.exmOrder;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.exmOrder.bo.ExmOrderBO;
import com.destroyer.core.entity.exmOrder.vo.ExmOrderVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：消费订单(简单对象)
 * 说明：消费订单(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-13
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "exm_order", autoResultMap = true)
@ApiModel("消费订单(简单对象)")
public class ExmOrderPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public ExmOrderPO(ExmOrderBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 学生ID
     */
    @ApiModelProperty(value = "学生ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuId;


    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号")
    private String orderNo;


    /**
     * 订单标题
     */
    @ApiModelProperty(value = "订单标题")
    private String title;


    /**
     * 订单描述
     */
    @ApiModelProperty(value = "订单描述")
    private String description;


    /**
     * 金额（单位：分）
     */
    @ApiModelProperty(value = "金额（单位：分）")
    private Integer total;


    /**
     * 微信预支付订单id
     */
    @ApiModelProperty(value = "微信预支付订单id")
    private String prepayId;


    /**
     * 订单状态；0:未支付、1:已支付
     */
    @ApiModelProperty(value = "订单状态；0:未支付、1:已支付")
    private String orderStatus;


    /**
     * 支付时间
     */
    @ApiModelProperty(value = "支付时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;




    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public ExmOrderVO parseVO() {
        ExmOrderVO rsp = new ExmOrderVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}