package com.destroyer.core.entity.exmWillreport;

import com.alibaba.fastjson.JSON;
import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.exmWillreport.bo.ExmWillreportBO;
import com.destroyer.core.entity.exmWillreport.vo.ExmWillreportVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：分析报告(简单对象)
 * 说明：分析报告(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "exm_willreport", autoResultMap = true)
@ApiModel("分析报告(简单对象)")
public class ExmWillreportPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public ExmWillreportPO(ExmWillreportBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 学生id
     */
    @ApiModelProperty(value = "学生id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuid;


    /**
     * 高考年份
     */
    @ApiModelProperty(value = "高考年份")
    private Integer gkyear;


    /**
     * 包括内容JSON格式
     */
    @ApiModelProperty(value = "包括内容JSON格式")
    private String report;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public ExmWillreportVO parseVO() {
        ExmWillreportVO rsp = new ExmWillreportVO();
        BeanUtils.copyProperties(this, rsp);
        if(StringUtils.isNotBlank(rsp.getReport())){
            rsp.setReportObj(JSON.parseObject(rsp.getReport()));
        }
        return rsp;
    }
}