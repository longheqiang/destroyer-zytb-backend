package com.destroyer.core.entity.userMenu.vo;

import com.alibaba.fastjson.JSONObject;
import com.destroyer.core.entity.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.List;

/**
 * 标题：系统菜单(基础响应视图对象)
 * 说明：系统菜单(基础响应视图对象),作为前端、其他服务调用时的响应出参
 * 时间：2024-2-27
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("系统菜单(基础响应视图对象)")
public class UserMenuVO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long pid;


    /**
     * 菜单类型;管理后台:background,客户端:client
     */
    @ApiModelProperty(value = "菜单类型;管理后台:background,客户端:client")
    private String menuType;


    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String name;


    /**
     * 目录地址
     */
    @ApiModelProperty(value = "目录地址")
    private String path;


    /**
     * 部件
     */
    @ApiModelProperty(value = "部件")
    private String component;


    /**
     * 重定向子路由
     */
    @ApiModelProperty(value = "重定向子路由")
    private String redirect;


    /**
     * 扩展元数据
     */
    @ApiModelProperty(value = "扩展元数据")
    private JSONObject meta;


    /**
     * 顺序
     */
    @ApiModelProperty(value = "顺序")
    private Integer ord;
    /**
     * 子集
     */
    @ApiModelProperty(value = "子集")
    private List<UserMenuVO> children;
}