package com.destroyer.core.entity.sysFile.bo;

import com.destroyer.core.entity.base.BaseBO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标题：系统附件信息(查询业务对象)
 * 说明：系统附件信息(查询业务对象),常用于查询入参
 * 时间：2023-10-11
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("系统附件信息(查询业务对象)")
public class SysFileQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 附件名称
     */
    @ApiModelProperty(value = "附件名称")
    private String name;


    /**
     * 附件类型;文档：doc，图片：imag，音频：audio，视频：video，压缩文件：cmp，其他：other
     */
    @ApiModelProperty(value = "附件类型;文档：doc，图片：imag，音频：audio，视频：video，压缩文件：cmp，其他：other")
    private String fileType;


    /**
     * URL
     */
    @ApiModelProperty(value = "URL")
    private String url;


    /** 大小 */
    @ApiModelProperty(value = "大小")
    private String fileSize ;


}