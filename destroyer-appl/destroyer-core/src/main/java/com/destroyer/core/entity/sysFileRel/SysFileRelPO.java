package com.destroyer.core.entity.sysFileRel;

import com.baomidou.mybatisplus.annotation.TableName;
import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.sysFile.vo.SysFileVO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelBO;
import com.destroyer.core.entity.sysFileRel.vo.SysFileRelVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Map;

/**
 * 标题：系统业务附件关联(简单对象)
 * 说明：系统业务附件关联(简单对象),属性与表字段一一对应，包括类型
 * 时间：2023-10-11
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "sys_file_rel", autoResultMap = true)
@ApiModel("系统业务附件关联(简单对象)")
public class SysFileRelPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public SysFileRelPO(SysFileRelBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 业务类型(维护到字典，取业务对应数据表名称+业务名称);沙盘测评-附件图片:sand_eva_head_image，沙盘测评-附件录音：sand_eva_head_audio，沙具信息-图片：sand_ware_image，辅导咨询-图片：cons_advisory_image，辅导咨询-录音：cons_advisory_audio，文章图片：art_info_imag
     */
    @ApiModelProperty(value = "业务类型(维护到字典，取业务对应数据表名称+业务名称);沙盘测评-附件图片:sand_eva_head_image，沙盘测评-附件录音：sand_eva_head_audio，沙具信息-图片：sand_ware_image，辅导咨询-图片：cons_advisory_image，辅导咨询-录音：cons_advisory_audio，文章图片：art_info_imag")
    private String bizType;


    /**
     * 附件ID
     */
    @ApiModelProperty(value = "附件ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long fileId;


    /**
     * 业务主键ID
     */
    @ApiModelProperty(value = "业务主键ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long bizId;






    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public SysFileRelVO parseVO() {
        SysFileRelVO rsp = new SysFileRelVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }


    /**
     * 解析为响应视图对象(自定义)
     *
     * @return VO对象
     */
    public SysFileRelVO parseVO(Map<Long, SysFileVO> sysFileMap) {
        SysFileRelVO rsp = this.parseVO();
        if(null != sysFileMap && sysFileMap.size() >0){
            SysFileVO file = sysFileMap.get(rsp.getFileId());
            rsp.setName(file.getName());
            rsp.setFileType(file.getFileType());
            rsp.setUrl(file.getUrl());
            rsp.setFileSize(file.getFileSize());

        }
        return rsp;
    }
}