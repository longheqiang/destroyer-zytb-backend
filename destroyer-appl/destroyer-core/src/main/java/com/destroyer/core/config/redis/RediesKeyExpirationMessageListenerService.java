package com.destroyer.core.config.redis;

import org.springframework.data.redis.connection.Message;

/**
 * 标题：RediesKeyExpirationMessageListenerService
 * 说明：
 * 时间：2021/12/3
 * 作者：nljlhq
 */
public interface RediesKeyExpirationMessageListenerService {
    /**
     * redis监听器响应方法
     * @param message
     * @return
     */
    void onMessage(Message message);
}
