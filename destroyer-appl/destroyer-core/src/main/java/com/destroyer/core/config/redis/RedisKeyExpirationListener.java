package com.destroyer.core.config.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

/**
 * 标题：RedisKeyExpirationListener
 * 说明：redis监听器
 * 时间：2021/12/3
 * 作者：nljlhq
 */
@Component
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {
    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Autowired
    StringRedisTemplate redisTemplate;
    @Autowired
    RediesKeyExpirationMessageListenerService rediesKeyExpirationMessageListenerService;

    public void onMessage(Message message, byte[] pattern) {
        String expireKey = message.toString();
        rediesKeyExpirationMessageListenerService.onMessage(message);
    }

}
