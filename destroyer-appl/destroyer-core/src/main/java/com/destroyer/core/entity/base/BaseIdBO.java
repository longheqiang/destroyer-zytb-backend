package com.destroyer.core.entity.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 标题：基础通用删除业务对象
 * 说明：基础通用删除业务对象
 * 时间：2023/8/2
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("基础ID实体类")
public class BaseIdBO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**主键ID */
    @ApiModelProperty(value = "主键ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;




}