package com.destroyer.core.entity.exmOrder.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.util.Date;
import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.core.entity.exmOrder.ExmOrderPO;

/**
 * 标题：消费订单(查询业务对象)
 * 说明：消费订单(查询业务对象),常用于查询入参
 * 时间：2024-3-13
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("消费订单(查询业务对象)")
public class ExmOrderQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;





    /** 学生ID */
    @ApiModelProperty(value = "学生ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuId;


    /** 订单编号 */
    @ApiModelProperty(value = "订单编号")
    private String orderNo;


    /** 订单编号 */
    @ApiModelProperty(value = "订单编号")
    private String orderNoLike;


    /** 订单标题 */
    @ApiModelProperty(value = "订单标题")
    private String title;


    /** 订单描述 */
    @ApiModelProperty(value = "订单描述")
    private String description;


    /** 金额（单位：分） */
    @ApiModelProperty(value = "金额（单位：分）")
    private Integer total;


    /** 微信预支付订单id */
    @ApiModelProperty(value = "微信预支付订单id")
    private String prepayId;


    /** 订单状态；0:未支付、1:已支付 */
    @ApiModelProperty(value = "订单状态；0:未支付、1:已支付")
    private String orderStatus;


    /** 支付时间 */
    @ApiModelProperty(value = "支付时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;











    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     * @return
     */
    public LambdaQueryWrapper<ExmOrderPO> getQueryWrapper(){
        if(Func.isEmpty(this)){
            throw new ServiceException(ResultEnum.PARAM_MISS,"构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<ExmOrderPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(null != this.getStuId(), ExmOrderPO::getStuId, this.getStuId());
        rsp.eq(StringUtils.isNotBlank(this.getOrderNo()), ExmOrderPO::getOrderNo, this.getOrderNo());
        rsp.like(StringUtils.isNotBlank(this.getOrderNoLike()), ExmOrderPO::getOrderNo, this.getOrderNoLike());
        rsp.like(StringUtils.isNotBlank(this.getTitle()), ExmOrderPO::getTitle, this.getTitle());
        rsp.like(StringUtils.isNotBlank(this.getDescription()), ExmOrderPO::getDescription, this.getDescription());
        rsp.eq(null != this.getTotal(), ExmOrderPO::getTotal, this.getTotal());
        rsp.eq(StringUtils.isNotBlank(this.getPrepayId()), ExmOrderPO::getPrepayId, this.getPrepayId());
        rsp.eq(StringUtils.isNotBlank(this.getOrderStatus()), ExmOrderPO::getOrderStatus, this.getOrderStatus());
        rsp.eq(null != this.getPayTime(), ExmOrderPO::getPayTime, this.getPayTime());
        return rsp;
    }

}