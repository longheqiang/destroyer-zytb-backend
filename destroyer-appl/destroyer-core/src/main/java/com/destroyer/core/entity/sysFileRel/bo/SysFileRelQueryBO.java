package com.destroyer.core.entity.sysFileRel.bo;

import com.destroyer.core.entity.base.BaseBO;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 标题：系统业务附件关联(查询业务对象)
 * 说明：系统业务附件关联(查询业务对象),常用于查询入参
 * 时间：2023-10-11
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("系统业务附件关联(查询业务对象)")
public class SysFileRelQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 业务类型(维护到字典，取业务对应数据表名称+业务名称);沙盘测评-附件图片:sand_eva_head_image，沙盘测评-附件录音：sand_eva_head_audio，沙具信息-图片：sand_ware_image，辅导咨询-图片：cons_advisory_image，辅导咨询-录音：cons_advisory_audio，文章图片：art_info_imag
     */
    @ApiModelProperty(value = "业务类型(维护到字典，取业务对应数据表名称+业务名称);沙盘测评-附件图片:sand_eva_head_image，沙盘测评-附件录音：sand_eva_head_audio，沙具信息-图片：sand_ware_image，辅导咨询-图片：cons_advisory_image，辅导咨询-录音：cons_advisory_audio，文章图片：art_info_imag")
    private String bizType;



    /**
     * 业务类型集合(维护到字典，取业务对应数据表名称+业务名称);沙盘测评-附件图片:sand_eva_head_imag，沙盘测评-附件录音：sand_eva_head_audio，沙具信息：sand_ware_imag
     */
    @ApiModelProperty(value = "业务类型集合(维护到字典，取业务对应数据表名称+业务名称);沙盘测评-附件图片:sand_eva_head_imag，沙盘测评-附件录音：sand_eva_head_audio，沙具信息：sand_ware_imag")
    private List<String> bizTypeList;

    /**
     * 附件ID
     */
    @ApiModelProperty(value = "附件ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long fileId;


    /**
     * 业务主键ID
     */
    @ApiModelProperty(value = "业务主键ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long bizId;


    /**
     * 业务主键ID集合
     */
    @ApiModelProperty(value = "业务主键ID集合")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private List<Long> bizIdList;


}