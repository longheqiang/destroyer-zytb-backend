package com.destroyer.core.entity.exmStudent;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.exmStudent.bo.ExmStudentBO;
import com.destroyer.core.entity.exmStudent.vo.ExmStudentVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：志愿学生(简单对象)
 * 说明：志愿学生(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "exm_student", autoResultMap = true)
@ApiModel("志愿学生(简单对象)")
public class ExmStudentPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public ExmStudentPO(ExmStudentBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 高考年份
     */
    @ApiModelProperty(value = "高考年份")
    private Integer gkyear;


    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String stuname;


    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private String gender;


    /**
     * 高中
     */
    @ApiModelProperty(value = "高中")
    private String highschool;


    /**
     * 电话
     */
    @ApiModelProperty(value = "电话")
    private String phone;


    /**
     * 微信号
     */
    @ApiModelProperty(value = "微信号")
    private String weixin;


    /**
     * 微信uid
     */
    @ApiModelProperty(value = "微信uid")
    private String wxunid;


    /**
     * 余额
     */
    @ApiModelProperty(value = "余额")
    private BigDecimal balance;


    /**
     * 选科情况
     */
    @ApiModelProperty(value = "选科情况")
    private String subjects;


    /**
     * 职业兴趣
     */
    @ApiModelProperty(value = "职业兴趣")
    private String career;


    /**
     * 兴趣
     */
    @ApiModelProperty(value = "兴趣")
    private String interest;


    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;


    /**
     * 微信openid
     */
    @ApiModelProperty(value = "微信openid")
    private String wxopenid;

    /** 是否vip */
    @ApiModelProperty(value = "是否vip")
    private Boolean isvip ;



    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public ExmStudentVO parseVO() {
        ExmStudentVO rsp = new ExmStudentVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}