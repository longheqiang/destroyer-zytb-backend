package com.destroyer.core.entity.base;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.destroyer.common.util.Func;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 标题：BaseBO
 * 说明：基础通用业务对象
 * 时间：2023/7/28
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("基础通用业务类")
public class BaseBO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 583740234300700073L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID集合")
    @JsonFormat(shape = JsonFormat.Shape.ARRAY)
    private List<Long> idList;
    /**
     * 当前页，默认 1
     */
    @ApiModelProperty(value = "当前页，默认 1")
    private long current = 1;

    /**
     * 每页显示条数，默认 10
     */
    @ApiModelProperty(value = "每页显示条数，默认 10")
    private long size = 10;

    /**
     * 起始创建时间
     */
    @ApiModelProperty(value = "起始创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTimeStart;

    /**
     * 降序字段
     */
    @ApiModelProperty(value = "降序字段")
    private List<String> descs;
    /**
     * 升序字段
     */
    @ApiModelProperty(value = "升序字段")
    private List<String> ascs;

    /**
     * 截止创建时间
     */
    @ApiModelProperty(value = "截止创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTimeEnd;

    /**
     * 起始更新时间
     */
    @ApiModelProperty(value = "起始更新时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTimeStart;

    /**
     * 截止更新时间
     */
    @ApiModelProperty(value = "截止更新时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTimeEnd;


    /**
     * 特殊附加参数
     */
    @ApiModelProperty(value = "特殊附加参数")
    private Map<String, Object> params = new HashMap<>();



    /**
     * 构造分页Page入参
     *
     * @param <T> 实体类型
     * @return
     */
    public <T> Page<T> buildPage() {
        Page<T> page = new Page<T>(this.current, this.size);
        return page;
    }

}
