package com.destroyer.core.entity.bscCareer.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.core.entity.bscCareer.BscCareerPO;

/**
 * 标题：职业库(查询业务对象)
 * 说明：职业库(查询业务对象),常用于查询入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("职业库(查询业务对象)")
public class BscCareerQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 父标识符
     */
    @ApiModelProperty(value = "父标识符")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long pid;


    /**
     * 职业代码
     */
    @ApiModelProperty(value = "职业代码")
    private String code;


    /**
     * 父代码
     */
    @ApiModelProperty(value = "父代码")
    private String pcode;


    /**
     * 职业名
     */
    @ApiModelProperty(value = "职业名")
    private String name;


    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String cmt;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<BscCareerPO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<BscCareerPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(null != this.getPid(), BscCareerPO::getPid, this.getPid());
        rsp.like(StringUtils.isNotBlank(this.getCode()), BscCareerPO::getCode, this.getCode());
        rsp.eq(StringUtils.isNotBlank(this.getPcode()), BscCareerPO::getPcode, this.getPcode());
        rsp.like(StringUtils.isNotBlank(this.getName()), BscCareerPO::getName, this.getName());
        rsp.like(StringUtils.isNotBlank(this.getCmt()), BscCareerPO::getCmt, this.getCmt());
        return rsp;
    }

}