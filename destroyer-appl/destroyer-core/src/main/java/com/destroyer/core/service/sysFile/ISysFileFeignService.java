package com.destroyer.core.service.sysFile;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.sysFile.bo.SysFileCreateBO;

import java.util.List;

/**
 * 标题：SysFileRelClientService
 * 说明：
 * 时间：2023/10/16
 * 作者：admin
 */
public interface ISysFileFeignService {

    List<Long> createAndBuildRel(BaseBatchBO<SysFileCreateBO> req);
}
