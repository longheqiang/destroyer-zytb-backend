package com.destroyer.core.entity.exmStudent.vo;

import com.destroyer.core.entity.exmStudent.ExmStudentPO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.List;

 /**
 * 标题：志愿学生(基础响应视图对象)
 * 说明：志愿学生(基础响应视图对象),作为前端、其他服务调用时的响应出参
 * 时间：2024-3-1
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("志愿学生(基础响应视图对象)")
public class ExmStudentVO extends ExmStudentPO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}