package com.destroyer.core.entity.bscYear;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.bscYear.bo.BscYearBO;
import com.destroyer.core.entity.bscYear.vo.BscYearVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：年份(简单对象)
 * 说明：年份(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "bsc_year", autoResultMap = true)
@ApiModel("年份(简单对象)")
public class BscYearPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public BscYearPO(BscYearBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 年份
     */
    @ApiModelProperty(value = "年份")
    private Integer annum;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public BscYearVO parseVO() {
        BscYearVO rsp = new BscYearVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}