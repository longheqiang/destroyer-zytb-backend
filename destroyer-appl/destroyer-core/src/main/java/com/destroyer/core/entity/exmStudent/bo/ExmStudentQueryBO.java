package com.destroyer.core.entity.exmStudent.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.core.entity.exmStudent.ExmStudentPO;

/**
 * 标题：志愿学生(查询业务对象)
 * 说明：志愿学生(查询业务对象),常用于查询入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("志愿学生(查询业务对象)")
public class ExmStudentQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 高考年份
     */
    @ApiModelProperty(value = "高考年份")
    private Integer gkyear;


    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String stuname;


    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private String gender;


    /**
     * 高中
     */
    @ApiModelProperty(value = "高中")
    private String highschool;


    /**
     * 电话
     */
    @ApiModelProperty(value = "电话")
    private String phone;


    /**
     * 微信号
     */
    @ApiModelProperty(value = "微信号")
    private String weixin;


    /**
     * 微信uid
     */
    @ApiModelProperty(value = "微信uid")
    private String wxunid;


    /**
     * 余额
     */
    @ApiModelProperty(value = "余额")
    private BigDecimal balance;


    /**
     * 选科情况
     */
    @ApiModelProperty(value = "选科情况")
    private String subjects;


    /**
     * 职业兴趣
     */
    @ApiModelProperty(value = "职业兴趣")
    private String career;


    /**
     * 兴趣
     */
    @ApiModelProperty(value = "兴趣")
    private String interest;


    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;


    /**
     * 微信openid
     */
    @ApiModelProperty(value = "微信openid")
    private String wxopenid;

    /** 是否vip */
    @ApiModelProperty(value = "是否vip")
    private Boolean isvip ;

    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<ExmStudentPO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<ExmStudentPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.like(StringUtils.isNotBlank(this.getProno()), ExmStudentPO::getProno, this.getProno());
        rsp.eq(null != this.getGkyear(), ExmStudentPO::getGkyear, this.getGkyear());
        rsp.like(StringUtils.isNotBlank(this.getStuname()), ExmStudentPO::getStuname, this.getStuname());
        rsp.like(StringUtils.isNotBlank(this.getGender()), ExmStudentPO::getGender, this.getGender());
        rsp.like(StringUtils.isNotBlank(this.getHighschool()), ExmStudentPO::getHighschool, this.getHighschool());
        rsp.like(StringUtils.isNotBlank(this.getPhone()), ExmStudentPO::getPhone, this.getPhone());
        rsp.like(StringUtils.isNotBlank(this.getWeixin()), ExmStudentPO::getWeixin, this.getWeixin());
        rsp.eq(StringUtils.isNotBlank(this.getWxunid()), ExmStudentPO::getWxunid, this.getWxunid());
        rsp.eq(null != this.getBalance(), ExmStudentPO::getBalance, this.getBalance());
        rsp.like(StringUtils.isNotBlank(this.getSubjects()), ExmStudentPO::getSubjects, this.getSubjects());
        rsp.like(StringUtils.isNotBlank(this.getCareer()), ExmStudentPO::getCareer, this.getCareer());
        rsp.like(StringUtils.isNotBlank(this.getInterest()), ExmStudentPO::getInterest, this.getInterest());
        rsp.eq(StringUtils.isNotBlank(this.getPassword()), ExmStudentPO::getPassword, this.getPassword());
        rsp.eq(StringUtils.isNotBlank(this.getWxopenid()), ExmStudentPO::getWxopenid, this.getWxopenid());
        rsp.eq(null != this.getIsvip(), ExmStudentPO::getIsvip, this.getIsvip());

        return rsp;
    }

}