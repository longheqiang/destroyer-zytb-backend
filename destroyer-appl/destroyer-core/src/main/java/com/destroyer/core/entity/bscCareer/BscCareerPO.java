package com.destroyer.core.entity.bscCareer;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.bscCareer.bo.BscCareerBO;
import com.destroyer.core.entity.bscCareer.vo.BscCareerVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：职业库(简单对象)
 * 说明：职业库(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "bsc_career", autoResultMap = true)
@ApiModel("职业库(简单对象)")
public class BscCareerPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public BscCareerPO(BscCareerBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 父标识符
     */
    @ApiModelProperty(value = "父标识符")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long pid;


    /**
     * 职业代码
     */
    @ApiModelProperty(value = "职业代码")
    private String code;


    /**
     * 父代码
     */
    @ApiModelProperty(value = "父代码")
    private String pcode;


    /**
     * 职业名
     */
    @ApiModelProperty(value = "职业名")
    private String name;


    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String cmt;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public BscCareerVO parseVO() {
        BscCareerVO rsp = new BscCareerVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}