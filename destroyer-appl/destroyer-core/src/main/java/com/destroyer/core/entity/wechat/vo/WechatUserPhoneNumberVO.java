package com.destroyer.core.entity.wechat.vo;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标题：WechatUserPhoneNumberVO
 * 说明：
 * 时间：2024/3/4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("微信用户电话号码视图对象")
public class WechatUserPhoneNumberVO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 用户绑定的手机号（国外手机号会有区号）
     */
    @ApiModelProperty(value = "用户绑定的手机号（国外手机号会有区号）")
    private String phoneNumber;

    /**
     * 没有区号的手机号
     */
    @ApiModelProperty(value = "没有区号的手机号")
    private String purePhoneNumber;


    /**
     * 区号
     */
    @ApiModelProperty(value = "区号")
    private String countryCode;


    /**
     * 数据水印
     */
    @ApiModelProperty(value = "数据水印")
    private JSONObject watermark;

}
