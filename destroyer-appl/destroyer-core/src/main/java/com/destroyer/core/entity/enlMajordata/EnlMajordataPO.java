package com.destroyer.core.entity.enlMajordata;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataBO;
import com.destroyer.core.entity.enlMajordata.vo.EnlMajordataVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：专业录取数据表(简单对象)
 * 说明：专业录取数据表(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "enl_majordata", autoResultMap = true)
@ApiModel("专业录取数据表(简单对象)")
public class EnlMajordataPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public EnlMajordataPO(EnlMajordataBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 年份
     */
    @ApiModelProperty(value = "年份")
    private Integer annum;


    /**
     * 批次代码
     */
    @ApiModelProperty(value = "批次代码")
    private String pcdm;


    /**
     * 批次名称
     */
    @ApiModelProperty(value = "批次名称")
    private String pcmc;


    /**
     * 科类代码
     */
    @ApiModelProperty(value = "科类代码")
    private String kldm;


    /**
     * 科类名称
     */
    @ApiModelProperty(value = "科类名称")
    private String klmc;


    /**
     * 院校代码
     */
    @ApiModelProperty(value = "院校代码")
    private String collegecode;


    /**
     * 院校名
     */
    @ApiModelProperty(value = "院校名")
    private String collegename;


    /**
     * 专业代码
     */
    @ApiModelProperty(value = "专业代码")
    private String majorcode;


    /**
     * 专业名
     */
    @ApiModelProperty(value = "专业名")
    private String majorname;


    /**
     * 专业备注
     */
    @ApiModelProperty(value = "专业备注")
    private String majorcmt;


    /**
     * 选科需求
     */
    @ApiModelProperty(value = "选科需求")
    private String subjectneeds;


    /**
     * 国标专业代码
     */
    @ApiModelProperty(value = "国标专业代码")
    private String stdmajorno;


    /**
     * 国标专业名
     */
    @ApiModelProperty(value = "国标专业名")
    private String stdmajorname;


    /**
     * 计划录取
     */
    @ApiModelProperty(value = "计划录取")
    private Integer plannum;


    /**
     * 实际录取
     */
    @ApiModelProperty(value = "实际录取")
    private Integer realnum;


    /**
     * 最低分
     */
    @ApiModelProperty(value = "最低分")
    private BigDecimal minscore;


    /**
     * 最低分位次
     */
    @ApiModelProperty(value = "最低分位次")
    private Integer minscoreranking;


    /**
     * 平均分
     */
    @ApiModelProperty(value = "平均分")
    private BigDecimal avgscore;


    /**
     * 平均分位次
     */
    @ApiModelProperty(value = "平均分位次")
    private Integer avgscoreranking;


    /**
     * 最高分
     */
    @ApiModelProperty(value = "最高分")
    private BigDecimal maxscore;


    /**
     * 最高分位次
     */
    @ApiModelProperty(value = "最高分位次")
    private Integer maxscoreranking;


    /**
     * 备用1
     */
    @ApiModelProperty(value = "备用1")
    private String opt1;


    /**
     * 备用2
     */
    @ApiModelProperty(value = "备用2")
    private String opt2;


    /**
     * 备用3
     */
    @ApiModelProperty(value = "备用3")
    private String opt3;


    /**
     * 备用4
     */
    @ApiModelProperty(value = "备用4")
    private String opt4;


    /**
     * 备用5
     */
    @ApiModelProperty(value = "备用5")
    private String opt5;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public EnlMajordataVO parseVO() {
        EnlMajordataVO rsp = new EnlMajordataVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}