package com.destroyer.core.entity.userRoleMenu.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 标题：系统角色菜单(修改权限业务对象)
 * 说明：系统角色菜单(修改权限业务对象)
 * 时间：2023-9-9
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("用户角色菜单权限(修改权限业务对象)")
public class UserRoleMenuBatchModifyBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roleId;


    /**
     * 菜单ID
     */
    @ApiModelProperty(value = "菜单ID集合")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private List<Long> menuIdList;





}