package com.destroyer.core.entity.sysFileRel.bo;

import com.destroyer.core.entity.sysFileRel.SysFileRelPO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

 /**
 * 标题：系统业务附件关联(通用业务对象)
 * 说明：系统业务附件关联(通用业务对象)，作为基础新增、修改操作的入参
 * 时间：2023-10-11
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("系统业务附件关联(通用业务对象)")
public class SysFileRelBO extends SysFileRelPO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}