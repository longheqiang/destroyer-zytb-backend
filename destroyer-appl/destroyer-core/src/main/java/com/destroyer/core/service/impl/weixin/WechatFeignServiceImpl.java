package com.destroyer.core.service.impl.weixin;

import com.destroyer.common.entity.system.Result;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.wechat.bo.WechatPrepayBO;
import com.destroyer.core.entity.wechat.bo.WechatUserCodeBO;
import com.destroyer.core.entity.wechat.vo.WechatPrepayWithRequestPaymentVO;
import com.destroyer.core.entity.wechat.vo.WechatTransactionVO;
import com.destroyer.core.entity.wechat.vo.WechatUserAuthVO;
import com.destroyer.core.entity.wechat.vo.WechatUserPhoneNumberVO;
import com.destroyer.core.feign.api.IWechatFeignClient;
import com.destroyer.core.service.wechat.IWechatFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 标题：WechatFeignServiceImpl
 * 说明：
 * 时间：2024/3/5
 * 作者：admin
 */
@Service
public class WechatFeignServiceImpl implements IWechatFeignService {
    @Autowired
    private IWechatFeignClient wechatFeignClient;


    /**
     * 获取微信授权token
     * @return
     */
    @Override
    public String getAccessToken() {
        String rsp = "";
        Result<String> result = wechatFeignClient.getAccessToken();
        if(result.isSuccess()){
            rsp = result.getData();
        }else {
            throw new ServiceException(ResultEnum.FAILURE, result.getMsg());
        }
        return rsp;
    }


    /**
     * 通过code获取用户OpenId
     * @param req
     * @return
     */
    @Override
    public WechatUserAuthVO getUserOpenIdByCode(WechatUserCodeBO req) {
        WechatUserAuthVO rsp;
        Result<WechatUserAuthVO> result = wechatFeignClient.getUserOpenIdByCode(req);
        if(result.isSuccess()){
            rsp = result.getData();
        }else {
            throw new ServiceException(ResultEnum.FAILURE, result.getMsg());
        }
        return rsp;
    }


    /**
     * 通过code获取用户手机号
     * @param req
     * @return
     */
    @Override
    public WechatUserPhoneNumberVO getPhoneNumber(WechatUserCodeBO req) {
        WechatUserPhoneNumberVO rsp;
        Result<WechatUserPhoneNumberVO> result = wechatFeignClient.getPhoneNumber(req);
        if(result.isSuccess()){
            rsp = result.getData();
        }else {
            throw new ServiceException(ResultEnum.FAILURE, result.getMsg());
        }
        return rsp;
    }


    /**
     * 微信建立支付预订单
     * @param req
     * @return
     */
    @Override
    public String prepay(WechatPrepayBO req) {
        String rsp;
        Result<String> result = wechatFeignClient.prepay(req);
        if(result.isSuccess()){
            rsp = result.getData();
        }else {
            throw new ServiceException(ResultEnum.FAILURE, result.getMsg());
        }
        return rsp;
    }


    /**
     * 下单并生成调起支付的参数
     * @param req
     * @return
     */
    @Override
    public WechatPrepayWithRequestPaymentVO prepayWithRequestPayment(WechatPrepayBO req) {
        WechatPrepayWithRequestPaymentVO rsp;
        Result<WechatPrepayWithRequestPaymentVO> result = wechatFeignClient.prepayWithRequestPayment(req);
        if(result.isSuccess()){
            rsp = result.getData();
        }else {
            throw new ServiceException(ResultEnum.FAILURE, result.getMsg());
        }
        return rsp;
    }


    /**
     * 通过预支付ID生成调起支付的参数
     * @param req
     * @return
     */
    @Override
    public WechatPrepayWithRequestPaymentVO prepayWithRequestPaymentByPrepayId(String req) {
        WechatPrepayWithRequestPaymentVO rsp;
        Result<WechatPrepayWithRequestPaymentVO> result = wechatFeignClient.prepayWithRequestPaymentByPrepayId(req);
        if(result.isSuccess()){
            rsp = result.getData();
        }else {
            throw new ServiceException(ResultEnum.FAILURE, result.getMsg());
        }
        return rsp;
    }


    /**
     * 支付成功回调业务接口
     * @param req 业务订单号
     * @return
     */
    @Override
    public Boolean wechatPaymentSuccessNotify(WechatTransactionVO req) {
        boolean rsp;
        Result<Boolean> result = wechatFeignClient.wechatPaymentSuccessNotify(req);
        if(result.isSuccess()){
            rsp = result.getData();
        }else {
            throw new ServiceException(ResultEnum.FAILURE, result.getMsg());
        }
        return rsp;
    }
}
