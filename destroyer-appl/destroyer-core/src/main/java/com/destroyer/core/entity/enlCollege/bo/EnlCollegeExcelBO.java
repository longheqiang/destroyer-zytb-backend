package com.destroyer.core.entity.enlCollege.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：招生院校(Excel业务对象)
 * 说明：招生院校(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-7
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("招生院校(Excel业务对象)")
public class EnlCollegeExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;



    /** 省份代码 */
    @ApiModelProperty(value = "省份代码")
    @ExcelProperty("省份代码")
    private String prono;

    /** 院校标识符 */
    @ApiModelProperty(value = "院校标识符")
    @ExcelProperty("院校标识符")
    private String collegeid;

    /** 招生单位代码 */
    @ApiModelProperty(value = "招生单位代码")
    @ExcelProperty("招生单位代码")
    private String collegecode;

    /** 院校名称 */
    @ApiModelProperty(value = "院校名称")
    @ExcelProperty("院校名称")
    private String collegename;









    /**
     * 改造Excel模版
     * @return
     */
    public EnlCollegeExcelBO buildExportModule(){
        this.prono = "省份代码";
        this.collegeid = "院校标识符";
        this.collegecode = "招生单位代码";
        this.collegename = "院校名称";
        return this;
    }
}