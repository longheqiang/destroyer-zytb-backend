package com.destroyer.core.entity.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 标题：BaseEntity
 * 说明：基础通用实体类
 * 时间：2023/8/2
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("基础通用实体类")
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**主键ID */
    @ApiModelProperty(value = "主键ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(type = IdType.ASSIGN_ID,value = "id")
    private Long id;

    /**创建时间*/
    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT,value = "create_time")
    private Date createTime;

    /**更新时间*/
    @ApiModelProperty(value = "更新时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE,value = "update_time")
    private Date updateTime;



    /**创建用户*/
    @ApiModelProperty(value = "创建用户")
    @TableField(fill = FieldFill.INSERT,value = "create_user")
    private Long createUser;



    /**更新用户*/
    @ApiModelProperty(value = "更新用户")
    @TableField(fill = FieldFill.INSERT_UPDATE,value = "update_user")
    private Long updateUser;
    /**是否删除*/
    @ApiModelProperty(value = "是否删除")
    private Boolean isDeleted = false;
}
