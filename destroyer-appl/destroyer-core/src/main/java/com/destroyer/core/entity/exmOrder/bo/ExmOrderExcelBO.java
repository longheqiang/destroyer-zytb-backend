package com.destroyer.core.entity.exmOrder.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：消费订单(Excel业务对象)
 * 说明：消费订单(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-13
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("消费订单(Excel业务对象)")
public class ExmOrderExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 学生ID
     */
    @ApiModelProperty(value = "学生ID")
    @ExcelProperty("学生ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuId;

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号")
    @ExcelProperty("订单编号")
    private String orderNo;

    /**
     * 订单标题
     */
    @ApiModelProperty(value = "订单标题")
    @ExcelProperty("订单标题")
    private String title;

    /**
     * 订单描述
     */
    @ApiModelProperty(value = "订单描述")
    @ExcelProperty("订单描述")
    private String description;

    /**
     * 金额（单位：分）
     */
    @ApiModelProperty(value = "金额（单位：分）")
    @ExcelProperty("金额（单位：分）")
    private Integer total;

    /**
     * 微信预支付订单id
     */
    @ApiModelProperty(value = "微信预支付订单id")
    @ExcelProperty("微信预支付订单id")
    private String prepayId;

    /**
     * 订单状态；0:未支付、1:已支付
     */
    @ApiModelProperty(value = "订单状态；0:未支付、1:已支付")
    @ExcelProperty("订单状态；0:未支付、1:已支付")
    private String orderStatus;

    /**
     * 支付时间
     */
    @ApiModelProperty(value = "支付时间")
    @ExcelProperty("支付时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;


    /**
     * 改造Excel模版
     *
     * @return
     */
    public ExmOrderExcelBO buildExportModule() {
        this.stuId = 0L;
        this.orderNo = "订单编号";
        this.title = "订单标题";
        this.description = "订单描述";
        this.total = 0;
        this.prepayId = "微信预支付订单id";
        this.orderStatus = "订单状态；0:未支付、1:已支付";
        this.payTime = DateUtils.getCurrentDateYYYYMMDDHHMMSS();
        return this;
    }
}