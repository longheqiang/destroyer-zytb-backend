package com.destroyer.core;

import com.destroyer.common.constant.PackageConstant;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@MapperScan({PackageConstant.MAPPER_DESTROYER_CORE})
public class DestroyerCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(DestroyerCoreApplication.class, args);
	}

}
