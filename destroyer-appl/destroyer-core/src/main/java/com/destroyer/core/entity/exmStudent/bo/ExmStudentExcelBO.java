package com.destroyer.core.entity.exmStudent.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 标题：志愿学生(Excel业务对象)
 * 说明：志愿学生(Excel业务对象),用于Excel导入导出
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("志愿学生(Excel业务对象)")
public class ExmStudentExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;





    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    @ExcelProperty("省份代码*")
    private String prono;


    /**
     * 高考年份
     */
    @ApiModelProperty(value = "高考年份")
    @ExcelProperty("高考年份*")
    private Integer gkyear;


    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    @ExcelProperty("姓名*")
    private String stuname;


    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    @ExcelProperty("性别*")
    private String gender;


    /**
     * 高中
     */
    @ApiModelProperty(value = "高中")
    @ExcelProperty("高中")
    private String highschool;


    /**
     * 电话
     */
    @ApiModelProperty(value = "电话")
    @ExcelProperty("电话")
    private String phone;


    /**
     * 微信号
     */
    @ApiModelProperty(value = "微信号")
    @ExcelProperty("微信号")
    private String weixin;


    /**
     * 微信uid
     */
    @ApiModelProperty(value = "微信uid")
    @ExcelProperty("微信uid")
    private String wxunid;


    /**
     * 余额
     */
    @ApiModelProperty(value = "余额")
    @ExcelProperty("余额")
    private BigDecimal balance;


    /**
     * 选科情况
     */
    @ApiModelProperty(value = "选科情况")
    @ExcelProperty("选科情况")
    private String subjects;


    /**
     * 职业兴趣
     */
    @ApiModelProperty(value = "职业兴趣")
    @ExcelProperty("职业兴趣")
    private String career;


    /**
     * 兴趣
     */
    @ApiModelProperty(value = "兴趣")
    @ExcelProperty("兴趣")
    private String interest;


    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    @ExcelProperty("密码")
    private String password;


    /**
     * 微信openid
     */
    @ApiModelProperty(value = "微信openid")
    @ExcelProperty("微信openid")
    private String wxopenid;

    /** 是否vip */
    @ApiModelProperty(value = "是否vip")
    @ExcelProperty("是否vip")
    private Boolean isvip;

    /**
     * 改造Excel模版
     * @return
     */
    public ExmStudentExcelBO buildExportModule(){
        this.prono = "省份代码";
        this.gkyear = 0;
        this.stuname = "姓名";
        this.gender = "性别";
        this.highschool = "高中";
        this.phone = "电话";
        this.weixin = "微信号";
        this.wxunid = "微信uid";
        this.balance = new BigDecimal(0.0);
        this.subjects = "选科情况";
        this.career = "职业兴趣";
        this.interest = "兴趣";
        this.password = "密码";
        this.wxopenid = "微信openid";
        this.isvip = true;
        return this;
    }
}