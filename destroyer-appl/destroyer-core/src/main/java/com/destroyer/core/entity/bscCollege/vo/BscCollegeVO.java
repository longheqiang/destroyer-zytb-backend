package com.destroyer.core.entity.bscCollege.vo;

import com.destroyer.core.entity.bscCollege.BscCollegePO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.List;

 /**
 * 标题：院校库(基础响应视图对象)
 * 说明：院校库(基础响应视图对象),作为前端、其他服务调用时的响应出参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("院校库(基础响应视图对象)")
public class BscCollegeVO extends BscCollegePO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}