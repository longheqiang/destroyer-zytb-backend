package com.destroyer.core.feign.api;

import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.sysFile.bo.SysFileCreateBO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 标题：ISysFileRelClient
 * 说明：
 * 时间：2023/10/16
 * 作者：admin
 */
@FeignClient(value="destroyer-system",contextId = "ISysFileFeignClient")
public interface ISysFileFeignClient {

    /**
     * 系统业务附件关联批量新增API
     * @param req
     * @return
     */
    @PostMapping(value="/api/destroyer/system/sysFile/createAndBuildRel")
    Result<List<Long>> createAndBuildRel(@RequestBody BaseBatchBO<SysFileCreateBO> req);
}
