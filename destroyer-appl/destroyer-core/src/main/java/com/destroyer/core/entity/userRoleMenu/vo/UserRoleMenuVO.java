package com.destroyer.core.entity.userRoleMenu.vo;

import com.destroyer.core.entity.userMenu.vo.UserMenuVO;
import com.destroyer.core.entity.userRoleMenu.UserRoleMenuPO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * 标题：用户角色菜单权限(基础响应视图对象)
 * 说明：用户角色菜单权限(基础响应视图对象),作为前端、其他服务调用时的响应出参
 * 时间：2024-2-27
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("用户角色菜单权限(基础响应视图对象)")
public class UserRoleMenuVO extends UserRoleMenuPO implements Serializable {
    private static final long serialVersionUID = 1L;

     /**
      * 菜单
      */
     @ApiModelProperty(value = "菜单")
     private UserMenuVO menu;

 }