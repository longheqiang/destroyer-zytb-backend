package com.destroyer.core.entity.bscCollege.bo;

import com.destroyer.core.entity.bscCollege.BscCollegePO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

 /**
 * 标题：院校库(通用业务对象)
 * 说明：院校库(通用业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("院校库(通用业务对象)")
public class BscCollegeBO extends BscCollegePO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}