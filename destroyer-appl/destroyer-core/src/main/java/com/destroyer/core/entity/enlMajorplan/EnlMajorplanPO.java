package com.destroyer.core.entity.enlMajorplan;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.enlMajorplan.bo.EnlMajorplanBO;
import com.destroyer.core.entity.enlMajorplan.vo.EnlMajorplanVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：专业录取计划(简单对象)
 * 说明：专业录取计划(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "enl_majorplan", autoResultMap = true)
@ApiModel("专业录取计划(简单对象)")
public class EnlMajorplanPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public EnlMajorplanPO(EnlMajorplanBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 年份
     */
    @ApiModelProperty(value = "年份")
    private Integer annum;


    /**
     * 批次代码
     */
    @ApiModelProperty(value = "批次代码")
    private String pcdm;


    /**
     * 批次名称
     */
    @ApiModelProperty(value = "批次名称")
    private String pcmc;


    /**
     * 科类代码
     */
    @ApiModelProperty(value = "科类代码")
    private String kldm;


    /**
     * 科类名称
     */
    @ApiModelProperty(value = "科类名称")
    private String klmc;


    /**
     * 院校代码
     */
    @ApiModelProperty(value = "院校代码")
    private String collegecode;


    /**
     * 院校名称
     */
    @ApiModelProperty(value = "院校名称")
    private String collegename;


    /**
     * 院校计划人数
     */
    @ApiModelProperty(value = "院校计划人数")
    private Integer collegeplan;


    /**
     * 专业代码
     */
    @ApiModelProperty(value = "专业代码")
    private String majorcode;


    /**
     * 专业名称
     */
    @ApiModelProperty(value = "专业名称")
    private String majorname;


    /**
     * 专业备注
     */
    @ApiModelProperty(value = "专业备注")
    private String majorcmt;


    /**
     * 学制
     */
    @ApiModelProperty(value = "学制")
    private Integer needyears;


    /**
     * 学费
     */
    @ApiModelProperty(value = "学费")
    private Integer fee;


    /**
     * 选课要求
     */
    @ApiModelProperty(value = "选课要求")
    private String subjectneed;


    /**
     * 专业计划人数
     */
    @ApiModelProperty(value = "专业计划人数")
    private Integer majorplan;


    /**
     * 国标专业代码
     */
    @ApiModelProperty(value = "国标专业代码")
    private String stdmajorcode;


    /**
     * 前年匹配标识符
     */
    @ApiModelProperty(value = "前年匹配标识符")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long lastyearmatchid;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public EnlMajorplanVO parseVO() {
        EnlMajorplanVO rsp = new EnlMajorplanVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}