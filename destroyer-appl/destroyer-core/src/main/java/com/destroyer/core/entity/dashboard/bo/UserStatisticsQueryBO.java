package com.destroyer.core.entity.dashboard.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标题：UserStatisticsRspVO
 * 说明：
 * 时间：2023/10/23
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("用户分析查询业务对象")
public class UserStatisticsQueryBO implements Serializable {
    private static final long serialVersionUID = 8817090300209346713L;


    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
