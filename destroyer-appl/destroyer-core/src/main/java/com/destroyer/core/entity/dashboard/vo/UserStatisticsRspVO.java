package com.destroyer.core.entity.dashboard.vo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标题：UserStatisticsRspVO
 * 说明：
 * 时间：2023/10/23
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("用户分析响应视图对象")
public class UserStatisticsRspVO implements Serializable {
    private static final long serialVersionUID = 8817090300209346713L;

    public UserStatisticsRspVO() {
    }

    @ApiModelProperty(value = "班级数量")
    private Long classOrgCount;


    @ApiModelProperty(value = "年级数量")
    private Long gradeOrgCount;

    @ApiModelProperty(value = "教师用户数量")
    private Long teacherUserCount;


    @ApiModelProperty(value = "咨询师用户数量")
    private Long cnUserCount;



    @ApiModelProperty(value = "学生用户数量")
    private Long stuUserCount;

    @ApiModelProperty(value = "学生男生用户数量")
    private Long stuBoyUserCount;

    @ApiModelProperty(value = "学生女生用户数量")
    private Long stuGirlUserCount;



    public UserStatisticsRspVO(JSONArray source, JSONObject other) {
        this.source = source;
        this.other = other;
    }



    /**
     * source
     * source: [
     *       ['name', value],
     *       ['正常', 123],
     *       ['轻度', 50],
     *       ['中度', 30],
     *     ]
     */
    @ApiModelProperty(value = "source")
    private JSONArray source;


    /**
     * other
     * other: {
     *       indicator: [
     *         { name: '正常'},
     *         { name: '轻度'},
     *         { name: '中度'}
     *       ],
     *       name: ['正常', '轻度','中度'],
     *       value: [123, 50, 30]
     *     }
     */
    @ApiModelProperty(value = "other")
    private JSONObject other;

}
