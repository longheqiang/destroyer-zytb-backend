package com.destroyer.core.entity.sysConfig.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.core.entity.sysConfig.SysConfigPO;
import com.destroyer.core.util.MpSqlWrapperUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * 标题：系统配置(查询业务对象)
 * 说明：系统配置(查询业务对象),常用于查询入参
 * 时间：2023-9-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("系统配置(查询业务对象)")
public class SysConfigQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 配置分类：WECHAT_CONFIG
     */
    @ApiModelProperty(value = "配置分类：WECHAT_CONFIG")
    private String type;


    /**
     * 分类文案：微信配置
     */
    @ApiModelProperty(value = "分类文案：微信配置")
    private String typeTxt;


    /**
     * 配置字段名称：WECHAT_APP_ID
     */
    @ApiModelProperty(value = "配置字段名称：WECHAT_APP_ID")
    private String file;


    /**
     * 配置名称：微信APPID
     */
    @ApiModelProperty(value = "配置名称：微信APPID")
    private String title;


    /**
     * 值：test
     */
    @ApiModelProperty(value = "值：test")
    private String val;


    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String des;

    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     * @return
     */
    public LambdaQueryWrapper<SysConfigPO> getQueryWrapper(){
        if(Func.isEmpty(this)){
            throw new ServiceException(ResultEnum.PARAM_MISS,"构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<SysConfigPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(null != this.getType(), SysConfigPO::getType, this.getType());
        rsp.eq(null != this.getTypeTxt(), SysConfigPO::getTypeTxt, this.getTypeTxt());
        rsp.eq(null != this.getFile(), SysConfigPO::getFile, this.getFile());
        rsp.eq(null != this.getTitle(), SysConfigPO::getTitle, this.getTitle());
        rsp.eq(null != this.getVal(), SysConfigPO::getVal, this.getVal());
        rsp.eq(null != this.getDes(), SysConfigPO::getDes, this.getDes());
        return rsp;
    }



}