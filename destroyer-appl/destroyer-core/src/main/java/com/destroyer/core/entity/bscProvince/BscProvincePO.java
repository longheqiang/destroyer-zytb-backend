package com.destroyer.core.entity.bscProvince;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.bscProvince.bo.BscProvinceBO;
import com.destroyer.core.entity.bscProvince.vo.BscProvinceVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：省份表(简单对象)
 * 说明：省份表(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "bsc_province", autoResultMap = true)
@ApiModel("省份表(简单对象)")
public class BscProvincePO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public BscProvincePO(BscProvinceBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 全称
     */
    @ApiModelProperty(value = "全称")
    private String proname;


    /**
     * 简称
     */
    @ApiModelProperty(value = "简称")
    private String proshortname;


    /**
     * 缩写
     */
    @ApiModelProperty(value = "缩写")
    private String proabbr;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public BscProvinceVO parseVO() {
        BscProvinceVO rsp = new BscProvinceVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}