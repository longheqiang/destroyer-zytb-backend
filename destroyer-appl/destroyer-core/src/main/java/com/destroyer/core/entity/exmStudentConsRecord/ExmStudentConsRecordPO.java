package com.destroyer.core.entity.exmStudentConsRecord;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.exmStudentConsRecord.bo.ExmStudentConsRecordBO;
import com.destroyer.core.entity.exmStudentConsRecord.vo.ExmStudentConsRecordVO;
import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：用户消费记录(简单对象)
 * 说明：用户消费记录(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "exm_student_cons_record", autoResultMap = true)
@ApiModel("用户消费记录(简单对象)")
public class ExmStudentConsRecordPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public ExmStudentConsRecordPO(ExmStudentConsRecordBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 学生id
     */
    @ApiModelProperty(value = "学生id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuid;


    /**
     * 金额
     */
    @ApiModelProperty(value = "金额")
    private BigDecimal amount;


    /**
     * 消费类型1：消费，-1：充值
     */
    @ApiModelProperty(value = "消费类型1：消费，-1：充值")
    private Integer consType;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public ExmStudentConsRecordVO parseVO() {
        ExmStudentConsRecordVO rsp = new ExmStudentConsRecordVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }
}