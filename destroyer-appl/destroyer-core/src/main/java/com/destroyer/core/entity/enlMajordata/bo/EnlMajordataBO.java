package com.destroyer.core.entity.enlMajordata.bo;

import com.destroyer.core.entity.enlMajordata.EnlMajordataPO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

 /**
 * 标题：专业录取数据表(通用业务对象)
 * 说明：专业录取数据表(通用业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("专业录取数据表(通用业务对象)")
public class EnlMajordataBO extends EnlMajordataPO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}