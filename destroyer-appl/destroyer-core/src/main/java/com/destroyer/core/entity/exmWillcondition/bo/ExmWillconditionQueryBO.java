package com.destroyer.core.entity.exmWillcondition.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.core.entity.exmWillcondition.ExmWillconditionPO;

/**
 * 标题：学生志愿条件(查询业务对象)
 * 说明：学生志愿条件(查询业务对象),常用于查询入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("学生志愿条件(查询业务对象)")
public class ExmWillconditionQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 学生id
     */
    @ApiModelProperty(value = "学生id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuid;


    /**
     * 高考年份
     */
    @ApiModelProperty(value = "高考年份")
    private Integer gkyear;


    /**
     * 条件json
     */
    @ApiModelProperty(value = "条件json")
    private String conditions;


    /**
     * 是否可用
     */
    @ApiModelProperty(value = "是否可用")
    private Boolean isenabled;


    /**
     * 顺序
     */
    @ApiModelProperty(value = "顺序")
    private Integer theorder;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     *
     * @return
     */
    public LambdaQueryWrapper<ExmWillconditionPO> getQueryWrapper() {
        if (Func.isEmpty(this)) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<ExmWillconditionPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(StringUtils.isNotBlank(this.getProno()), ExmWillconditionPO::getProno, this.getProno());
        rsp.eq(null != this.getStuid(), ExmWillconditionPO::getStuid, this.getStuid());
        rsp.eq(null != this.getGkyear(), ExmWillconditionPO::getGkyear, this.getGkyear());
        rsp.like(StringUtils.isNotBlank(this.getConditions()), ExmWillconditionPO::getConditions, this.getConditions());
        rsp.eq(null != this.getIsenabled(), ExmWillconditionPO::getIsenabled, this.getIsenabled());
        rsp.eq(null != this.getTheorder(), ExmWillconditionPO::getTheorder, this.getTheorder());
        return rsp;
    }

}