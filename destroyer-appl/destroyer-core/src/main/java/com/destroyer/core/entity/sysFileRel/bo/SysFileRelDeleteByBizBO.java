package com.destroyer.core.entity.sysFileRel.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 标题：系统业务附件关联(简单对象)
 * 说明：系统业务附件关联(简单对象),根据业务数据业务对象
 * 时间：2023-10-11
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("系统业务附件关联(根据业务数据业务对象)")
public class SysFileRelDeleteByBizBO implements Serializable {
    private static final long serialVersionUID = 1L;




    /**
     * 业务类型(维护到字典，取业务对应数据表名称+业务名称);沙盘测评-附件图片:sand_eva_head_image，沙盘测评-附件录音：sand_eva_head_audio，沙具信息-图片：sand_ware_image，辅导咨询-图片：cons_advisory_image，辅导咨询-录音：cons_advisory_audio，文章图片：art_info_imag
     */
    @ApiModelProperty(value = "业务类型(维护到字典，取业务对应数据表名称+业务名称);沙盘测评-附件图片:sand_eva_head_image，沙盘测评-附件录音：sand_eva_head_audio，沙具信息-图片：sand_ware_image，辅导咨询-图片：cons_advisory_image，辅导咨询-录音：cons_advisory_audio，文章图片：art_info_imag")
    private String bizType;


    /**
     * 业务类型(维护到字典，取业务对应数据表名称+业务名称);沙盘测评-附件图片:sand_eva_head_image，沙盘测评-附件录音：sand_eva_head_audio，沙具信息-图片：sand_ware_image，辅导咨询-图片：cons_advisory_image，辅导咨询-录音：cons_advisory_audio，文章图片：art_info_imag
     */
    @ApiModelProperty(value = "业务类型(维护到字典，取业务对应数据表名称+业务名称);沙盘测评-附件图片:sand_eva_head_image，沙盘测评-附件录音：sand_eva_head_audio，沙具信息-图片：sand_ware_image，辅导咨询-图片：cons_advisory_image，辅导咨询-录音：cons_advisory_audio，文章图片：art_info_imag")
    private List<String> bizTypeList;


    /**
     * 业务主键ID
     */
    @ApiModelProperty(value = "业务主键ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long bizId;


}