package com.destroyer.core.util;


import cn.hutool.json.JSONObject;
import com.destroyer.common.constant.DicConstant;
import com.destroyer.common.entity.auth.AuthBaseVO;
import com.destroyer.common.entity.auth.AuthUserInfoVO;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.auth.UserContextHolder;
/**
 * 标题：UserUtils
 * 说明：用户工具类
 * 时间：2023/4/4
 * 作者：admin
 */
public class UserUtils {

    /**
     * 获取当前用户信息
     *
     * @return
     */
    public static AuthBaseVO getUser() {
        AuthBaseVO rsp = (AuthBaseVO) UserContextHolder.holder.get();
        return rsp;
    }

    /**
     * 获取当前管理后台用户信息
     *
     * @return
     */
    public static AuthUserInfoVO getSysUser() {
        AuthUserInfoVO rsp = null;
        AuthBaseVO<JSONObject> authBaseVO = getUser();
        //管理后台用户
        if(isSysUser()){
            rsp = authBaseVO.getAuth().toBean(AuthUserInfoVO.class);

        }else {
            throw new ServiceException("获取管理用户信息失败！");

        }
        return rsp;
    }


    /**
     * 是否登录
     * @return
     */
    public static boolean isLogin(){
        boolean rsp = false;
        AuthBaseVO<JSONObject> authBaseVO = getUser();
        if(null != authBaseVO){
            rsp = true;
        }
        return rsp;
    }

    /**
     * 获取当前用户系统角色
     *
     * @return
     */
    public static String getUserSysRole() {
        String sysRole = null;
        if(isLogin()){
            AuthBaseVO<JSONObject> authBaseVO = getUser();
            sysRole = authBaseVO.getSysRole();

        }

        return sysRole;
    }


    /**
     * 获取当前用户ID
     *
     * @return
     */
    public static Long getUserId() {
        Long userId = null;
        if(isLogin()){
            AuthBaseVO<JSONObject> authBaseVO = getUser();
            //管理后台用户
            if(isSysUser()){
                AuthUserInfoVO authUserInfoVO = authBaseVO.getAuth().toBean(AuthUserInfoVO.class);
                userId = authUserInfoVO.getId();
            }
            //学生用户
            if(isStuUser()){

            }

        }


        return userId;
    }

    /**
     * 获取当前用户名称
     *
     * @return
     */
    public static String getUserName() {
        String userName = null;
        if(isLogin()){
            AuthBaseVO<JSONObject> authBaseVO = getUser();
            //管理后台用户
            if(isSysUser()){
                AuthUserInfoVO authUserInfoVO = authBaseVO.getAuth().toBean(AuthUserInfoVO.class);
                userName = authUserInfoVO.getUsername();
            }
            //学生用户
            if(isStuUser()){

            }

        }

        return userName;
    }


    /**
     * 是否超级管理员
     * @return
     */
    public static boolean isSuperAdmin() {
        boolean rsp = false;
        String sysRole = getUserSysRole();
        if(Func.isEmpty(sysRole)){
            throw new ServiceException("判断用户角色时未获取到当前用户角色！");
        }
        if(sysRole.equals(DicConstant.SYS_ROLE.SUPER_ADMIN)){
            rsp = true;
        }
        return rsp;
    }


    /**
     * 是否系统管理后台用户
     * @return
     */
    public static boolean isSysUser() {
        boolean rsp = false;
        String sysRole = getUserSysRole();
        if(Func.isEmpty(sysRole)){
            throw new ServiceException("判断用户角色时未获取到当前用户角色！");
        }
        if(sysRole.equals(DicConstant.SYS_ROLE.SUPER_ADMIN) || sysRole.equals(DicConstant.SYS_ROLE.NORMAL_USER)){
            rsp = true;
        }
        return rsp;
    }


    /**
     * 是否学生用户
     * @return
     */
    public static boolean isStuUser() {
        boolean rsp = false;
        String sysRole = getUserSysRole();
        if(Func.isEmpty(sysRole)){
            throw new ServiceException("判断用户角色时未获取到当前用户角色！");
        }
        if(sysRole.equals(DicConstant.SYS_ROLE.STU_USER)){
            rsp = true;
        }
        return rsp;
    }










}
