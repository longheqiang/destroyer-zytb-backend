package com.destroyer.core.entity.exmStudent.bo;

import com.destroyer.core.entity.exmStudent.ExmStudentPO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 标题：志愿学生(通用业务对象)
 * 说明：志愿学生(通用业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-1
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("志愿学生(通用业务对象)")
public class ExmStudentBO extends ExmStudentPO implements Serializable {
    private static final long serialVersionUID = 1L;



}