package com.destroyer.core.util;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.PageUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.base.BaseBO;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 标题：PageUtils
 * 说明：分页工具类，继承hutool PageUtil工具类
 * 时间：2023/8/1
 * 作者：admin
 */
public class PageUtils extends PageUtil {


    /**
     * po分页对象转vo分页对象，只适用mp框架的分页
     *
     * @param poPage  po分页对象
     * @param voClass vo分页对象泛型实体类
     * @return vo分页对象
     */
    public static <T> Page<T> poPageToVoPage(Page poPage, Class<T> voClass) {
        Page<T> voPage = new Page<T>();
        BeanUtil.copyProperties(poPage, voPage, new String[]{"records"});
        return voPage;
    }

    public static <T> void build(Page<T> page, List<T> rows) {
        if (rows == null) {
            rows = new ArrayList<>();
        }
        page.setTotal(rows.size());
        page.setRecords(rows.stream()
                .skip(page.getSize() * (page.getCurrent() - 1))
                .limit(page.getSize())
                .collect(Collectors.toList()));
    }

    public static <T> Page<T> build(long current, long pageSize, List<T> rows) {
        Page<T> page = new Page<>(current, pageSize);
        build(page, rows);
        return page;
    }

    public static <T> Page<T> build(BaseBO query, List<T> rows) {
        Page<T> page = new Page<>(query.getCurrent(), query.getSize());
        build(page, rows);
        return page;
    }

    public static <T, V> Page<T> build(Page<V> page) {
        Page<T> tPage = new Page<>(page.getCurrent(), page.getSize());
        tPage.setTotal(page.getTotal());
        return tPage;
    }


}
