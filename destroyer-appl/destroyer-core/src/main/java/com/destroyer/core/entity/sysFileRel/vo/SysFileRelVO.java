package com.destroyer.core.entity.sysFileRel.vo;

import com.destroyer.core.entity.sysFileRel.SysFileRelPO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

 /**
 * 标题：系统业务附件关联(基础响应视图对象)
 * 说明：系统业务附件关联(基础响应视图对象),作为前端、其他服务调用时的响应出参
 * 时间：2023-10-11
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("系统业务附件关联(基础响应视图对象)")
public class SysFileRelVO extends SysFileRelPO implements Serializable {
    private static final long serialVersionUID = 1L;

//     /**
//      * 附件
//      */
//     @ApiModelProperty(value = "附件")
//     private SysFileBaseRspVO file;

  /**
   * 附件名称
   */
  @ApiModelProperty(value = "附件名称")
  private String name;


  /**
   * 附件类型;文档：doc，图片：imag，音频：audio，视频：video，压缩文件：cmp，其他：other
   */
  @ApiModelProperty(value = "附件类型;文档：doc，图片：imag，音频：audio，视频：video，压缩文件：cmp，其他：other")
  private String fileType;


  /**
   * URL
   */
  @ApiModelProperty(value = "URL")
  private String url;


  /** 大小 */
  @ApiModelProperty(value = "大小")
  private String fileSize ;

 }