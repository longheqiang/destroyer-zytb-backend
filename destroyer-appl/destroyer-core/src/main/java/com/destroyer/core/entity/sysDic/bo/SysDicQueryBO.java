package com.destroyer.core.entity.sysDic.bo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.entity.base.BaseBO;
import com.destroyer.core.entity.sysDic.SysDicPO;
import com.destroyer.core.util.MpSqlWrapperUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * 标题：系统字典(查询业务对象)
 * 说明：系统字典(查询业务对象),常用于查询入参
 * 时间：2023-9-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("系统字典(查询业务对象)")
public class SysDicQueryBO extends BaseBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 字典分类：ORD_STATUS
     */
    @ApiModelProperty(value = "字典分类：ORD_STATUS")
    private String type;


    @ApiModelProperty(value = "字典分类集合")
    private List<String> typeList;
    /**
     * 字典名称：订单状态
     */
    @ApiModelProperty(value = "字典名称：订单状态")
    private String typeTxt;


    /**
     * 字典标题：待付款
     */
    @ApiModelProperty(value = "字典标题：待付款")
    private String title;


    /**
     * 字典名称：DFK
     */
    @ApiModelProperty(value = "字典名称：DFK")
    private String file;


    /**
     * 字典值：1
     */
    @ApiModelProperty(value = "字典值：1")
    private String val;


    /**
     * 排序：1
     */
    @ApiModelProperty(value = "排序：1")
    private Integer ord;


    /**
     * 字典描述
     */
    @ApiModelProperty(value = "字典描述")
    private String des;


    /**
     * 根据QueryBO获取MP查询Wrapper包装器
     * @return
     */
    public LambdaQueryWrapper<SysDicPO> getQueryWrapper(){
        if(Func.isEmpty(this)){
            throw new ServiceException(ResultEnum.PARAM_MISS,"构造查询Wrapper，入参不能为空！");
        }
        LambdaQueryWrapper<SysDicPO> rsp = MpSqlWrapperUtils.buildQueryWrapper(this);
        rsp.eq(StringUtils.isNotBlank(this.getType()), SysDicPO::getType, this.getType());
        //构建列表查询条件
        MpSqlWrapperUtils.buildListConditionWrapper(rsp,SysDicPO::getType,this.getTypeList());
        rsp.like(StringUtils.isNotBlank(this.getTypeTxt()), SysDicPO::getTypeTxt, this.getTypeTxt());
        rsp.like(StringUtils.isNotBlank(this.getTitle()), SysDicPO::getTitle, this.getTitle());
        rsp.eq(StringUtils.isNotBlank(this.getFile()), SysDicPO::getFile, this.getFile());
        rsp.eq(StringUtils.isNotBlank(this.getVal()), SysDicPO::getVal, this.getVal());
        rsp.eq(null != this.getOrd(), SysDicPO::getOrd, this.getOrd());
        rsp.like(StringUtils.isNotBlank(this.getDes()), SysDicPO::getDes, this.getDes());
        return rsp;
    }


}