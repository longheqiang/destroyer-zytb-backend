package com.destroyer.core.entity.sysConfig;

import com.baomidou.mybatisplus.annotation.TableName;
import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.sysConfig.bo.SysConfigBO;
import com.destroyer.core.entity.sysConfig.vo.SysConfigVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * 标题：系统配置(简单对象)
 * 说明：系统配置(简单对象),属性与表字段一一对应，包括类型
 * 时间：2023-9-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "sys_config", autoResultMap = true)
@ApiModel("系统配置(简单对象)")
public class SysConfigPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public SysConfigPO(SysConfigBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 配置分类：WECHAT_CONFIG
     */
    @ApiModelProperty(value = "配置分类：WECHAT_CONFIG")
    private String type;


    /**
     * 分类文案：微信配置
     */
    @ApiModelProperty(value = "分类文案：微信配置")
    private String typeTxt;


    /**
     * 配置字段名称：WECHAT_APP_ID
     */
    @ApiModelProperty(value = "配置字段名称：WECHAT_APP_ID")
    private String file;


    /**
     * 配置名称：微信APPID
     */
    @ApiModelProperty(value = "配置名称：微信APPID")
    private String title;


    /**
     * 值：test
     */
    @ApiModelProperty(value = "值：test")
    private String val;


    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String des;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public SysConfigVO parseVO() {
        SysConfigVO rsp = new SysConfigVO();
        BeanUtils.copyProperties(this,rsp);
        return rsp;
    }
}