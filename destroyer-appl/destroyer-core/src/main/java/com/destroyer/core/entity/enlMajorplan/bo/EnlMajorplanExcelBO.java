package com.destroyer.core.entity.enlMajorplan.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：专业录取计划(Excel业务对象)
 * 说明：专业录取计划(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-7
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("专业录取计划(Excel业务对象)")
public class EnlMajorplanExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;



    /** 省份代码 */
    @ApiModelProperty(value = "省份代码")
    @ExcelProperty("省份代码")
    private String prono;

    /** 年份 */
    @ApiModelProperty(value = "年份")
    @ExcelProperty("年份")
    private Integer annum;

    /** 批次代码 */
    @ApiModelProperty(value = "批次代码")
    @ExcelProperty("批次代码")
    private String pcdm;

    /** 批次名称 */
    @ApiModelProperty(value = "批次名称")
    @ExcelProperty("批次名称")
    private String pcmc;

    /** 科类代码 */
    @ApiModelProperty(value = "科类代码")
    @ExcelProperty("科类代码")
    private String kldm;

    /** 科类名称 */
    @ApiModelProperty(value = "科类名称")
    @ExcelProperty("科类名称")
    private String klmc;

    /** 院校代码 */
    @ApiModelProperty(value = "院校代码")
    @ExcelProperty("院校代码")
    private String collegecode;

    /** 院校名称 */
    @ApiModelProperty(value = "院校名称")
    @ExcelProperty("院校名称")
    private String collegename;

    /** 院校计划人数 */
    @ApiModelProperty(value = "院校计划人数")
    @ExcelProperty("院校计划人数")
    private Integer collegeplan;

    /** 专业代码 */
    @ApiModelProperty(value = "专业代码")
    @ExcelProperty("专业代码")
    private String majorcode;

    /** 专业名称 */
    @ApiModelProperty(value = "专业名称")
    @ExcelProperty("专业名称")
    private String majorname;

    /** 专业备注 */
    @ApiModelProperty(value = "专业备注")
    @ExcelProperty("专业备注")
    private String majorcmt;

    /** 学制 */
    @ApiModelProperty(value = "学制")
    @ExcelProperty("学制")
    private Integer needyears;

    /** 学费 */
    @ApiModelProperty(value = "学费")
    @ExcelProperty("学费")
    private Integer fee;

    /** 选课要求 */
    @ApiModelProperty(value = "选课要求")
    @ExcelProperty("选课要求")
    private String subjectneed;

    /** 专业计划人数 */
    @ApiModelProperty(value = "专业计划人数")
    @ExcelProperty("专业计划人数")
    private Integer majorplan;

    /** 国标专业代码 */
    @ApiModelProperty(value = "国标专业代码")
    @ExcelProperty("国标专业代码")
    private String stdmajorcode;

    /** 前年匹配标识符 */
    @ApiModelProperty(value = "前年匹配标识符")
    @ExcelProperty("前年匹配标识符")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long lastyearmatchid;









    /**
     * 改造Excel模版
     * @return
     */
    public EnlMajorplanExcelBO buildExportModule(){
        this.prono = "省份代码";
        this.annum = 0;
        this.pcdm = "批次代码";
        this.pcmc = "批次名称";
        this.kldm = "科类代码";
        this.klmc = "科类名称";
        this.collegecode = "院校代码";
        this.collegename = "院校名称";
        this.collegeplan = 0;
        this.majorcode = "专业代码";
        this.majorname = "专业名称";
        this.majorcmt = "专业备注";
        this.needyears = 0;
        this.fee = 0;
        this.subjectneed = "选课要求";
        this.majorplan = 0;
        this.stdmajorcode = "国标专业代码";
        this.lastyearmatchid = 0L;
        return this;
    }
}