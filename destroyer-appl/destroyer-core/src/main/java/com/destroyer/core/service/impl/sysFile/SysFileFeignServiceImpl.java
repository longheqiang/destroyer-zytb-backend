package com.destroyer.core.service.impl.sysFile;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.sysFile.bo.SysFileCreateBO;
import com.destroyer.core.feign.api.ISysFileFeignClient;
import com.destroyer.common.entity.system.Result;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.service.sysFile.ISysFileFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 标题：SysFileRelClientServiceImpl
 * 说明：
 * 时间：2023/10/16
 * 作者：admin
 */
@Service
public class SysFileFeignServiceImpl implements ISysFileFeignService {

    @Autowired
    private ISysFileFeignClient iSysFileFeignClient;


    @Override
    public List<Long> createAndBuildRel(BaseBatchBO<SysFileCreateBO> req) {
        List<Long> rsp = new ArrayList<>();
        Result<List<Long>> result = iSysFileFeignClient.createAndBuildRel(req);
        if(result.isSuccess()){
            rsp = result.getData();
        }else {
            throw new ServiceException(ResultEnum.FAILURE, result.getMsg());
        }
        return rsp;
    }
}
