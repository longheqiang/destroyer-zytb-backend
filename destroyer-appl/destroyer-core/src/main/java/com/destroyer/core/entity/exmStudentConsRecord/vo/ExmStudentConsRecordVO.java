package com.destroyer.core.entity.exmStudentConsRecord.vo;

import com.destroyer.core.entity.exmStudentConsRecord.ExmStudentConsRecordPO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.List;

 /**
 * 标题：用户消费记录(基础响应视图对象)
 * 说明：用户消费记录(基础响应视图对象),作为前端、其他服务调用时的响应出参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("用户消费记录(基础响应视图对象)")
public class ExmStudentConsRecordVO extends ExmStudentConsRecordPO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}