package com.destroyer.core.feign.api;

import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.wechat.bo.WechatPrepayBO;
import com.destroyer.core.entity.wechat.bo.WechatUserCodeBO;
import com.destroyer.core.entity.wechat.vo.WechatPrepayWithRequestPaymentVO;
import com.destroyer.core.entity.wechat.vo.WechatTransactionVO;
import com.destroyer.core.entity.wechat.vo.WechatUserAuthVO;
import com.destroyer.core.entity.wechat.vo.WechatUserPhoneNumberVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 标题：IWechatFeignClient
 * 说明：
 * 时间：2024/3/5
 * 作者：admin
 */
@FeignClient(value="destroyer-system",contextId = "IWechatFeignClient")
public interface IWechatFeignClient {


    /**
     * 获取微信授权tokenAPI
     * @return
     */
    @PostMapping(value = "/api/destroyer/system/wechat/getAccessToken")
    Result<String> getAccessToken();

    /**
     * 通过code获取用户OpenIdAPI
     * @param req
     * @return
     */
    @PostMapping(value = "/api/destroyer/system/wechat/getUserOpenIdByCode")
    Result<WechatUserAuthVO> getUserOpenIdByCode(@RequestBody WechatUserCodeBO req);


    /**
     * 通过code获取用户手机号API
     * @param req
     * @return
     */
    @PostMapping(value = "/api/destroyer/system/wechat/getPhoneNumber")
    Result<WechatUserPhoneNumberVO> getPhoneNumber(@RequestBody WechatUserCodeBO req);


    /**
     * 微信建立支付预订单API
     * @param req
     * @return
     */
    @PostMapping(value = "/api/destroyer/system/wechat/prepay")
    Result<String> prepay(@RequestBody WechatPrepayBO req);

    /**
     * 下单并生成调起支付的参数API
     * @param req
     * @return
     */
    @PostMapping(value = "/api/destroyer/system/wechat/prepayWithRequestPayment")
    Result<WechatPrepayWithRequestPaymentVO> prepayWithRequestPayment(@RequestBody WechatPrepayBO req);


    /**
     * 通过预支付ID生成调起支付的参数API
     * @param req
     * @return
     */
    @PostMapping(value = "/api/destroyer/system/wechat/prepayWithRequestPaymentByPrepayId")
    Result<WechatPrepayWithRequestPaymentVO> prepayWithRequestPaymentByPrepayId(@RequestBody String req);


    /**
     * /api/destroyer/服务中心/业务领域/wechatPaymentSuccessNotify
     * 支付成功回调业务接口
     * @param req 业务订单号
     * @return
     */
    @PostMapping(value = "/api/destroyer/will/exmOrder/wechatPaymentSuccessNotify")
    Result<Boolean> wechatPaymentSuccessNotify(@RequestBody WechatTransactionVO req);
}
