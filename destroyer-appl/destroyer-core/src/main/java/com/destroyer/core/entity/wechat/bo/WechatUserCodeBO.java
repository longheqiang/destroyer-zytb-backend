package com.destroyer.core.entity.wechat.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 标题：WechatUserCodeBO
 * 说明：微信用户code业务对象
 * 时间：2024/3/4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("微信用户code业务对象")
public class WechatUserCodeBO implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 用户code
     */
    @ApiModelProperty(value = "用户code")
    String code;
}
