package com.destroyer.core.entity.sysDic;

import com.baomidou.mybatisplus.annotation.TableName;
import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.sysDic.bo.SysDicBO;
import com.destroyer.core.entity.sysDic.vo.SysDicVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * 标题：系统字典(简单对象)
 * 说明：系统字典(简单对象),属性与表字段一一对应，包括类型
 * 时间：2023-9-4
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "sys_dic", autoResultMap = true)
@ApiModel("系统字典(简单对象)")
public class SysDicPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public SysDicPO(SysDicBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 字典分类：ORD_STATUS
     */
    @ApiModelProperty(value = "字典分类：ORD_STATUS")
    private String type;


    /**
     * 字典名称：订单状态
     */
    @ApiModelProperty(value = "字典名称：订单状态")
    private String typeTxt;


    /**
     * 字典标题：待付款
     */
    @ApiModelProperty(value = "字典标题：待付款")
    private String title;


    /**
     * 字典名称：DFK
     */
    @ApiModelProperty(value = "字典名称：DFK")
    private String file;


    /**
     * 字典值：1
     */
    @ApiModelProperty(value = "字典值：1")
    private String val;


    /**
     * 排序：1
     */
    @ApiModelProperty(value = "排序：1")
    private Integer ord;


    /**
     * 字典描述
     */
    @ApiModelProperty(value = "字典描述")
    private String des;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public SysDicVO parseVO() {
        SysDicVO rsp = new SysDicVO();
        BeanUtils.copyProperties(this,rsp);
        return rsp;
    }
}