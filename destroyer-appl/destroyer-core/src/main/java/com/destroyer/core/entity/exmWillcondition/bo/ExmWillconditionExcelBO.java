package com.destroyer.core.entity.exmWillcondition.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.destroyer.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：学生志愿条件(Excel业务对象)
 * 说明：学生志愿条件(Excel业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-7
 * 作者：admin
 */
@Data
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("学生志愿条件(Excel业务对象)")
public class ExmWillconditionExcelBO implements Serializable {
    private static final long serialVersionUID = 1L;



    /** 省份代码 */
    @ApiModelProperty(value = "省份代码")
    @ExcelProperty("省份代码")
    private String prono;

    /** 学生id */
    @ApiModelProperty(value = "学生id")
    @ExcelProperty("学生id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long stuid;

    /** 高考年份 */
    @ApiModelProperty(value = "高考年份")
    @ExcelProperty("高考年份")
    private Integer gkyear;

    /** 条件json */
    @ApiModelProperty(value = "条件json")
    @ExcelProperty("条件json")
    private String conditions;

    /** 是否可用 */
    @ApiModelProperty(value = "是否可用")
    @ExcelProperty("是否可用")
    private Boolean isenabled;

    /** 顺序 */
    @ApiModelProperty(value = "顺序")
    @ExcelProperty("顺序")
    private Integer theorder;









    /**
     * 改造Excel模版
     * @return
     */
    public ExmWillconditionExcelBO buildExportModule(){
        this.prono = "省份代码";
        this.stuid = 0L;
        this.gkyear = 0;
        this.conditions = "条件json";
        this.isenabled = true;
        this.theorder = 0;
        return this;
    }
}