package com.destroyer.core.feign.api;

import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.sysDic.bo.SysDicQueryBO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * 标题：字典Feign客户端
 * 说明：字典Feign客户端
 * 时间：2023/9/7
 * 作者：admin
 */
@FeignClient(value="destroyer-system",contextId = "SysDicFeignClient")
public interface ISysDicFeignClient {


    /**
     * 根据类型分组查询字典mapAPI
     * @param req
     * @return
     */
    @PostMapping(value="/api/destroyer/system/sysDic/queryMapGroupByType")
    Result<Map<String, Map<String, String>>> queryMapGroupByType(@RequestBody SysDicQueryBO req);
}
