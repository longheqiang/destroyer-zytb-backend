package com.destroyer.core.entity.bscYear.bo;

import com.destroyer.core.entity.bscYear.BscYearPO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;

 /**
 * 标题：年份(通用业务对象)
 * 说明：年份(通用业务对象)，作为基础新增、修改操作的入参
 * 时间：2024-3-4
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("年份(通用业务对象)")
public class BscYearBO extends BscYearPO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
}