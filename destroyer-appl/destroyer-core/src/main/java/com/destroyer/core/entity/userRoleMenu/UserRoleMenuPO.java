package com.destroyer.core.entity.userRoleMenu;

import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.userMenu.vo.UserMenuVO;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuBO;
import com.destroyer.core.entity.userRoleMenu.vo.UserRoleMenuVO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Map;

/**
 * 标题：用户角色菜单权限(简单对象)
 * 说明：用户角色菜单权限(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-2-27
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "user_role_menu", autoResultMap = true)
@ApiModel("用户角色菜单权限(简单对象)")
public class UserRoleMenuPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public UserRoleMenuPO(UserRoleMenuBO req) {
        BeanUtils.copyProperties(req, this);
    }


    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roleId;


    /**
     * 菜单ID
     */
    @ApiModelProperty(value = "菜单ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long menuId;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public UserRoleMenuVO parseVO() {
        UserRoleMenuVO rsp = new UserRoleMenuVO();
        BeanUtils.copyProperties(this, rsp);
        return rsp;
    }

    /**
     * 解析为响应视图对象(自定义)
     *
     * @return VO对象
     */
    public UserRoleMenuVO parseVO(Map<Long, UserMenuVO> userMenuMap) {
        UserRoleMenuVO rsp = this.parseVO();
        if(null != userMenuMap && userMenuMap.size() >0){
            rsp.setMenu(userMenuMap.get(rsp.getMenuId()));
        }
        return rsp;
    }
}