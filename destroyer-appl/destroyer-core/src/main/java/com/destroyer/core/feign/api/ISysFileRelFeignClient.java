package com.destroyer.core.feign.api;

import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelDeleteByBizBO;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelQueryBO;
import com.destroyer.core.entity.sysFileRel.vo.SysFileRelVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * 标题：ISysFileRelFeignClient
 * 说明：
 * 时间：2023/10/19
 * 作者：admin
 */
@FeignClient(value="destroyer-system",contextId = "ISysFileRelFeignClient")
public interface ISysFileRelFeignClient {


    /**
     * 删除数据API(根据业务ID和业务类型)
     * @param req
     * @return
     */
    @PostMapping(value = "/api/destroyer/system/sysFileRel/deleteByBizTypeAndBizId")
    Result<Boolean> deleteByBizTypeAndBizId(@RequestBody SysFileRelDeleteByBizBO req);

    /**
     * 根据业务ID和业务类型分组查询map集合API
     * @param req
     * @return
     */
    @PostMapping(value = "/api/destroyer/system/sysFileRel/mapGroupByBizIdAndBizType")
    Result<Map<Long, Map<String, List<SysFileRelVO>>>> mapGroupByBizIdAndBizType(@RequestBody SysFileRelQueryBO req);


}
