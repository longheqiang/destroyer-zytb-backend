package com.destroyer.core.entity.userMenu;

import com.alibaba.fastjson.JSON;
import com.destroyer.common.util.Func;
import com.destroyer.core.entity.base.BaseEntity;
import com.destroyer.core.entity.userMenu.bo.UserMenuBO;
import com.destroyer.core.entity.userMenu.vo.UserMenuVO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;


/**
 * 标题：系统菜单(简单对象)
 * 说明：系统菜单(简单对象),属性与表字段一一对应，包括类型
 * 时间：2024-2-27
 * 作者：admin
 */
@Data
@NoArgsConstructor
@TableName(value = "user_menu", autoResultMap = true)
@ApiModel("系统菜单(简单对象)")
public class UserMenuPO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 构造PO对象(根据通用业务对象)
     *
     * @param req
     */
    public UserMenuPO(UserMenuBO req) {
        BeanUtils.copyProperties(req, this);
        if(Func.isNotEmpty(req.getMeta())){
            this.meta = req.getMeta().toJSONString();
        }
    }


    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long pid;


    /**
     * 菜单类型;管理后台:background,客户端:client
     */
    @ApiModelProperty(value = "菜单类型;管理后台:background,客户端:client")
    private String menuType;


    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String name;


    /**
     * 目录地址
     */
    @ApiModelProperty(value = "目录地址")
    private String path;


    /**
     * 部件
     */
    @ApiModelProperty(value = "部件")
    private String component;


    /**
     * 重定向子路由
     */
    @ApiModelProperty(value = "重定向子路由")
    private String redirect;


    /**
     * 扩展元数据
     */
    @ApiModelProperty(value = "扩展元数据")
    private String meta;


    /**
     * 顺序
     */
    @ApiModelProperty(value = "顺序")
    private Integer ord;


    /**
     * 解析为响应视图对象(默认)
     *
     * @return VO对象
     */
    public UserMenuVO parseVO() {
        UserMenuVO rsp = new UserMenuVO();
        BeanUtils.copyProperties(this, rsp);
        if(StringUtils.isNotBlank(this.meta)){
            rsp.setMeta(JSON.parseObject(this.meta));
        }
        return rsp;
    }
}