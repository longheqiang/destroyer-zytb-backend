package com.destroyer.user.service.impl.userRole;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.userRole.UserRolePO;
import com.destroyer.user.mapper.UserRoleMapper;
import com.destroyer.user.service.userRole.IUserRoleBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：用户角色(基础通用服务实现)
 * 说明：用户角色(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class  UserRoleBaseServiceImpl extends ServiceImpl<UserRoleMapper, UserRolePO> implements IUserRoleBaseService {
}