package com.destroyer.user.ability.userRoleMenu;

import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuBatchModifyBO;

/**
 * 标题：SchMenuModifyAbilityService
 * 说明：系统角色菜单权限修改
 * 时间：2023/9/17
 * 作者：admin
 */
public interface UserRoleMenuBatchModifyAbilityService {

    /**
     * 修改权限
     * @param req
     * @return
     */
    boolean batchModify(UserRoleMenuBatchModifyBO req);
}
