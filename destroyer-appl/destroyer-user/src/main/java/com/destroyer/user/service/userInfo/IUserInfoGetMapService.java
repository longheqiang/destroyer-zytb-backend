package com.destroyer.user.service.userInfo;

import com.destroyer.core.entity.userInfo.bo.UserInfoQueryBO;
import com.destroyer.core.entity.userInfo.vo.UserInfoVO;
import java.util.Map;

 /**
 * 标题：用户信息(获取Map集合服务接口)
 * 说明：用户信息(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-2-26
 * 作者：admin
 */
public interface IUserInfoGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, UserInfoVO> getMapGroupById(UserInfoQueryBO req);
}