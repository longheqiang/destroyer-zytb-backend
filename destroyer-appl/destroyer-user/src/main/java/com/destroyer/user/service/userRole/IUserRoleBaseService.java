package com.destroyer.user.service.userRole;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.userRole.UserRolePO;

 /**
 * 标题：用户角色(基础通用服务接口)
 * 说明：用户角色(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleBaseService extends IService<UserRolePO> {
}