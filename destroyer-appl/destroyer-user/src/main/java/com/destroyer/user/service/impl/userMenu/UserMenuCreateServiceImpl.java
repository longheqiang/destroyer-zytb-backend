package com.destroyer.user.service.impl.userMenu;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.userMenu.UserMenuPO;
import com.destroyer.core.entity.userMenu.bo.UserMenuBO;
import com.destroyer.user.service.userMenu.IUserMenuBaseService;
import com.destroyer.user.service.userMenu.IUserMenuCreateService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 标题：系统菜单(新增服务实现)
 * 说明：系统菜单(新增服务实现),自定义新增数据实现
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class UserMenuCreateServiceImpl implements IUserMenuCreateService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private IUserMenuBaseService baseService;
    
    
    /**
     * 新增数据
     * @param req
     * @return
     */
    @Override
    public Long create(UserMenuBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造PO对象(根据业务对象)
        UserMenuPO po = new UserMenuPO(req);
        //调用基础保存服务
        boolean states = this.baseService.save(po);
        if (!states){
            throw new ServiceException(ResultEnum.FAILURE, "新增数据失败");
        }
        Long rsp = po.getId();
        return rsp;
    }
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(UserMenuBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
    if(null == req.getPid()){
        throw new ServiceException(ResultEnum.PARAM_MISS,"入参[父ID]不能为空!");
    }
    if(StringUtils.isBlank(req.getName())){
        throw new ServiceException(ResultEnum.PARAM_MISS,"入参[菜单名称]不能为空!");
    }
    if(StringUtils.isBlank(req.getPath())){
        throw new ServiceException(ResultEnum.PARAM_MISS,"入参[目录地址]不能为空!");
    }
        }
    }
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(UserMenuBO req) {
        // TODO 执行前调用逻辑
    }
}