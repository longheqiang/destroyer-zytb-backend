package com.destroyer.user.service.impl.userRoleMenu;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.userRoleMenu.UserRoleMenuPO;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuBO;
import com.destroyer.user.service.userRoleMenu.IUserRoleMenuBaseService;
import com.destroyer.user.service.userRoleMenu.IUserRoleMenuCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 标题：用户角色菜单权限(新增服务实现)
 * 说明：用户角色菜单权限(新增服务实现),自定义新增数据实现
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class UserRoleMenuCreateServiceImpl implements IUserRoleMenuCreateService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private IUserRoleMenuBaseService baseService;
    
    
    /**
     * 新增数据
     * @param req
     * @return
     */
    @Override
    public Long create(UserRoleMenuBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造PO对象(根据业务对象)
        UserRoleMenuPO po = new UserRoleMenuPO(req);
        //调用基础保存服务
        boolean states = this.baseService.save(po);
        if (!states){
            throw new ServiceException(ResultEnum.FAILURE, "新增数据失败");
        }
        Long rsp = po.getId();
        return rsp;
    }
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(UserRoleMenuBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
        }
    }
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(UserRoleMenuBO req) {
        // TODO 执行前调用逻辑
    }
}