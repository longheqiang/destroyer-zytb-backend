package com.destroyer.user.service.impl.userMenu;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.userMenu.UserMenuPO;
import com.destroyer.user.mapper.UserMenuMapper;
import com.destroyer.user.service.userMenu.IUserMenuBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：系统菜单(基础通用服务实现)
 * 说明：系统菜单(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class  UserMenuBaseServiceImpl extends ServiceImpl<UserMenuMapper, UserMenuPO> implements IUserMenuBaseService {
}