package com.destroyer.user.ability.impl.auth;

import cn.hutool.json.JSONObject;
import com.destroyer.common.entity.auth.AuthBaseVO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.userInfo.vo.UserInfoVO;
import com.destroyer.core.util.UserUtils;
import com.destroyer.user.ability.auth.IAuthUserInfoDetailAbilityService;
import com.destroyer.user.service.userInfo.IUserInfoGetDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 标题：AuthUserInfoDetailServiceImpl
 * 说明：
 * 时间：2024/2/28
 * 作者：admin
 */
@Service
public class AuthUserInfoDetailAbilityServiceImpl implements IAuthUserInfoDetailAbilityService {


    @Autowired
    private IUserInfoGetDetailService userInfoGetDetailService;

    /**
     * 身份认证信息
     */
    private AuthBaseVO<JSONObject> authBase;


    /**
     * 获取当前登录的系统用户身份信息
     * @return
     */
    @Override
    public UserInfoVO getAuthUserInfoDetail() {
        //入参效验
        this.validParams();
        //执行核心逻辑前调用
        this.beforeToDo();
        UserInfoVO rsp = userInfoGetDetailService.getDetailById(UserUtils.getUserId());
        return rsp;
    }


    /**
     * 类通用入参校验
     *
     */
    private void validParams( ) {
        this.authBase = UserUtils.getUser();
        if(null == authBase){
            throw new ServiceException(ResultEnum.UN_AUTHORIZED);
        }
    }

    /**
     * 类通用执行前调用逻辑
     *
     */
    private void beforeToDo( ) {
        // TODO 执行前调用逻辑

    }
}
