package com.destroyer.user.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.base.BaseIdListBO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.userRole.bo.UserRoleBO;
import com.destroyer.core.entity.userRole.bo.UserRoleQueryBO;
import com.destroyer.core.entity.userRole.vo.UserRoleVO;
import com.destroyer.user.service.userRole.*;

 /**
 * 标题：用户角色(控制层)
 * 说明：用户角色(控制层),包括所有对外前端API接口
 * 时间：2024-2-27
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/user/userRole")
@Api(value = "UserRoleController", tags = "用户角色API")
public class UserRoleController {
    
    
    /**
     * 基础服务
     */
    @Autowired
    private IUserRoleBaseService baseService;
    
    
    /**
     * 新增数据服务
     */
    @Autowired
    private IUserRoleCreateService createService;
    
    
    
    /**
     * 批量新增服务
     */
    @Autowired
    private IUserRoleBatchCreateService batchCreateService;
    
    
    
    /**
      * 删除数据服务
      */
    @Autowired
    private IUserRoleDeleteService deleteService;
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IUserRoleGetListService getListService;
    
    
     /**
     * map服务
     */
    @Autowired
    private IUserRoleGetMapService getMapService;
    
    
    /**
     * 详情服务
     */
    @Autowired
    private IUserRoleGetDetailService getDetailService;
    
    
    /**
     * 编辑服务
     */
    @Autowired
    private IUserRoleEditService editService;
    
    
    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody UserRoleBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }
    
    
    
    @PostMapping(value = "/batchCreate")
    @ApiOperation("批量新增API")
    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<UserRoleBO> req) {
        List<Long> rsp = this.batchCreateService.batchCreate(req);
        return Result.success(rsp);
    }


     @PostMapping(value = "/deleteById")
     @ApiOperation("删除数据API(根据主键ID)")
     public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
         boolean rsp = this.deleteService.deleteById(req.getId());
         return Result.success(rsp);
     }
    
    
    @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }
    
    
    
    @PostMapping(value = "/deleteBy")
    @ApiOperation("删除数据API(根据查询条件删除)")
    public Result<Boolean> deleteBy(@RequestBody UserRoleQueryBO req) {
        boolean rsp = this.deleteService.batchDeleteBy(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<UserRoleVO>> list(@RequestBody UserRoleQueryBO req) {
        List<UserRoleVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/mapGroupById")
    @ApiOperation("查询map(ID分组)API")
    public Result<Map<Long, UserRoleVO>> mapGroupById(@RequestBody UserRoleQueryBO req) {
        Map<Long, UserRoleVO> rsp = this.getMapService.getMapGroupById(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<UserRoleVO>> page(@RequestBody UserRoleQueryBO req) {
        Page<UserRoleVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<UserRoleVO> detailBy(@RequestBody UserRoleQueryBO req) {
        UserRoleVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


     @PostMapping(value = "/detailById")
     @ApiOperation("根据ID查询详情API")
     public Result<UserRoleVO> detailById(@RequestBody BaseIdBO req) {
         UserRoleVO rsp = this.getDetailService.getDetailById(req.getId());
         return Result.success(rsp);
     }
    
    
    @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody UserRoleBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }
}