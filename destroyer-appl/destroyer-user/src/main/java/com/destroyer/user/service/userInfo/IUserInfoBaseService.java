package com.destroyer.user.service.userInfo;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.userInfo.UserInfoPO;

 /**
 * 标题：用户信息(基础通用服务接口)
 * 说明：用户信息(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-2-26
 * 作者：admin
 */
public interface IUserInfoBaseService extends IService<UserInfoPO> {
}