package com.destroyer.user.service.userMenu;

import com.destroyer.core.entity.userMenu.bo.UserMenuQueryBO;
import com.destroyer.core.entity.userMenu.vo.UserMenuVO;

import java.util.Map;

 /**
 * 标题：系统菜单(获取Map集合服务接口)
 * 说明：系统菜单(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserMenuGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, UserMenuVO> getMapGroupById(UserMenuQueryBO req);
}