package com.destroyer.user.ability.auth;

import com.destroyer.core.entity.userMenu.vo.UserMenuVO;

import java.util.List;

/**
 * 标题：AuthGetUserRoleMenuAbilityService
 * 说明：
 * 时间：2024/2/28
 * 作者：admin
 */
public interface IAutheGetUserRoleMenuAbilityService {

    /**
     * 获取当前用户所属角色菜单
     * @return
     */
    List<UserMenuVO> getUserRoleMenu();
}
