package com.destroyer.user.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.base.BaseIdListBO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.userInfo.bo.UserInfoBO;
import com.destroyer.core.entity.userInfo.bo.UserInfoQueryBO;
import com.destroyer.core.entity.userInfo.vo.UserInfoVO;
import com.destroyer.user.service.userInfo.*;

/**
 * 标题：用户信息(控制层)
 * 说明：用户信息(控制层),包括所有对外前端API接口
 * 时间：2024-2-26
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/user/userInfo")
@Api(value = "UserInfoController", tags = "用户信息API")
public class UserInfoController {


    /**
     * 基础服务
     */
    @Autowired
    private IUserInfoBaseService baseService;


    /**
     * 新增数据服务
     */
    @Autowired
    private IUserInfoCreateService createService;



    /**
     * 批量新增服务
     */
    @Autowired
    private IUserInfoBatchCreateService batchCreateService;



    /**
     * 删除数据服务
     */
    @Autowired
    private IUserInfoDeleteService deleteService;


    /**
     * 列表服务
     */
    @Autowired
    private IUserInfoGetListService getListService;


    /**
     * map服务
     */
    @Autowired
    private IUserInfoGetMapService getMapService;


    /**
     * 详情服务
     */
    @Autowired
    private IUserInfoGetDetailService getDetailService;


    /**
     * 编辑服务
     */
    @Autowired
    private IUserInfoEditService editService;


    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody UserInfoBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }



    @PostMapping(value = "/batchCreate")
    @ApiOperation("批量新增API")
    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<UserInfoBO> req) {
        List<Long> rsp = this.batchCreateService.batchCreate(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/deleteById")
    @ApiOperation("删除数据API(根据主键ID)")
    public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
        boolean rsp = this.deleteService.deleteById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }



    @PostMapping(value = "/deleteBy")
    @ApiOperation("删除数据API(根据查询条件删除)")
    public Result<Boolean> deleteBy(@RequestBody UserInfoQueryBO req) {
        boolean rsp = this.deleteService.batchDeleteBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<UserInfoVO>> list(@RequestBody UserInfoQueryBO req) {
        List<UserInfoVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/mapGroupById")
    @ApiOperation("查询map(ID分组)API")
    public Result<Map<Long, UserInfoVO>> mapGroupById(@RequestBody UserInfoQueryBO req) {
        Map<Long, UserInfoVO> rsp = this.getMapService.getMapGroupById(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<UserInfoVO>> page(@RequestBody UserInfoQueryBO req) {
        Page<UserInfoVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<UserInfoVO> detailBy(@RequestBody UserInfoQueryBO req) {
        UserInfoVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailById")
    @ApiOperation("根据ID查询详情API")
    public Result<UserInfoVO> detailById(@RequestBody BaseIdBO req) {
        UserInfoVO rsp = this.getDetailService.getDetailById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody UserInfoBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }
}