package com.destroyer.user.ability.auth;

import com.destroyer.common.entity.auth.AuthBaseVO;
import com.destroyer.common.entity.auth.AuthLoginBO;
import com.destroyer.common.entity.auth.AuthUserInfoVO;

/**
 * 标题：IAuthLoginAbilityService
 * 说明：
 * 时间：2023/12/21
 * 作者：admin
 */
public interface IAuthLoginAbilityService {

    /**
     * 系统用户登录
     * @param req
     * @return
     */
    AuthBaseVO<AuthUserInfoVO> login(AuthLoginBO req);
}
