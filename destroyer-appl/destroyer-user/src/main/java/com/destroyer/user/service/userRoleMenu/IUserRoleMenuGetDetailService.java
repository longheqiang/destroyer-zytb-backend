package com.destroyer.user.service.userRoleMenu;

import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuQueryBO;
import com.destroyer.core.entity.userRoleMenu.vo.UserRoleMenuVO;

/**
 * 标题：用户角色菜单权限(查询详情服务接口)
 * 说明：用户角色菜单权限(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleMenuGetDetailService {
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    UserRoleMenuVO getDetailBy(UserRoleMenuQueryBO req);


     /**
      * 根据主键ID查询详情
      * @param req
      * @return
      */
     UserRoleMenuVO getDetailById(Long req);
}