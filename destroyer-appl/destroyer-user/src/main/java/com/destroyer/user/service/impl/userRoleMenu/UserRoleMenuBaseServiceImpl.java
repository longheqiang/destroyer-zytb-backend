package com.destroyer.user.service.impl.userRoleMenu;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.userRoleMenu.UserRoleMenuPO;
import com.destroyer.user.mapper.UserRoleMenuMapper;
import com.destroyer.user.service.userRoleMenu.IUserRoleMenuBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：用户角色菜单权限(基础通用服务实现)
 * 说明：用户角色菜单权限(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class  UserRoleMenuBaseServiceImpl extends ServiceImpl<UserRoleMenuMapper, UserRoleMenuPO> implements IUserRoleMenuBaseService {
}