package com.destroyer.user.service.impl.userInfo;

import cn.hutool.crypto.SecureUtil;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.userInfo.UserInfoPO;
import com.destroyer.core.entity.userInfo.bo.UserInfoBO;
import com.destroyer.user.service.userInfo.IUserInfoBaseService;
import com.destroyer.user.service.userInfo.IUserInfoCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 标题：用户信息(新增服务实现)
 * 说明：用户信息(新增服务实现),自定义新增数据实现
 * 时间：2024-2-26
 * 作者：admin
 */
@Service
public class UserInfoCreateServiceImpl implements IUserInfoCreateService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private IUserInfoBaseService baseService;

     /**
      * 密码前后盐
      */
     @Value("${pwd-salt-s}")
     private String pwdSaltS;
     @Value("${pwd-salt-e}")
     private String pwdSaltE;
    
    
    /**
     * 新增数据
     * @param req
     * @return
     */
    @Override
    public Long create(UserInfoBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造PO对象(根据业务对象)
        UserInfoPO po = new UserInfoPO(req);
        //调用基础保存服务
        boolean states = this.baseService.save(po);
        if (!states){
            throw new ServiceException(ResultEnum.FAILURE, "新增数据失败");
        }
        Long rsp = po.getId();
        return rsp;
    }
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(UserInfoBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
        }
    }
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(UserInfoBO req) {
        // TODO 执行前调用逻辑
        String pwd = pwdSaltS+req.getPassword()+pwdSaltE;
        req.setPassword(SecureUtil.md5(pwd));
    }
}