package com.destroyer.user.service.impl.userRoleMenu;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.common.util.Func;
import com.destroyer.core.entity.userMenu.bo.UserMenuQueryBO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.userMenu.vo.UserMenuVO;
import com.destroyer.core.util.PageUtils;
import com.destroyer.core.entity.userRoleMenu.UserRoleMenuPO;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuQueryBO;
import com.destroyer.core.entity.userRoleMenu.vo.UserRoleMenuVO;
import com.destroyer.user.service.userMenu.IUserMenuGetMapService;
import com.destroyer.user.service.userRoleMenu.IUserRoleMenuBaseService;
import com.destroyer.user.service.userRoleMenu.IUserRoleMenuGetListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * 标题：用户角色菜单权限(获取列表服务实现)
 * 说明：用户角色菜单权限(获取列表服务实现),自定义基础列表查询,基础分页列表查询服务实现
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class UserRoleMenuGetListServiceImpl implements IUserRoleMenuGetListService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private IUserRoleMenuBaseService baseService;

    @Autowired
    private IUserMenuGetMapService userMenuGetMapService;
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    @Override
    public List<UserRoleMenuVO> getList(UserRoleMenuQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<UserRoleMenuPO> poList = this.baseService.list(this.buildQueryWrapper(req));
        List<UserRoleMenuVO> rsp = this.buildRspVO(poList);
        return rsp;
    }
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    @Override
    public Page<UserRoleMenuVO> getPageList(UserRoleMenuQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造分页入参对象
        Page<UserRoleMenuPO> page = req.buildPage();
        //分页查询列表数据
        Page<UserRoleMenuPO> poPage = this.baseService.page(page, this.buildQueryWrapper(req));
        //构造出参
        Page<UserRoleMenuVO> rsp = PageUtils.poPageToVoPage(poPage, UserRoleMenuVO.class);
        List<UserRoleMenuVO> voList = this.buildRspVO(poPage.getRecords());
        rsp.setRecords(voList);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(UserRoleMenuQueryBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(UserRoleMenuQueryBO req) {
        // TODO 执行前调用逻辑
    }
    
    
    /**
     * 构造QueryWrapper查询入参
     *
     * @param req 查询业务对象
     * @return QueryWrapper对象
     */
    private LambdaQueryWrapper<UserRoleMenuPO> buildQueryWrapper(UserRoleMenuQueryBO req) {
        //构造查询包装器
        LambdaQueryWrapper<UserRoleMenuPO> rsp = req.getQueryWrapper();
        //数据权限范围控制
        this.buildDataRangeOnQueryWrapper(rsp);
        return rsp;
    }
    
    
    /**
     *在查询包装器里构建查询数据范围
     * @param wrapper
     */
    private void buildDataRangeOnQueryWrapper(LambdaQueryWrapper<UserRoleMenuPO> wrapper){

    }
    
    
    /**
     * 构造响应视图对象列表
     * @param req PO列表数据
     * @return VO列表数据
     */
    private List<UserRoleMenuVO> buildRspVO(List<UserRoleMenuPO> req) {
        List<UserRoleMenuVO> rsp = new ArrayList<>();
        if(null != req && req.size() >0){
            rsp = req.stream().map(k->{
                List<Long> menuIdList = Func.getPropList(req, UserRoleMenuPO::getMenuId);
                UserMenuQueryBO userMenuQueryBO = new UserMenuQueryBO();
                userMenuQueryBO.setIdList(menuIdList);
                Map<Long, UserMenuVO> userMenuMap = userMenuGetMapService.getMapGroupById(userMenuQueryBO);
                UserRoleMenuVO vo = k.parseVO(userMenuMap);
                return vo;
            }).collect(Collectors.toList());
        }
        return rsp;
    }
}