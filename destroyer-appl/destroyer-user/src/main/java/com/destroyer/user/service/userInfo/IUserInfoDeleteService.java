package com.destroyer.user.service.userInfo;

import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.userInfo.bo.UserInfoQueryBO;

/**
 * 标题：用户信息(删除数据服务接口)
 * 说明：用户信息(删除数据服务接口),自定义删除数据服务接口
 * 时间：2024-2-26
 * 作者：admin
 */
public interface IUserInfoDeleteService {
    /**
     * 删除数据（根据主键ID）
     * @param req
     * @return
     */
    boolean deleteById(Long req);
    
    
    /**
     * 删除数据（根据主键ID集合批量删除）
     * @param req
     * @return
     */
    boolean batchDeleteByIds(BaseIdListBO req);
    
    
     /**
     * 删除数据（根据查询条件）
     * @param req
     * @return
     */
    boolean batchDeleteBy(UserInfoQueryBO req);
}