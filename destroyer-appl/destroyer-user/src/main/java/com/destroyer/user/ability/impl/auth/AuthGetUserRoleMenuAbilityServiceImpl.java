package com.destroyer.user.ability.impl.auth;

import com.destroyer.common.entity.auth.AuthUserInfoVO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.userMenu.vo.UserMenuVO;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuQueryBO;
import com.destroyer.core.entity.userRoleMenu.vo.UserRoleMenuVO;
import com.destroyer.core.util.UserUtils;
import com.destroyer.user.ability.auth.IAutheGetUserRoleMenuAbilityService;
import com.destroyer.user.service.userRoleMenu.IUserRoleMenuGetListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 标题：AuthGetUserRoleMenuAbilityServiceImpl
 * 说明：
 * 时间：2024/2/28
 * 作者：admin
 */
@Service
public class AuthGetUserRoleMenuAbilityServiceImpl implements IAutheGetUserRoleMenuAbilityService {


    @Autowired
    private IUserRoleMenuGetListService userRoleMenuGetListService;
    /**
     * 获取当前用户所属角色菜单
     * @return
     */
    @Override
    public List<UserMenuVO> getUserRoleMenu() {
        List<UserMenuVO> rsp = new ArrayList<>();
        //入参效验
        this.validParams();
        //执行核心逻辑前调用
        this.beforeToDo();
        AuthUserInfoVO userInfoVO = UserUtils.getSysUser();
        Long roleId = userInfoVO.getRoleId();
        UserRoleMenuQueryBO userRoleMenuQueryBO = new UserRoleMenuQueryBO();
        userRoleMenuQueryBO.setRoleId(roleId);
        List<UserRoleMenuVO> userRoleMenuList = userRoleMenuGetListService.getList(userRoleMenuQueryBO);
        if(null != userRoleMenuList){
            for (UserRoleMenuVO userRoleMenuVO : userRoleMenuList){
                rsp.add(userRoleMenuVO.getMenu());
            }
        }

        return rsp;
    }


    /**
     * 类通用入参校验
     *
     */
    private void validParams( ) {
      if(!UserUtils.isLogin()){
          throw new ServiceException(ResultEnum.UN_AUTHORIZED,"未登录用户！");
      }
      if(!UserUtils.isSysUser()){
          throw new ServiceException("非管理后台用户！");

      }
    }

    /**
     * 类通用执行前调用逻辑
     *
     */
    private void beforeToDo( ) {

    }
}
