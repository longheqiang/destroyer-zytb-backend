package com.destroyer.user.service.userInfo;

import com.destroyer.core.service.excel.IExcelBaseService;
/**
 * 标题：用户信息(Excel服务接口)
 * 说明：用户信息(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-7
 * 作者：admin
 */
public interface IUserInfoExcelService extends IExcelBaseService {



}