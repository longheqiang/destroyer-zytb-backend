package com.destroyer.user.service.userRoleMenu;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuBO;
import java.util.List;

 /**
 * 标题：用户角色菜单权限(批量新增服务接口)
 * 说明：用户角色菜单权限(批量新增服务接口),自定义批量新增据服务接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleMenuBatchCreateService {
    
    
    /**
    * 批量新增数据
    * @param req
    * @return 新增数据的组件ID集合
    */
    List<Long> batchCreate(BaseBatchBO<UserRoleMenuBO> req);
}