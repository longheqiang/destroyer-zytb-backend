package com.destroyer.user.service.impl.userMenu;

import com.destroyer.core.entity.userMenu.bo.UserMenuQueryBO;
import com.destroyer.core.entity.userMenu.vo.UserMenuVO;
import com.destroyer.user.service.userMenu.IUserMenuGetDetailService;
import com.destroyer.user.service.userMenu.IUserMenuGetListService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

 /**
 * 标题：系统菜单(查询详情服务实现)
 * 说明：系统菜单(查询详情服务实现),自定义基础详情查询接口
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class UserMenuGetDetailServiceImpl implements IUserMenuGetDetailService {
    
    
    /**
     * 查询列表服务
     */
    @Autowired
    private IUserMenuGetListService getListService;
    /**
     * 根据条件查询详情
     * @param req
     * @return
     */
    @Override
    public UserMenuVO getDetailBy(UserMenuQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<UserMenuVO> list = getListService.getList(req);
        if (list.size() > 1) {
            throw new ServiceException(ResultEnum.FAILURE,"查询到的数据不唯一！");
        }
        UserMenuVO rsp = list.size() == 1? list.get(0):null;
        return rsp;
    }


    /**
     * 根据主键ID查询详情
     * @param req
     * @return
     */
    @Override
    public UserMenuVO getDetailById(Long req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"查询详情入参ID不能为空");
        }
        UserMenuQueryBO queryBO = new UserMenuQueryBO();
        queryBO.setId(req);
        UserMenuVO rsp = this.getDetailBy(queryBO);
        return rsp;
    }
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(UserMenuQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        } else {
        }
    }
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(UserMenuQueryBO req) {
        // TODO 执行前调用逻辑
    }
}