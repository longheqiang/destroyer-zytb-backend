package com.destroyer.user.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuBatchModifyBO;
import com.destroyer.user.ability.userRoleMenu.UserRoleMenuBatchModifyAbilityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuBO;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuQueryBO;
import com.destroyer.core.entity.userRoleMenu.vo.UserRoleMenuVO;
import com.destroyer.user.service.userRoleMenu.*;

 /**
 * 标题：用户角色菜单权限(控制层)
 * 说明：用户角色菜单权限(控制层),包括所有对外前端API接口
 * 时间：2024-2-27
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/user/userRoleMenu")
@Api(value = "UserRoleMenuController", tags = "用户角色菜单权限API")
public class UserRoleMenuController {
    
    
    /**
     * 基础服务
     */
    @Autowired
    private IUserRoleMenuBaseService baseService;
    
    
    /**
     * 新增数据服务
     */
    @Autowired
    private IUserRoleMenuCreateService createService;
    
    
    
    /**
     * 批量新增服务
     */
    @Autowired
    private IUserRoleMenuBatchCreateService batchCreateService;
    
    
    
    /**
      * 删除数据服务
      */
    @Autowired
    private IUserRoleMenuDeleteService deleteService;
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IUserRoleMenuGetListService getListService;
    
    
     /**
     * map服务
     */
    @Autowired
    private IUserRoleMenuGetMapService getMapService;
    
    
    /**
     * 详情服务
     */
    @Autowired
    private IUserRoleMenuGetDetailService getDetailService;
    
    
    /**
     * 编辑服务
     */
    @Autowired
    private IUserRoleMenuEditService editService;

     /**
      * 修改权限能力
      */
    @Autowired
    private UserRoleMenuBatchModifyAbilityService batchModifyAbilityService;
    
    
    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody UserRoleMenuBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }
    
    
    
    @PostMapping(value = "/batchCreate")
    @ApiOperation("批量新增API")
    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<UserRoleMenuBO> req) {
        List<Long> rsp = this.batchCreateService.batchCreate(req);
        return Result.success(rsp);
    }


     @PostMapping(value = "/deleteById")
     @ApiOperation("删除数据API(根据主键ID)")
     public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
         boolean rsp = this.deleteService.deleteById(req.getId());
         return Result.success(rsp);
     }
    
    
    @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }
    
    
    
    @PostMapping(value = "/deleteBy")
    @ApiOperation("删除数据API(根据查询条件删除)")
    public Result<Boolean> deleteBy(@RequestBody UserRoleMenuQueryBO req) {
        boolean rsp = this.deleteService.batchDeleteBy(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<UserRoleMenuVO>> list(@RequestBody UserRoleMenuQueryBO req) {
        List<UserRoleMenuVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/mapGroupById")
    @ApiOperation("查询map(ID分组)API")
    public Result<Map<Long, UserRoleMenuVO>> mapGroupById(@RequestBody UserRoleMenuQueryBO req) {
        Map<Long, UserRoleMenuVO> rsp = this.getMapService.getMapGroupById(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<UserRoleMenuVO>> page(@RequestBody UserRoleMenuQueryBO req) {
        Page<UserRoleMenuVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<UserRoleMenuVO> detailBy(@RequestBody UserRoleMenuQueryBO req) {
        UserRoleMenuVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


     @PostMapping(value = "/detailById")
     @ApiOperation("根据ID查询详情API")
     public Result<UserRoleMenuVO> detailById(@RequestBody BaseIdBO req) {
         UserRoleMenuVO rsp = this.getDetailService.getDetailById(req.getId());
         return Result.success(rsp);
     }
    
    
    @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody UserRoleMenuBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }

     @PostMapping(value = "/batchModify")
     @ApiOperation("批量修改权限API")
     public Result<Boolean> batchModify(@RequestBody UserRoleMenuBatchModifyBO req) {
         boolean rsp = this.batchModifyAbilityService.batchModify(req);
         return Result.success(rsp);
     }
}