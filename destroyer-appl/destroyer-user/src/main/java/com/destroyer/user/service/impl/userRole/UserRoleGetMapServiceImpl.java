package com.destroyer.user.service.impl.userRole;

import com.destroyer.core.entity.userRole.bo.UserRoleQueryBO;
import com.destroyer.core.entity.userRole.vo.UserRoleVO;
import com.destroyer.user.service.userRole.IUserRoleGetListService;
import com.destroyer.user.service.userRole.IUserRoleGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：用户角色(获取Map集合服务实现)
 * 说明：用户角色(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class UserRoleGetMapServiceImpl implements IUserRoleGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IUserRoleGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, UserRoleVO> getMapGroupById(UserRoleQueryBO req) {
        Map<Long, UserRoleVO> rsp = new HashMap<>();
        List<UserRoleVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(UserRoleVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}