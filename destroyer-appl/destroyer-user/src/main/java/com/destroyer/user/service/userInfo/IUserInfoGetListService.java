package com.destroyer.user.service.userInfo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.userInfo.bo.UserInfoQueryBO;
import com.destroyer.core.entity.userInfo.vo.UserInfoVO;
import java.util.List;

 /**
 * 标题：用户信息(获取列表服务接口)
 * 说明：用户信息(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-2-26
 * 作者：admin
 */
public interface IUserInfoGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<UserInfoVO> getList(UserInfoQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<UserInfoVO> getPageList(UserInfoQueryBO req);
}