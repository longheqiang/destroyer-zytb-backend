package com.destroyer.user.ability.impl.userRoleMenu;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.Func;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuBO;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuBatchModifyBO;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuQueryBO;
import com.destroyer.user.ability.userRoleMenu.UserRoleMenuBatchModifyAbilityService;
import com.destroyer.user.service.userRoleMenu.IUserRoleMenuBatchCreateService;
import com.destroyer.user.service.userRoleMenu.IUserRoleMenuDeleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 标题：UserRoleMenuBatchModifyAbilityServiceImpl
 * 说明：
 * 时间：2024/2/27
 * 作者：admin
 */
@Service
@Transactional
public class UserRoleMenuBatchModifyAbilityServiceImpl implements UserRoleMenuBatchModifyAbilityService {


    @Autowired
    private IUserRoleMenuBatchCreateService batchCreateService;
    @Autowired
    private IUserRoleMenuDeleteService deleteService;
    /**
     * 修改权限
     * @param req
     * @return
     */
    @Override
    public boolean batchModify(UserRoleMenuBatchModifyBO req) {
        boolean rsp = false;
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        if(Func.isNotEmpty(req.getMenuIdList())){
            BaseBatchBO<UserRoleMenuBO> batchCreateBO = new BaseBatchBO<>();
            for(Long menuId: req.getMenuIdList()){
                UserRoleMenuBO bo = new UserRoleMenuBO();
                bo.setRoleId(req.getRoleId());
                bo.setMenuId(menuId);
                batchCreateBO.add(bo);
            }
            rsp = batchCreateService.batchCreate(batchCreateBO).size() > 0;


        }

        return rsp;
    }


    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(UserRoleMenuBatchModifyBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
            if(null == req.getRoleId()){
                throw new ServiceException(ResultEnum.PARAM_MISS, "系统角色不能为空");
            }
        }
    }

    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(UserRoleMenuBatchModifyBO req) {
        // TODO 执行前调用逻辑
        UserRoleMenuQueryBO queryBO = new UserRoleMenuQueryBO();
        queryBO.setRoleId(req.getRoleId());
        deleteService.batchDeleteBy(queryBO);
    }
}
