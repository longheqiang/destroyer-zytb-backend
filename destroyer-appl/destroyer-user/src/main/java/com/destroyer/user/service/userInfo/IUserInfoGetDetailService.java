package com.destroyer.user.service.userInfo;

import com.destroyer.core.entity.userInfo.bo.UserInfoQueryBO;
import com.destroyer.core.entity.userInfo.vo.UserInfoVO;


 /**
 * 标题：用户信息(查询详情服务接口)
 * 说明：用户信息(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-2-26
 * 作者：admin
 */
public interface IUserInfoGetDetailService {
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    UserInfoVO getDetailBy(UserInfoQueryBO req);
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    UserInfoVO getDetailById(Long req);
}