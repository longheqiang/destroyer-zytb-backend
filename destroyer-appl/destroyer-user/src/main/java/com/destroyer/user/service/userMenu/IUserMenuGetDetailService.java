package com.destroyer.user.service.userMenu;

import com.destroyer.core.entity.userMenu.bo.UserMenuQueryBO;
import com.destroyer.core.entity.userMenu.vo.UserMenuVO;

/**
 * 标题：系统菜单(查询详情服务接口)
 * 说明：系统菜单(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserMenuGetDetailService {
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    UserMenuVO getDetailBy(UserMenuQueryBO req);
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    UserMenuVO getDetailById(Long req);
}