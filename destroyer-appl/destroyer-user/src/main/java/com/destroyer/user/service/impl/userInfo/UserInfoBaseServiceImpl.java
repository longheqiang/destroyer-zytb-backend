package com.destroyer.user.service.impl.userInfo;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.userInfo.UserInfoPO;
import com.destroyer.user.mapper.UserInfoMapper;
import com.destroyer.user.service.userInfo.IUserInfoBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：用户信息(基础通用服务实现)
 * 说明：用户信息(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-2-26
 * 作者：admin
 */
@Service
public class  UserInfoBaseServiceImpl extends ServiceImpl<UserInfoMapper, UserInfoPO> implements IUserInfoBaseService {
}