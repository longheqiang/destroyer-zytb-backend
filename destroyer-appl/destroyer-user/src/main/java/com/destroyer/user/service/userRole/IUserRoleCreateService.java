package com.destroyer.user.service.userRole;

import com.destroyer.core.entity.userRole.bo.UserRoleBO;

 /**
 * 标题：用户角色(新增服务接口)
 * 说明：用户角色(新增服务接口),自定义新增数据接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(UserRoleBO req);
}