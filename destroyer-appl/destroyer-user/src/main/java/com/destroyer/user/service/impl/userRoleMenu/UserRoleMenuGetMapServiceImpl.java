package com.destroyer.user.service.impl.userRoleMenu;

import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuQueryBO;
import com.destroyer.core.entity.userRoleMenu.vo.UserRoleMenuVO;
import com.destroyer.user.service.userRoleMenu.IUserRoleMenuGetListService;
import com.destroyer.user.service.userRoleMenu.IUserRoleMenuGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：用户角色菜单权限(获取Map集合服务实现)
 * 说明：用户角色菜单权限(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class UserRoleMenuGetMapServiceImpl implements IUserRoleMenuGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IUserRoleMenuGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, UserRoleMenuVO> getMapGroupById(UserRoleMenuQueryBO req) {
        Map<Long, UserRoleMenuVO> rsp = new HashMap<>();
        List<UserRoleMenuVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(UserRoleMenuVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}