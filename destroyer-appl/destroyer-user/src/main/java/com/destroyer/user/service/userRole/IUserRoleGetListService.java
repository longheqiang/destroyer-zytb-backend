package com.destroyer.user.service.userRole;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.userRole.bo.UserRoleQueryBO;
import com.destroyer.core.entity.userRole.vo.UserRoleVO;
import java.util.List;

 /**
 * 标题：用户角色(获取列表服务接口)
 * 说明：用户角色(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<UserRoleVO> getList(UserRoleQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<UserRoleVO> getPageList(UserRoleQueryBO req);
}