package com.destroyer.user.service.userInfo;

import com.destroyer.core.entity.userInfo.bo.UserInfoBO;

 /**
 * 标题：用户信息(编辑数据服务接口)
 * 说明：用户信息(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-2-26
 * 作者：admin
 */
public interface IUserInfoEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(UserInfoBO req);
}