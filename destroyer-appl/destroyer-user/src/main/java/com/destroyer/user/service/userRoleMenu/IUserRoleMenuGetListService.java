package com.destroyer.user.service.userRoleMenu;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuQueryBO;
import com.destroyer.core.entity.userRoleMenu.vo.UserRoleMenuVO;
import java.util.List;

 /**
 * 标题：用户角色菜单权限(获取列表服务接口)
 * 说明：用户角色菜单权限(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleMenuGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<UserRoleMenuVO> getList(UserRoleMenuQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<UserRoleMenuVO> getPageList(UserRoleMenuQueryBO req);
}