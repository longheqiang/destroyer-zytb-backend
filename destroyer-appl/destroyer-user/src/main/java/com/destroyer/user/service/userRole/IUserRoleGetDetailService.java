package com.destroyer.user.service.userRole;

import com.destroyer.core.entity.userRole.bo.UserRoleQueryBO;
import com.destroyer.core.entity.userRole.vo.UserRoleVO;

/**
 * 标题：用户角色(查询详情服务接口)
 * 说明：用户角色(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleGetDetailService {
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    UserRoleVO getDetailBy(UserRoleQueryBO req);


     /**
      * 根据主键ID查询详情
      * @param req
      * @return
      */
     UserRoleVO getDetailById(Long req);
}