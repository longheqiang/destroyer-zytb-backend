package com.destroyer.user.service.userMenu;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.userMenu.UserMenuPO;

 /**
 * 标题：系统菜单(基础通用服务接口)
 * 说明：系统菜单(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserMenuBaseService extends IService<UserMenuPO> {
}