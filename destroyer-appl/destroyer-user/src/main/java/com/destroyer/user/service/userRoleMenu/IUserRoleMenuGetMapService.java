package com.destroyer.user.service.userRoleMenu;

import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuQueryBO;
import com.destroyer.core.entity.userRoleMenu.vo.UserRoleMenuVO;
import java.util.Map;

 /**
 * 标题：用户角色菜单权限(获取Map集合服务接口)
 * 说明：用户角色菜单权限(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleMenuGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, UserRoleMenuVO> getMapGroupById(UserRoleMenuQueryBO req);
}