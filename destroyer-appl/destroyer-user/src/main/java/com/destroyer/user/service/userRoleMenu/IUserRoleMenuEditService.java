package com.destroyer.user.service.userRoleMenu;

import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuBO;

 /**
 * 标题：用户角色菜单权限(编辑数据服务接口)
 * 说明：用户角色菜单权限(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleMenuEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(UserRoleMenuBO req);
}