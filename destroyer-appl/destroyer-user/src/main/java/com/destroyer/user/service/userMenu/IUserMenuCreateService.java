package com.destroyer.user.service.userMenu;

import com.destroyer.core.entity.userMenu.bo.UserMenuBO;

 /**
 * 标题：系统菜单(新增服务接口)
 * 说明：系统菜单(新增服务接口),自定义新增数据接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserMenuCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(UserMenuBO req);
}