package com.destroyer.user.service.impl.userInfo;

import com.destroyer.core.entity.userInfo.bo.UserInfoQueryBO;
import com.destroyer.core.entity.userInfo.vo.UserInfoVO;
import com.destroyer.user.service.userInfo.IUserInfoGetListService;
import com.destroyer.user.service.userInfo.IUserInfoGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：用户信息(获取Map集合服务实现)
 * 说明：用户信息(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-2-26
 * 作者：admin
 */
@Service
public class UserInfoGetMapServiceImpl implements IUserInfoGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IUserInfoGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, UserInfoVO> getMapGroupById(UserInfoQueryBO req) {
        Map<Long, UserInfoVO> rsp = new HashMap<>();
        List<UserInfoVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(UserInfoVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}