package com.destroyer.user.service.impl.userMenu;

import com.destroyer.core.entity.userMenu.bo.UserMenuQueryBO;
import com.destroyer.core.entity.userMenu.vo.UserMenuVO;
import com.destroyer.user.service.userMenu.IUserMenuGetListService;
import com.destroyer.user.service.userMenu.IUserMenuGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：系统菜单(获取Map集合服务实现)
 * 说明：系统菜单(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class UserMenuGetMapServiceImpl implements IUserMenuGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IUserMenuGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, UserMenuVO> getMapGroupById(UserMenuQueryBO req) {
        Map<Long, UserMenuVO> rsp = new HashMap<>();
        List<UserMenuVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(UserMenuVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}