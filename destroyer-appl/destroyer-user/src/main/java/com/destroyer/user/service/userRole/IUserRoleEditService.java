package com.destroyer.user.service.userRole;

import com.destroyer.core.entity.userRole.bo.UserRoleBO;

 /**
 * 标题：用户角色(编辑数据服务接口)
 * 说明：用户角色(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(UserRoleBO req);
}