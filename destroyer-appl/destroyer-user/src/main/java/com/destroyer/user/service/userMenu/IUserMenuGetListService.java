package com.destroyer.user.service.userMenu;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.userMenu.bo.UserMenuQueryBO;
import com.destroyer.core.entity.userMenu.vo.UserMenuVO;

import java.util.List;

 /**
 * 标题：系统菜单(获取列表服务接口)
 * 说明：系统菜单(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserMenuGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<UserMenuVO> getList(UserMenuQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<UserMenuVO> getPageList(UserMenuQueryBO req);
}