package com.destroyer.user.controller;

import com.alibaba.fastjson.JSONObject;
import com.destroyer.common.entity.system.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * 标题：TestController
 * 说明：
 * 时间：2023/8/30
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/assess/test")
@Api(value = "TestController", tags = "测试API")
public class TestController {


    @PostMapping(value = "/test1")
    @ApiOperation("模板API")
    public Result<JSONObject> test(@RequestBody JSONObject req) {
        return Result.success(req);
    }
}
