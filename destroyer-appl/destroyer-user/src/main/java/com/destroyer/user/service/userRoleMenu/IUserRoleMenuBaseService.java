package com.destroyer.user.service.userRoleMenu;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.userRoleMenu.UserRoleMenuPO;

 /**
 * 标题：用户角色菜单权限(基础通用服务接口)
 * 说明：用户角色菜单权限(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleMenuBaseService extends IService<UserRoleMenuPO> {
}