package com.destroyer.user.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.userMenu.vo.UserMenuVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.userMenu.bo.UserMenuBO;
import com.destroyer.core.entity.userMenu.bo.UserMenuQueryBO;
import com.destroyer.user.service.userMenu.*;

 /**
 * 标题：系统菜单(控制层)
 * 说明：系统菜单(控制层),包括所有对外前端API接口
 * 时间：2024-2-27
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/user/userMenu")
@Api(value = "UserMenuController", tags = "系统菜单API")
public class UserMenuController {
    
    
    /**
     * 基础服务
     */
    @Autowired
    private IUserMenuBaseService baseService;
    
    
    /**
     * 新增数据服务
     */
    @Autowired
    private IUserMenuCreateService createService;
    
    
    
    /**
     * 批量新增服务
     */
    @Autowired
    private IUserMenuBatchCreateService batchCreateService;
    
    
    
    /**
      * 删除数据服务
      */
    @Autowired
    private IUserMenuDeleteService deleteService;
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IUserMenuGetListService getListService;
    
    
     /**
     * map服务
     */
    @Autowired
    private IUserMenuGetMapService getMapService;
    
    
    /**
     * 详情服务
     */
    @Autowired
    private IUserMenuGetDetailService getDetailService;
    
    
    /**
     * 编辑服务
     */
    @Autowired
    private IUserMenuEditService editService;
    
    
    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody UserMenuBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }
    
    
    
    @PostMapping(value = "/batchCreate")
    @ApiOperation("批量新增API")
    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<UserMenuBO> req) {
        List<Long> rsp = this.batchCreateService.batchCreate(req);
        return Result.success(rsp);
    }



     @PostMapping(value = "/deleteById")
     @ApiOperation("删除数据API(根据主键ID)")
     public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
         boolean rsp = this.deleteService.deleteById(req.getId());
         return Result.success(rsp);
     }


     @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }
    
    
    
    @PostMapping(value = "/deleteBy")
    @ApiOperation("删除数据API(根据查询条件删除)")
    public Result<Boolean> deleteBy(@RequestBody UserMenuQueryBO req) {
        boolean rsp = this.deleteService.batchDeleteBy(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<UserMenuVO>> list(@RequestBody UserMenuQueryBO req) {
        List<UserMenuVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/mapGroupById")
    @ApiOperation("查询map(ID分组)API")
    public Result<Map<Long, UserMenuVO>> mapGroupById(@RequestBody UserMenuQueryBO req) {
        Map<Long, UserMenuVO> rsp = this.getMapService.getMapGroupById(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<UserMenuVO>> page(@RequestBody UserMenuQueryBO req) {
        Page<UserMenuVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<UserMenuVO> detailBy(@RequestBody UserMenuQueryBO req) {
        UserMenuVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


     @PostMapping(value = "/detailById")
     @ApiOperation("根据ID查询详情API")
     public Result<UserMenuVO> detailById(@RequestBody BaseIdBO req) {
         UserMenuVO rsp = this.getDetailService.getDetailById(req.getId());
         return Result.success(rsp);
     }
    
    
    @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody UserMenuBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }
}