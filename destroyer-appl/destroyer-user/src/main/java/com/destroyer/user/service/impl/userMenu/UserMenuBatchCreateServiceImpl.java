package com.destroyer.user.service.impl.userMenu;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.userMenu.UserMenuPO;
import com.destroyer.core.entity.userMenu.bo.UserMenuBO;
import com.destroyer.user.service.userMenu.IUserMenuBaseService;
import com.destroyer.user.service.userMenu.IUserMenuBatchCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

/**
 * 标题：系统菜单(批量新增服务实现)
 * 说明：系统菜单(批量新增服务实现),自定义批量新增据服务实现
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class UserMenuBatchCreateServiceImpl implements IUserMenuBatchCreateService {


    /**
     * 基础通用服务
     */
    @Autowired
    private IUserMenuBaseService baseService;

    /**
     * 批量新增数据
     *
     * @param req
     * @return 新增数据的组件ID集合
     */
    @Override
    public List<Long> batchCreate(BaseBatchBO<UserMenuBO> req) {
        List<Long> rsp = new ArrayList<>();
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<UserMenuPO> poList = req.getList().stream().map(k -> {
            UserMenuPO po = new UserMenuPO(k);
            return po;
        }).collect(Collectors.toList());
        boolean state = baseService.saveBatch(poList);
        if (!state) {
            throw new ServiceException(ResultEnum.FAILURE, "批量新增数据失败");
        }
        rsp = poList.stream().map(UserMenuPO::getId).collect(Collectors.toList());
        return rsp;
    }

    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(BaseBatchBO<UserMenuBO> req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
            if (null == req || req.getList().size() == 0) {
                throw new ServiceException(ResultEnum.PARAM_MISS, "批量新增入参不能为空");
            }
            List<UserMenuBO> list = req.getList();
            for (UserMenuBO i : list) {


                if (null == i.getPid()) {
                    throw new ServiceException(ResultEnum.PARAM_MISS, "入参[父ID]不能为空!");
                }


                if (StringUtils.isBlank(i.getName())) {
                    throw new ServiceException(ResultEnum.PARAM_MISS, "入参[菜单名称]不能为空!");
                }


                if (StringUtils.isBlank(i.getPath())) {
                    throw new ServiceException(ResultEnum.PARAM_MISS, "入参[目录地址]不能为空!");
                }


            }
        }
    }

    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(BaseBatchBO<UserMenuBO> req) {
        // TODO 执行前调用逻辑
    }
}