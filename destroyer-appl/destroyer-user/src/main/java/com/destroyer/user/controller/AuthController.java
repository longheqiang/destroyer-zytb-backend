package com.destroyer.user.controller;

import com.destroyer.common.entity.auth.AuthBaseVO;
import com.destroyer.common.entity.auth.AuthLoginBO;
import com.destroyer.common.entity.auth.AuthUserInfoVO;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.userInfo.vo.UserInfoVO;
import com.destroyer.core.entity.userMenu.vo.UserMenuVO;
import com.destroyer.user.ability.auth.IAutheGetUserRoleMenuAbilityService;
import com.destroyer.user.ability.auth.IAuthLoginAbilityService;

import com.destroyer.user.ability.auth.IAuthUserInfoDetailAbilityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 标题：AuthController
 * 说明：
 * 时间：2023/9/7
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/user/auth")
@Api(value = "AuthController", tags = "身份认证API")
public class AuthController {


    /**
     * 基础用户登录能力服务
     */
    @Autowired
    private IAuthLoginAbilityService authLoginAbilityService;




    /**
     * 身份认证详情能力服务
     */
    @Autowired
    private IAuthUserInfoDetailAbilityService authUserInfoDetailService;

    /**
     * 获取当前用户所属角色的菜单能力服务
     */
    @Autowired
    private IAutheGetUserRoleMenuAbilityService authGetUserRoleMenuAbilityService;

    @PostMapping(value = "/login")
    @ApiOperation("系统用户登录API")
    public Result< AuthBaseVO<AuthUserInfoVO>> login(@RequestBody AuthLoginBO req) {
        AuthBaseVO<AuthUserInfoVO> rsp = authLoginAbilityService.login(req);
        return Result.success(rsp);
    }

    @PostMapping(value = "/getAuthUserInfoDetail")
    @ApiOperation("获取当前登录的系统用户身份信息信息API")
    public Result<UserInfoVO> getAuthUserInfoDetail() {
        UserInfoVO rsp = authUserInfoDetailService.getAuthUserInfoDetail();
        return Result.success(rsp);
    }

    @PostMapping(value = "/getAuthUserRoleMenu")
    @ApiOperation("获取当前用户菜单权限API")
    public Result<List<UserMenuVO>> getAuthUserRoleMenu() {
        List<UserMenuVO> rsp = authGetUserRoleMenuAbilityService.getUserRoleMenu();
        return Result.success(rsp);
    }

}
