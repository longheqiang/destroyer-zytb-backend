package com.destroyer.user.service.userMenu;

import com.destroyer.core.entity.userMenu.bo.UserMenuBO;

 /**
 * 标题：系统菜单(编辑数据服务接口)
 * 说明：系统菜单(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserMenuEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(UserMenuBO req);
}