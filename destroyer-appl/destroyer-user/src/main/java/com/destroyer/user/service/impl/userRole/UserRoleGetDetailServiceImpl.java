package com.destroyer.user.service.impl.userRole;

import com.destroyer.core.entity.userRole.bo.UserRoleQueryBO;
import com.destroyer.core.entity.userRole.vo.UserRoleVO;
import com.destroyer.user.service.userRole.IUserRoleGetDetailService;
import com.destroyer.user.service.userRole.IUserRoleGetListService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

 /**
 * 标题：用户角色(查询详情服务实现)
 * 说明：用户角色(查询详情服务实现),自定义基础详情查询接口
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class UserRoleGetDetailServiceImpl implements IUserRoleGetDetailService {
    
    
    /**
     * 查询列表服务
     */
    @Autowired
    private IUserRoleGetListService getListService;
    /**
     * 根据条件查询详情
     * @param req
     * @return
     */
    @Override
    public UserRoleVO getDetailBy(UserRoleQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<UserRoleVO> list = getListService.getList(req);
        if (list.size() > 1) {
            throw new ServiceException(ResultEnum.FAILURE,"查询到的数据不唯一！");
        }
        UserRoleVO rsp = list.size() == 1? list.get(0):null;
        return rsp;
    }


     /**
      * 根据主键ID查询详情
      * @param req
      * @return
      */
     @Override
     public UserRoleVO getDetailById(Long req) {
         if (null == req) {
             throw new ServiceException(ResultEnum.PARAM_MISS,"查询详情入参ID不能为空");
         }
         UserRoleQueryBO queryBO = new UserRoleQueryBO();
         queryBO.setId(req);
         UserRoleVO rsp = this.getDetailBy(queryBO);
         return rsp;
     }
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(UserRoleQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        } else {
        }
    }
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(UserRoleQueryBO req) {
        // TODO 执行前调用逻辑
    }
}