package com.destroyer.user.service.impl.userInfo;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.userInfo.UserInfoPO;
import com.destroyer.core.entity.userInfo.bo.UserInfoBO;
import com.destroyer.user.service.userInfo.IUserInfoBaseService;
import com.destroyer.user.service.userInfo.IUserInfoEditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：用户信息(编辑数据服务实现)
 * 说明：用户信息(编辑数据服务实现),自定义编辑数据服务实现
 * 时间：2024-2-26
 * 作者：admin
 */
@Service
public class UserInfoEditServiceImpl implements IUserInfoEditService {
    
    
     /**
     * 基础通用服务
     */
    @Autowired
    private IUserInfoBaseService baseService;
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    @Override
    public boolean editById(UserInfoBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //入参处理
        UserInfoPO po = new UserInfoPO(req);
        //更新数据
        boolean rsp = this.baseService.updateById(po);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(UserInfoBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
	
        if (null == req.getId()) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "更新数据入参ID不能为空");
        }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(UserInfoBO req) {
        // TODO 执行前调用逻辑
    }
}