package com.destroyer.user.service.impl.userInfo;

import com.destroyer.core.entity.userInfo.bo.UserInfoQueryBO;
import com.destroyer.core.entity.userInfo.vo.UserInfoVO;
import com.destroyer.user.service.userInfo.IUserInfoGetDetailService;
import com.destroyer.user.service.userInfo.IUserInfoGetListService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 标题：用户信息(查询详情服务实现)
 * 说明：用户信息(查询详情服务实现),自定义基础详情查询接口
 * 时间：2024-2-26
 * 作者：admin
 */
@Service
public class UserInfoGetDetailServiceImpl implements IUserInfoGetDetailService {


    /**
     * 查询列表服务
     */
    @Autowired
    private IUserInfoGetListService getListService;

    /**
     * 根据条件查询详情
     *
     * @param req
     * @return
     */
    @Override
    public UserInfoVO getDetailBy(UserInfoQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<UserInfoVO> list = getListService.getList(req);
        if (list.size() > 1) {
            throw new ServiceException(ResultEnum.FAILURE, "查询到的数据不唯一！");
        }
        UserInfoVO rsp = list.size() == 1 ? list.get(0) : null;
        return rsp;
    }

    /**
     * 根据主键ID查询详情
     *
     * @param req
     * @return
     */
    @Override
    public UserInfoVO getDetailById(Long req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "查询详情入参ID不能为空");
        }
        UserInfoQueryBO queryBO = new UserInfoQueryBO();
        queryBO.setId(req);
        UserInfoVO rsp = this.getDetailBy(queryBO);
        return rsp;
    }

    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(UserInfoQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
        }
    }

    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(UserInfoQueryBO req) {
        // TODO 执行前调用逻辑
    }
}