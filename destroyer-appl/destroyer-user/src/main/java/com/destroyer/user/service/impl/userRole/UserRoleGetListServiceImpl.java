package com.destroyer.user.service.impl.userRole;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.common.entity.auth.AuthUserInfoVO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.util.PageUtils;
import com.destroyer.core.entity.userRole.UserRolePO;
import com.destroyer.core.entity.userRole.bo.UserRoleQueryBO;
import com.destroyer.core.entity.userRole.vo.UserRoleVO;
import com.destroyer.user.service.userRole.IUserRoleBaseService;
import com.destroyer.user.service.userRole.IUserRoleGetListService;
import com.destroyer.core.util.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

 /**
 * 标题：用户角色(获取列表服务实现)
 * 说明：用户角色(获取列表服务实现),自定义基础列表查询,基础分页列表查询服务实现
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class UserRoleGetListServiceImpl implements IUserRoleGetListService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private IUserRoleBaseService baseService;
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    @Override
    public List<UserRoleVO> getList(UserRoleQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<UserRolePO> poList = this.baseService.list(this.buildQueryWrapper(req));
        List<UserRoleVO> rsp = this.buildRspVO(poList);
        return rsp;
    }
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    @Override
    public Page<UserRoleVO> getPageList(UserRoleQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造分页入参对象
        Page<UserRolePO> page = req.buildPage();
        //分页查询列表数据
        Page<UserRolePO> poPage = this.baseService.page(page, this.buildQueryWrapper(req));
        //构造出参
        Page<UserRoleVO> rsp = PageUtils.poPageToVoPage(poPage, UserRoleVO.class);
        List<UserRoleVO> voList = this.buildRspVO(poPage.getRecords());
        rsp.setRecords(voList);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(UserRoleQueryBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(UserRoleQueryBO req) {
        // TODO 执行前调用逻辑
    }
    
    
    /**
     * 构造QueryWrapper查询入参
     *
     * @param req 查询业务对象
     * @return QueryWrapper对象
     */
    private LambdaQueryWrapper<UserRolePO> buildQueryWrapper(UserRoleQueryBO req) {
        //构造查询包装器
        LambdaQueryWrapper<UserRolePO> rsp = req.getQueryWrapper();
        //数据权限范围控制
        this.buildDataRangeOnQueryWrapper(rsp);
        return rsp;
    }
    
    
    /**
     *在查询包装器里构建查询数据范围
     * @param wrapper
     */
    private void buildDataRangeOnQueryWrapper(LambdaQueryWrapper<UserRolePO> wrapper){
        //非超级管理员
       if(UserUtils.isLogin() && !UserUtils.isSuperAdmin() && UserUtils.isSysUser()){
           AuthUserInfoVO userInfoVO = UserUtils.getSysUser();
           Long roleId = userInfoVO.getRole().getId();
           wrapper.eq(UserRolePO::getId,roleId);
       }
    }
    
    
    /**
     * 构造响应视图对象列表
     * @param req PO列表数据
     * @return VO列表数据
     */
    private List<UserRoleVO> buildRspVO(List<UserRolePO> req) {
        List<UserRoleVO> rsp = new ArrayList<>();
        if(null != req && req.size() >0){
            rsp = req.stream().map(k->{
                UserRoleVO vo = k.parseVO();
                return vo;
            }).collect(Collectors.toList());
        }
        return rsp;
    }
}