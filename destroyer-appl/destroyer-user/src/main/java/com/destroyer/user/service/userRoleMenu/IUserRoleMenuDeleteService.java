package com.destroyer.user.service.userRoleMenu;

import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.userRoleMenu.bo.UserRoleMenuQueryBO;

/**
 * 标题：用户角色菜单权限(删除数据服务接口)
 * 说明：用户角色菜单权限(删除数据服务接口),自定义删除数据服务接口
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleMenuDeleteService {
    /**
     * 删除数据（根据主键ID）
     * @param req
     * @return
     */
    boolean deleteById(Long req);
    
    
    /**
     * 删除数据（根据主键ID集合批量删除）
     * @param req
     * @return
     */
    boolean batchDeleteByIds(BaseIdListBO req);
    
    
     /**
     * 删除数据（根据查询条件）
     * @param req
     * @return
     */
    boolean batchDeleteBy(UserRoleMenuQueryBO req);
}