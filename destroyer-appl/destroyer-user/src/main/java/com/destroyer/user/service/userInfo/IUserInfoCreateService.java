package com.destroyer.user.service.userInfo;

import com.destroyer.core.entity.userInfo.bo.UserInfoBO;

 /**
 * 标题：用户信息(新增服务接口)
 * 说明：用户信息(新增服务接口),自定义新增数据接口
 * 时间：2024-2-26
 * 作者：admin
 */
public interface IUserInfoCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(UserInfoBO req);
}