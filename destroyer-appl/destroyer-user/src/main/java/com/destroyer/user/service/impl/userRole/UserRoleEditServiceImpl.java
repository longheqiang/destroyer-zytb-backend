package com.destroyer.user.service.impl.userRole;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.userRole.UserRolePO;
import com.destroyer.core.entity.userRole.bo.UserRoleBO;
import com.destroyer.user.service.userRole.IUserRoleBaseService;
import com.destroyer.user.service.userRole.IUserRoleEditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：用户角色(编辑数据服务实现)
 * 说明：用户角色(编辑数据服务实现),自定义编辑数据服务实现
 * 时间：2024-2-27
 * 作者：admin
 */
@Service
public class UserRoleEditServiceImpl implements IUserRoleEditService {
    
    
     /**
     * 基础通用服务
     */
    @Autowired
    private IUserRoleBaseService baseService;
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    @Override
    public boolean editById(UserRoleBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //入参处理
        UserRolePO po = new UserRolePO(req);
        //更新数据
        boolean rsp = this.baseService.updateById(po);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(UserRoleBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
	
        if (null == req.getId()) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "更新数据入参ID不能为空");
        }
	
	
	
	
	
	
	
	
	
	
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(UserRoleBO req) {
        // TODO 执行前调用逻辑
    }
}