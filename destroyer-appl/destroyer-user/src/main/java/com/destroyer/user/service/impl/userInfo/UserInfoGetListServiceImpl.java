package com.destroyer.user.service.impl.userInfo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.common.util.Func;
import com.destroyer.core.entity.userRole.bo.UserRoleQueryBO;
import com.destroyer.core.entity.userRole.vo.UserRoleVO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.util.PageUtils;
import com.destroyer.core.entity.userInfo.UserInfoPO;
import com.destroyer.core.entity.userInfo.bo.UserInfoQueryBO;
import com.destroyer.core.entity.userInfo.vo.UserInfoVO;
import com.destroyer.user.service.userInfo.IUserInfoBaseService;
import com.destroyer.user.service.userInfo.IUserInfoGetListService;
import com.destroyer.user.service.userRole.IUserRoleGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * 标题：用户信息(获取列表服务实现)
 * 说明：用户信息(获取列表服务实现),自定义基础列表查询,基础分页列表查询服务实现
 * 时间：2024-2-26
 * 作者：admin
 */
@Service
public class UserInfoGetListServiceImpl implements IUserInfoGetListService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private IUserInfoBaseService baseService;

    @Autowired
    private IUserRoleGetMapService userRoleGetMapService;
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    @Override
    public List<UserInfoVO> getList(UserInfoQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<UserInfoPO> poList = this.baseService.list(this.buildQueryWrapper(req));
        List<UserInfoVO> rsp = this.buildRspVO(poList);
        return rsp;
    }
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    @Override
    public Page<UserInfoVO> getPageList(UserInfoQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造分页入参对象
        Page<UserInfoPO> page = req.buildPage();
        //分页查询列表数据
        Page<UserInfoPO> poPage = this.baseService.page(page, this.buildQueryWrapper(req));
        //构造出参
        Page<UserInfoVO> rsp = PageUtils.poPageToVoPage(poPage, UserInfoVO.class);
        List<UserInfoVO> voList = this.buildRspVO(poPage.getRecords());
        rsp.setRecords(voList);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(UserInfoQueryBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(UserInfoQueryBO req) {
        // TODO 执行前调用逻辑
    }
    
    
    /**
     * 构造QueryWrapper查询入参
     *
     * @param req 查询业务对象
     * @return QueryWrapper对象
     */
    private LambdaQueryWrapper<UserInfoPO> buildQueryWrapper(UserInfoQueryBO req) {
        //构造查询包装器
        LambdaQueryWrapper<UserInfoPO> rsp = req.getQueryWrapper();
        //数据权限范围控制
        this.buildDataRangeOnQueryWrapper(rsp);
        return rsp;
    }
    
    
    /**
     *在查询包装器里构建查询数据范围
     * @param wrapper
     */
    private void buildDataRangeOnQueryWrapper(LambdaQueryWrapper<UserInfoPO> wrapper){
    }
    
    
    /**
     * 构造响应视图对象列表
     * @param req PO列表数据
     * @return VO列表数据
     */
    private List<UserInfoVO> buildRspVO(List<UserInfoPO> req) {
        List<UserInfoVO> rsp = new ArrayList<>();
        if(null != req && req.size() >0){
            rsp = req.stream().map(k->{
                List<Long> roleIdList = Func.getPropList(req, UserInfoPO::getRoleId);
                UserRoleQueryBO userRoleQueryBO = new UserRoleQueryBO();
                userRoleQueryBO.setIdList(roleIdList);
                Map<Long, UserRoleVO> userRoleMap = userRoleGetMapService.getMapGroupById(userRoleQueryBO);
                UserInfoVO vo = k.parseVO(userRoleMap);
                return vo;
            }).collect(Collectors.toList());
        }
        return rsp;
    }
}