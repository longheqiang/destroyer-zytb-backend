package com.destroyer.user.ability.auth;


import com.destroyer.core.entity.userInfo.vo.UserInfoVO;

/**
 * 标题：身份认证详情服务接口
 * 说明：身份认证详情服务接口
 * 时间：2023/9/8
 * 作者：admin
 */
public interface IAuthUserInfoDetailAbilityService {


    /**
     * 获取当前登录的系统用户身份信息
     * @return
     */
    UserInfoVO getAuthUserInfoDetail();


}
