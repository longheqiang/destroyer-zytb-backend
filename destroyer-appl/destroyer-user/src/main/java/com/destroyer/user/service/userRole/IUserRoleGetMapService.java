package com.destroyer.user.service.userRole;

import com.destroyer.core.entity.userRole.bo.UserRoleQueryBO;
import com.destroyer.core.entity.userRole.vo.UserRoleVO;
import java.util.Map;

 /**
 * 标题：用户角色(获取Map集合服务接口)
 * 说明：用户角色(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-2-27
 * 作者：admin
 */
public interface IUserRoleGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, UserRoleVO> getMapGroupById(UserRoleQueryBO req);
}