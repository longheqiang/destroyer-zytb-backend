package com.destroyer.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.destroyer.common.entity.system.Result;
import com.destroyer.system.ability.test.TestExcelAbilityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 标题：TestController
 * 说明：
 * 时间：2023/8/30
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/system/test")
@Api(value = "TestController", tags = "测试API")
public class TestController {


    @Autowired
    private TestExcelAbilityService excelAbilityService;



    @PostMapping(value = "/test1")
    @ApiOperation("模板API")
    public Result<JSONObject> test(@RequestBody JSONObject req) {
        return Result.success(req);
    }

    /**
     * 导出Excel模板
     */
    @GetMapping("/exportExcelTemplate")
    @ApiOperation("导出Excel模板")
    @ResponseBody
    public void exportExcelTemplate(HttpServletResponse response) throws IOException {
        excelAbilityService.exportExcelTemplate(response);

    }

    /**
     * 处理数据后导出
     */
    @GetMapping("/exportExcelData")
    @ApiOperation("处理数据后导出")
    @ResponseBody
    public void exportExcelData(@RequestParam("file") MultipartFile req,HttpServletResponse response) throws IOException {
        excelAbilityService.exportExcelData(req,response);

    }
}
