package com.destroyer.system.service.impl.sysDic;

import com.destroyer.core.entity.sysDic.bo.SysDicQueryBO;
import com.destroyer.core.entity.sysDic.vo.SysDicVO;
import com.destroyer.system.service.sysDic.ISysDicBaseService;
import com.destroyer.system.service.sysDic.ISysDicGetListService;
import com.destroyer.system.service.sysDic.ISysDicGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 标题：系统字典(获取Map集合服务接口)
 * 说明：系统字典(获取Map集合服务实现)，用于查询各种Map集合服务
 * 时间：2023/9/4
 * 作者：admin
 */
@Service
public class SysDicGetMapServiceImpl implements ISysDicGetMapService {

    /**
     * 基础通用服务
     */
    @Autowired
    private ISysDicBaseService baseService;

    @Autowired
    private ISysDicGetListService getListService;


    /**
     * 根据类型分组查询字典map
     * @param req
     * @return
     */
    @Override
    public Map<String, Map<String, String>> getMapGroupByType(SysDicQueryBO req) {
        Map<String, Map<String, String>> rsp = new HashMap<>();
        List<SysDicVO> voList = this.getListService.getList(req);
        Map<String, List<SysDicVO>> listMap =
                voList.stream().collect(
                        Collectors.groupingBy(SysDicVO::getType)
                );
        for (String keySet : listMap.keySet()) {
            List<SysDicVO> dicMapList = listMap.get(keySet);
            Map<String, String> dicMap =
                    dicMapList.stream().collect(
                            Collectors.toMap(
                                    SysDicVO::getVal,
                                    SysDicVO::getTitle,
                                    (key1, key2) -> key2)
                    );
            rsp.put(keySet, dicMap);
        }
        return rsp;
    }
}
