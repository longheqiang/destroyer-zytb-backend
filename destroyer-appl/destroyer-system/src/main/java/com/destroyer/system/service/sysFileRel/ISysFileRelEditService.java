package com.destroyer.system.service.sysFileRel;

import com.destroyer.core.entity.sysFileRel.bo.SysFileRelBO;

 /**
 * 标题：系统业务附件关联(编辑数据服务接口)
 * 说明：系统业务附件关联(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileRelEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(SysFileRelBO req);
}