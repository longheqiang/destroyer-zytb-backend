package com.destroyer.system.service.impl.sysDic;

import com.destroyer.core.entity.sysDic.SysDicPO;
import com.destroyer.core.entity.sysDic.bo.SysDicBO;
import com.destroyer.system.service.sysDic.ISysDicBaseService;
import com.destroyer.system.service.sysDic.ISysDicEditService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：系统字典(编辑数据服务实现)
 * 说明：系统字典(编辑数据服务实现),自定义编辑数据服务实现
 * 时间：2023-9-4
 * 作者：admin
 */
@Service
public class SysDicEditServiceImpl implements ISysDicEditService {


     /**
      * 基础通用服务
      */
    @Autowired
    private ISysDicBaseService baseService;
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    @Override
    public boolean editById(SysDicBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //入参处理
        SysDicPO po = new SysDicPO(req);
        //更新数据
        boolean rsp = this.baseService.updateById(po);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(SysDicBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
	
        if (null == req.getId()) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "更新数据入参ID不能为空");
        }
	
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(SysDicBO req) {
        //执行前调用逻辑
    }
}