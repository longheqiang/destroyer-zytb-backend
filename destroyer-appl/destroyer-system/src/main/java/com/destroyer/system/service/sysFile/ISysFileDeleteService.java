package com.destroyer.system.service.sysFile;

import com.destroyer.core.entity.base.BaseIdListBO;

/**
 * 标题：系统附件信息(删除数据服务接口)
 * 说明：系统附件信息(删除数据服务接口),自定义删除数据服务接口
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileDeleteService {
    /**
     * 删除数据（根据主键ID）
     * @param req
     * @return
     */
    boolean deleteById(Long req);
    
    
    /**
     * 删除数据（根据主键ID集合批量删除）
     * @param req
     * @return
     */
    boolean batchDeleteByIds(BaseIdListBO req);
}