package com.destroyer.system.service.sysConfig;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.sysConfig.bo.SysConfigBO;

import java.util.List;

/**
 * 标题：系统配置(批量新增服务接口)
 * 说明：系统配置(批量新增服务接口),自定义批量新增据服务接口
 * 时间：2023-9-7
 * 作者：admin
 */
public interface ISysConfigBatchCreateService {


    /**
     * 批量新增数据
     * @param req
     * @return 新增数据的组件ID集合
     */
    List<Long> batchCreate(BaseBatchBO<SysConfigBO> req);
}