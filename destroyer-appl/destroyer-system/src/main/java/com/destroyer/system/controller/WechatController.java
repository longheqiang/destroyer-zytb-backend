package com.destroyer.system.controller;

import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.wechat.bo.WechatPrepayBO;
import com.destroyer.core.entity.wechat.bo.WechatUserCodeBO;
import com.destroyer.core.entity.wechat.vo.WechatPrepayWithRequestPaymentVO;
import com.destroyer.core.entity.wechat.vo.WechatUserAuthVO;
import com.destroyer.core.entity.wechat.vo.WechatUserPhoneNumberVO;
import com.destroyer.system.wechat.service.IWechatBaseService;
import com.destroyer.system.wechat.service.IWechatPaymentsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * 标题：WechatController
 * 说明：微信相关API
 * 时间：2024/3/5
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/system/wechat")
@Api(value = "WechatController", tags = "微信相关API")
public class WechatController {

    @Autowired
    private IWechatBaseService wechatService;

    @Autowired
    private IWechatPaymentsService wechatPayService;

    @PostMapping(value = "/getAccessToken")
    @ApiOperation("获取微信授权tokenAPI")
    public Result<String> getAccessToken() {
        String rsp = wechatService.getAccessToken();
        return Result.success(rsp);
    }

    @PostMapping(value = "/getUserOpenIdByCode")
    @ApiOperation("通过code获取用户OpenIdAPI")
    public Result<WechatUserAuthVO> getUserOpenIdByCode(@RequestBody WechatUserCodeBO req) {
        WechatUserAuthVO rsp = wechatService.getUserOpenIdByCode(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/getPhoneNumber")
    @ApiOperation("通过code获取用户手机号API")
    public Result<WechatUserPhoneNumberVO> getPhoneNumber(@RequestBody WechatUserCodeBO req) {
        WechatUserPhoneNumberVO rsp = wechatService.getPhoneNumber(req);
        return Result.success(rsp);
    }

    @PostMapping(value = "/prepay")
    @ApiOperation("微信建立支付预订单API")
    public Result<String> prepay(@RequestBody WechatPrepayBO req) {
        String rsp = wechatPayService.prepay(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/prepayWithRequestPayment")
    @ApiOperation("下单并生成调起支付的参数API")
    public Result<WechatPrepayWithRequestPaymentVO> prepayWithRequestPayment(@RequestBody WechatPrepayBO req) {
        WechatPrepayWithRequestPaymentVO rsp = wechatPayService.prepayWithRequestPayment(req);
        return Result.success(rsp);
    }

    @PostMapping(value = "/prepayWithRequestPaymentByPrepayId")
    @ApiOperation("通过预支付ID生成调起支付的参数API")
    public Result<WechatPrepayWithRequestPaymentVO> prepayWithRequestPaymentByPrepayId(@RequestBody String req) {
        WechatPrepayWithRequestPaymentVO rsp = wechatPayService.prepayWithRequestPaymentByPrepayId(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/paymentNotify")
    @ApiOperation("处理微信支付支付回调API")
    public Result<ResponseEntity.BodyBuilder> paymentNotify(@RequestBody String requestBody) {
        ResponseEntity.BodyBuilder rsp = this.wechatPayService.paymentNotify(requestBody);
        return Result.success(rsp);
    }

}
