package com.destroyer.system.service.sysDic;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.sysDic.bo.SysDicQueryBO;
import com.destroyer.core.entity.sysDic.vo.SysDicVO;

import java.util.List;

 /**
 * 标题：系统字典(获取列表服务接口)
 * 说明：系统字典(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2023-9-4
 * 作者：admin
 */
public interface ISysDicGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<SysDicVO> getList(SysDicQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<SysDicVO> getPageList(SysDicQueryBO req);
}