package com.destroyer.system.service.sysConfig;

import com.destroyer.core.entity.sysConfig.bo.SysConfigQueryBO;
import com.destroyer.core.entity.sysConfig.vo.SysConfigVO;

import java.util.Map;

/**
 * 标题：系统配置(获取Map集合服务接口)
 * 说明：系统配置(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2023-9-7
 * 作者：admin
 */
public interface ISysConfigGetMapService {


    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, SysConfigVO> getMapGroupById(SysConfigQueryBO req);
}