package com.destroyer.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.system.service.sysConfig.*;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.sysConfig.bo.SysConfigBO;
import com.destroyer.core.entity.sysConfig.bo.SysConfigQueryBO;
import com.destroyer.core.entity.sysConfig.vo.SysConfigVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

 /**
 * 标题：系统配置(控制层)
 * 说明：系统配置(控制层),包括所有对外前端API接口
 * 时间：2023-9-4
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/system/sysConfig")
@Api(value = "SysConfigController", tags = "系统配置API")
public class SysConfigController {
    
    
    /**
     * 基础服务
     */
    @Autowired
    private ISysConfigBaseService baseService;
    
    
    /**
     * 新增数据服务
     */
    @Autowired
    private ISysConfigCreateService createService;
    
    
    /**
      * 删除数据服务
      */
    @Autowired
    private ISysConfigDeleteService deleteService;
    
    
    /**
     * 列表服务
     */
    @Autowired
    private ISysConfigGetListService getListService;

     /**
      * 详情服务
      */
     @Autowired
     private ISysConfigGetDetailService getDetailService;



     /**
     * 编辑服务
     */
    @Autowired
    private ISysConfigEditService editService;
    
    
    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody SysConfigBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }


     @PostMapping(value = "/deleteById")
     @ApiOperation("删除数据API(根据主键ID)")
     public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
         boolean rsp = this.deleteService.deleteById(req.getId());
         return Result.success(rsp);
     }

    @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<SysConfigVO>> list(@RequestBody SysConfigQueryBO req) {
        List<SysConfigVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }
    
    
    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<SysConfigVO>> page(@RequestBody SysConfigQueryBO req) {
        Page<SysConfigVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }

     @PostMapping(value = "/detailBy")
     @ApiOperation("根据查询条件查询详情API")
     public Result<SysConfigVO> detailBy(@RequestBody SysConfigQueryBO req) {
         SysConfigVO rsp = this.getDetailService.getDetailBy(req);
         return Result.success(rsp);
     }


     @PostMapping(value = "/detailById")
     @ApiOperation("根据ID查询详情API")
     public Result<SysConfigVO> detailById(@RequestBody BaseIdBO req) {
         SysConfigVO rsp = this.getDetailService.getDetailById(req.getId());
         return Result.success(rsp);
     }

     @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody SysConfigBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }
}