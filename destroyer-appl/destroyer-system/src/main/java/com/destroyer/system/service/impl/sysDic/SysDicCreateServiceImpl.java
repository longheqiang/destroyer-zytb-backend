package com.destroyer.system.service.impl.sysDic;

import com.destroyer.core.entity.sysDic.SysDicPO;
import com.destroyer.core.entity.sysDic.bo.SysDicBO;
import com.destroyer.system.service.sysDic.ISysDicBaseService;
import com.destroyer.system.service.sysDic.ISysDicCreateService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：系统字典(新增服务实现)
 * 说明：系统字典(新增服务实现),自定义新增数据实现
 * 时间：2023-9-4
 * 作者：admin
 */
@Service
public class SysDicCreateServiceImpl implements ISysDicCreateService {

     /**
      * 基础通用服务
      */
    @Autowired
    private ISysDicBaseService baseService;
    
    
    /**
     * 新增数据
     * @param req
     * @return
     */
    @Override
    public Long create(SysDicBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造PO对象(根据业务对象)
        SysDicPO po = new SysDicPO(req);
        //调用基础保存服务
        boolean states = this.baseService.save(po);
        if (!states){
            throw new ServiceException(ResultEnum.FAILURE, "新增数据失败");
        }
        Long rsp = po.getId();
        return rsp;
    }
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(SysDicBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
        }
    }
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(SysDicBO req) {
        //执行前调用逻辑
    }
}