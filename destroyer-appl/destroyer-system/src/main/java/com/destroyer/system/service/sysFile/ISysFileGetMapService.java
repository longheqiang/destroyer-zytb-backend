package com.destroyer.system.service.sysFile;

import com.destroyer.core.entity.sysFile.bo.SysFileQueryBO;
import com.destroyer.core.entity.sysFile.vo.SysFileVO;
import java.util.Map;

 /**
 * 标题：系统附件信息(获取Map集合服务接口)
 * 说明：系统附件信息(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, SysFileVO> getMapGroupById(SysFileQueryBO req);
}