package com.destroyer.system.service.impl.sysFileRel;

import com.destroyer.system.service.sysFileRel.ISysFileRelGetDetailService;
import com.destroyer.system.service.sysFileRel.ISysFileRelGetListService;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelQueryBO;
import com.destroyer.core.entity.sysFileRel.vo.SysFileRelVO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

 /**
 * 标题：系统业务附件关联(查询详情服务实现)
 * 说明：系统业务附件关联(查询详情服务实现),自定义基础详情查询接口
 * 时间：2023-10-11
 * 作者：admin
 */
@Service
public class SysFileRelGetDetailServiceImpl implements ISysFileRelGetDetailService {
    
    
    /**
     * 查询列表服务
     */
    @Autowired
    private ISysFileRelGetListService getListService;
    /**
     * 根据条件查询详情
     * @param req
     * @return
     */
    @Override
    public SysFileRelVO getDetailBy(SysFileRelQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<SysFileRelVO> list = getListService.getList(req);
        if (list.size() > 1) {
            throw new ServiceException(ResultEnum.FAILURE,"查询到的数据不唯一！");
        }
        SysFileRelVO rsp = list.size() == 1? list.get(0):null;
        return rsp;
    }

     /**
      * 根据主键ID查询详情
      * @param req
      * @return
      */
     @Override
     public SysFileRelVO getDetailById(Long req) {
         if (null == req) {
             throw new ServiceException(ResultEnum.PARAM_MISS,"查询详情入参ID不能为空");
         }
         SysFileRelQueryBO queryBO = new SysFileRelQueryBO();
         queryBO.setId(req);
         SysFileRelVO rsp = this.getDetailBy(queryBO);
         return rsp;
     }

    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(SysFileRelQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        } else {
        }
    }
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(SysFileRelQueryBO req) {
        //执行前调用逻辑
    }
}