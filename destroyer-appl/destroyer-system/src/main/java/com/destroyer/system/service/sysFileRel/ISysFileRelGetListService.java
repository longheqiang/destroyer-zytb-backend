package com.destroyer.system.service.sysFileRel;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelQueryBO;
import com.destroyer.core.entity.sysFileRel.vo.SysFileRelVO;
import java.util.List;

 /**
 * 标题：系统业务附件关联(获取列表服务接口)
 * 说明：系统业务附件关联(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileRelGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<SysFileRelVO> getList(SysFileRelQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<SysFileRelVO> getPageList(SysFileRelQueryBO req);
}