package com.destroyer.system.service.sysConfig;

import com.destroyer.core.entity.sysConfig.bo.SysConfigBO;

 /**
 * 标题：系统配置(编辑数据服务接口)
 * 说明：系统配置(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2023-9-4
 * 作者：admin
 */
public interface ISysConfigEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(SysConfigBO req);
}