package com.destroyer.system.service.sysFileRel;

import com.destroyer.core.entity.sysFileRel.bo.SysFileRelBO;

 /**
 * 标题：系统业务附件关联(新增服务接口)
 * 说明：系统业务附件关联(新增服务接口),自定义新增数据接口
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileRelCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(SysFileRelBO req);
}