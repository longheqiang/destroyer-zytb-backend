package com.destroyer.system.ability.impl.sysFile;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.system.service.sysFile.ISysFileCreateService;
import com.destroyer.system.service.sysFileRel.ISysFileRelCreateService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.sysFile.bo.SysFileBO;
import com.destroyer.core.entity.sysFile.bo.SysFileCreateBO;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelBO;
import com.destroyer.system.ability.sysFile.SysFileCreateAndBuildRelAbilityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 标题：SysFileCreateAndRelAbilityServiceImpl
 * 说明：
 * 时间：2023/10/18
 * 作者：admin
 */
@Service
public class SysFileCreateAndBuildRelAbilityServiceImpl implements SysFileCreateAndBuildRelAbilityService {


    @Autowired
    private ISysFileCreateService createService;

    @Autowired
    private ISysFileRelCreateService sysFileRelCreateService;

    /**
     * 系统附件新建并建立业务关联
     * @param req
     * @return
     */
    @Transactional
    @Override
    public List<Long> createAndBuildRel(BaseBatchBO<SysFileCreateBO> req) {
        List<Long> rsp = new ArrayList<>();
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<SysFileCreateBO> list = req.getList();
        for (SysFileCreateBO sysFileCreateBO:list){
            //新增附件
            SysFileBO sysFileBO = new SysFileBO();
            BeanUtils.copyProperties(sysFileCreateBO, sysFileBO);
            Long fileId = createService.create(sysFileBO);
            //新增关联
            SysFileRelBO sysFileRelBO = new SysFileRelBO();
            BeanUtils.copyProperties(sysFileCreateBO, sysFileRelBO);
            sysFileRelBO.setFileId(fileId);
            sysFileRelCreateService.create(sysFileRelBO);
            rsp.add(fileId);
        }
        return rsp;

    }


    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(BaseBatchBO<SysFileCreateBO> req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
            List<SysFileCreateBO> list = req.getList();
            if (null == list || list.size() == 0){
                throw new ServiceException(ResultEnum.PARAM_MISS, "新增附件，入参[附件集合]不能为空！");
            }
            for (int i = 0; i < list.size(); i++) {
                SysFileCreateBO one = list.get(i);
                if(StringUtils.isBlank(one.getName())){
                    throw new ServiceException(ResultEnum.PARAM_MISS, "新增附件，附件"+i+"，[名称]不能为空！");

                }
                if(StringUtils.isBlank(one.getFileType())){
                    throw new ServiceException(ResultEnum.PARAM_MISS, "新增附件，附件"+i+"，[文件类型]不能为空！");

                }
                if(StringUtils.isBlank(one.getBizType())){
                    throw new ServiceException(ResultEnum.PARAM_MISS, "新增附件，附件"+i+"，[业务类型]不能为空！");

                }
                if(StringUtils.isBlank(one.getUrl())){
                    throw new ServiceException(ResultEnum.PARAM_MISS, "新增附件，附件"+i+"，[URL]不能为空！");

                }
                if(null == one.getBizId()){
                    throw new ServiceException(ResultEnum.PARAM_MISS, "新增附件，附件"+i+"，[业务ID]不能为空！");

                }
            }
        }
    }

    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(BaseBatchBO<SysFileCreateBO> req) {
        //执行前调用逻辑

    }
}
