package com.destroyer.system.service.sysConfig;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.sysConfig.SysConfigPO;

 /**
 * 标题：系统配置(基础通用服务接口)
 * 说明：系统配置(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2023-9-4
 * 作者：admin
 */
public interface ISysConfigBaseService extends IService<SysConfigPO> {
}