package com.destroyer.system.service.sysFile;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.sysFile.bo.SysFileQueryBO;
import com.destroyer.core.entity.sysFile.vo.SysFileVO;
import java.util.List;

 /**
 * 标题：系统附件信息(获取列表服务接口)
 * 说明：系统附件信息(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<SysFileVO> getList(SysFileQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<SysFileVO> getPageList(SysFileQueryBO req);
}