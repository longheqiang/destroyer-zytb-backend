package com.destroyer.system.service.impl.sysFile;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.system.service.sysFile.ISysFileBaseService;
import com.destroyer.core.entity.sysFile.SysFilePO;
import com.destroyer.system.mapper.SysFileMapper;
import org.springframework.stereotype.Service;

 /**
 * 标题：系统附件信息(基础通用服务实现)
 * 说明：系统附件信息(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2023-10-11
 * 作者：admin
 */
@Service
public class  SysFileBaseServiceImpl extends ServiceImpl<SysFileMapper, SysFilePO> implements ISysFileBaseService {
}