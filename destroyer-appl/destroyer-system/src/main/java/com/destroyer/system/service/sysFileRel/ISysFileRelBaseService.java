package com.destroyer.system.service.sysFileRel;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.sysFileRel.SysFileRelPO;

 /**
 * 标题：系统业务附件关联(基础通用服务接口)
 * 说明：系统业务附件关联(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileRelBaseService extends IService<SysFileRelPO> {
}