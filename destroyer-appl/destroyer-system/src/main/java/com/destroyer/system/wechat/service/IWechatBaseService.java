package com.destroyer.system.wechat.service;

import com.destroyer.core.entity.wechat.bo.WechatUserCodeBO;
import com.destroyer.core.entity.wechat.vo.WechatUserAuthVO;
import com.destroyer.core.entity.wechat.vo.WechatUserPhoneNumberVO;

/**
 * 标题：IWechatBaseService
 * 说明：微信相关基础服务接口
 * 时间：2024/3/4
 * 作者：admin
 */
public interface IWechatBaseService {




    /**
     * 获取微信授权token
     * @return
     */
    String getAccessToken();


    /**
     * 通过code获取用户OpenId
     * @param req
     * @return
     */
    WechatUserAuthVO getUserOpenIdByCode(WechatUserCodeBO req);





    /**
     * 通过code获取用户手机号
     * @param req
     * @return
     */
    WechatUserPhoneNumberVO getPhoneNumber(WechatUserCodeBO req);









}
