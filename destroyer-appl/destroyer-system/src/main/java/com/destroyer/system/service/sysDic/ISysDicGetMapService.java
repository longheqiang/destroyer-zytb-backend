package com.destroyer.system.service.sysDic;

import com.destroyer.core.entity.sysDic.bo.SysDicQueryBO;

import java.util.Map;

/**
 * 标题：系统字典(获取Map集合服务接口)
 * 说明：系统字典(获取Map集合服务接口)，用于查询各种Map集合服务
 * 时间：2023/9/4
 * 作者：admin
 */
public interface ISysDicGetMapService {

    /**
     * 根据类型分组查询字典map
     * @param req
     * @return
     */
    Map<String, Map<String, String>> getMapGroupByType(SysDicQueryBO req);
}
