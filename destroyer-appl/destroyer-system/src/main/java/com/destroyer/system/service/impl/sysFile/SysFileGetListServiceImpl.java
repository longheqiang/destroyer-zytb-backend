package com.destroyer.system.service.impl.sysFile;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.util.PageUtils;
import com.destroyer.system.service.sysFile.ISysFileBaseService;
import com.destroyer.system.service.sysFile.ISysFileGetListService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.sysFile.SysFilePO;
import com.destroyer.core.entity.sysFile.bo.SysFileQueryBO;
import com.destroyer.core.entity.sysFile.vo.SysFileVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 标题：系统附件信息(获取列表服务实现)
 * 说明：系统附件信息(获取列表服务实现),自定义基础列表查询,基础分页列表查询服务实现
 * 时间：2023-10-11
 * 作者：admin
 */
@Service
public class SysFileGetListServiceImpl implements ISysFileGetListService {


    /**
     * 基础通用服务
     */
    @Autowired
    private ISysFileBaseService baseService;


    /**
     * 获取列表(根据QueryBO)
     *
     * @param req
     * @return
     */
    @Override
    public List<SysFileVO> getList(SysFileQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<SysFilePO> poList = this.baseService.list(this.buildQueryWrapper(req));
        List<SysFileVO> rsp = this.buildRspVO(poList);
        return rsp;
    }


    /**
     * 获取分页列表(根据QueryBO)
     *
     * @param req
     * @return
     */
    @Override
    public Page<SysFileVO> getPageList(SysFileQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造分页入参对象
        Page<SysFilePO> page = req.buildPage();
        //分页查询列表数据
        Page<SysFilePO> poPage = this.baseService.page(page, this.buildQueryWrapper(req));
        //构造出参
        Page<SysFileVO> rsp = PageUtils.poPageToVoPage(poPage, SysFileVO.class);
        List<SysFileVO> voList = this.buildRspVO(poPage.getRecords());
        rsp.setRecords(voList);
        return rsp;
    }


    /**
     * 类通用入参校验
     *
     * @param req 入参
     */
    private void validParams(SysFileQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
        }
    }


    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(SysFileQueryBO req) {
        //执行前调用逻辑
    }


    /**
     * 构造QueryWrapper查询入参
     *
     * @param req 查询业务对象
     * @return QueryWrapper对象
     */
    private LambdaQueryWrapper<SysFilePO> buildQueryWrapper(SysFileQueryBO req) {
        LambdaQueryWrapper<SysFilePO> rsp = Wrappers.lambdaQuery();
        rsp.in(null != req.getIdList() && req.getIdList().size() > 0, SysFilePO::getId, req.getIdList());
        rsp.eq(null != req.getId(), SysFilePO::getId, req.getId());
        rsp.like(StringUtils.isNotBlank(req.getName()), SysFilePO::getName, req.getName());
        rsp.like(StringUtils.isNotBlank(req.getFileType()), SysFilePO::getFileType, req.getFileType());
        rsp.like(StringUtils.isNotBlank(req.getUrl()), SysFilePO::getUrl, req.getUrl());
        rsp.like(StringUtils.isNotBlank(req.getFileSize()), SysFilePO::getFileSize, req.getSize());
        rsp.eq(null != req.getCreateTime(), SysFilePO::getCreateTime, req.getCreateTime());
        rsp.ge(null != req.getCreateTimeStart(), SysFilePO::getCreateTime, req.getCreateTimeStart());
        rsp.le(null != req.getCreateTimeEnd(), SysFilePO::getCreateTime, req.getCreateTimeEnd());
        rsp.eq(null != req.getUpdateTime(), SysFilePO::getUpdateTime, req.getUpdateTime());
        rsp.ge(null != req.getUpdateTimeStart(), SysFilePO::getUpdateTime, req.getUpdateTimeStart());
        rsp.le(null != req.getUpdateTimeEnd(), SysFilePO::getUpdateTime, req.getUpdateTimeEnd());
        rsp.eq(null != req.getCreateUser(), SysFilePO::getCreateUser, req.getCreateUser());
        rsp.eq(null != req.getUpdateUser(), SysFilePO::getUpdateUser, req.getUpdateUser());
        rsp.orderByDesc(SysFilePO::getCreateTime);
        return rsp;
    }


    /**
     * 构造响应视图对象列表
     *
     * @param req PO列表数据
     * @return VO列表数据
     */
    private List<SysFileVO> buildRspVO(List<SysFilePO> req) {
        List<SysFileVO> rsp = new ArrayList<>();
        if (null != req && req.size() > 0) {
            rsp = req.stream().map(k -> {
                SysFileVO vo = k.parseVO();
                return vo;
            }).collect(Collectors.toList());
        }
        return rsp;
    }
}