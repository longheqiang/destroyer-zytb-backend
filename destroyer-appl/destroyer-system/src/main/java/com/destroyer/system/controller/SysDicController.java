package com.destroyer.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.system.service.sysDic.*;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.sysDic.bo.SysDicBO;
import com.destroyer.core.entity.sysDic.bo.SysDicQueryBO;
import com.destroyer.core.entity.sysDic.vo.SysDicVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 标题：系统字典(控制层)
 * 说明：系统字典(控制层),包括所有对外前端API接口
 * 时间：2023-9-4
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/system/sysDic")
@Api(value = "SysDicController", tags = "系统字典API")
public class SysDicController {


    /**
     * 基础服务
     */
    @Autowired
    private ISysDicBaseService baseService;


    /**
     * 新增数据服务
     */
    @Autowired
    private ISysDicCreateService createService;


    /**
     * 删除数据服务
     */
    @Autowired
    private ISysDicDeleteService deleteService;


    /**
     * 列表服务
     */
    @Autowired
    private ISysDicGetListService getListService;

    /**
     * 查询Map服务
     */
    @Autowired
    private ISysDicGetMapService getMapService;

    /**
     * 详情服务
     */
    @Autowired
    private ISysDicGetDetailService getDetailService;

    /**
     * 常量服务
     */
    @Autowired
    private ISysDicConstantService constantService;


    /**
     * 编辑服务
     */
    @Autowired
    private ISysDicEditService editService;


    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody SysDicBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/deleteById")
    @ApiOperation("删除数据API(根据主键ID)")
    public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
        boolean rsp = this.deleteService.deleteById(req.getId());
        return Result.success(rsp);
    }

    @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/deleteBy")
    @ApiOperation("删除数据API(根据查询条件删除)")
    public Result<Boolean> deleteBy(@RequestBody SysDicQueryBO req) {
        boolean rsp = this.deleteService.batchDeleteBy(req);
        return Result.success(rsp);
    }

    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<SysDicVO>> list(@RequestBody SysDicQueryBO req) {
        List<SysDicVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<SysDicVO>> page(@RequestBody SysDicQueryBO req) {
        Page<SysDicVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<SysDicVO> detailBy(@RequestBody SysDicQueryBO req) {
        SysDicVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailById")
    @ApiOperation("根据ID查询详情API")
    public Result<SysDicVO> detailById(@RequestBody BaseIdBO req) {
        SysDicVO rsp = this.getDetailService.getDetailById(req.getId());
        return Result.success(rsp);
    }

    @PostMapping(value = "/queryMapGroupByType")
    @ApiOperation("根据类型分组查询字典mapAPI")
    public Result<Map<String, Map<String, String>>> queryMapGroupByType(@RequestBody SysDicQueryBO req) {
        Map<String, Map<String, String>> rsp = this.getMapService.getMapGroupByType(req);
        return Result.success(rsp);
    }

    @PostMapping(value = "/constant")
    @ApiOperation("查询常量字符串API")
    public Result<String> constant() {
        String rsp = this.constantService.getConstant();
        return Result.success(rsp);
    }



    @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody SysDicBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }
}