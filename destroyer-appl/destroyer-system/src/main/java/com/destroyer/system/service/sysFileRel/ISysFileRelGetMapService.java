package com.destroyer.system.service.sysFileRel;

import com.destroyer.core.entity.sysFileRel.bo.SysFileRelQueryBO;
import com.destroyer.core.entity.sysFileRel.vo.SysFileRelVO;

import java.util.List;
import java.util.Map;

 /**
 * 标题：系统业务附件关联(获取Map集合服务接口)
 * 说明：系统业务附件关联(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileRelGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, SysFileRelVO> getMapGroupById(SysFileRelQueryBO req);


     /**
      * 根据业务ID和业务类型分组查询map集合
      * @param req
      * @return
      */
     Map<Long, Map<String, List<SysFileRelVO>>> getMapGroupByBizIdAndBizType(SysFileRelQueryBO req);
}