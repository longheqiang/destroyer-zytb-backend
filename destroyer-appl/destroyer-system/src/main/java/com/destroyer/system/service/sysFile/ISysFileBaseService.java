package com.destroyer.system.service.sysFile;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.sysFile.SysFilePO;

 /**
 * 标题：系统附件信息(基础通用服务接口)
 * 说明：系统附件信息(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileBaseService extends IService<SysFilePO> {
}