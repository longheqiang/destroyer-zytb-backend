package com.destroyer.system.ability.test;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 标题：importByExcelAbilityService
 * 说明：
 * 时间：2023/10/26
 * 作者：admin
 */
public interface TestExcelAbilityService {



    boolean importExcel(MultipartFile req) throws IOException;


    boolean exportExcelTemplate( HttpServletResponse response) throws IOException;

    boolean exportExcelData(MultipartFile req, HttpServletResponse response) throws IOException;

}
