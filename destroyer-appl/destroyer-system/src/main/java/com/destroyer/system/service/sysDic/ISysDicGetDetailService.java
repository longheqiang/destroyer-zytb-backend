package com.destroyer.system.service.sysDic;

import com.destroyer.core.entity.sysDic.bo.SysDicQueryBO;
import com.destroyer.core.entity.sysDic.vo.SysDicVO;

/**
 * 标题：系统字典(查询详情服务接口)
 * 说明：系统字典(查询详情服务接口),自定义基础详情查询接口
 * 时间：2023-9-6
 * 作者：admin
 */
public interface ISysDicGetDetailService {
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    SysDicVO getDetailBy(SysDicQueryBO req);


    /**
     * 根据主键ID查询详情
     * @param req
     * @return
     */
    SysDicVO getDetailById(Long req);
}