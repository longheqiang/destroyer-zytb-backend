package com.destroyer.system.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("阿里云地址请求参数")
public class SignQuery {
    @ApiModelProperty("文件名")
    private String fileName;

    @ApiModelProperty("文件类型")
    private String mime;
}
