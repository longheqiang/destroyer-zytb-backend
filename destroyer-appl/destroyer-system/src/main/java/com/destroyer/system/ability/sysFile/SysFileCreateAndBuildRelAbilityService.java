package com.destroyer.system.ability.sysFile;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.sysFile.bo.SysFileCreateBO;

import java.util.List;

/**
 * 标题：SysFileCreateAndRelAbilityService
 * 说明：系统附件新建并建立业务关联能力服务接口
 * 时间：2023/10/18
 * 作者：admin
 */
public interface SysFileCreateAndBuildRelAbilityService {


    /**
     * 系统附件新建并建立业务关联
     * @param req
     * @return
     */
    List<Long> createAndBuildRel(BaseBatchBO<SysFileCreateBO> req);
}
