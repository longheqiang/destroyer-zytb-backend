package com.destroyer.system.ability.sysFileRel;

import com.destroyer.core.entity.sysFileRel.bo.SysFileRelDeleteByBizBO;

/**
 * 标题：SysFileRelDeleteByBizTypeAndBizIdAbilityService
 * 说明：
 * 时间：2023/10/19
 * 作者：admin
 */
public interface ISysFileRelDeleteByBizTypeAndBizIdAbilityService {


    boolean DeleteByBizTypeAndBizId(SysFileRelDeleteByBizBO req);

}
