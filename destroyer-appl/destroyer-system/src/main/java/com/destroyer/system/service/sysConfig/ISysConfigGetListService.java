package com.destroyer.system.service.sysConfig;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.sysConfig.bo.SysConfigQueryBO;
import com.destroyer.core.entity.sysConfig.vo.SysConfigVO;

import java.util.List;

 /**
 * 标题：系统配置(获取列表服务接口)
 * 说明：系统配置(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2023-9-4
 * 作者：admin
 */
public interface ISysConfigGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<SysConfigVO> getList(SysConfigQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<SysConfigVO> getPageList(SysConfigQueryBO req);
}