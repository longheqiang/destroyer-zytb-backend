package com.destroyer.system.mapper;

import com.destroyer.core.entity.sysFile.SysFilePO;
import com.destroyer.core.entity.sysFile.bo.SysFileQueryBO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

 /**
 * 标题：系统附件信息(持久化Mapper)
 * 说明：系统附件信息(持久化Mapper),默认继承MP的BaseMapper<PO>,特殊业务场景可自定义扩展
 * 时间：2023-10-11
 * 作者：admin
 */
@Mapper
public interface SysFileMapper extends BaseMapper<SysFilePO> {
    
    
        /**
     * 查询树结构数据新数据编号
     *
     * @param noh        头编号，首节点传0
     * @param length     长度
     * @param step       步长
     * @param columnName 列名称
     * @param exMap QueryBO扩展参数          
     * @return
     */
    String getTreeNo(@Param("noh") String noh, @Param("length") int length, @Param("step") int step, @Param("columnName") String columnName, @Param("exMap") SysFileQueryBO exMap);
}