package com.destroyer.system.service.sysFileRel;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelBO;
import java.util.List;

 /**
 * 标题：系统业务附件关联(批量新增服务接口)
 * 说明：系统业务附件关联(批量新增服务接口),自定义批量新增据服务接口
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileRelBatchCreateService {
    
    
    /**
    * 批量新增数据
    * @param req
    * @return 新增数据的组件ID集合
    */
    List<Long> batchCreate(BaseBatchBO<SysFileRelBO> req);
}