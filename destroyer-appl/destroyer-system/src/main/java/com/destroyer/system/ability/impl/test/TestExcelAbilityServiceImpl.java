package com.destroyer.system.ability.impl.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.excel.EasyExcelUtil;
import com.destroyer.common.excel.MyReadListener;
import com.destroyer.common.excel.ReadBack;
import com.destroyer.common.exception.ServerDataException;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.DateUtils;
import com.destroyer.core.entity.test.TestExcelBO;
import com.destroyer.core.util.ResponseUtil;
import com.destroyer.system.ability.test.TestExcelAbilityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 标题：SandWareExcelAbilityServiceImpl
 * 说明：
 * 时间：2023/11/6
 * 作者：admin
 */
@Service
public class TestExcelAbilityServiceImpl implements TestExcelAbilityService {




    private final static String SHEET_NAME = "sheet1";


    @Override
    public boolean importExcel(MultipartFile req) throws IOException {
        boolean rsp = false;
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        final List<TestExcelBO> excelList = new ArrayList<>();
        EasyExcel.read(req.getInputStream(), TestExcelBO.class,
                new MyReadListener<>((ReadBack<TestExcelBO>) excelList::add)).sheet(SHEET_NAME).doRead();
        this.validExcelData(excelList);
        return rsp;
    }

    @Override
    public boolean exportExcelTemplate(HttpServletResponse response) throws IOException {
        List<TestExcelBO> rsp = new ArrayList<>();
        TestExcelBO testExcelBO = new TestExcelBO(true);
        rsp.add(testExcelBO);
        ResponseUtil.setExcelResponse(response,"模版文件");
        EasyExcelUtil.getExcelWriterBuilder(response.getOutputStream(), TestExcelBO.class).sheet(SHEET_NAME).doWrite(rsp);
        return false;
    }


    @Override
    public boolean exportExcelData(MultipartFile req, HttpServletResponse response) throws IOException {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        final List<TestExcelBO> excelBOList = new ArrayList<>();
        EasyExcel.read(req.getInputStream(), TestExcelBO.class,
                new MyReadListener<>((ReadBack<TestExcelBO>) excelBOList::add)).sheet(SHEET_NAME).doRead();
        this.validExcelData(excelBOList);
        List<TestExcelBO> rsp = this.handleExcelData(excelBOList);
        String name = req.getName();
        ResponseUtil.setExcelResponse(response,name);
        EasyExcelUtil.getExcelWriterBuilder(response.getOutputStream(), TestExcelBO.class).sheet(SHEET_NAME).doWrite(rsp);
        return false;
    }


    private void validExcelData(List<TestExcelBO> req) {
        if (null == req || req.size() == 0) {
            throw new ServerDataException("excel 解析失败");
        } else {

        }
    }


    private List<TestExcelBO> handleExcelData(List<TestExcelBO> req) {
        List<TestExcelBO> rsp = new ArrayList<>();
        for (TestExcelBO testExcelBO:req){
            Integer stayPeriod = testExcelBO.getStayPeriod();
            if(null != stayPeriod && stayPeriod >1){
                //开始日期
                String startFrom = testExcelBO.getStartFrom();
                for(int i = 0;i < stayPeriod;i++){
                    TestExcelBO testExcelBOSplit = new TestExcelBO();
                    BeanUtils.copyProperties(testExcelBO,testExcelBOSplit);
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");
                    // 解析字符串为LocalDate对象
                    LocalDate originalDate = LocalDate.parse(startFrom, formatter);
                    // 为日期增加一天
                    LocalDate nextDay = originalDate.plusDays(i);
                    // 格式化为新的日期字符串
                    String nextDayStr = nextDay.format(formatter);
                    testExcelBOSplit.setStartFrom(nextDayStr);
                    testExcelBOSplit.setEndAt(nextDayStr);
                    testExcelBOSplit.setStayPeriodSplit(1);
                    rsp.add(testExcelBOSplit);
                }
            }else {
                rsp.add(testExcelBO);
            }
        }

        return rsp;
    }

    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(MultipartFile req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {

        }
    }



    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(MultipartFile req) {
        // TODO 执行前调用逻辑

    }
}
