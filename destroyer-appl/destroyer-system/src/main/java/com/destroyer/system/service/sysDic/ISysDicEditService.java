package com.destroyer.system.service.sysDic;

import com.destroyer.core.entity.sysDic.bo.SysDicBO;

 /**
 * 标题：系统字典(编辑数据服务接口)
 * 说明：系统字典(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2023-9-4
 * 作者：admin
 */
public interface ISysDicEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(SysDicBO req);
}