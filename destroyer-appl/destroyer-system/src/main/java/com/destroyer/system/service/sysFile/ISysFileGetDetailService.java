package com.destroyer.system.service.sysFile;

import com.destroyer.core.entity.sysFile.bo.SysFileQueryBO;
import com.destroyer.core.entity.sysFile.vo.SysFileVO;

/**
 * 标题：系统附件信息(查询详情服务接口)
 * 说明：系统附件信息(查询详情服务接口),自定义基础详情查询接口
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileGetDetailService {
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    SysFileVO getDetailBy(SysFileQueryBO req);


    /**
     * 根据主键ID查询详情
     * @param req
     * @return
     */
    SysFileVO getDetailById(Long req);
}