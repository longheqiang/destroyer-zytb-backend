package com.destroyer.system.service.sysFile;

import com.destroyer.core.entity.sysFile.bo.SysFileBO;

 /**
 * 标题：系统附件信息(新增服务接口)
 * 说明：系统附件信息(新增服务接口),自定义新增数据接口
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(SysFileBO req);
}