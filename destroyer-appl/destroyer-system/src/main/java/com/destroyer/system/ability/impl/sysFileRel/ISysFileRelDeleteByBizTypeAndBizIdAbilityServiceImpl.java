package com.destroyer.system.ability.impl.sysFileRel;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.destroyer.system.ability.sysFileRel.ISysFileRelDeleteByBizTypeAndBizIdAbilityService;
import com.destroyer.system.service.sysFileRel.ISysFileRelBaseService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.sysFileRel.SysFileRelPO;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelDeleteByBizBO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 标题：SysFileRelDeleteByBizTypeAndBizIdAbilityServiceImpl
 * 说明：
 * 时间：2023/10/19
 * 作者：admin
 */
@Service
public class ISysFileRelDeleteByBizTypeAndBizIdAbilityServiceImpl implements ISysFileRelDeleteByBizTypeAndBizIdAbilityService {


    @Autowired
    private ISysFileRelBaseService baseService;

    /**
     * 根据业务类型和业务ID删除数据
     *
     * @param req
     * @return
     */
    @Override
    public boolean DeleteByBizTypeAndBizId(SysFileRelDeleteByBizBO req) {
        boolean rsp = false;
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        LambdaQueryWrapper<SysFileRelPO> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(StringUtils.isNotBlank(req.getBizType()),SysFileRelPO::getBizType,req.getBizType());
        wrapper.in(null != req.getBizTypeList() && req.getBizTypeList().size() > 0, SysFileRelPO::getBizType, req.getBizTypeList());
        wrapper.eq(SysFileRelPO::getBizId,req.getBizId());
        rsp =  baseService.remove(wrapper);
        return rsp;
    }


    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(SysFileRelDeleteByBizBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
            if(StringUtils.isBlank(req.getBizType()) && (null == req.getBizTypeList() || req.getBizTypeList().size() == 0)){
                throw new ServiceException(ResultEnum.PARAM_MISS, "入参[业务类型]不能为空");

            }
            if(null == req.getBizId()){
                throw new ServiceException(ResultEnum.PARAM_MISS, "入参[业务ID]不能为空");

            }
        }
    }

    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(SysFileRelDeleteByBizBO req) {
        //执行前调用逻辑

    }
}
