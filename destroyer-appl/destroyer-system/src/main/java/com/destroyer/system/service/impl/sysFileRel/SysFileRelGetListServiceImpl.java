package com.destroyer.system.service.impl.sysFileRel;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.util.PageUtils;
import com.destroyer.system.service.sysFile.ISysFileGetMapService;
import com.destroyer.system.service.sysFileRel.ISysFileRelBaseService;
import com.destroyer.system.service.sysFileRel.ISysFileRelGetListService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.sysFile.bo.SysFileQueryBO;
import com.destroyer.core.entity.sysFile.vo.SysFileVO;
import com.destroyer.core.entity.sysFileRel.SysFileRelPO;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelQueryBO;
import com.destroyer.core.entity.sysFileRel.vo.SysFileRelVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 标题：系统业务附件关联(获取列表服务实现)
 * 说明：系统业务附件关联(获取列表服务实现),自定义基础列表查询,基础分页列表查询服务实现
 * 时间：2023-10-11
 * 作者：admin
 */
@Service
public class SysFileRelGetListServiceImpl implements ISysFileRelGetListService {


    /**
     * 基础通用服务
     */
    @Autowired
    private ISysFileRelBaseService baseService;

    @Autowired
    private ISysFileGetMapService sysFileGetMapService;



    /**
     * 获取列表(根据QueryBO)
     *
     * @param req
     * @return
     */
    @Override
    public List<SysFileRelVO> getList(SysFileRelQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<SysFileRelPO> poList = this.baseService.list(this.buildQueryWrapper(req));
        List<SysFileRelVO> rsp = this.buildRspVO(poList);
        return rsp;
    }


    /**
     * 获取分页列表(根据QueryBO)
     *
     * @param req
     * @return
     */
    @Override
    public Page<SysFileRelVO> getPageList(SysFileRelQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造分页入参对象
        Page<SysFileRelPO> page = req.buildPage();
        //分页查询列表数据
        Page<SysFileRelPO> poPage = this.baseService.page(page, this.buildQueryWrapper(req));
        //构造出参
        Page<SysFileRelVO> rsp = PageUtils.poPageToVoPage(poPage, SysFileRelVO.class);
        List<SysFileRelVO> voList = this.buildRspVO(poPage.getRecords());
        rsp.setRecords(voList);
        return rsp;
    }


    /**
     * 类通用入参校验
     *
     * @param req 入参
     */
    private void validParams(SysFileRelQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
        }
    }


    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(SysFileRelQueryBO req) {
        //执行前调用逻辑
    }


    /**
     * 构造QueryWrapper查询入参
     *
     * @param req 查询业务对象
     * @return QueryWrapper对象
     */
    private LambdaQueryWrapper<SysFileRelPO> buildQueryWrapper(SysFileRelQueryBO req) {
        LambdaQueryWrapper<SysFileRelPO> rsp = Wrappers.lambdaQuery();
        rsp.in(null != req.getIdList() && req.getIdList().size() > 0, SysFileRelPO::getId, req.getIdList());
        rsp.eq(null != req.getId(), SysFileRelPO::getId, req.getId());
        rsp.eq(StringUtils.isNotBlank(req.getBizType()), SysFileRelPO::getBizType, req.getBizType());
        rsp.in(null != req.getBizTypeList() && req.getBizTypeList().size() > 0, SysFileRelPO::getBizType, req.getBizTypeList());
        rsp.eq(null != req.getFileId(), SysFileRelPO::getFileId, req.getFileId());
        rsp.eq(null != req.getBizId(), SysFileRelPO::getBizId, req.getBizId());
        rsp.in(null != req.getBizIdList() && req.getBizIdList().size() > 0, SysFileRelPO::getBizId, req.getBizIdList());
        rsp.eq(null != req.getCreateTime(), SysFileRelPO::getCreateTime, req.getCreateTime());
        rsp.ge(null != req.getCreateTimeStart(), SysFileRelPO::getCreateTime, req.getCreateTimeStart());
        rsp.le(null != req.getCreateTimeEnd(), SysFileRelPO::getCreateTime, req.getCreateTimeEnd());
        rsp.eq(null != req.getUpdateTime(), SysFileRelPO::getUpdateTime, req.getUpdateTime());
        rsp.ge(null != req.getUpdateTimeStart(), SysFileRelPO::getUpdateTime, req.getUpdateTimeStart());
        rsp.le(null != req.getUpdateTimeEnd(), SysFileRelPO::getUpdateTime, req.getUpdateTimeEnd());
        rsp.eq(null != req.getCreateUser(), SysFileRelPO::getCreateUser, req.getCreateUser());
        rsp.eq(null != req.getUpdateUser(), SysFileRelPO::getUpdateUser, req.getUpdateUser());
        rsp.orderByDesc(SysFileRelPO::getCreateTime);
        return rsp;
    }


    /**
     * 构造响应视图对象列表
     *
     * @param req PO列表数据
     * @return VO列表数据
     */
    private List<SysFileRelVO> buildRspVO(List<SysFileRelPO> req) {
        List<SysFileRelVO> rsp = new ArrayList<>();
        if (null != req && req.size() > 0) {
            SysFileQueryBO sysFileQueryBO = new SysFileQueryBO();
            List<Long> fileIdList = req.stream().map(SysFileRelPO::getFileId).collect(Collectors.toList());
            sysFileQueryBO.setIdList(fileIdList);
            Map<Long, SysFileVO> sysFileMap = sysFileGetMapService.getMapGroupById(sysFileQueryBO);
            rsp = req.stream().map(k -> {
                SysFileRelVO vo = k.parseVO(sysFileMap);
                return vo;
            }).collect(Collectors.toList());
        }
        return rsp;
    }
}