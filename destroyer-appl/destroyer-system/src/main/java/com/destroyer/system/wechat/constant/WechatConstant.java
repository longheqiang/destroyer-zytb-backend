package com.destroyer.system.wechat.constant;

import java.io.Serializable;

/**
 * 标题：WechatConstant
 * 说明：微信常量
 * 时间：2024/3/12
 * 作者：admin
 */
public class WechatConstant implements Serializable {
    private static final long serialVersionUID = -243573076448414260L;

    /**
     * 微信基础API域名
     */
    public static final String WECHAT_BASE_URL = "https://api.weixin.qq.com";

    /**
     * 微信商户相关API域名
     */
    public static final String WECHAT_MCH_URL = "https://api.mch.weixin.qq.com";




    /**
     * rds 鉴权token key名称
     */
    public static final String RDS_WECHAT_ACCESSTOKEN_NAME = "RDS_WECHAT_ACCESSTOKEN";


    /**
     * 微信授权token接口
     */
    public static final String WECHAT_ACCESSTOKEN_URI = "/cgi-bin/token?grant_type=client_credential&appid=#{appid}&secret=#{secret}";


    /**
     * 通过code获取微信用户openid接口
     */
    public static final String WECHAT_USER_OPENID_URI = "/sns/jscode2session?appid=#{appid}&secret=#{secret}&js_code=#{code}&grant_type=authorization_code";

    /**
     * 通过微信用户openid获取当前微信用户详细信息接口
     */
    public static final String WECHAT_USERINFO_URI = "/cgi-bin/user/info?access_token=#{accesstoken}&openid=#{openid}&lang=zh_CN";


    /**
     * 通过code获取微信用户电话号码接口
     */
    public static final String WECHAT_USER_PHONENUMBER_URI = "/wxa/business/getuserphonenumber?access_token=#{accesstoken}&code=#{code}";


    /**
     * 生成微信预支付交易单接口
     */
    public static final String WECHAT_CREATE_PPMT_URI = "/v3/pay/transactions/jsapi";

}
