package com.destroyer.system.service.sysDic;

import com.destroyer.core.entity.sysDic.bo.SysDicBO;

 /**
 * 标题：系统字典(新增服务接口)
 * 说明：系统字典(新增服务接口),自定义新增数据接口
 * 时间：2023-9-4
 * 作者：admin
 */
public interface ISysDicCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(SysDicBO req);
}