package com.destroyer.system.service.impl.sysConfig;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.system.service.sysConfig.ISysConfigBaseService;
import com.destroyer.core.entity.sysConfig.SysConfigPO;
import com.destroyer.system.mapper.SysConfigMapper;
import org.springframework.stereotype.Service;

 /**
 * 标题：系统配置(基础通用服务实现)
 * 说明：系统配置(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2023-9-4
 * 作者：admin
 */
@Service
public class SysConfigBaseServiceImpl extends ServiceImpl<SysConfigMapper, SysConfigPO> implements ISysConfigBaseService {
}