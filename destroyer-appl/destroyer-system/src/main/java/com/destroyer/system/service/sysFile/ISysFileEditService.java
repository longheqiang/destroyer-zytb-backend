package com.destroyer.system.service.sysFile;

import com.destroyer.core.entity.sysFile.bo.SysFileBO;

 /**
 * 标题：系统附件信息(编辑数据服务接口)
 * 说明：系统附件信息(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(SysFileBO req);
}