package com.destroyer.system.service.impl.sysDic;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.sysDic.SysDicPO;
import com.destroyer.core.entity.sysDic.bo.SysDicQueryBO;
import com.destroyer.core.entity.sysDic.vo.SysDicVO;
import com.destroyer.core.util.PageUtils;
import com.destroyer.system.service.sysDic.ISysDicBaseService;
import com.destroyer.system.service.sysDic.ISysDicGetListService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 标题：系统字典(获取列表服务实现)
 * 说明：系统字典(获取列表服务实现),自定义基础列表查询,基础分页列表查询服务实现
 * 时间：2023-9-4
 * 作者：admin
 */
@Service
public class SysDicGetListServiceImpl implements ISysDicGetListService {

    /**
     * 基础通用服务
     */
    @Autowired
    private ISysDicBaseService baseService;


    /**
     * 获取列表(根据QueryBO)
     *
     * @param req
     * @return
     */
    @Override
    public List<SysDicVO> getList(SysDicQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<SysDicPO> poList = this.baseService.list(this.buildQueryWrapper(req));
        List<SysDicVO> rsp = this.buildRspVO(poList);
        return rsp;
    }


    /**
     * 获取分页列表(根据QueryBO)
     *
     * @param req
     * @return
     */
    @Override
    public Page<SysDicVO> getPageList(SysDicQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造分页入参对象
        Page<SysDicPO> page = req.buildPage();
        //分页查询列表数据
        Page<SysDicPO> poPage = this.baseService.page(page, this.buildQueryWrapper(req));
        //构造出参
        Page<SysDicVO> rsp = PageUtils.poPageToVoPage(poPage, SysDicVO.class);
        List<SysDicVO> voList = this.buildRspVO(poPage.getRecords());
        rsp.setRecords(voList);
        return rsp;
    }


    /**
     * 类通用入参校验
     *
     * @param req 入参
     */
    private void validParams(SysDicQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
        }
    }


    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(SysDicQueryBO req) {
        //执行前调用逻辑
    }



    /**
     * 构造QueryWrapper查询入参
     *
     * @param req 查询业务对象
     * @return QueryWrapper对象
     */
    private LambdaQueryWrapper<SysDicPO> buildQueryWrapper(SysDicQueryBO req){
        //构造查询包装器
        LambdaQueryWrapper<SysDicPO> rsp = req.getQueryWrapper();
        //数据权限范围控制

        return rsp;

    }




    /**
     * 构造响应视图对象列表
     *
     * @param req PO列表数据
     * @return VO列表数据
     */
    private List<SysDicVO> buildRspVO(List<SysDicPO> req) {
        List<SysDicVO> rsp = new ArrayList<>();
        if(null != req && req.size() >0){
            rsp = req.stream().map(k -> {
                SysDicVO vo = k.parseVO();
                return vo;
            }).collect(Collectors.toList());
        }
        return rsp;
    }
}