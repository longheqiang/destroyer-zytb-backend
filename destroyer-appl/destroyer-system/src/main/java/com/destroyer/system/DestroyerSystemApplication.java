package com.destroyer.system;

import com.destroyer.common.constant.PackageConstant;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;



@Slf4j
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = {PackageConstant.BASE_DESTROYER_CORE,PackageConstant.BASE_DESTROYER_SYSTEM})
@MapperScan({PackageConstant.MAPPER_DESTROYER_SYSTEM})
public class DestroyerSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(DestroyerSystemApplication.class, args);
        log.info("=======================系统核心中心启动成功=======================");

    }

}
