package com.destroyer.system.service.impl.sysConfig;

import com.destroyer.system.service.sysConfig.ISysConfigGetDetailService;
import com.destroyer.system.service.sysConfig.ISysConfigGetListService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.sysConfig.bo.SysConfigQueryBO;
import com.destroyer.core.entity.sysConfig.vo.SysConfigVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

 /**
 * 标题：系统配置(查询详情服务实现)
 * 说明：系统配置(查询详情服务实现),自定义基础详情查询接口
 * 时间：2023-9-6
 * 作者：admin
 */
@Service
public class SysConfigGetDetailServiceImpl implements ISysConfigGetDetailService {
    
    
    /**
     * 查询列表服务
     */
    @Autowired
    private ISysConfigGetListService getListService;
    /**
     * 根据条件查询详情
     * @param req
     * @return
     */
    @Override
    public SysConfigVO getDetailBy(SysConfigQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<SysConfigVO> list = getListService.getList(req);
        if (list.size() > 1) {
            throw new ServiceException(ResultEnum.FAILURE,"查询到的数据不唯一！");
        }
        SysConfigVO rsp = list.size() == 1? list.get(0):null;
        return rsp;
    }

     /**
      * 根据主键ID查询详情
      * @param req
      * @return
      */
     @Override
     public SysConfigVO getDetailById(Long req) {
         if (null == req) {
             throw new ServiceException(ResultEnum.PARAM_MISS,"查询详情入参ID不能为空");
         }
         SysConfigQueryBO queryBO = new SysConfigQueryBO();
         queryBO.setId(req);
         SysConfigVO rsp = this.getDetailBy(queryBO);
         return rsp;
     }

    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(SysConfigQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        } else {
        }
    }
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(SysConfigQueryBO req) {
        //执行前调用逻辑
    }
}