package com.destroyer.system.service.impl.sysDic;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.sysDic.SysDicPO;
import com.destroyer.system.service.sysDic.ISysDicBaseService;
import com.destroyer.system.mapper.SysDicMapper;
import org.springframework.stereotype.Service;

 /**
 * 标题：系统字典(基础通用服务实现)
 * 说明：系统字典(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2023-9-4
 * 作者：admin
 */
@Service
public class  SysDicBaseServiceImpl extends ServiceImpl<SysDicMapper, SysDicPO> implements ISysDicBaseService {
}