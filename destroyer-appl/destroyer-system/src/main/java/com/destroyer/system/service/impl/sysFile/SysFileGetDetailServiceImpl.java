package com.destroyer.system.service.impl.sysFile;

import com.destroyer.system.service.sysFile.ISysFileGetDetailService;
import com.destroyer.system.service.sysFile.ISysFileGetListService;
import com.destroyer.core.entity.sysFile.bo.SysFileQueryBO;
import com.destroyer.core.entity.sysFile.vo.SysFileVO;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

 /**
 * 标题：系统附件信息(查询详情服务实现)
 * 说明：系统附件信息(查询详情服务实现),自定义基础详情查询接口
 * 时间：2023-10-11
 * 作者：admin
 */
@Service
public class SysFileGetDetailServiceImpl implements ISysFileGetDetailService {
    
    
    /**
     * 查询列表服务
     */
    @Autowired
    private ISysFileGetListService getListService;
    /**
     * 根据条件查询详情
     * @param req
     * @return
     */
    @Override
    public SysFileVO getDetailBy(SysFileQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<SysFileVO> list = getListService.getList(req);
        if (list.size() > 1) {
            throw new ServiceException(ResultEnum.FAILURE,"查询到的数据不唯一！");
        }
        SysFileVO rsp = list.size() == 1? list.get(0):null;
        return rsp;
    }


     /**
      * 根据主键ID查询详情
      * @param req
      * @return
      */
     @Override
     public SysFileVO getDetailById(Long req) {
         if (null == req) {
             throw new ServiceException(ResultEnum.PARAM_MISS,"查询详情入参ID不能为空");
         }
         SysFileQueryBO queryBO = new SysFileQueryBO();
         queryBO.setId(req);
         SysFileVO rsp = this.getDetailBy(queryBO);
         return rsp;
     }

    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(SysFileQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        } else {
        }
    }
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(SysFileQueryBO req) {
        //执行前调用逻辑
    }
}