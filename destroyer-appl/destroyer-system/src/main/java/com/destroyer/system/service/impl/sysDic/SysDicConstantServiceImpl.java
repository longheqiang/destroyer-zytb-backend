package com.destroyer.system.service.impl.sysDic;

import com.destroyer.core.entity.sysDic.SysDicPO;
import com.destroyer.system.service.sysDic.ISysDicBaseService;
import com.destroyer.system.service.sysDic.ISysDicConstantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 标题：系统字典(常量服务实现)
 * 说明：系统字典(常量服务实现),用于生成字典常量字符串
 * 时间：2023-9-4
 * 作者：admin
 */
@Service
public class SysDicConstantServiceImpl implements ISysDicConstantService {


    @Autowired
    private ISysDicBaseService baseService;

    /**
     * 生成字典常量字符串
     * @return
     */
    @Override
    public String getConstant() {
        List<SysDicPO> poList = baseService.list();
        Map<String, List<SysDicPO>> dicMaps = poList.stream().collect(Collectors.groupingBy(SysDicPO::getType));
        String t = "";
        for (String key : dicMaps.keySet()) {
            List<SysDicPO> poItemList = dicMaps.get(key);
            if (null != poItemList && poItemList.size() >0){
                String className = key;
                t += " /** * " +  dicMaps.get(key).get(0).getTypeTxt() + " */ ";
                t += "public static final class " + className + "{public static final String NAME = " + "\"" + className + "\"" + ";";

                for (SysDicPO po : poItemList) {
                    String title = po.getTitle();
                    String file = po.getFile();
                    String val = po.getVal();

                    t += " /** * " + title + " */ ";
                    t += " public static final String " + file + " = \"" + val + "\";";
                }
            }

            t += "}";
        }
        return t;
    }
}
