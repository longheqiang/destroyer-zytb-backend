package com.destroyer.system.wechat.service;

import com.destroyer.core.entity.wechat.bo.WechatPrepayBO;
import com.destroyer.core.entity.wechat.vo.WechatPrepayWithRequestPaymentVO;
import org.springframework.http.ResponseEntity;

/**
 * 标题：IWechatPaymentsService
 * 说明：微信支付服务接口
 * 时间：2024/3/12
 * 作者：admin
 */
public interface IWechatPaymentsService {


    /**
     * 微信建立支付预订单
     * @param req 支付预订单ID
     * @return
     */
    String prepay(WechatPrepayBO req);

    /**
     * 下单并生成调起支付的参数
     * @param req
     * @return
     */
    WechatPrepayWithRequestPaymentVO prepayWithRequestPayment(WechatPrepayBO req);


    /**
     * 通过预支付ID生成调起支付的参数
     * @param req
     * @return
     */
    WechatPrepayWithRequestPaymentVO prepayWithRequestPaymentByPrepayId(String req);


    /**
     * 支付回调
     * @param requestBody
     * @return
     */
    ResponseEntity.BodyBuilder paymentNotify(String requestBody);


    /**
     * 根据业务订单编号关闭订单
     * @param req
     * @return
     */
    boolean closeOrderByOutTradeNo(String req);

}
