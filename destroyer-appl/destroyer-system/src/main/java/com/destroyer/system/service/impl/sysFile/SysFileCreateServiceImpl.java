package com.destroyer.system.service.impl.sysFile;

import com.destroyer.system.service.sysFile.ISysFileBaseService;
import com.destroyer.system.service.sysFile.ISysFileCreateService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.sysFile.SysFilePO;
import com.destroyer.core.entity.sysFile.bo.SysFileBO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：系统附件信息(新增服务实现)
 * 说明：系统附件信息(新增服务实现),自定义新增数据实现
 * 时间：2023-10-11
 * 作者：admin
 */
@Service
public class SysFileCreateServiceImpl implements ISysFileCreateService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private ISysFileBaseService baseService;
    
    
    /**
     * 新增数据
     * @param req
     * @return
     */
    @Override
    public Long create(SysFileBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造PO对象(根据业务对象)
        SysFilePO po = new SysFilePO(req);
        //调用基础保存服务
        boolean states = this.baseService.save(po);
        if (!states){
            throw new ServiceException(ResultEnum.FAILURE, "新增数据失败");
        }
        Long rsp = po.getId();
        return rsp;
    }
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(SysFileBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
    if(StringUtils.isBlank(req.getName())){
        throw new ServiceException(ResultEnum.PARAM_MISS,"入参[附件名称]不能为空!");
    }
    if(StringUtils.isBlank(req.getUrl())){
        throw new ServiceException(ResultEnum.PARAM_MISS,"入参[URL]不能为空!");
    }
        }
    }
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(SysFileBO req) {
        //执行前调用逻辑
    }
}