package com.destroyer.system.service.impl.sysConfig;

import com.destroyer.system.service.sysConfig.ISysConfigBaseService;
import com.destroyer.system.service.sysConfig.ISysConfigEditService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.sysConfig.SysConfigPO;
import com.destroyer.core.entity.sysConfig.bo.SysConfigBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：系统配置(编辑数据服务实现)
 * 说明：系统配置(编辑数据服务实现),自定义编辑数据服务实现
 * 时间：2023-9-4
 * 作者：admin
 */
@Service
public class SysConfigEditServiceImpl implements ISysConfigEditService {
    
    
    
    @Autowired
    private ISysConfigBaseService baseService;
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    @Override
    public boolean editById(SysConfigBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //入参处理
        SysConfigPO po = new SysConfigPO(req);
        //更新数据
        boolean rsp = this.baseService.updateById(po);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(SysConfigBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
	
        if (null == req.getId()) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "更新数据入参ID不能为空");
        }
	
	
	
	
	
	
	
	
	
	
	
	
	
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(SysConfigBO req) {
        //执行前调用逻辑
    }
}