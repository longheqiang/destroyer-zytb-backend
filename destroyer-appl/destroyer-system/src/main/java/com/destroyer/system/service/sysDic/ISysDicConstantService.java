package com.destroyer.system.service.sysDic;

/**
 * 标题：系统字典(常量服务接口)
 * 说明：系统字典(常量服务接口),用于生成字典常量字符串
 * 时间：2023-9-4
 * 作者：admin
 */
public interface ISysDicConstantService {

    /**
     * 生成字典常量字符串
     * @return
     */
    String getConstant();
}
