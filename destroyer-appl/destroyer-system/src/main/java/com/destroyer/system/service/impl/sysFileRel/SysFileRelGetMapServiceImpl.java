package com.destroyer.system.service.impl.sysFileRel;

import com.destroyer.system.service.sysFileRel.ISysFileRelGetListService;
import com.destroyer.system.service.sysFileRel.ISysFileRelGetMapService;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelQueryBO;
import com.destroyer.core.entity.sysFileRel.vo.SysFileRelVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：系统业务附件关联(获取Map集合服务实现)
 * 说明：系统业务附件关联(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2023-10-11
 * 作者：admin
 */
@Service
public class SysFileRelGetMapServiceImpl implements ISysFileRelGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private ISysFileRelGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, SysFileRelVO> getMapGroupById(SysFileRelQueryBO req) {
        Map<Long, SysFileRelVO> rsp = new HashMap<>();
        List<SysFileRelVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(SysFileRelVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }



     /**
      * 根据业务ID和业务类型分组查询map集合
      * @param req
      * @return
      */
     @Override
     public Map<Long, Map<String, List<SysFileRelVO>>> getMapGroupByBizIdAndBizType(SysFileRelQueryBO req) {
         Map<Long, Map<String, List<SysFileRelVO>>> rsp = new HashMap<>();
         List<SysFileRelVO> voList = this.getListService.getList(req);
         Map<Long, List<SysFileRelVO>> listMap =
                 voList.stream().collect(
                         Collectors.groupingBy(SysFileRelVO::getBizId)
                 );
         for (Long keySet : listMap.keySet()) {
             List<SysFileRelVO> list = listMap.get(keySet);
             Map<String, List<SysFileRelVO>> mapList=
             list.stream().collect(
                     Collectors.groupingBy(SysFileRelVO::getBizType)
             );
             rsp.put(keySet,mapList);
         }
         return rsp;
     }
 }