package com.destroyer.system.service.impl.sysDic;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.sysDic.SysDicPO;
import com.destroyer.core.entity.sysDic.bo.SysDicQueryBO;
import com.destroyer.system.service.sysDic.ISysDicBaseService;
import com.destroyer.system.service.sysDic.ISysDicDeleteService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 标题：系统字典(删除数据服务接口)
 * 说明：系统字典(删除数据服务接口),自定义删除数据服务实现
 * 时间：2023-9-4
 * 作者：admin
 */
@Service
public class SysDicDeleteServiceImpl implements ISysDicDeleteService {


    /**
     * 基础通用服务
     */
    @Autowired
    private ISysDicBaseService baseService;


    /**
     * 删除数据（根据主键ID）
     * @param req
     * @return
     */
    @Override
    public boolean deleteById(Long req) {
        if(null == req){
            throw new ServiceException(ResultEnum.PARAM_MISS,"删除数据时，主键ID不能为空！");
        }
        boolean rsp = this.baseService.removeById(req);
        return rsp;
    }


    /**
     * 删除数据（根据主键ID集合批量删除）
     * @param req
     * @return
     */
    @Override
    public boolean batchDeleteByIds(BaseIdListBO req) {
        if(null == req.getIdList() || req.getIdList().size() == 0){
            throw new ServiceException(ResultEnum.PARAM_MISS,"批量删除数据时，ID集合不能为空！");
        }
        boolean rsp = this.baseService.removeBatchByIds(req.getIdList());
        return rsp;
    }


    /**
     * 删除数据（根据查询条件）
     * @param req
     * @return
     */
    @Override
    public boolean batchDeleteBy(SysDicQueryBO req) {
        if(null == req){
            throw new ServiceException(ResultEnum.PARAM_MISS,"删除数据时，入参不能为空！");
        }
        LambdaQueryWrapper<SysDicPO> wrapper = req.getQueryWrapper();
        boolean rsp = this.baseService.remove(wrapper);
        return rsp;
    }
}