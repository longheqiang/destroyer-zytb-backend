package com.destroyer.system.controller;

import com.destroyer.system.service.sysFileRel.*;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelDeleteByBizBO;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelQueryBO;
import com.destroyer.core.entity.sysFileRel.vo.SysFileRelVO;
import com.destroyer.system.ability.sysFileRel.ISysFileRelDeleteByBizTypeAndBizIdAbilityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 标题：系统业务附件关联(控制层)
 * 说明：系统业务附件关联(控制层),包括所有对外前端API接口
 * 时间：2023-10-19
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/system/sysFileRel")
@Api(value = "SysFileRelController", tags = "系统业务附件关联API")
public class SysFileRelController {


    /**
     * 基础服务
     */
    @Autowired
    private ISysFileRelBaseService baseService;


    /**
     * 新增数据服务
     */
    @Autowired
    private ISysFileRelCreateService createService;



    /**
     * 批量新增服务
     */
    @Autowired
    private ISysFileRelBatchCreateService batchCreateService;



    /**
     * 删除数据服务
     */
    @Autowired
    private ISysFileRelDeleteService deleteService;


    /**
     * 列表服务
     */
    @Autowired
    private ISysFileRelGetListService getListService;


    /**
     * map服务
     */
    @Autowired
    private ISysFileRelGetMapService getMapService;


    /**
     * 详情服务
     */
    @Autowired
    private ISysFileRelGetDetailService getDetailService;


    /**
     * 编辑服务
     */
    @Autowired
    private ISysFileRelEditService editService;

    @Autowired
    private ISysFileRelDeleteByBizTypeAndBizIdAbilityService deleteByBizTypeAndBizIdAbilityService;





    @PostMapping(value = "/deleteByBizTypeAndBizId")
    @ApiOperation("删除数据API(根据业务ID和业务类型)")
    public Result<Boolean> deleteByBizTypeAndBizId(@RequestBody SysFileRelDeleteByBizBO req) {
        boolean rsp = this.deleteByBizTypeAndBizIdAbilityService.DeleteByBizTypeAndBizId(req);
        return Result.success(rsp);
    }

    @PostMapping(value = "/mapGroupByBizIdAndBizType")
    @ApiOperation("根据业务ID和业务类型分组查询map集合API")
    public Result<Map<Long, Map<String, List<SysFileRelVO>>>> mapGroupByBizIdAndBizType(@RequestBody SysFileRelQueryBO req) {
        Map<Long, Map<String, List<SysFileRelVO>>> rsp = this.getMapService.getMapGroupByBizIdAndBizType(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<SysFileRelVO>> list(@RequestBody SysFileRelQueryBO req) {
        List<SysFileRelVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }


}