package com.destroyer.system.service.sysFileRel;

import com.destroyer.core.entity.sysFileRel.bo.SysFileRelQueryBO;
import com.destroyer.core.entity.sysFileRel.vo.SysFileRelVO;

/**
 * 标题：系统业务附件关联(查询详情服务接口)
 * 说明：系统业务附件关联(查询详情服务接口),自定义基础详情查询接口
 * 时间：2023-10-11
 * 作者：admin
 */
public interface ISysFileRelGetDetailService {
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    SysFileRelVO getDetailBy(SysFileRelQueryBO req);


    /**
     * 根据主键ID查询详情
     * @param req
     * @return
     */
    SysFileRelVO getDetailById(Long req);
}