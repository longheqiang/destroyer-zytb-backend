package com.destroyer.system.wechat.config;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.Serializable;

/**
 * 标题：WeChatConfig
 * 说明：微信基础配置
 * 时间：2024/3/19
 * 作者：admin
 */
@Data
@Configuration
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "wechat")
public class WechatConfig implements Serializable {
    private static final long serialVersionUID = -1;

    @ApiModelProperty(value = "APPID")
    private String appid;

    @ApiModelProperty(value = "App密钥")
    private String secret;

    @ApiModelProperty(value = "商户号")
    private String mchid;


    @ApiModelProperty(value = "商户API私钥路径")
    private String privateKeyPath;


    @ApiModelProperty(value = "商户证书序列号")
    private String merchantSerialNumber;


    @ApiModelProperty(value = "商户APIV3密钥")
    private String apiV3Key;


    @ApiModelProperty(value = "商户API证书路径")
    private String certificatesPath;

    @ApiModelProperty(value = "商户支付下单回调地址")
    private String prepayNotifyUrl;

}
