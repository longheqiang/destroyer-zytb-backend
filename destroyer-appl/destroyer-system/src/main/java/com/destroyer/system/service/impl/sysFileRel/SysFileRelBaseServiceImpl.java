package com.destroyer.system.service.impl.sysFileRel;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.system.service.sysFileRel.ISysFileRelBaseService;
import com.destroyer.core.entity.sysFileRel.SysFileRelPO;
import com.destroyer.system.mapper.SysFileRelMapper;
import org.springframework.stereotype.Service;

 /**
 * 标题：系统业务附件关联(基础通用服务实现)
 * 说明：系统业务附件关联(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2023-10-11
 * 作者：admin
 */
@Service
public class  SysFileRelBaseServiceImpl extends ServiceImpl<SysFileRelMapper, SysFileRelPO> implements ISysFileRelBaseService {
}