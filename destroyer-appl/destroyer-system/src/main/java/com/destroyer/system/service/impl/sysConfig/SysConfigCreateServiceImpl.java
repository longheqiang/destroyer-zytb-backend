package com.destroyer.system.service.impl.sysConfig;

import com.destroyer.system.service.sysConfig.ISysConfigBaseService;
import com.destroyer.system.service.sysConfig.ISysConfigCreateService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.sysConfig.SysConfigPO;
import com.destroyer.core.entity.sysConfig.bo.SysConfigBO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：系统配置(新增服务实现)
 * 说明：系统配置(新增服务实现),自定义新增数据实现
 * 时间：2023-9-4
 * 作者：admin
 */
@Service
public class SysConfigCreateServiceImpl implements ISysConfigCreateService {
    @Autowired
    private ISysConfigBaseService baseService;
    
    
    /**
     * 新增数据
     * @param req
     * @return
     */
    @Override
    public Long create(SysConfigBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造PO对象(根据业务对象)
        SysConfigPO po = new SysConfigPO(req);
        //调用基础保存服务
        boolean states = this.baseService.save(po);
        if (!states){
            throw new ServiceException(ResultEnum.FAILURE, "新增数据失败");
        }
        Long rsp = po.getId();
        return rsp;
    }
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(SysConfigBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
    if(StringUtils.isBlank(req.getType())){
        throw new ServiceException(ResultEnum.PARAM_MISS,"入参[配置分类：WECHAT_CONFIG]不能为空!");
    }
    if(StringUtils.isBlank(req.getTypeTxt())){
        throw new ServiceException(ResultEnum.PARAM_MISS,"入参[分类文案：微信配置]不能为空!");
    }
    if(StringUtils.isBlank(req.getFile())){
        throw new ServiceException(ResultEnum.PARAM_MISS,"入参[配置字段名称：WECHAT_APP_ID]不能为空!");
    }
    if(StringUtils.isBlank(req.getTitle())){
        throw new ServiceException(ResultEnum.PARAM_MISS,"入参[配置名称：微信APPID]不能为空!");
    }
    if(StringUtils.isBlank(req.getVal())){
        throw new ServiceException(ResultEnum.PARAM_MISS,"入参[值：test]不能为空!");
    }
        }
    }
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(SysConfigBO req) {
        //执行前调用逻辑
    }
}