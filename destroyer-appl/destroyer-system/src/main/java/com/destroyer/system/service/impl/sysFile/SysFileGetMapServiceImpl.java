package com.destroyer.system.service.impl.sysFile;

import com.destroyer.system.service.sysFile.ISysFileGetListService;
import com.destroyer.system.service.sysFile.ISysFileGetMapService;
import com.destroyer.core.entity.sysFile.bo.SysFileQueryBO;
import com.destroyer.core.entity.sysFile.vo.SysFileVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：系统附件信息(获取Map集合服务实现)
 * 说明：系统附件信息(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2023-10-11
 * 作者：admin
 */
@Service
public class SysFileGetMapServiceImpl implements ISysFileGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private ISysFileGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, SysFileVO> getMapGroupById(SysFileQueryBO req) {
        Map<Long, SysFileVO> rsp = new HashMap<>();
        List<SysFileVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(SysFileVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}