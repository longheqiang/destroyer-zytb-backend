package com.destroyer.system.service.impl.sysConfig;

import com.destroyer.system.service.sysConfig.ISysConfigGetListService;
import com.destroyer.system.service.sysConfig.ISysConfigGetMapService;
import com.destroyer.core.entity.sysConfig.bo.SysConfigQueryBO;
import com.destroyer.core.entity.sysConfig.vo.SysConfigVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 标题：系统配置(获取Map集合服务实现)
 * 说明：系统配置(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2023-9-7
 * 作者：admin
 */
@Service
public class SysConfigGetMapServiceImpl implements ISysConfigGetMapService {


    /**
     * 列表服务
     */
    @Autowired
    private ISysConfigGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, SysConfigVO> getMapGroupById(SysConfigQueryBO req) {
        Map<Long, SysConfigVO> rsp = new HashMap<>();
        List<SysConfigVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(SysConfigVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}