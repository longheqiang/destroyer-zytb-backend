package com.destroyer.system.service.sysConfig;

import com.destroyer.core.entity.sysConfig.bo.SysConfigBO;

 /**
 * 标题：系统配置(新增服务接口)
 * 说明：系统配置(新增服务接口),自定义新增数据接口
 * 时间：2023-9-4
 * 作者：admin
 */
public interface ISysConfigCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(SysConfigBO req);
}