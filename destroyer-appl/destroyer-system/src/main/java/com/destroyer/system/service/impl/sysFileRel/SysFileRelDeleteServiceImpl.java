package com.destroyer.system.service.impl.sysFileRel;

import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.system.service.sysFileRel.ISysFileRelBaseService;
import com.destroyer.system.service.sysFileRel.ISysFileRelDeleteService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：系统业务附件关联(删除数据服务接口)
 * 说明：系统业务附件关联(删除数据服务接口),自定义删除数据服务实现
 * 时间：2023-10-11
 * 作者：admin
 */
@Service
public class SysFileRelDeleteServiceImpl implements ISysFileRelDeleteService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private ISysFileRelBaseService baseService;


     /**
      * 删除数据（根据主键ID）
      * @param req
      * @return
      */
     @Override
     public boolean deleteById(Long req) {
         if(null == req){
             throw new ServiceException(ResultEnum.PARAM_MISS,"删除数据时，主键ID不能为空！");
         }
         boolean rsp = this.baseService.removeById(req);
         return rsp;
     }
    
    
    /**
     * 删除数据（根据主键ID集合批量删除）
     * @param req
     * @return
     */
    @Override
    public boolean batchDeleteByIds(BaseIdListBO req) {
        if(null == req.getIdList() || req.getIdList().size() == 0){
            throw new ServiceException(ResultEnum.PARAM_MISS,"批量删除数据时，ID集合不能为空！");
        }
        boolean rsp = this.baseService.removeBatchByIds(req.getIdList());
        return rsp;
    }
}