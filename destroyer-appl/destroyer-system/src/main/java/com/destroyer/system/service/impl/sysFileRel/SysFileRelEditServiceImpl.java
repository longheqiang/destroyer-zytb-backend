package com.destroyer.system.service.impl.sysFileRel;

import com.destroyer.system.service.sysFileRel.ISysFileRelBaseService;
import com.destroyer.system.service.sysFileRel.ISysFileRelEditService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.sysFileRel.SysFileRelPO;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：系统业务附件关联(编辑数据服务实现)
 * 说明：系统业务附件关联(编辑数据服务实现),自定义编辑数据服务实现
 * 时间：2023-10-11
 * 作者：admin
 */
@Service
public class SysFileRelEditServiceImpl implements ISysFileRelEditService {
    
    
     /**
     * 基础通用服务
     */
    @Autowired
    private ISysFileRelBaseService baseService;
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    @Override
    public boolean editById(SysFileRelBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //入参处理
        SysFileRelPO po = new SysFileRelPO(req);
        //更新数据
        boolean rsp = this.baseService.updateById(po);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(SysFileRelBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
	
        if (null == req.getId()) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "更新数据入参ID不能为空");
        }
	
	
	
	
	
	
	
	
	
	
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(SysFileRelBO req) {
        //执行前调用逻辑
    }
}