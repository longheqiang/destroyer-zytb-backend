package com.destroyer.system.service.impl.sysFileRel;

import com.destroyer.system.service.sysFileRel.ISysFileRelBaseService;
import com.destroyer.system.service.sysFileRel.ISysFileRelCreateService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.sysFileRel.SysFileRelPO;
import com.destroyer.core.entity.sysFileRel.bo.SysFileRelBO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 标题：系统业务附件关联(新增服务实现)
 * 说明：系统业务附件关联(新增服务实现),自定义新增数据实现
 * 时间：2023-10-11
 * 作者：admin
 */
@Service
public class SysFileRelCreateServiceImpl implements ISysFileRelCreateService {


    /**
     * 基础通用服务
     */
    @Autowired
    private ISysFileRelBaseService baseService;


    /**
     * 新增数据
     *
     * @param req
     * @return
     */
    @Override
    public Long create(SysFileRelBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造PO对象(根据业务对象)
        SysFileRelPO po = new SysFileRelPO(req);
        //调用基础保存服务
        boolean states = this.baseService.save(po);
        if (!states) {
            throw new ServiceException(ResultEnum.FAILURE, "新增数据失败");
        }
        Long rsp = po.getId();
        return rsp;
    }

    /**
     * 类通用入参校验
     *
     * @param req 入参
     */
    private void validParams(SysFileRelBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
            if (StringUtils.isBlank(req.getBizType())) {
                throw new ServiceException(ResultEnum.PARAM_MISS, "入参[业务类型]不能为空!");
            }
            if (null == req.getFileId()) {
                throw new ServiceException(ResultEnum.PARAM_MISS, "入参[附件ID]不能为空!");
            }
            if (null == req.getBizId()) {
                throw new ServiceException(ResultEnum.PARAM_MISS, "入参[业务主键ID]不能为空!");
            }
        }
    }

    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(SysFileRelBO req) {
        //执行前调用逻辑
    }
}