package com.destroyer.system.service.sysConfig;

import com.destroyer.core.entity.base.BaseIdListBO;

/**
 * 标题：系统配置(删除数据服务接口)
 * 说明：系统配置(删除数据服务接口),自定义删除数据服务接口
 * 时间：2023-9-4
 * 作者：admin
 */
public interface ISysConfigDeleteService {
    /**
     * 删除数据（根据主键ID）
     * @param req
     * @return
     */
    boolean deleteById(Long req);
    
    
    /**
     * 删除数据（根据主键ID集合批量删除）
     * @param req
     * @return
     */
    boolean batchDeleteByIds(BaseIdListBO req);
}