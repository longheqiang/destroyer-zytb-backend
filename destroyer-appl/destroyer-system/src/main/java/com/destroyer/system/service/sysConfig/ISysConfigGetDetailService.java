package com.destroyer.system.service.sysConfig;

import com.destroyer.core.entity.sysConfig.bo.SysConfigQueryBO;
import com.destroyer.core.entity.sysConfig.vo.SysConfigVO;

/**
 * 标题：系统配置(查询详情服务接口)
 * 说明：系统配置(查询详情服务接口),自定义基础详情查询接口
 * 时间：2023-9-6
 * 作者：admin
 */
public interface ISysConfigGetDetailService {
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    SysConfigVO getDetailBy(SysConfigQueryBO req);



    /**
     * 根据主键ID查询详情
     * @param req
     * @return
     */
    SysConfigVO getDetailById(Long req);
}