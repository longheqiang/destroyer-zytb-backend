package com.destroyer.system.controller;

import com.destroyer.system.service.sysFile.*;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.sysFile.bo.SysFileBO;
import com.destroyer.core.entity.sysFile.bo.SysFileCreateBO;
import com.destroyer.system.ability.sysFile.SysFileCreateAndBuildRelAbilityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

 /**
 * 标题：系统附件信息(控制层)
 * 说明：系统附件信息(控制层),包括所有对外前端API接口
 * 时间：2023-10-11
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/system/sysFile")
@Api(value = "SysFileController", tags = "系统附件信息API")
public class SysFileController {
    
    
    /**
     * 基础服务
     */
    @Autowired
    private ISysFileBaseService baseService;
    
    
    /**
     * 新增数据服务
     */
    @Autowired
    private ISysFileCreateService createService;
    
    
    
    /**
     * 批量新增服务
     */
    @Autowired
    private ISysFileBatchCreateService batchCreateService;
    
    
    
    /**
      * 删除数据服务
      */
    @Autowired
    private ISysFileDeleteService deleteService;
    
    
    /**
     * 列表服务
     */
    @Autowired
    private ISysFileGetListService getListService;
    
    
     /**
     * map服务
     */
    @Autowired
    private ISysFileGetMapService getMapService;
    
    
    /**
     * 详情服务
     */
    @Autowired
    private ISysFileGetDetailService getDetailService;
    
    
    /**
     * 编辑服务
     */
    @Autowired
    private ISysFileEditService editService;



     /**
      * 新增并建立关联服务
      */
    @Autowired
    private SysFileCreateAndBuildRelAbilityService createAndBuildRelAbilityService;
    
    
    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody SysFileBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }
    
    
    
    @PostMapping(value = "/batchCreate")
    @ApiOperation("批量新增API")
    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<SysFileBO> req) {
        List<Long> rsp = this.batchCreateService.batchCreate(req);
        return Result.success(rsp);
    }

     @PostMapping(value = "/createAndBuildRel")
     @ApiOperation("新增附件并建立业务关联API")
     public Result<List<Long>> createAndBuildRel(@RequestBody BaseBatchBO<SysFileCreateBO> req) {
         List<Long> rsp = this.createAndBuildRelAbilityService.createAndBuildRel(req);
         return Result.success(rsp);
     }





//
//
//    @PostMapping(value = "/batchDeleteByIds")
//    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
//    public Result<Boolean> batchDeleteByIds(@RequestBody BaseDeleteBO req) {
//        boolean rsp = this.deleteService.batchDeleteByIds(req);
//        return Result.success(rsp);
//    }
//
//
//    @PostMapping(value = "/list")
//    @ApiOperation("查询列表API")
//    public Result<List<SysFileBaseRspVO>> list(@RequestBody SysFileQueryBO req) {
//        List<SysFileBaseRspVO> rsp = this.getListService.getList(req);
//        return Result.success(rsp);
//    }
//
//
//    @PostMapping(value = "/mapGroupById")
//    @ApiOperation("查询map(ID分组)API")
//    public Result<Map<Long, SysFileBaseRspVO>> mapGroupById(@RequestBody SysFileQueryBO req) {
//        Map<Long, SysFileBaseRspVO> rsp = this.getMapService.getMapGroupById(req);
//        return Result.success(rsp);
//    }
//
//
//    @PostMapping(value = "/page")
//    @ApiOperation("查询分页列表API")
//    public Result<Page<SysFileBaseRspVO>> page(@RequestBody SysFileQueryBO req) {
//        Page<SysFileBaseRspVO> rsp = this.getListService.getPageList(req);
//        return Result.success(rsp);
//    }
//
//
//    @PostMapping(value = "/detailBy")
//    @ApiOperation("根据查询条件查询详情API")
//    public Result<SysFileBaseRspVO> detailBy(@RequestBody SysFileQueryBO req) {
//        SysFileBaseRspVO rsp = this.getDetailService.getDetailBy(req);
//        return Result.success(rsp);
//    }
//
//
//    @PostMapping(value = "/detailById")
//    @ApiOperation("根据ID查询详情API")
//    public Result<SysFileBaseRspVO> detailById(@RequestBody SysFileQueryBO req) {
//        SysFileBaseRspVO rsp = this.getDetailService.getDetailById(req);
//        return Result.success(rsp);
//    }
//
//
//    @PostMapping(value = "/edit")
//    @ApiOperation("修改数据API")
//    public Result<Boolean> edit(@RequestBody SysFileBaseBO req) {
//        boolean rsp = this.editService.editById(req);
//        return Result.success(rsp);
//    }
}