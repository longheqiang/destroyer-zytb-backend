package com.destroyer.system.service.impl.sysConfig;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.util.PageUtils;
import com.destroyer.system.service.sysConfig.ISysConfigBaseService;
import com.destroyer.system.service.sysConfig.ISysConfigGetListService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.sysConfig.SysConfigPO;
import com.destroyer.core.entity.sysConfig.bo.SysConfigQueryBO;
import com.destroyer.core.entity.sysConfig.vo.SysConfigVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 标题：系统配置(获取列表服务实现)
 * 说明：系统配置(获取列表服务实现),自定义基础列表查询,基础分页列表查询服务实现
 * 时间：2023-9-4
 * 作者：admin
 */
@Service
public class SysConfigGetListServiceImpl implements ISysConfigGetListService {
    @Autowired
    private ISysConfigBaseService baseService;


    /**
     * 获取列表(根据QueryBO)
     *
     * @param req
     * @return
     */
    @Override
    public List<SysConfigVO> getList(SysConfigQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<SysConfigPO> poList = this.baseService.list(this.buildQueryWrapper(req));
        List<SysConfigVO> rsp = this.buildRspVO(poList);
        return rsp;
    }


    /**
     * 获取分页列表(根据QueryBO)
     *
     * @param req
     * @return
     */
    @Override
    public Page<SysConfigVO> getPageList(SysConfigQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造分页入参对象
        Page<SysConfigPO> page = req.buildPage();
        //分页查询列表数据
        Page<SysConfigPO> poPage = this.baseService.page(page, this.buildQueryWrapper(req));
        //构造出参
        Page<SysConfigVO> rsp = PageUtils.poPageToVoPage(poPage, SysConfigVO.class);
        List<SysConfigVO> voList = this.buildRspVO(poPage.getRecords());
        rsp.setRecords(voList);
        return rsp;
    }


    /**
     * 类通用入参校验
     *
     * @param req 入参
     */
    private void validParams(SysConfigQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
        }
    }


    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(SysConfigQueryBO req) {
        //执行前调用逻辑
    }


    /**
     * 构造QueryWrapper查询入参
     *
     * @param req 查询业务对象
     * @return QueryWrapper对象
     */
    private LambdaQueryWrapper<SysConfigPO> buildQueryWrapper(SysConfigQueryBO req) {
        //构造查询包装器
        LambdaQueryWrapper<SysConfigPO> rsp = req.getQueryWrapper();
        //数据权限范围控制


        return rsp;
    }


    /**
     * 构造响应视图对象列表
     *
     * @param req PO列表数据
     * @return VO列表数据
     */
    private List<SysConfigVO> buildRspVO(List<SysConfigPO> req) {
        List<SysConfigVO> rsp = new ArrayList<>();
        if(null != req && req.size() >0){
            rsp = req.stream().map(k -> {
                SysConfigVO vo = k.parseVO();
                return vo;
            }).collect(Collectors.toList());
        }
        return rsp;
    }
}