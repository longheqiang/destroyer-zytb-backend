package com.destroyer.will.service.bscYear;

import com.destroyer.core.entity.bscYear.bo.BscYearBO;

 /**
 * 标题：年份(编辑数据服务接口)
 * 说明：年份(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscYearEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(BscYearBO req);
}