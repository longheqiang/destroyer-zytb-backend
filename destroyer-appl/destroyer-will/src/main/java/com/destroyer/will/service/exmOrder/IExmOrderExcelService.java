package com.destroyer.will.service.exmOrder;

import com.destroyer.core.service.excel.IExcelBaseService;

 /**
 * 标题：消费订单(Excel服务接口)
 * 说明：消费订单(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-13
 * 作者：admin
 */
public interface IExmOrderExcelService extends IExcelBaseService{
    
    
}