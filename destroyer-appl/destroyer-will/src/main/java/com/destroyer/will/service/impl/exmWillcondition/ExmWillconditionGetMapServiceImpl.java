package com.destroyer.will.service.impl.exmWillcondition;

import com.destroyer.core.entity.exmWillcondition.bo.ExmWillconditionQueryBO;
import com.destroyer.core.entity.exmWillcondition.vo.ExmWillconditionVO;
import com.destroyer.will.service.exmWillcondition.IExmWillconditionGetListService;
import com.destroyer.will.service.exmWillcondition.IExmWillconditionGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：学生志愿条件(获取Map集合服务实现)
 * 说明：学生志愿条件(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class ExmWillconditionGetMapServiceImpl implements IExmWillconditionGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IExmWillconditionGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, ExmWillconditionVO> getMapGroupById(ExmWillconditionQueryBO req) {
        Map<Long, ExmWillconditionVO> rsp = new HashMap<>();
        List<ExmWillconditionVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(ExmWillconditionVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}