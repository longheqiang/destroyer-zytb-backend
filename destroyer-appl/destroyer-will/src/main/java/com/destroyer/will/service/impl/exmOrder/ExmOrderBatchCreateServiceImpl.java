package com.destroyer.will.service.impl.exmOrder;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.exmOrder.ExmOrderPO;
import com.destroyer.core.entity.exmOrder.bo.ExmOrderBO;
import com.destroyer.will.service.exmOrder.IExmOrderBaseService;
import com.destroyer.will.service.exmOrder.IExmOrderBatchCreateService;
import com.destroyer.core.util.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

/**
 * 标题：消费订单(批量新增服务实现)
 * 说明：消费订单(批量新增服务实现),自定义批量新增据服务实现
 * 时间：2024-3-13
 * 作者：admin
 */
@Service
public class ExmOrderBatchCreateServiceImpl implements IExmOrderBatchCreateService {


    /**
     * 基础通用服务
     */
    @Autowired
    private IExmOrderBaseService baseService;

    /**
     * 批量新增数据
     *
     * @param req
     * @return 新增数据的组件ID集合
     */
    @Override
    public List<Long> batchCreate(BaseBatchBO<ExmOrderBO> req) {
        List<Long> rsp = new ArrayList<>();
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<ExmOrderPO> poList = req.getList().stream().map(k -> {
            ExmOrderPO po = new ExmOrderPO(k);
            return po;
        }).collect(Collectors.toList());
        boolean state = baseService.saveBatch(poList);
        if (!state) {
            throw new ServiceException(ResultEnum.FAILURE, "批量新增数据失败");
        }
        rsp = poList.stream().map(ExmOrderPO::getId).collect(Collectors.toList());
        return rsp;
    }

    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(BaseBatchBO<ExmOrderBO> req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
            if (null == req || req.getList().size() == 0) {
                throw new ServiceException(ResultEnum.PARAM_MISS, "批量新增入参不能为空");
            }
            List<ExmOrderBO> list = req.getList();
            for (ExmOrderBO i : list) {


                if (null == i.getStuId()) {
                    throw new ServiceException(ResultEnum.PARAM_MISS, "入参[学生ID]不能为空!");
                }


                if (StringUtils.isBlank(i.getOrderNo())) {
                    throw new ServiceException(ResultEnum.PARAM_MISS, "入参[订单编号]不能为空!");
                }


            }
        }
    }

    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(BaseBatchBO<ExmOrderBO> req) {
        // TODO 执行前调用逻辑

    }
}