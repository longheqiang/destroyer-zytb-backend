package com.destroyer.will.service.exmStuwill;

import com.destroyer.core.entity.exmStuwill.bo.ExmStuwillQueryBO;
import com.destroyer.core.entity.exmStuwill.vo.ExmStuwillVO;
import java.util.Map;

 /**
 * 标题：学生志愿(获取Map集合服务接口)
 * 说明：学生志愿(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStuwillGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, ExmStuwillVO> getMapGroupById(ExmStuwillQueryBO req);
}