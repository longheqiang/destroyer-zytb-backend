package com.destroyer.will.ability.auth;

import com.destroyer.common.entity.auth.AuthBaseVO;
import com.destroyer.common.entity.auth.AuthStudentVO;
import com.destroyer.core.entity.wechat.bo.WechatUserCodeBO;

/**
 * 标题：IAuthStudentWechatLoginAbilityService
 * 说明：学生微信授权登录能力接口
 * 时间：2024/3/5
 * 作者：admin
 */
public interface IAuthStudentWechatLoginAbilityService {
    /**
     * 学生用户登录
     * @param req
     * @return
     */
    AuthBaseVO<AuthStudentVO> login(WechatUserCodeBO req);
}
