package com.destroyer.will.service.exmStufavoritewill;

import com.destroyer.core.entity.exmStufavoritewill.bo.ExmStufavoritewillQueryBO;
import com.destroyer.core.entity.exmStufavoritewill.vo.ExmStufavoritewillVO;
import java.util.List;

 /**
 * 标题：学生收藏志愿(查询详情服务接口)
 * 说明：学生收藏志愿(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmStufavoritewillGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    ExmStufavoritewillVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    ExmStufavoritewillVO getDetailBy(ExmStufavoritewillQueryBO req);
   
}