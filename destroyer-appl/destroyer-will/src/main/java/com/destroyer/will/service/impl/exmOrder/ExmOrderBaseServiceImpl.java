package com.destroyer.will.service.impl.exmOrder;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.exmOrder.ExmOrderPO;
import com.destroyer.will.mapper.ExmOrderMapper;
import com.destroyer.will.service.exmOrder.IExmOrderBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：消费订单(基础通用服务实现)
 * 说明：消费订单(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-13
 * 作者：admin
 */
@Service
public class  ExmOrderBaseServiceImpl extends ServiceImpl<ExmOrderMapper, ExmOrderPO> implements IExmOrderBaseService {
}