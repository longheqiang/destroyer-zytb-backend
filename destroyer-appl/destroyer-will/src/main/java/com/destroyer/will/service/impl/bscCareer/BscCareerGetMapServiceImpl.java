package com.destroyer.will.service.impl.bscCareer;

import com.destroyer.core.entity.bscCareer.bo.BscCareerQueryBO;
import com.destroyer.core.entity.bscCareer.vo.BscCareerVO;
import com.destroyer.will.service.bscCareer.IBscCareerGetListService;
import com.destroyer.will.service.bscCareer.IBscCareerGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：职业库(获取Map集合服务实现)
 * 说明：职业库(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class BscCareerGetMapServiceImpl implements IBscCareerGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IBscCareerGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, BscCareerVO> getMapGroupById(BscCareerQueryBO req) {
        Map<Long, BscCareerVO> rsp = new HashMap<>();
        List<BscCareerVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(BscCareerVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}