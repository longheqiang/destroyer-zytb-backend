package com.destroyer.will.mapper;

import com.destroyer.core.entity.exmOrder.ExmOrderPO;
import com.destroyer.core.entity.exmOrder.bo.ExmOrderQueryBO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

 /**
 * 标题：消费订单(持久化Mapper)
 * 说明：消费订单(持久化Mapper),默认继承MP的BaseMapper<PO>,特殊业务场景可自定义扩展
 * 时间：2024-3-13
 * 作者：admin
 */
@Mapper
public interface ExmOrderMapper extends BaseMapper<ExmOrderPO> {
    
    
        /**
     * 查询树结构数据新数据编号
     *
     * @param noh        头编号，首节点传0
     * @param length     长度
     * @param step       步长
     * @param columnName 列名称
     * @param exMap QueryBO扩展参数          
     * @return
     */
    String getTreeNo(@Param("noh") String noh, @Param("length") int length, @Param("step") int step, @Param("columnName") String columnName, @Param("exMap") ExmOrderQueryBO exMap);
}