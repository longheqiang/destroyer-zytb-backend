package com.destroyer.will.service.impl.exmStufavoritewill;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.exmStufavoritewill.ExmStufavoritewillPO;
import com.destroyer.will.mapper.ExmStufavoritewillMapper;
import com.destroyer.will.service.exmStufavoritewill.IExmStufavoritewillBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：学生收藏志愿(基础通用服务实现)
 * 说明：学生收藏志愿(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class  ExmStufavoritewillBaseServiceImpl extends ServiceImpl<ExmStufavoritewillMapper, ExmStufavoritewillPO> implements IExmStufavoritewillBaseService {
}