package com.destroyer.will.service.enlMajorplan;

import com.destroyer.core.entity.enlMajorplan.bo.EnlMajorplanQueryBO;
import com.destroyer.core.entity.enlMajorplan.vo.EnlMajorplanVO;
import java.util.List;

 /**
 * 标题：专业录取计划(查询详情服务接口)
 * 说明：专业录取计划(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajorplanGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    EnlMajorplanVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    EnlMajorplanVO getDetailBy(EnlMajorplanQueryBO req);
   
}