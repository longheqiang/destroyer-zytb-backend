package com.destroyer.will.service.exmStudentConsRecord;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.exmStudentConsRecord.bo.ExmStudentConsRecordQueryBO;
import com.destroyer.core.entity.exmStudentConsRecord.vo.ExmStudentConsRecordVO;
import java.util.List;

 /**
 * 标题：用户消费记录(获取列表服务接口)
 * 说明：用户消费记录(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmStudentConsRecordGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<ExmStudentConsRecordVO> getList(ExmStudentConsRecordQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<ExmStudentConsRecordVO> getPageList(ExmStudentConsRecordQueryBO req);
}