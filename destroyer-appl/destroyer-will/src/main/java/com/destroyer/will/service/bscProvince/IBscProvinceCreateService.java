package com.destroyer.will.service.bscProvince;

import com.destroyer.core.entity.bscProvince.bo.BscProvinceBO;

 /**
 * 标题：省份表(新增服务接口)
 * 说明：省份表(新增服务接口),自定义新增数据接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscProvinceCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(BscProvinceBO req);
}