package com.destroyer.will.service.exmWillreport;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.exmWillreport.bo.ExmWillreportQueryBO;
import com.destroyer.core.entity.exmWillreport.vo.ExmWillreportVO;
import java.util.List;

 /**
 * 标题：分析报告(获取列表服务接口)
 * 说明：分析报告(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillreportGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<ExmWillreportVO> getList(ExmWillreportQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<ExmWillreportVO> getPageList(ExmWillreportQueryBO req);
}