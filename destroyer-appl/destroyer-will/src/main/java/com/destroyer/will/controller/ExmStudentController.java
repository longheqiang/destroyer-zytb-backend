package com.destroyer.will.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.base.BaseBatchBO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.exmStudent.bo.ExmStudentBO;
import com.destroyer.core.entity.exmStudent.bo.ExmStudentQueryBO;
import com.destroyer.core.entity.exmStudent.vo.ExmStudentVO;
import com.destroyer.will.service.exmStudent.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 标题：志愿学生(控制层)
 * 说明：志愿学生(控制层),包括所有对外前端API接口
 * 时间：2024-3-1
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/will/exmStudent")
@Api(value = "ExmStudentController", tags = "志愿学生API")
public class ExmStudentController {


    /**
     * 基础服务
     */
    @Autowired
    private IExmStudentBaseService baseService;


    /**
     * 新增数据服务
     */
    @Autowired
    private IExmStudentCreateService createService;


    /**
     * 批量新增服务
     */
    @Autowired
    private IExmStudentBatchCreateService batchCreateService;


    /**
     * 删除数据服务
     */
    @Autowired
    private IExmStudentDeleteService deleteService;


    /**
     * 列表服务
     */
    @Autowired
    private IExmStudentGetListService getListService;


    /**
     * map服务
     */
    @Autowired
    private IExmStudentGetMapService getMapService;


    /**
     * 详情服务
     */
    @Autowired
    private IExmStudentGetDetailService getDetailService;


    /**
     * 编辑服务
     */
    @Autowired
    private IExmStudentEditService editService;


    /**
     * Excel服务
     */
    @Autowired
    private IExmStudentExcelService excelService;


    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody ExmStudentBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/batchCreate")
    @ApiOperation("批量新增API")
    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<ExmStudentBO> req) {
        List<Long> rsp = this.batchCreateService.batchCreate(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/deleteById")
    @ApiOperation("删除数据API(根据主键ID)")
    public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
        boolean rsp = this.deleteService.deleteById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/deleteBy")
    @ApiOperation("删除数据API(根据查询条件删除)")
    public Result<Boolean> deleteBy(@RequestBody ExmStudentQueryBO req) {
        boolean rsp = this.deleteService.batchDeleteBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<ExmStudentVO>> list(@RequestBody ExmStudentQueryBO req) {
        List<ExmStudentVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/mapGroupById")
    @ApiOperation("查询map(ID分组)API")
    public Result<Map<Long, ExmStudentVO>> mapGroupById(@RequestBody ExmStudentQueryBO req) {
        Map<Long, ExmStudentVO> rsp = this.getMapService.getMapGroupById(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<ExmStudentVO>> page(@RequestBody ExmStudentQueryBO req) {
        Page<ExmStudentVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<ExmStudentVO> detailBy(@RequestBody ExmStudentQueryBO req) {
        ExmStudentVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailById")
    @ApiOperation("根据ID查询详情API")
    public Result<ExmStudentVO> detailById(@RequestBody BaseIdBO req) {
        ExmStudentVO rsp = this.getDetailService.getDetailById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody ExmStudentBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }



    @GetMapping("/exportExcelModule")
    @ApiOperation("导出Excel模板API")
    @ResponseBody
    public void exportExcelModule(HttpServletResponse response) throws IOException {
        excelService.exportExcelModule(response);
    }


    @PostMapping("/importExcelData")
    @ApiOperation("导入Excel数据API")
    public Result<Boolean> importExcelData(@RequestParam("file") MultipartFile req) throws IOException {
        boolean rsp = excelService.importExcelData(req);
        return Result.success(rsp);
    }




}