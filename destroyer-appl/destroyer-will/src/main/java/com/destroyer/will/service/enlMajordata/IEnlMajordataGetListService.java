package com.destroyer.will.service.enlMajordata;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataQueryBO;
import com.destroyer.core.entity.enlMajordata.vo.EnlMajordataVO;
import java.util.List;

 /**
 * 标题：专业录取数据表(获取列表服务接口)
 * 说明：专业录取数据表(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajordataGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<EnlMajordataVO> getList(EnlMajordataQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<EnlMajordataVO> getPageList(EnlMajordataQueryBO req);
}