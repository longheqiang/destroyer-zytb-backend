package com.destroyer.will.service.impl.enlMajordata;

import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataQueryBO;
import com.destroyer.core.entity.enlMajordata.vo.EnlMajordataVO;
import com.destroyer.will.service.enlMajordata.IEnlMajordataGetDetailService;
import com.destroyer.will.service.enlMajordata.IEnlMajordataGetListService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

 /**
 * 标题：专业录取数据表(查询详情服务实现)
 * 说明：专业录取数据表(查询详情服务实现),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class EnlMajordataGetDetailServiceImpl implements IEnlMajordataGetDetailService {
    
    
    /**
     * 查询列表服务
     */
    @Autowired
    private IEnlMajordataGetListService getListService;
    /**
     * 根据主键ID查询详情
     * @param req
     * @return
     */
    @Override
    public EnlMajordataVO getDetailById(Long req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"查询详情入参ID不能为空");
        }
        EnlMajordataQueryBO queryBO = new EnlMajordataQueryBO();
        queryBO.setId(req);
        EnlMajordataVO rsp = this.getDetailBy(queryBO);
        return rsp;
    }
    
    
    /**
     * 根据条件查询详情
     * @param req
     * @return
     */
    @Override
    public EnlMajordataVO getDetailBy(EnlMajordataQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<EnlMajordataVO> list = getListService.getList(req);
        if (list.size() > 1) {
            throw new ServiceException(ResultEnum.FAILURE,"查询到的数据不唯一！");
        }
        EnlMajordataVO rsp = list.size() == 1? list.get(0):null;
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(EnlMajordataQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        } else {
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(EnlMajordataQueryBO req) {
        // TODO 执行前调用逻辑
    }
}