package com.destroyer.will.service.enlMajorplan;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.enlMajorplan.bo.EnlMajorplanBO;
import java.util.List;

 /**
 * 标题：专业录取计划(批量新增服务接口)
 * 说明：专业录取计划(批量新增服务接口),自定义批量新增据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajorplanBatchCreateService {
    
    
    /**
    * 批量新增数据
    * @param req
    * @return 新增数据的组件ID集合
    */
    List<Long> batchCreate(BaseBatchBO<EnlMajorplanBO> req);
}