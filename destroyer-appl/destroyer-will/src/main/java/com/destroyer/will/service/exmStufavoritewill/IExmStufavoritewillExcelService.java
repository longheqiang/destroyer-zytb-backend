package com.destroyer.will.service.exmStufavoritewill;

import com.destroyer.core.service.excel.IExcelBaseService;


/**
 * 标题：学生收藏志愿(Excel服务接口)
 * 说明：学生收藏志愿(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-7
 * 作者：admin
 */
public interface IExmStufavoritewillExcelService extends IExcelBaseService {



}