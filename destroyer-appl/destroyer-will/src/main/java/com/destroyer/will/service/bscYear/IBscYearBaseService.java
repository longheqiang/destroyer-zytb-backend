package com.destroyer.will.service.bscYear;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.bscYear.BscYearPO;

 /**
 * 标题：年份(基础通用服务接口)
 * 说明：年份(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscYearBaseService  extends IService<BscYearPO> {
}