package com.destroyer.will.service.exmStufavoritewill;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.exmStufavoritewill.ExmStufavoritewillPO;

 /**
 * 标题：学生收藏志愿(基础通用服务接口)
 * 说明：学生收藏志愿(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmStufavoritewillBaseService  extends IService<ExmStufavoritewillPO> {
}