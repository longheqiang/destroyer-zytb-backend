package com.destroyer.will.service.enlCollege;

import com.destroyer.core.entity.enlCollege.bo.EnlCollegeBO;

 /**
 * 标题：招生院校(编辑数据服务接口)
 * 说明：招生院校(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlCollegeEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(EnlCollegeBO req);
}