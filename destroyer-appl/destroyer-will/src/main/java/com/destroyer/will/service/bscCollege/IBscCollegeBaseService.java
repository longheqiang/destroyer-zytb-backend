package com.destroyer.will.service.bscCollege;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.bscCollege.BscCollegePO;

 /**
 * 标题：院校库(基础通用服务接口)
 * 说明：院校库(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscCollegeBaseService  extends IService<BscCollegePO> {
}