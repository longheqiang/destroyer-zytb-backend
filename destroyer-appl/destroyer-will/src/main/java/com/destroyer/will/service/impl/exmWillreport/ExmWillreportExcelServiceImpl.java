package com.destroyer.will.service.impl.exmWillreport;

import com.alibaba.excel.EasyExcel;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.excel.EasyExcelUtil;
import com.destroyer.common.excel.MyReadListener;
import com.destroyer.common.excel.ReadBack;
import com.destroyer.common.exception.ServerDataException;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.util.ResponseUtil;
import com.destroyer.core.entity.exmWillreport.bo.ExmWillreportBO;
import com.destroyer.core.entity.exmWillreport.bo.ExmWillreportExcelBO;
import com.destroyer.common.util.DateUtils;
import com.destroyer.will.service.exmWillreport.IExmWillreportBatchCreateService;
import com.destroyer.will.service.exmWillreport.IExmWillreportExcelService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 标题：分析报告(Excel服务实现)
 * 说明：分析报告(Excel服务实现),Excel相关服务
 * 时间：2024-3-7
 * 作者：admin
 */
@Service
public class ExmWillreportExcelServiceImpl implements IExmWillreportExcelService {


    /**
     * 批量新增服务
     */
    @Autowired
    private IExmWillreportBatchCreateService batchCreateService;


    private final static String SHEET_NAME = "分析报告";


    /**
     * 导出模板
     *
     * @param response
     * @return
     */
    @Override
    public boolean exportExcelModule(HttpServletResponse response) throws IOException {
        List<ExmWillreportExcelBO> rsp = new ArrayList<>();
        ExmWillreportExcelBO example = new ExmWillreportExcelBO().buildExportModule();
        rsp.add(example);
        String name = SHEET_NAME + "Excel模板";
        ResponseUtil.setExcelResponse(response, name);
        EasyExcelUtil.getExcelWriterBuilder(response.getOutputStream(), ExmWillreportExcelBO.class).sheet(SHEET_NAME).doWrite(rsp);
        return false;
    }


    /**
     * 导入数据
     * @param req
     * @return
     */
    @Override
    public boolean importExcelData(MultipartFile req) throws IOException {
        boolean rsp = false;
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        final List<ExmWillreportExcelBO> excelList = new ArrayList<>();
        //读取Excel内容
        EasyExcel.read(req.getInputStream(), ExmWillreportExcelBO.class,
                new MyReadListener<>((ReadBack<ExmWillreportExcelBO>) excelList::add)).sheet(SHEET_NAME).doRead();
        if (excelList.size() == 0) {
            throw new ServerDataException("excel 解析失败");
        }
        //保存数据
        this.saveData(excelList);
        return rsp;
    }
    /**
     * 保存数据
     * @param req
     * @return
     */
    private boolean saveData(List<ExmWillreportExcelBO> req){
        boolean rsp = false;
        BaseBatchBO<ExmWillreportBO> baseBatchBO = new BaseBatchBO<>();
        List<ExmWillreportBO>  boList = new ArrayList<>();
        for (ExmWillreportExcelBO excelBO:req){
            ExmWillreportBO bo = new ExmWillreportBO();
            //新增逻辑控制，内容拷贝
            BeanUtils.copyProperties(excelBO,bo);
            boList.add(bo);
        }
        if (boList.size() > 0) {
            baseBatchBO.setList(boList);
            rsp = batchCreateService.batchCreate(baseBatchBO).size() > 0;
        }
        return rsp;
    }
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(MultipartFile req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
        }
    }
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(MultipartFile req) {
        // TODO 执行前调用逻辑
    }
}