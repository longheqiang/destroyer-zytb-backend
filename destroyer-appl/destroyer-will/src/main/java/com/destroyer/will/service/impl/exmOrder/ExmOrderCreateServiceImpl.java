package com.destroyer.will.service.impl.exmOrder;

import cn.hutool.core.util.RandomUtil;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.DateUtils;
import com.destroyer.core.entity.exmOrder.ExmOrderPO;
import com.destroyer.core.entity.exmOrder.bo.ExmOrderBO;
import com.destroyer.will.service.exmOrder.IExmOrderBaseService;
import com.destroyer.will.service.exmOrder.IExmOrderCreateService;
import com.destroyer.core.util.UserUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;

/**
 * 标题：消费订单(新增服务实现)
 * 说明：消费订单(新增服务实现),自定义新增数据实现
 * 时间：2024-3-13
 * 作者：admin
 */
@Service
public class ExmOrderCreateServiceImpl implements IExmOrderCreateService {


    /**
     * 基础通用服务
     */
    @Autowired
    private IExmOrderBaseService baseService;


    /**
     * 新增数据
     *
     * @param req
     * @return
     */
    @Override
    public Long create(ExmOrderBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造PO对象(根据业务对象)
        ExmOrderPO po = new ExmOrderPO(req);
        //调用基础保存服务
        boolean states = this.baseService.save(po);
        if (!states) {
            throw new ServiceException(ResultEnum.FAILURE, "新增数据失败");
        }
        Long rsp = po.getId();
        return rsp;
    }

    /**
     * 类通用入参校验
     *
     * @param req 入参
     */
    private void validParams(ExmOrderBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
            if(null == req.getStuId()){
                throw new ServiceException(ResultEnum.PARAM_MISS,"入参[学生ID]不能为空!");
            }
            if(StringUtils.isBlank(req.getOrderNo())){
                throw new ServiceException(ResultEnum.PARAM_MISS,"入参[订单编号]不能为空!");
            }
            if(null == req.getTotal()){
                throw new ServiceException(ResultEnum.PARAM_MISS,"入参[金额（单位：分）]不能为空!");
            }

        }
    }

    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(ExmOrderBO req) {
        // TODO 执行前调用逻辑

    }
}