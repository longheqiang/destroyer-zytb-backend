package com.destroyer.will.service.impl.enlCollege;

import com.destroyer.core.entity.enlCollege.bo.EnlCollegeQueryBO;
import com.destroyer.core.entity.enlCollege.vo.EnlCollegeVO;
import com.destroyer.will.service.enlCollege.IEnlCollegeGetListService;
import com.destroyer.will.service.enlCollege.IEnlCollegeGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：招生院校(获取Map集合服务实现)
 * 说明：招生院校(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class EnlCollegeGetMapServiceImpl implements IEnlCollegeGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IEnlCollegeGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, EnlCollegeVO> getMapGroupById(EnlCollegeQueryBO req) {
        Map<Long, EnlCollegeVO> rsp = new HashMap<>();
        List<EnlCollegeVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(EnlCollegeVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}