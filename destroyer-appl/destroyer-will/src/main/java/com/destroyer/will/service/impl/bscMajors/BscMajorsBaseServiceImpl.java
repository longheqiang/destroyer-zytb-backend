package com.destroyer.will.service.impl.bscMajors;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.bscMajors.BscMajorsPO;
import com.destroyer.will.mapper.BscMajorsMapper;
import com.destroyer.will.service.bscMajors.IBscMajorsBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：专业库(基础通用服务实现)
 * 说明：专业库(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class  BscMajorsBaseServiceImpl extends ServiceImpl<BscMajorsMapper, BscMajorsPO> implements IBscMajorsBaseService {
}