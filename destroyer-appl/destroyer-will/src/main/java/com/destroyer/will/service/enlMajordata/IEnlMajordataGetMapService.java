package com.destroyer.will.service.enlMajordata;

import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataQueryBO;
import com.destroyer.core.entity.enlMajordata.vo.EnlMajordataVO;
import java.util.Map;

 /**
 * 标题：专业录取数据表(获取Map集合服务接口)
 * 说明：专业录取数据表(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajordataGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, EnlMajordataVO> getMapGroupById(EnlMajordataQueryBO req);
}