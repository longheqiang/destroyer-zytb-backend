package com.destroyer.will.service.exmStufavoritewill;

import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.exmStufavoritewill.bo.ExmStufavoritewillQueryBO;

/**
 * 标题：学生收藏志愿(删除数据服务接口)
 * 说明：学生收藏志愿(删除数据服务接口),自定义删除数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmStufavoritewillDeleteService {
    /**
     * 删除数据（根据主键ID）
     * @param req
     * @return
     */
    boolean deleteById(Long req);
    
    
    /**
     * 删除数据（根据主键ID集合批量删除）
     * @param req
     * @return
     */
    boolean batchDeleteByIds(BaseIdListBO req);
    
    
     /**
     * 删除数据（根据查询条件）
     * @param req
     * @return
     */
    boolean batchDeleteBy(ExmStufavoritewillQueryBO req);
}