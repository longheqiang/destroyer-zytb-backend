package com.destroyer.will.service.impl.bscCollege;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.bscCollege.BscCollegePO;
import com.destroyer.core.entity.bscCollege.bo.BscCollegeBO;
import com.destroyer.will.service.bscCollege.IBscCollegeBaseService;
import com.destroyer.will.service.bscCollege.IBscCollegeEditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：院校库(编辑数据服务实现)
 * 说明：院校库(编辑数据服务实现),自定义编辑数据服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class BscCollegeEditServiceImpl implements IBscCollegeEditService {
    
    
     /**
     * 基础通用服务
     */
    @Autowired
    private IBscCollegeBaseService baseService;
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    @Override
    public boolean editById(BscCollegeBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //入参处理
        BscCollegePO po = new BscCollegePO(req);
        //更新数据
        boolean rsp = this.baseService.updateById(po);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(BscCollegeBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
	
        if (null == req.getId()) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "更新数据入参ID不能为空");
        }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(BscCollegeBO req) {
        // TODO 执行前调用逻辑
    }
}