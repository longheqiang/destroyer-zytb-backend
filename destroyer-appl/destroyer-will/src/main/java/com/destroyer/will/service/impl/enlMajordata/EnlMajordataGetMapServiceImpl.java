package com.destroyer.will.service.impl.enlMajordata;

import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataQueryBO;
import com.destroyer.core.entity.enlMajordata.vo.EnlMajordataVO;
import com.destroyer.will.service.enlMajordata.IEnlMajordataGetListService;
import com.destroyer.will.service.enlMajordata.IEnlMajordataGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：专业录取数据表(获取Map集合服务实现)
 * 说明：专业录取数据表(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class EnlMajordataGetMapServiceImpl implements IEnlMajordataGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IEnlMajordataGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, EnlMajordataVO> getMapGroupById(EnlMajordataQueryBO req) {
        Map<Long, EnlMajordataVO> rsp = new HashMap<>();
        List<EnlMajordataVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(EnlMajordataVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}