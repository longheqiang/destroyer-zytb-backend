package com.destroyer.will.service.impl.bscYear;

import com.destroyer.core.entity.bscYear.bo.BscYearQueryBO;
import com.destroyer.core.entity.bscYear.vo.BscYearVO;
import com.destroyer.will.service.bscYear.IBscYearGetListService;
import com.destroyer.will.service.bscYear.IBscYearGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：年份(获取Map集合服务实现)
 * 说明：年份(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class BscYearGetMapServiceImpl implements IBscYearGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IBscYearGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, BscYearVO> getMapGroupById(BscYearQueryBO req) {
        Map<Long, BscYearVO> rsp = new HashMap<>();
        List<BscYearVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(BscYearVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}