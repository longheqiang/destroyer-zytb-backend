package com.destroyer.will.service.impl.exmWillcondition;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.exmWillcondition.ExmWillconditionPO;
import com.destroyer.core.entity.exmWillcondition.bo.ExmWillconditionBO;
import com.destroyer.will.service.exmWillcondition.IExmWillconditionBaseService;
import com.destroyer.will.service.exmWillcondition.IExmWillconditionEditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：学生志愿条件(编辑数据服务实现)
 * 说明：学生志愿条件(编辑数据服务实现),自定义编辑数据服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class ExmWillconditionEditServiceImpl implements IExmWillconditionEditService {
    
    
     /**
     * 基础通用服务
     */
    @Autowired
    private IExmWillconditionBaseService baseService;
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    @Override
    public boolean editById(ExmWillconditionBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //入参处理
        ExmWillconditionPO po = new ExmWillconditionPO(req);
        //更新数据
        boolean rsp = this.baseService.updateById(po);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(ExmWillconditionBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
	
        if (null == req.getId()) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "更新数据入参ID不能为空");
        }
	
	
	
	
	
	
	
	
	
	
	
	
	
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(ExmWillconditionBO req) {
        // TODO 执行前调用逻辑
    }
}