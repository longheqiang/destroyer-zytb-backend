package com.destroyer.will.service.exmWillcondition;

import com.destroyer.core.entity.exmWillcondition.bo.ExmWillconditionBO;

 /**
 * 标题：学生志愿条件(新增服务接口)
 * 说明：学生志愿条件(新增服务接口),自定义新增数据接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillconditionCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(ExmWillconditionBO req);
}