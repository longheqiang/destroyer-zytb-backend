package com.destroyer.will.service.bscProvince;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.bscProvince.bo.BscProvinceQueryBO;
import com.destroyer.core.entity.bscProvince.vo.BscProvinceVO;
import java.util.List;

 /**
 * 标题：省份表(获取列表服务接口)
 * 说明：省份表(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscProvinceGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<BscProvinceVO> getList(BscProvinceQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<BscProvinceVO> getPageList(BscProvinceQueryBO req);
}