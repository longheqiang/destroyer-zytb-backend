package com.destroyer.will.service.enlMajorplan;

import com.destroyer.core.service.excel.IExcelBaseService;


/**
 * 标题：专业录取计划(Excel服务接口)
 * 说明：专业录取计划(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-7
 * 作者：admin
 */
public interface IEnlMajorplanExcelService extends IExcelBaseService {


}