package com.destroyer.will.service.exmStuwill;

import com.destroyer.core.service.excel.IExcelBaseService;

/**
 * 标题：学生志愿(Excel服务接口)
 * 说明：学生志愿(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-7
 * 作者：admin
 */
public interface IExmStuwillExcelService extends IExcelBaseService {



}