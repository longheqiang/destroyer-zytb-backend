package com.destroyer.will.service.exmWillreport;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.exmWillreport.ExmWillreportPO;

 /**
 * 标题：分析报告(基础通用服务接口)
 * 说明：分析报告(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillreportBaseService  extends IService<ExmWillreportPO> {
}