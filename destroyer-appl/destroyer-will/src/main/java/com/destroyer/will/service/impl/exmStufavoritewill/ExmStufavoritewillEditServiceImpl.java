package com.destroyer.will.service.impl.exmStufavoritewill;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.exmStufavoritewill.ExmStufavoritewillPO;
import com.destroyer.core.entity.exmStufavoritewill.bo.ExmStufavoritewillBO;
import com.destroyer.will.service.exmStufavoritewill.IExmStufavoritewillBaseService;
import com.destroyer.will.service.exmStufavoritewill.IExmStufavoritewillEditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：学生收藏志愿(编辑数据服务实现)
 * 说明：学生收藏志愿(编辑数据服务实现),自定义编辑数据服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class ExmStufavoritewillEditServiceImpl implements IExmStufavoritewillEditService {
    
    
     /**
     * 基础通用服务
     */
    @Autowired
    private IExmStufavoritewillBaseService baseService;
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    @Override
    public boolean editById(ExmStufavoritewillBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //入参处理
        ExmStufavoritewillPO po = new ExmStufavoritewillPO(req);
        //更新数据
        boolean rsp = this.baseService.updateById(po);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(ExmStufavoritewillBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
	
        if (null == req.getId()) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "更新数据入参ID不能为空");
        }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(ExmStufavoritewillBO req) {
        // TODO 执行前调用逻辑
    }
}