package com.destroyer.will.service.bscProvince;

import com.destroyer.core.entity.bscProvince.bo.BscProvinceQueryBO;
import com.destroyer.core.entity.bscProvince.vo.BscProvinceVO;
import java.util.Map;

 /**
 * 标题：省份表(获取Map集合服务接口)
 * 说明：省份表(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscProvinceGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, BscProvinceVO> getMapGroupById(BscProvinceQueryBO req);
}