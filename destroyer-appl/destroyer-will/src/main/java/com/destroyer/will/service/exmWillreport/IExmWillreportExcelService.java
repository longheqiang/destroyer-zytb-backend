package com.destroyer.will.service.exmWillreport;

import com.destroyer.core.service.excel.IExcelBaseService;

/**
 * 标题：分析报告(Excel服务接口)
 * 说明：分析报告(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-7
 * 作者：admin
 */
public interface IExmWillreportExcelService extends IExcelBaseService {



}