package com.destroyer.will.service.exmStudent;

import com.destroyer.core.entity.exmStudent.bo.ExmStudentBO;

 /**
 * 标题：志愿学生(编辑数据服务接口)
 * 说明：志愿学生(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStudentEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(ExmStudentBO req);
}