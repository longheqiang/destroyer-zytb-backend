package com.destroyer.will.service.impl.enlCollege;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.enlCollege.EnlCollegePO;
import com.destroyer.core.entity.enlCollege.bo.EnlCollegeBO;
import com.destroyer.will.service.enlCollege.IEnlCollegeBaseService;
import com.destroyer.will.service.enlCollege.IEnlCollegeCreateService;
import com.destroyer.core.util.UserUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;

 /**
 * 标题：招生院校(新增服务实现)
 * 说明：招生院校(新增服务实现),自定义新增数据实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class EnlCollegeCreateServiceImpl implements IEnlCollegeCreateService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private IEnlCollegeBaseService baseService;
    
    
    /**
     * 新增数据
     * @param req
     * @return
     */
    @Override
    public Long create(EnlCollegeBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造PO对象(根据业务对象)
        EnlCollegePO po = new EnlCollegePO(req);
        //调用基础保存服务
        boolean states = this.baseService.save(po);
        if (!states){
            throw new ServiceException(ResultEnum.FAILURE, "新增数据失败");
        }
        Long rsp = po.getId();
        return rsp;
    }
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(EnlCollegeBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
        }
    }
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(EnlCollegeBO req) {
        // TODO 执行前调用逻辑
    }
}