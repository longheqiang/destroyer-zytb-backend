package com.destroyer.will.service.bscCareer;

import com.destroyer.core.service.excel.IExcelBaseService;


/**
 * 标题：职业库(Excel服务接口)
 * 说明：职业库(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-7
 * 作者：admin
 */
public interface IBscCareerExcelService extends IExcelBaseService {


}