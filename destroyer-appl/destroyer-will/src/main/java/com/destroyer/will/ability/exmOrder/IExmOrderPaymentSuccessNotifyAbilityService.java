package com.destroyer.will.ability.exmOrder;

import com.destroyer.core.entity.wechat.vo.WechatTransactionVO;

/**
 * 标题：IExmOrderPaymentSuccessNotifyAbilityService
 * 说明：支付成功回调业处理
 * 时间：2024/3/13
 * 作者：admin
 */
public interface IExmOrderPaymentSuccessNotifyAbilityService {


    /**
     * 支付成功回调业处理
     * @param req
     * @return
     */
    Boolean paymentSuccessNotify(WechatTransactionVO req);
}
