package com.destroyer.will.service.impl.enlCollege;

import com.destroyer.core.entity.enlCollege.bo.EnlCollegeQueryBO;
import com.destroyer.core.entity.enlCollege.vo.EnlCollegeVO;
import com.destroyer.will.service.enlCollege.IEnlCollegeGetDetailService;
import com.destroyer.will.service.enlCollege.IEnlCollegeGetListService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

 /**
 * 标题：招生院校(查询详情服务实现)
 * 说明：招生院校(查询详情服务实现),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class EnlCollegeGetDetailServiceImpl implements IEnlCollegeGetDetailService {
    
    
    /**
     * 查询列表服务
     */
    @Autowired
    private IEnlCollegeGetListService getListService;
    /**
     * 根据主键ID查询详情
     * @param req
     * @return
     */
    @Override
    public EnlCollegeVO getDetailById(Long req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"查询详情入参ID不能为空");
        }
        EnlCollegeQueryBO queryBO = new EnlCollegeQueryBO();
        queryBO.setId(req);
        EnlCollegeVO rsp = this.getDetailBy(queryBO);
        return rsp;
    }
    
    
    /**
     * 根据条件查询详情
     * @param req
     * @return
     */
    @Override
    public EnlCollegeVO getDetailBy(EnlCollegeQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<EnlCollegeVO> list = getListService.getList(req);
        if (list.size() > 1) {
            throw new ServiceException(ResultEnum.FAILURE,"查询到的数据不唯一！");
        }
        EnlCollegeVO rsp = list.size() == 1? list.get(0):null;
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(EnlCollegeQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        } else {
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(EnlCollegeQueryBO req) {
        // TODO 执行前调用逻辑
    }
}