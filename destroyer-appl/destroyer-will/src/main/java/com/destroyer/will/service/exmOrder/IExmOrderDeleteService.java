package com.destroyer.will.service.exmOrder;

import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.exmOrder.bo.ExmOrderQueryBO;

/**
 * 标题：消费订单(删除数据服务接口)
 * 说明：消费订单(删除数据服务接口),自定义删除数据服务接口
 * 时间：2024-3-13
 * 作者：admin
 */
public interface IExmOrderDeleteService {
    /**
     * 删除数据（根据主键ID）
     * @param req
     * @return
     */
    boolean deleteById(Long req);
    
    
    /**
     * 删除数据（根据主键ID集合批量删除）
     * @param req
     * @return
     */
    boolean batchDeleteByIds(BaseIdListBO req);
    
    
     /**
     * 删除数据（根据查询条件）
     * @param req
     * @return
     */
    boolean batchDeleteBy(ExmOrderQueryBO req);
}