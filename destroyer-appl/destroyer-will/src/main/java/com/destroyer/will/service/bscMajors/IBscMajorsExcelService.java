package com.destroyer.will.service.bscMajors;

import com.destroyer.core.service.excel.IExcelBaseService;


/**
 * 标题：专业库(Excel服务接口)
 * 说明：专业库(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-7
 * 作者：admin
 */
public interface IBscMajorsExcelService extends IExcelBaseService {


}