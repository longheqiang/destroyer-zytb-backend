package com.destroyer.will.service.impl.enlMajorplan;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.enlMajorplan.EnlMajorplanPO;
import com.destroyer.will.mapper.EnlMajorplanMapper;
import com.destroyer.will.service.enlMajorplan.IEnlMajorplanBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：专业录取计划(基础通用服务实现)
 * 说明：专业录取计划(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class  EnlMajorplanBaseServiceImpl extends ServiceImpl<EnlMajorplanMapper, EnlMajorplanPO> implements IEnlMajorplanBaseService {
}