package com.destroyer.will.service.exmWillreport;

import com.destroyer.core.entity.exmWillreport.bo.ExmWillreportBO;

 /**
 * 标题：分析报告(新增服务接口)
 * 说明：分析报告(新增服务接口),自定义新增数据接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillreportCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(ExmWillreportBO req);
}