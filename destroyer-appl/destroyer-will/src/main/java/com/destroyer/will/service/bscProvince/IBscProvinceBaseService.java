package com.destroyer.will.service.bscProvince;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.bscProvince.BscProvincePO;

 /**
 * 标题：省份表(基础通用服务接口)
 * 说明：省份表(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscProvinceBaseService  extends IService<BscProvincePO> {
}