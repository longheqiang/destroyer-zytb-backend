package com.destroyer.will.service.enlMajorplan;

import com.destroyer.core.entity.enlMajorplan.bo.EnlMajorplanBO;

 /**
 * 标题：专业录取计划(新增服务接口)
 * 说明：专业录取计划(新增服务接口),自定义新增数据接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajorplanCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(EnlMajorplanBO req);
}