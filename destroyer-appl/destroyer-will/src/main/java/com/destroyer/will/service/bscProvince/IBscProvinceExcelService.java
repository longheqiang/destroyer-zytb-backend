package com.destroyer.will.service.bscProvince;

import com.destroyer.core.service.excel.IExcelBaseService;

/**
 * 标题：省份表(Excel服务接口)
 * 说明：省份表(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-7
 * 作者：admin
 */
public interface IBscProvinceExcelService extends IExcelBaseService {

}