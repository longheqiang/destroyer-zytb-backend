package com.destroyer.will.service.impl.bscCollege;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.bscCollege.BscCollegePO;
import com.destroyer.will.mapper.BscCollegeMapper;
import com.destroyer.will.service.bscCollege.IBscCollegeBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：院校库(基础通用服务实现)
 * 说明：院校库(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class  BscCollegeBaseServiceImpl extends ServiceImpl<BscCollegeMapper, BscCollegePO> implements IBscCollegeBaseService {
}