package com.destroyer.will.service.bscCollege;

import com.destroyer.core.entity.bscCollege.bo.BscCollegeBO;

 /**
 * 标题：院校库(编辑数据服务接口)
 * 说明：院校库(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscCollegeEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(BscCollegeBO req);
}