package com.destroyer.will.service.bscCareer;

import com.destroyer.core.entity.bscCareer.bo.BscCareerBO;

 /**
 * 标题：职业库(编辑数据服务接口)
 * 说明：职业库(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscCareerEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(BscCareerBO req);
}