package com.destroyer.will.service.enlMajordata;

import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataQueryBO;
import com.destroyer.core.entity.enlMajordata.vo.EnlMajordataVO;
import java.util.List;

 /**
 * 标题：专业录取数据表(查询详情服务接口)
 * 说明：专业录取数据表(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajordataGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    EnlMajordataVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    EnlMajordataVO getDetailBy(EnlMajordataQueryBO req);
   
}