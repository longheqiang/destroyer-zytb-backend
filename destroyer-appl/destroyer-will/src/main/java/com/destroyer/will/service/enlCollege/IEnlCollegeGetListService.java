package com.destroyer.will.service.enlCollege;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.enlCollege.bo.EnlCollegeQueryBO;
import com.destroyer.core.entity.enlCollege.vo.EnlCollegeVO;
import java.util.List;

 /**
 * 标题：招生院校(获取列表服务接口)
 * 说明：招生院校(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlCollegeGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<EnlCollegeVO> getList(EnlCollegeQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<EnlCollegeVO> getPageList(EnlCollegeQueryBO req);
}