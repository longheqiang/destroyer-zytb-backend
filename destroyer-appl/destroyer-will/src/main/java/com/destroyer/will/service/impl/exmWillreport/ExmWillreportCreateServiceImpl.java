package com.destroyer.will.service.impl.exmWillreport;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.exmWillreport.ExmWillreportPO;
import com.destroyer.core.entity.exmWillreport.bo.ExmWillreportBO;
import com.destroyer.will.service.exmWillreport.IExmWillreportBaseService;
import com.destroyer.will.service.exmWillreport.IExmWillreportCreateService;
import com.destroyer.core.util.UserUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;

 /**
 * 标题：分析报告(新增服务实现)
 * 说明：分析报告(新增服务实现),自定义新增数据实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class ExmWillreportCreateServiceImpl implements IExmWillreportCreateService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private IExmWillreportBaseService baseService;
    
    
    /**
     * 新增数据
     * @param req
     * @return
     */
    @Override
    public Long create(ExmWillreportBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造PO对象(根据业务对象)
        ExmWillreportPO po = new ExmWillreportPO(req);
        //调用基础保存服务
        boolean states = this.baseService.save(po);
        if (!states){
            throw new ServiceException(ResultEnum.FAILURE, "新增数据失败");
        }
        Long rsp = po.getId();
        return rsp;
    }
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(ExmWillreportBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
        }
    }
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(ExmWillreportBO req) {
        // TODO 执行前调用逻辑
    }
}