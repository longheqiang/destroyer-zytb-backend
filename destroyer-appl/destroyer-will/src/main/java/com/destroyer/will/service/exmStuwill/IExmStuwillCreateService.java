package com.destroyer.will.service.exmStuwill;

import com.destroyer.core.entity.exmStuwill.bo.ExmStuwillBO;

 /**
 * 标题：学生志愿(新增服务接口)
 * 说明：学生志愿(新增服务接口),自定义新增数据接口
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStuwillCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(ExmStuwillBO req);
}