package com.destroyer.will.service.bscProvince;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.bscProvince.bo.BscProvinceBO;
import java.util.List;

 /**
 * 标题：省份表(批量新增服务接口)
 * 说明：省份表(批量新增服务接口),自定义批量新增据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscProvinceBatchCreateService {
    
    
    /**
    * 批量新增数据
    * @param req
    * @return 新增数据的组件ID集合
    */
    List<Long> batchCreate(BaseBatchBO<BscProvinceBO> req);
}