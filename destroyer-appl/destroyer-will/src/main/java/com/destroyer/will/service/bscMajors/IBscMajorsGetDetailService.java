package com.destroyer.will.service.bscMajors;

import com.destroyer.core.entity.bscMajors.bo.BscMajorsQueryBO;
import com.destroyer.core.entity.bscMajors.vo.BscMajorsVO;
import java.util.List;

 /**
 * 标题：专业库(查询详情服务接口)
 * 说明：专业库(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscMajorsGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    BscMajorsVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    BscMajorsVO getDetailBy(BscMajorsQueryBO req);
   
}