package com.destroyer.will.service.impl.bscProvince;

import com.destroyer.core.entity.bscProvince.bo.BscProvinceQueryBO;
import com.destroyer.core.entity.bscProvince.vo.BscProvinceVO;
import com.destroyer.will.service.bscProvince.IBscProvinceGetListService;
import com.destroyer.will.service.bscProvince.IBscProvinceGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：省份表(获取Map集合服务实现)
 * 说明：省份表(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class BscProvinceGetMapServiceImpl implements IBscProvinceGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IBscProvinceGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, BscProvinceVO> getMapGroupById(BscProvinceQueryBO req) {
        Map<Long, BscProvinceVO> rsp = new HashMap<>();
        List<BscProvinceVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(BscProvinceVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}