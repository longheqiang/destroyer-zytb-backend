package com.destroyer.will.service.bscProvince;

import com.destroyer.core.entity.bscProvince.bo.BscProvinceBO;

 /**
 * 标题：省份表(编辑数据服务接口)
 * 说明：省份表(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscProvinceEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(BscProvinceBO req);
}