package com.destroyer.will.service.bscYear;

import com.destroyer.core.entity.bscYear.bo.BscYearQueryBO;
import com.destroyer.core.entity.bscYear.vo.BscYearVO;
import java.util.List;

 /**
 * 标题：年份(查询详情服务接口)
 * 说明：年份(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscYearGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    BscYearVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    BscYearVO getDetailBy(BscYearQueryBO req);
   
}