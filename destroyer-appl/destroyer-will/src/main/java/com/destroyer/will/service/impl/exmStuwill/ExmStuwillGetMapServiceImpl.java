package com.destroyer.will.service.impl.exmStuwill;

import com.destroyer.core.entity.exmStuwill.bo.ExmStuwillQueryBO;
import com.destroyer.core.entity.exmStuwill.vo.ExmStuwillVO;
import com.destroyer.will.service.exmStuwill.IExmStuwillGetListService;
import com.destroyer.will.service.exmStuwill.IExmStuwillGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：学生志愿(获取Map集合服务实现)
 * 说明：学生志愿(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-1
 * 作者：admin
 */
@Service
public class ExmStuwillGetMapServiceImpl implements IExmStuwillGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IExmStuwillGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, ExmStuwillVO> getMapGroupById(ExmStuwillQueryBO req) {
        Map<Long, ExmStuwillVO> rsp = new HashMap<>();
        List<ExmStuwillVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(ExmStuwillVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}