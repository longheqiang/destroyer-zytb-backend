package com.destroyer.will.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataBO;
import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataQueryBO;
import com.destroyer.core.entity.enlMajordata.vo.EnlMajordataVO;
import com.destroyer.will.service.enlMajordata.*;

/**
 * 标题：专业录取数据表(控制层)
 * 说明：专业录取数据表(控制层),包括所有对外前端API接口
 * 时间：2024-3-7
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/will/enlMajordata")
@Api(value = "EnlMajordataController", tags = "专业录取数据表API")
public class EnlMajordataController {


    /**
     * 基础服务
     */
    @Autowired
    private IEnlMajordataBaseService baseService;


    /**
     * 新增数据服务
     */
    @Autowired
    private IEnlMajordataCreateService createService;



    /**
     * 批量新增服务
     */
    @Autowired
    private IEnlMajordataBatchCreateService batchCreateService;



    /**
     * 删除数据服务
     */
    @Autowired
    private IEnlMajordataDeleteService deleteService;


    /**
     * 列表服务
     */
    @Autowired
    private IEnlMajordataGetListService getListService;


    /**
     * map服务
     */
    @Autowired
    private IEnlMajordataGetMapService getMapService;


    /**
     * 详情服务
     */
    @Autowired
    private IEnlMajordataGetDetailService getDetailService;


    /**
     * 编辑服务
     */
    @Autowired
    private IEnlMajordataEditService editService;


    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody EnlMajordataBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }


    /**
     * Excel服务
     */
    @Autowired
    private IEnlMajordataExcelService excelService;



    @PostMapping(value = "/batchCreate")
    @ApiOperation("批量新增API")
    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<EnlMajordataBO> req) {
        List<Long> rsp = this.batchCreateService.batchCreate(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/deleteById")
    @ApiOperation("删除数据API(根据主键ID)")
    public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
        boolean rsp = this.deleteService.deleteById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }



    @PostMapping(value = "/deleteBy")
    @ApiOperation("删除数据API(根据查询条件删除)")
    public Result<Boolean> deleteBy(@RequestBody EnlMajordataQueryBO req) {
        boolean rsp = this.deleteService.batchDeleteBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<EnlMajordataVO>> list(@RequestBody EnlMajordataQueryBO req) {
        List<EnlMajordataVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/mapGroupById")
    @ApiOperation("查询map(ID分组)API")
    public Result<Map<Long, EnlMajordataVO>> mapGroupById(@RequestBody EnlMajordataQueryBO req) {
        Map<Long, EnlMajordataVO> rsp = this.getMapService.getMapGroupById(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<EnlMajordataVO>> page(@RequestBody EnlMajordataQueryBO req) {
        Page<EnlMajordataVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<EnlMajordataVO> detailBy(@RequestBody EnlMajordataQueryBO req) {
        EnlMajordataVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailById")
    @ApiOperation("根据ID查询详情API")
    public Result<EnlMajordataVO> detailById(@RequestBody BaseIdBO req) {
        EnlMajordataVO rsp = this.getDetailService.getDetailById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody EnlMajordataBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }


    @GetMapping("/exportExcelModule")
    @ApiOperation("导出Excel模板API")
    @ResponseBody
    public void exportExcelModule(HttpServletResponse response) throws IOException {
        excelService.exportExcelModule(response);
    }


    @PostMapping("/importExcelData")
    @ApiOperation("导入Excel数据API")
    public Result<Boolean> importExcelData(@RequestParam("file") MultipartFile req) throws IOException {
        boolean rsp = excelService.importExcelData(req);
        return Result.success(rsp);
    }
}