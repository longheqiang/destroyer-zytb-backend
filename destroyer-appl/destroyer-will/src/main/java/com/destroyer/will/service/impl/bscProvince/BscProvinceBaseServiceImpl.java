package com.destroyer.will.service.impl.bscProvince;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.bscProvince.BscProvincePO;
import com.destroyer.will.mapper.BscProvinceMapper;
import com.destroyer.will.service.bscProvince.IBscProvinceBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：省份表(基础通用服务实现)
 * 说明：省份表(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class  BscProvinceBaseServiceImpl extends ServiceImpl<BscProvinceMapper, BscProvincePO> implements IBscProvinceBaseService {
}