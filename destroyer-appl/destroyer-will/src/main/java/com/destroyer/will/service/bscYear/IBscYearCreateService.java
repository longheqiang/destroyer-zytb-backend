package com.destroyer.will.service.bscYear;

import com.destroyer.core.entity.bscYear.bo.BscYearBO;

 /**
 * 标题：年份(新增服务接口)
 * 说明：年份(新增服务接口),自定义新增数据接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscYearCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(BscYearBO req);
}