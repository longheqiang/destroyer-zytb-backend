package com.destroyer.will.service.impl.exmWillcondition;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.exmWillcondition.ExmWillconditionPO;
import com.destroyer.core.entity.exmWillcondition.bo.ExmWillconditionBO;
import com.destroyer.will.service.exmWillcondition.IExmWillconditionBaseService;
import com.destroyer.will.service.exmWillcondition.IExmWillconditionCreateService;
import com.destroyer.core.util.UserUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;

 /**
 * 标题：学生志愿条件(新增服务实现)
 * 说明：学生志愿条件(新增服务实现),自定义新增数据实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class ExmWillconditionCreateServiceImpl implements IExmWillconditionCreateService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private IExmWillconditionBaseService baseService;
    
    
    /**
     * 新增数据
     * @param req
     * @return
     */
    @Override
    public Long create(ExmWillconditionBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造PO对象(根据业务对象)
        ExmWillconditionPO po = new ExmWillconditionPO(req);
        //调用基础保存服务
        boolean states = this.baseService.save(po);
        if (!states){
            throw new ServiceException(ResultEnum.FAILURE, "新增数据失败");
        }
        Long rsp = po.getId();
        return rsp;
    }
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(ExmWillconditionBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
    if(null == req.getStuid()){
        throw new ServiceException(ResultEnum.PARAM_MISS,"入参[学生id]不能为空!");
    }
        }
    }
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(ExmWillconditionBO req) {
        // TODO 执行前调用逻辑
    }
}