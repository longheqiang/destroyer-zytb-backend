package com.destroyer.will.service.exmOrder;

import com.destroyer.core.entity.exmOrder.bo.ExmOrderQueryBO;
import com.destroyer.core.entity.exmOrder.vo.ExmOrderVO;
import java.util.List;

 /**
 * 标题：消费订单(查询详情服务接口)
 * 说明：消费订单(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-13
 * 作者：admin
 */
public interface IExmOrderGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    ExmOrderVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    ExmOrderVO getDetailBy(ExmOrderQueryBO req);
   
}