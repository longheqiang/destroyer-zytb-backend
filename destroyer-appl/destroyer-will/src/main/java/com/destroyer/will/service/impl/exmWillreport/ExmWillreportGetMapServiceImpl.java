package com.destroyer.will.service.impl.exmWillreport;

import com.destroyer.core.entity.exmWillreport.bo.ExmWillreportQueryBO;
import com.destroyer.core.entity.exmWillreport.vo.ExmWillreportVO;
import com.destroyer.will.service.exmWillreport.IExmWillreportGetListService;
import com.destroyer.will.service.exmWillreport.IExmWillreportGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：分析报告(获取Map集合服务实现)
 * 说明：分析报告(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class ExmWillreportGetMapServiceImpl implements IExmWillreportGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IExmWillreportGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, ExmWillreportVO> getMapGroupById(ExmWillreportQueryBO req) {
        Map<Long, ExmWillreportVO> rsp = new HashMap<>();
        List<ExmWillreportVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(ExmWillreportVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}