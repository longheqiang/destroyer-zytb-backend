package com.destroyer.will.service.bscCareer;

import com.destroyer.core.entity.bscCareer.bo.BscCareerQueryBO;
import com.destroyer.core.entity.bscCareer.vo.BscCareerVO;
import java.util.Map;

 /**
 * 标题：职业库(获取Map集合服务接口)
 * 说明：职业库(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscCareerGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, BscCareerVO> getMapGroupById(BscCareerQueryBO req);
}