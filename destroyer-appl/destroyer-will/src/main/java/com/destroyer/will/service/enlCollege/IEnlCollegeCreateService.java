package com.destroyer.will.service.enlCollege;

import com.destroyer.core.entity.enlCollege.bo.EnlCollegeBO;

 /**
 * 标题：招生院校(新增服务接口)
 * 说明：招生院校(新增服务接口),自定义新增数据接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlCollegeCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(EnlCollegeBO req);
}