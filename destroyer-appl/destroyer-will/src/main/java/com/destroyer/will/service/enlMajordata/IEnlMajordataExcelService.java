package com.destroyer.will.service.enlMajordata;

import com.destroyer.core.service.excel.IExcelBaseService;


/**
 * 标题：专业录取数据表(Excel服务接口)
 * 说明：专业录取数据表(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-7
 * 作者：admin
 */
public interface IEnlMajordataExcelService extends IExcelBaseService {



}