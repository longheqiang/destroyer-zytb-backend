package com.destroyer.will.service.exmOrder;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.exmOrder.bo.ExmOrderBO;
import java.util.List;

 /**
 * 标题：消费订单(批量新增服务接口)
 * 说明：消费订单(批量新增服务接口),自定义批量新增据服务接口
 * 时间：2024-3-13
 * 作者：admin
 */
public interface IExmOrderBatchCreateService {
    
    
    /**
    * 批量新增数据
    * @param req
    * @return 新增数据的组件ID集合
    */
    List<Long> batchCreate(BaseBatchBO<ExmOrderBO> req);
}