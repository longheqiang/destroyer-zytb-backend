package com.destroyer.will.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.wechat.vo.WechatPrepayWithRequestPaymentVO;
import com.destroyer.core.entity.wechat.vo.WechatTransactionVO;
import com.destroyer.will.ability.exmOrder.IExmOrderCreateAbilityService;
import com.destroyer.will.ability.exmOrder.IExmOrderPaymentSuccessNotifyAbilityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.exmOrder.bo.ExmOrderBO;
import com.destroyer.core.entity.exmOrder.bo.ExmOrderQueryBO;
import com.destroyer.core.entity.exmOrder.vo.ExmOrderVO;
import com.destroyer.will.service.exmOrder.*;

/**
 * 标题：消费订单(控制层)
 * 说明：消费订单(控制层),包括所有对外前端API接口
 * 时间：2024-3-13
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/will/exmOrder")
@Api(value = "ExmOrderController", tags = "消费订单API")
public class ExmOrderController {


    /**
     * 基础服务
     */
    @Autowired
    private IExmOrderBaseService baseService;


    /**
     * 新增数据服务
     */
    @Autowired
    private IExmOrderCreateService createService;


    /**
     * 批量新增服务
     */
    @Autowired
    private IExmOrderBatchCreateService batchCreateService;


    /**
     * 删除数据服务
     */
    @Autowired
    private IExmOrderDeleteService deleteService;


    /**
     * 列表服务
     */
    @Autowired
    private IExmOrderGetListService getListService;


    /**
     * map服务
     */
    @Autowired
    private IExmOrderGetMapService getMapService;


    /**
     * 详情服务
     */
    @Autowired
    private IExmOrderGetDetailService getDetailService;


    /**
     * 编辑服务
     */
    @Autowired
    private IExmOrderEditService editService;

    /**
     * Excel服务
     */
    @Autowired
    private IExmOrderExcelService excelService;

    @Autowired
    private IExmOrderCreateAbilityService createAbilityService;

    @Autowired
    private IExmOrderPaymentSuccessNotifyAbilityService paymentSuccessNotifyAbilityService;


    @PostMapping(value = "/createAndRequestPayment")
    @ApiOperation("新增订单并返回发起支付配置参数API")
    public Result<WechatPrepayWithRequestPaymentVO> createAndRequestPayment(@RequestBody ExmOrderBO req) {
        WechatPrepayWithRequestPaymentVO rsp = this.createAbilityService.createAndRequestPayment(req);
        return Result.success(rsp);
    }

    @PostMapping(value = "/wechatPaymentSuccessNotify")
    @ApiOperation("支付成功回调业务接口API")
    public Result<Boolean> wechatPaymentSuccessNotify(@RequestBody WechatTransactionVO req) {
        boolean rsp = this.paymentSuccessNotifyAbilityService.paymentSuccessNotify(req);
        return Result.success(rsp);
    }


//
//
//    @PostMapping(value = "/batchCreate")
//    @ApiOperation("批量新增API")
//    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<ExmOrderBO> req) {
//        List<Long> rsp = this.batchCreateService.batchCreate(req);
//        return Result.success(rsp);
//    }
//
//
//    @PostMapping(value = "/deleteById")
//    @ApiOperation("删除数据API(根据主键ID)")
//    public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
//        boolean rsp = this.deleteService.deleteById(req.getId());
//        return Result.success(rsp);
//    }
//
//
//    @PostMapping(value = "/batchDeleteByIds")
//    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
//    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
//        boolean rsp = this.deleteService.batchDeleteByIds(req);
//        return Result.success(rsp);
//    }
//
//
//    @PostMapping(value = "/deleteBy")
//    @ApiOperation("删除数据API(根据查询条件删除)")
//    public Result<Boolean> deleteBy(@RequestBody ExmOrderQueryBO req) {
//        boolean rsp = this.deleteService.batchDeleteBy(req);
//        return Result.success(rsp);
//    }


    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<ExmOrderVO>> list(@RequestBody ExmOrderQueryBO req) {
        List<ExmOrderVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }


//    @PostMapping(value = "/mapGroupById")
//    @ApiOperation("查询map(ID分组)API")
//    public Result<Map<Long, ExmOrderVO>> mapGroupById(@RequestBody ExmOrderQueryBO req) {
//        Map<Long, ExmOrderVO> rsp = this.getMapService.getMapGroupById(req);
//        return Result.success(rsp);
//    }


    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<ExmOrderVO>> page(@RequestBody ExmOrderQueryBO req) {
        Page<ExmOrderVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<ExmOrderVO> detailBy(@RequestBody ExmOrderQueryBO req) {
        ExmOrderVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailById")
    @ApiOperation("根据ID查询详情API")
    public Result<ExmOrderVO> detailById(@RequestBody BaseIdBO req) {
        ExmOrderVO rsp = this.getDetailService.getDetailById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/topay")
    @ApiOperation("支付订单API")
    public Result<Boolean> topay(@RequestBody ExmOrderBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }


//    @GetMapping("/exportExcelModule")
//    @ApiOperation("导出Excel模板API")
//    @ResponseBody
//    public void exportExcelModule(HttpServletResponse response) throws IOException {
//        excelService.exportExcelModule(response);
//    }
//
//
//    @PostMapping("/importExcelData")
//    @ApiOperation("导入Excel数据API")
//    public Result<Boolean> importExcelData(@RequestParam("file") MultipartFile req) throws IOException {
//        boolean rsp = excelService.importExcelData(req);
//        return Result.success(rsp);
//    }
}