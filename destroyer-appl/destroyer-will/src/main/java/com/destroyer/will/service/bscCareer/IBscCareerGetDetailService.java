package com.destroyer.will.service.bscCareer;

import com.destroyer.core.entity.bscCareer.bo.BscCareerQueryBO;
import com.destroyer.core.entity.bscCareer.vo.BscCareerVO;
import java.util.List;

 /**
 * 标题：职业库(查询详情服务接口)
 * 说明：职业库(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscCareerGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    BscCareerVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    BscCareerVO getDetailBy(BscCareerQueryBO req);
   
}