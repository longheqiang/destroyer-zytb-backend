package com.destroyer.will.service.impl.enlCollege;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.enlCollege.EnlCollegePO;
import com.destroyer.will.mapper.EnlCollegeMapper;
import com.destroyer.will.service.enlCollege.IEnlCollegeBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：招生院校(基础通用服务实现)
 * 说明：招生院校(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class  EnlCollegeBaseServiceImpl extends ServiceImpl<EnlCollegeMapper, EnlCollegePO> implements IEnlCollegeBaseService {
}