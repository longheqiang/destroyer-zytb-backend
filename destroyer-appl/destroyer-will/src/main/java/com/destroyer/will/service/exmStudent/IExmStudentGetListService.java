package com.destroyer.will.service.exmStudent;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.exmStudent.bo.ExmStudentQueryBO;
import com.destroyer.core.entity.exmStudent.vo.ExmStudentVO;
import java.util.List;

 /**
 * 标题：志愿学生(获取列表服务接口)
 * 说明：志愿学生(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStudentGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<ExmStudentVO> getList(ExmStudentQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<ExmStudentVO> getPageList(ExmStudentQueryBO req);
}