package com.destroyer.will.service.enlCollege;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.enlCollege.bo.EnlCollegeBO;
import java.util.List;

 /**
 * 标题：招生院校(批量新增服务接口)
 * 说明：招生院校(批量新增服务接口),自定义批量新增据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlCollegeBatchCreateService {
    
    
    /**
    * 批量新增数据
    * @param req
    * @return 新增数据的组件ID集合
    */
    List<Long> batchCreate(BaseBatchBO<EnlCollegeBO> req);
}