package com.destroyer.will.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.bscYear.bo.BscYearBO;
import com.destroyer.core.entity.bscYear.bo.BscYearQueryBO;
import com.destroyer.core.entity.bscYear.vo.BscYearVO;
import com.destroyer.will.service.bscYear.*;

/**
 * 标题：年份(控制层)
 * 说明：年份(控制层),包括所有对外前端API接口
 * 时间：2024-3-7
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/will/bscYear")
@Api(value = "BscYearController", tags = "年份API")
public class BscYearController {


    /**
     * 基础服务
     */
    @Autowired
    private IBscYearBaseService baseService;


    /**
     * 新增数据服务
     */
    @Autowired
    private IBscYearCreateService createService;



    /**
     * 批量新增服务
     */
    @Autowired
    private IBscYearBatchCreateService batchCreateService;



    /**
     * 删除数据服务
     */
    @Autowired
    private IBscYearDeleteService deleteService;


    /**
     * 列表服务
     */
    @Autowired
    private IBscYearGetListService getListService;


    /**
     * map服务
     */
    @Autowired
    private IBscYearGetMapService getMapService;


    /**
     * 详情服务
     */
    @Autowired
    private IBscYearGetDetailService getDetailService;


    /**
     * 编辑服务
     */
    @Autowired
    private IBscYearEditService editService;


    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody BscYearBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }


    /**
     * Excel服务
     */
    @Autowired
    private IBscYearExcelService excelService;



    @PostMapping(value = "/batchCreate")
    @ApiOperation("批量新增API")
    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<BscYearBO> req) {
        List<Long> rsp = this.batchCreateService.batchCreate(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/deleteById")
    @ApiOperation("删除数据API(根据主键ID)")
    public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
        boolean rsp = this.deleteService.deleteById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }



    @PostMapping(value = "/deleteBy")
    @ApiOperation("删除数据API(根据查询条件删除)")
    public Result<Boolean> deleteBy(@RequestBody BscYearQueryBO req) {
        boolean rsp = this.deleteService.batchDeleteBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<BscYearVO>> list(@RequestBody BscYearQueryBO req) {
        List<BscYearVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/mapGroupById")
    @ApiOperation("查询map(ID分组)API")
    public Result<Map<Long, BscYearVO>> mapGroupById(@RequestBody BscYearQueryBO req) {
        Map<Long, BscYearVO> rsp = this.getMapService.getMapGroupById(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<BscYearVO>> page(@RequestBody BscYearQueryBO req) {
        Page<BscYearVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<BscYearVO> detailBy(@RequestBody BscYearQueryBO req) {
        BscYearVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailById")
    @ApiOperation("根据ID查询详情API")
    public Result<BscYearVO> detailById(@RequestBody BaseIdBO req) {
        BscYearVO rsp = this.getDetailService.getDetailById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody BscYearBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }


    @GetMapping("/exportExcelModule")
    @ApiOperation("导出Excel模板API")
    @ResponseBody
    public void exportExcelModule(HttpServletResponse response) throws IOException {
        excelService.exportExcelModule(response);
    }


    @PostMapping("/importExcelData")
    @ApiOperation("导入Excel数据API")
    public Result<Boolean> importExcelData(@RequestParam("file") MultipartFile req) throws IOException {
        boolean rsp = excelService.importExcelData(req);
        return Result.success(rsp);
    }
}