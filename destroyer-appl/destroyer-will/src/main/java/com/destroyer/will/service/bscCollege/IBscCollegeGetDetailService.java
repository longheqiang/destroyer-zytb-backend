package com.destroyer.will.service.bscCollege;

import com.destroyer.core.entity.bscCollege.bo.BscCollegeQueryBO;
import com.destroyer.core.entity.bscCollege.vo.BscCollegeVO;
import java.util.List;

 /**
 * 标题：院校库(查询详情服务接口)
 * 说明：院校库(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscCollegeGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    BscCollegeVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    BscCollegeVO getDetailBy(BscCollegeQueryBO req);
   
}