package com.destroyer.will.service.bscMajors;

import com.destroyer.core.entity.bscMajors.bo.BscMajorsQueryBO;
import com.destroyer.core.entity.bscMajors.vo.BscMajorsVO;
import java.util.Map;

 /**
 * 标题：专业库(获取Map集合服务接口)
 * 说明：专业库(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscMajorsGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, BscMajorsVO> getMapGroupById(BscMajorsQueryBO req);
}