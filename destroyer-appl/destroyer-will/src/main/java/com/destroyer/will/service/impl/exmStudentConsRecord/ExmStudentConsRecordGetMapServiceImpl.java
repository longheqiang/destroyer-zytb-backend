package com.destroyer.will.service.impl.exmStudentConsRecord;

import com.destroyer.core.entity.exmStudentConsRecord.bo.ExmStudentConsRecordQueryBO;
import com.destroyer.core.entity.exmStudentConsRecord.vo.ExmStudentConsRecordVO;
import com.destroyer.will.service.exmStudentConsRecord.IExmStudentConsRecordGetListService;
import com.destroyer.will.service.exmStudentConsRecord.IExmStudentConsRecordGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：用户消费记录(获取Map集合服务实现)
 * 说明：用户消费记录(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class ExmStudentConsRecordGetMapServiceImpl implements IExmStudentConsRecordGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IExmStudentConsRecordGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, ExmStudentConsRecordVO> getMapGroupById(ExmStudentConsRecordQueryBO req) {
        Map<Long, ExmStudentConsRecordVO> rsp = new HashMap<>();
        List<ExmStudentConsRecordVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(ExmStudentConsRecordVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}