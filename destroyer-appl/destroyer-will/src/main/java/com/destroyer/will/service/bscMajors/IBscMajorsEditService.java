package com.destroyer.will.service.bscMajors;

import com.destroyer.core.entity.bscMajors.bo.BscMajorsBO;

 /**
 * 标题：专业库(编辑数据服务接口)
 * 说明：专业库(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscMajorsEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(BscMajorsBO req);
}