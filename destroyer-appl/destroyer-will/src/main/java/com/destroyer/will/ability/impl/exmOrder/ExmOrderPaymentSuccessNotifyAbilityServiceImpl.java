package com.destroyer.will.ability.impl.exmOrder;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.DateUtils;
import com.destroyer.common.util.Func;
import com.destroyer.core.entity.exmOrder.ExmOrderPO;
import com.destroyer.core.entity.exmOrder.bo.ExmOrderBO;
import com.destroyer.core.entity.wechat.vo.WechatTransactionVO;
import com.destroyer.will.ability.exmOrder.IExmOrderPaymentSuccessNotifyAbilityService;
import com.destroyer.will.service.exmOrder.IExmOrderBaseService;
import com.destroyer.will.service.exmOrder.IExmOrderEditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;

/**
 * 标题：ExmOrderPaymentSuccessNotifyAbilityServiceImpl
 * 说明：
 * 时间：2024/3/14
 * 作者：admin
 */
@Service
public class ExmOrderPaymentSuccessNotifyAbilityServiceImpl implements IExmOrderPaymentSuccessNotifyAbilityService {

    @Autowired
    private IExmOrderBaseService baseService;

    @Autowired
    private IExmOrderEditService editService;
    /**
     * 支付成功回调业处理
     * @param req
     * @return
     */
    @Override
    public Boolean paymentSuccessNotify(WechatTransactionVO req) {
        boolean rsp = false;
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        String orderNo = req.getOutTradeNo();
        LambdaQueryWrapper<ExmOrderPO> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(ExmOrderPO::getOrderNo,orderNo);
        ExmOrderPO po = baseService.getOne(wrapper);
        //修改业务订单状态
        ExmOrderBO editBO = new ExmOrderBO();
        editBO.setId(po.getId());
        editBO.setOrderStatus("1");
        Date payTime = DateUtils.getCurrentDateYYYYMMDDHHMMSS();
        if(Func.isNotEmpty(req.getSuccessTime())){
            payTime = DateUtils.strToDateyyyyMMddHHmmss(req.getSuccessTime());
        }
        editBO.setPayTime(payTime);
        rsp = editService.editById(editBO);
        //核心业务执行后
        this.afterToDo();
        return rsp;
    }


    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(WechatTransactionVO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "支付成功回调业处理，入参不能为空");
        } else {
            if(Func.isEmpty(req.getOutTradeNo())){
                throw new ServiceException(ResultEnum.PARAM_MISS, "支付成功回调业处理，入参订单编号不能为空！");

            }
        }
    }

    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(WechatTransactionVO req) {
        // TODO 执行前调用逻辑

    }

    /**
     * 核心逻辑执行后调用
     *
     */
    private void afterToDo() {

    }
}
