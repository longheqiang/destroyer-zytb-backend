package com.destroyer.will.service.impl.bscCareer;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.bscCareer.BscCareerPO;
import com.destroyer.core.entity.bscCareer.bo.BscCareerBO;
import com.destroyer.will.service.bscCareer.IBscCareerBaseService;
import com.destroyer.will.service.bscCareer.IBscCareerBatchCreateService;
import com.destroyer.core.util.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;

 /**
 * 标题：职业库(批量新增服务实现)
 * 说明：职业库(批量新增服务实现),自定义批量新增据服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class BscCareerBatchCreateServiceImpl implements IBscCareerBatchCreateService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private IBscCareerBaseService baseService;
    /**
     * 批量新增数据
     * @param req
     * @return 新增数据的组件ID集合
     */
    @Override
    public List<Long> batchCreate(BaseBatchBO<BscCareerBO> req) {
        List<Long> rsp = new ArrayList<>();
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<BscCareerPO> poList = req.getList().stream().map(k->{
            BscCareerPO po = new BscCareerPO(k);
            return po;
        }).collect(Collectors.toList());
        boolean state = baseService.saveBatch(poList);
        if(!state){
            throw new ServiceException(ResultEnum.FAILURE, "批量新增数据失败");
        }
        rsp = poList.stream().map(BscCareerPO::getId).collect(Collectors.toList());
        return rsp;
    }
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(BaseBatchBO<BscCareerBO> req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
            if(null == req || req.getList().size() == 0){
                throw new ServiceException(ResultEnum.PARAM_MISS, "批量新增入参不能为空");
            }
            List<BscCareerBO> list = req.getList();
            for (BscCareerBO i : list) {
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
            }
        }
    }
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(BaseBatchBO<BscCareerBO> req) {
        // TODO 执行前调用逻辑
    }
}