package com.destroyer.will.service.impl.bscCareer;

import com.destroyer.core.entity.bscCareer.bo.BscCareerQueryBO;
import com.destroyer.core.entity.bscCareer.vo.BscCareerVO;
import com.destroyer.will.service.bscCareer.IBscCareerGetDetailService;
import com.destroyer.will.service.bscCareer.IBscCareerGetListService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

 /**
 * 标题：职业库(查询详情服务实现)
 * 说明：职业库(查询详情服务实现),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class BscCareerGetDetailServiceImpl implements IBscCareerGetDetailService {
    
    
    /**
     * 查询列表服务
     */
    @Autowired
    private IBscCareerGetListService getListService;
    /**
     * 根据主键ID查询详情
     * @param req
     * @return
     */
    @Override
    public BscCareerVO getDetailById(Long req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"查询详情入参ID不能为空");
        }
        BscCareerQueryBO queryBO = new BscCareerQueryBO();
        queryBO.setId(req);
        BscCareerVO rsp = this.getDetailBy(queryBO);
        return rsp;
    }
    
    
    /**
     * 根据条件查询详情
     * @param req
     * @return
     */
    @Override
    public BscCareerVO getDetailBy(BscCareerQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<BscCareerVO> list = getListService.getList(req);
        if (list.size() > 1) {
            throw new ServiceException(ResultEnum.FAILURE,"查询到的数据不唯一！");
        }
        BscCareerVO rsp = list.size() == 1? list.get(0):null;
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(BscCareerQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        } else {
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(BscCareerQueryBO req) {
        // TODO 执行前调用逻辑
    }
}