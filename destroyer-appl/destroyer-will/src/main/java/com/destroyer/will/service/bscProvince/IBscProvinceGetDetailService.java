package com.destroyer.will.service.bscProvince;

import com.destroyer.core.entity.bscProvince.bo.BscProvinceQueryBO;
import com.destroyer.core.entity.bscProvince.vo.BscProvinceVO;
import java.util.List;

 /**
 * 标题：省份表(查询详情服务接口)
 * 说明：省份表(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscProvinceGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    BscProvinceVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    BscProvinceVO getDetailBy(BscProvinceQueryBO req);
   
}