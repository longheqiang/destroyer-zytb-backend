package com.destroyer.will.service.bscCareer;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.bscCareer.BscCareerPO;

 /**
 * 标题：职业库(基础通用服务接口)
 * 说明：职业库(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscCareerBaseService  extends IService<BscCareerPO> {
}