package com.destroyer.will.service.exmOrder;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.exmOrder.bo.ExmOrderQueryBO;
import com.destroyer.core.entity.exmOrder.vo.ExmOrderVO;
import java.util.List;

 /**
 * 标题：消费订单(获取列表服务接口)
 * 说明：消费订单(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-13
 * 作者：admin
 */
public interface IExmOrderGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<ExmOrderVO> getList(ExmOrderQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<ExmOrderVO> getPageList(ExmOrderQueryBO req);
}