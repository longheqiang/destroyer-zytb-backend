package com.destroyer.will.service.enlMajordata;

import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataBO;

 /**
 * 标题：专业录取数据表(编辑数据服务接口)
 * 说明：专业录取数据表(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajordataEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(EnlMajordataBO req);
}