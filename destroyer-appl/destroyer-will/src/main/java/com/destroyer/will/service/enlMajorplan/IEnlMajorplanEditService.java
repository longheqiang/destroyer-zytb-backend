package com.destroyer.will.service.enlMajorplan;

import com.destroyer.core.entity.enlMajorplan.bo.EnlMajorplanBO;

 /**
 * 标题：专业录取计划(编辑数据服务接口)
 * 说明：专业录取计划(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajorplanEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(EnlMajorplanBO req);
}