package com.destroyer.will.service.bscCareer;

import com.destroyer.core.entity.bscCareer.bo.BscCareerBO;

 /**
 * 标题：职业库(新增服务接口)
 * 说明：职业库(新增服务接口),自定义新增数据接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscCareerCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(BscCareerBO req);
}