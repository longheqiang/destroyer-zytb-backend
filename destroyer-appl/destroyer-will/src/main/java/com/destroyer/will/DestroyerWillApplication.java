package com.destroyer.will;

import com.destroyer.common.constant.PackageConstant;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@Slf4j
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan(PackageConstant.MAPPER_DESTROYER_WILL)
@ComponentScan(basePackages = {PackageConstant.BASE_DESTROYER_CORE,PackageConstant.BASE_DESTROYER_WILL})
@EnableFeignClients(PackageConstant.BASE_DESTROYER_CORE)
public class DestroyerWillApplication {

    public static void main(String[] args) {
        SpringApplication.run(DestroyerWillApplication.class, args);
        log.info("=======================志愿中心启动成功=======================");

    }

}
