package com.destroyer.will.service.impl.bscYear;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.bscYear.BscYearPO;
import com.destroyer.will.mapper.BscYearMapper;
import com.destroyer.will.service.bscYear.IBscYearBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：年份(基础通用服务实现)
 * 说明：年份(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class  BscYearBaseServiceImpl extends ServiceImpl<BscYearMapper, BscYearPO> implements IBscYearBaseService {
}