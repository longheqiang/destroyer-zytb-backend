package com.destroyer.will.service.bscYear;

import com.destroyer.core.entity.bscYear.bo.BscYearQueryBO;
import com.destroyer.core.entity.bscYear.vo.BscYearVO;
import java.util.Map;

 /**
 * 标题：年份(获取Map集合服务接口)
 * 说明：年份(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscYearGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, BscYearVO> getMapGroupById(BscYearQueryBO req);
}