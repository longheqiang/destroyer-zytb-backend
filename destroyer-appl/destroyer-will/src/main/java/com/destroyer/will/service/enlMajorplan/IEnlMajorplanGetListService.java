package com.destroyer.will.service.enlMajorplan;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.enlMajorplan.bo.EnlMajorplanQueryBO;
import com.destroyer.core.entity.enlMajorplan.vo.EnlMajorplanVO;
import java.util.List;

 /**
 * 标题：专业录取计划(获取列表服务接口)
 * 说明：专业录取计划(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajorplanGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<EnlMajorplanVO> getList(EnlMajorplanQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<EnlMajorplanVO> getPageList(EnlMajorplanQueryBO req);
}