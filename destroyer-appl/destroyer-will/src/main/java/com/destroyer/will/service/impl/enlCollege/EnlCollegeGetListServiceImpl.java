package com.destroyer.will.service.impl.enlCollege;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.util.MpSqlWrapperUtils;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.util.PageUtils;
import com.destroyer.core.entity.enlCollege.EnlCollegePO;
import com.destroyer.core.entity.enlCollege.bo.EnlCollegeQueryBO;
import com.destroyer.core.entity.enlCollege.vo.EnlCollegeVO;
import com.destroyer.will.service.enlCollege.IEnlCollegeBaseService;
import com.destroyer.will.service.enlCollege.IEnlCollegeGetListService;
import org.apache.commons.lang3.StringUtils;
import com.destroyer.core.util.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

 /**
 * 标题：招生院校(获取列表服务实现)
 * 说明：招生院校(获取列表服务实现),自定义基础列表查询,基础分页列表查询服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class EnlCollegeGetListServiceImpl implements IEnlCollegeGetListService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private IEnlCollegeBaseService baseService;
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    @Override
    public List<EnlCollegeVO> getList(EnlCollegeQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<EnlCollegePO> poList = this.baseService.list(this.buildQueryWrapper(req));
        List<EnlCollegeVO> rsp = this.buildRspVO(poList);
        return rsp;
    }
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    @Override
    public Page<EnlCollegeVO> getPageList(EnlCollegeQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //构造分页入参对象
        Page<EnlCollegePO> page = req.buildPage();
        //分页查询列表数据
        Page<EnlCollegePO> poPage = this.baseService.page(page, this.buildQueryWrapper(req));
        //构造出参
        Page<EnlCollegeVO> rsp = PageUtils.poPageToVoPage(poPage, EnlCollegeVO.class);
        List<EnlCollegeVO> voList = this.buildRspVO(poPage.getRecords());
        rsp.setRecords(voList);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     * @param req 入参
     */
    private void validParams(EnlCollegeQueryBO req){
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        }else{
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     * @param req 入参
     */
    private void beforeToDo(EnlCollegeQueryBO req) {
        // TODO 执行前调用逻辑
    }
    
    
    /**
     * 构造QueryWrapper查询入参
     *
     * @param req 查询业务对象
     * @return QueryWrapper对象
     */
    private LambdaQueryWrapper<EnlCollegePO> buildQueryWrapper(EnlCollegeQueryBO req) {
        //构造查询包装器
        LambdaQueryWrapper<EnlCollegePO> rsp = req.getQueryWrapper();
        //数据权限范围控制
        this.buildDataRangeOnQueryWrapper(rsp);
        return rsp;
    }
    
    
    /**
     *在查询包装器里构建查询数据范围
     * @param wrapper
     */
    private void buildDataRangeOnQueryWrapper(LambdaQueryWrapper<EnlCollegePO> wrapper){
    }
    
    
    /**
     * 构造响应视图对象列表
     * @param req PO列表数据
     * @return VO列表数据
     */
    private List<EnlCollegeVO> buildRspVO(List<EnlCollegePO> req) {
        List<EnlCollegeVO> rsp = new ArrayList<>();
        if(null != req && req.size() >0){
            rsp = req.stream().map(k->{
                EnlCollegeVO vo = k.parseVO();
                return vo;
            }).collect(Collectors.toList());
        }
        return rsp;
    }
}