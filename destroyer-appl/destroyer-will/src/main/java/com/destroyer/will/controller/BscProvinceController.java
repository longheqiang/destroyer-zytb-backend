package com.destroyer.will.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.bscProvince.bo.BscProvinceBO;
import com.destroyer.core.entity.bscProvince.bo.BscProvinceQueryBO;
import com.destroyer.core.entity.bscProvince.vo.BscProvinceVO;
import com.destroyer.will.service.bscProvince.*;

/**
 * 标题：省份表(控制层)
 * 说明：省份表(控制层),包括所有对外前端API接口
 * 时间：2024-3-7
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/will/bscProvince")
@Api(value = "BscProvinceController", tags = "省份表API")
public class BscProvinceController {


    /**
     * 基础服务
     */
    @Autowired
    private IBscProvinceBaseService baseService;


    /**
     * 新增数据服务
     */
    @Autowired
    private IBscProvinceCreateService createService;



    /**
     * 批量新增服务
     */
    @Autowired
    private IBscProvinceBatchCreateService batchCreateService;



    /**
     * 删除数据服务
     */
    @Autowired
    private IBscProvinceDeleteService deleteService;


    /**
     * 列表服务
     */
    @Autowired
    private IBscProvinceGetListService getListService;


    /**
     * map服务
     */
    @Autowired
    private IBscProvinceGetMapService getMapService;


    /**
     * 详情服务
     */
    @Autowired
    private IBscProvinceGetDetailService getDetailService;


    /**
     * 编辑服务
     */
    @Autowired
    private IBscProvinceEditService editService;


    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody BscProvinceBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }


    /**
     * Excel服务
     */
    @Autowired
    private IBscProvinceExcelService excelService;



    @PostMapping(value = "/batchCreate")
    @ApiOperation("批量新增API")
    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<BscProvinceBO> req) {
        List<Long> rsp = this.batchCreateService.batchCreate(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/deleteById")
    @ApiOperation("删除数据API(根据主键ID)")
    public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
        boolean rsp = this.deleteService.deleteById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }



    @PostMapping(value = "/deleteBy")
    @ApiOperation("删除数据API(根据查询条件删除)")
    public Result<Boolean> deleteBy(@RequestBody BscProvinceQueryBO req) {
        boolean rsp = this.deleteService.batchDeleteBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<BscProvinceVO>> list(@RequestBody BscProvinceQueryBO req) {
        List<BscProvinceVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/mapGroupById")
    @ApiOperation("查询map(ID分组)API")
    public Result<Map<Long, BscProvinceVO>> mapGroupById(@RequestBody BscProvinceQueryBO req) {
        Map<Long, BscProvinceVO> rsp = this.getMapService.getMapGroupById(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<BscProvinceVO>> page(@RequestBody BscProvinceQueryBO req) {
        Page<BscProvinceVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<BscProvinceVO> detailBy(@RequestBody BscProvinceQueryBO req) {
        BscProvinceVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailById")
    @ApiOperation("根据ID查询详情API")
    public Result<BscProvinceVO> detailById(@RequestBody BaseIdBO req) {
        BscProvinceVO rsp = this.getDetailService.getDetailById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody BscProvinceBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }


    @GetMapping("/exportExcelModule")
    @ApiOperation("导出Excel模板API")
    @ResponseBody
    public void exportExcelModule(HttpServletResponse response) throws IOException {
        excelService.exportExcelModule(response);
    }


    @PostMapping("/importExcelData")
    @ApiOperation("导入Excel数据API")
    public Result<Boolean> importExcelData(@RequestParam("file") MultipartFile req) throws IOException {
        boolean rsp = excelService.importExcelData(req);
        return Result.success(rsp);
    }
}