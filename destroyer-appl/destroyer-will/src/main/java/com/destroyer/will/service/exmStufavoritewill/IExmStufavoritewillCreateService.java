package com.destroyer.will.service.exmStufavoritewill;

import com.destroyer.core.entity.exmStufavoritewill.bo.ExmStufavoritewillBO;

 /**
 * 标题：学生收藏志愿(新增服务接口)
 * 说明：学生收藏志愿(新增服务接口),自定义新增数据接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmStufavoritewillCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(ExmStufavoritewillBO req);
}