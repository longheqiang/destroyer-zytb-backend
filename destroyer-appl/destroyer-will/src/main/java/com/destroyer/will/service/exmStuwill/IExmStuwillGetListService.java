package com.destroyer.will.service.exmStuwill;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.exmStuwill.bo.ExmStuwillQueryBO;
import com.destroyer.core.entity.exmStuwill.vo.ExmStuwillVO;
import java.util.List;

 /**
 * 标题：学生志愿(获取列表服务接口)
 * 说明：学生志愿(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStuwillGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<ExmStuwillVO> getList(ExmStuwillQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<ExmStuwillVO> getPageList(ExmStuwillQueryBO req);
}