package com.destroyer.will.service.exmStudentConsRecord;

import com.destroyer.core.service.excel.IExcelBaseService;


/**
 * 标题：用户消费记录(Excel服务接口)
 * 说明：用户消费记录(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-7
 * 作者：admin
 */
public interface IExmStudentConsRecordExcelService extends IExcelBaseService {


}