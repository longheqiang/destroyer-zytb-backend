package com.destroyer.will.service.exmWillcondition;

import com.destroyer.core.entity.exmWillcondition.bo.ExmWillconditionBO;

 /**
 * 标题：学生志愿条件(编辑数据服务接口)
 * 说明：学生志愿条件(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillconditionEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(ExmWillconditionBO req);
}