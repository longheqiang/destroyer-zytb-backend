package com.destroyer.will.controller;

import com.destroyer.common.entity.auth.AuthBaseVO;
import com.destroyer.common.entity.auth.AuthStudentVO;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.wechat.bo.WechatUserCodeBO;
import com.destroyer.will.ability.auth.IAuthStudentWechatLoginAbilityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 标题：AuthController
 * 说明：
 * 时间：2023/9/7
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/will/auth")
@Api(value = "AuthController", tags = "身份认证API")
public class AuthController {


    /**
     * 基础用户登录能力服务
     */
    @Autowired
    private IAuthStudentWechatLoginAbilityService authStudentWechatLoginAbilityService;

    @PostMapping(value = "/login")
    @ApiOperation("学生用户登录API")
    public Result<AuthBaseVO<AuthStudentVO>> login(@RequestBody WechatUserCodeBO req) {
        AuthBaseVO<AuthStudentVO> rsp = authStudentWechatLoginAbilityService.login(req);
        return Result.success(rsp);
    }


}
