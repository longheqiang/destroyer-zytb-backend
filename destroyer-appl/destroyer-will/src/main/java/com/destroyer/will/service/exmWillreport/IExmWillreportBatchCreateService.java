package com.destroyer.will.service.exmWillreport;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.exmWillreport.bo.ExmWillreportBO;
import java.util.List;

 /**
 * 标题：分析报告(批量新增服务接口)
 * 说明：分析报告(批量新增服务接口),自定义批量新增据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillreportBatchCreateService {
    
    
    /**
    * 批量新增数据
    * @param req
    * @return 新增数据的组件ID集合
    */
    List<Long> batchCreate(BaseBatchBO<ExmWillreportBO> req);
}