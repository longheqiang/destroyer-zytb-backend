package com.destroyer.will.service.exmOrder;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.exmOrder.ExmOrderPO;

 /**
 * 标题：消费订单(基础通用服务接口)
 * 说明：消费订单(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-13
 * 作者：admin
 */
public interface IExmOrderBaseService  extends IService<ExmOrderPO> {
}