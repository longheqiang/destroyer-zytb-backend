package com.destroyer.will.service.impl.exmOrder;

import com.destroyer.core.entity.exmOrder.bo.ExmOrderQueryBO;
import com.destroyer.core.entity.exmOrder.vo.ExmOrderVO;
import com.destroyer.will.service.exmOrder.IExmOrderGetDetailService;
import com.destroyer.will.service.exmOrder.IExmOrderGetListService;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

 /**
 * 标题：消费订单(查询详情服务实现)
 * 说明：消费订单(查询详情服务实现),自定义基础详情查询接口
 * 时间：2024-3-13
 * 作者：admin
 */
@Service
public class ExmOrderGetDetailServiceImpl implements IExmOrderGetDetailService {
    
    
    /**
     * 查询列表服务
     */
    @Autowired
    private IExmOrderGetListService getListService;
    /**
     * 根据主键ID查询详情
     * @param req
     * @return
     */
    @Override
    public ExmOrderVO getDetailById(Long req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"查询详情入参ID不能为空");
        }
        ExmOrderQueryBO queryBO = new ExmOrderQueryBO();
        queryBO.setId(req);
        ExmOrderVO rsp = this.getDetailBy(queryBO);
        return rsp;
    }
    
    
    /**
     * 根据条件查询详情
     * @param req
     * @return
     */
    @Override
    public ExmOrderVO getDetailBy(ExmOrderQueryBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<ExmOrderVO> list = getListService.getList(req);
        if (list.size() > 1) {
            throw new ServiceException(ResultEnum.FAILURE,"查询到的数据不唯一！");
        }
        ExmOrderVO rsp = list.size() == 1? list.get(0):null;
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(ExmOrderQueryBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS,"入参不能为空");
        } else {
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(ExmOrderQueryBO req) {
        // TODO 执行前调用逻辑
    }
}