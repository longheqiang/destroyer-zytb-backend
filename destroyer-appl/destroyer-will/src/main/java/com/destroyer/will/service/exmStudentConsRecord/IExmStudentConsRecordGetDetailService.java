package com.destroyer.will.service.exmStudentConsRecord;

import com.destroyer.core.entity.exmStudentConsRecord.bo.ExmStudentConsRecordQueryBO;
import com.destroyer.core.entity.exmStudentConsRecord.vo.ExmStudentConsRecordVO;
import java.util.List;

 /**
 * 标题：用户消费记录(查询详情服务接口)
 * 说明：用户消费记录(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmStudentConsRecordGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    ExmStudentConsRecordVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    ExmStudentConsRecordVO getDetailBy(ExmStudentConsRecordQueryBO req);
   
}