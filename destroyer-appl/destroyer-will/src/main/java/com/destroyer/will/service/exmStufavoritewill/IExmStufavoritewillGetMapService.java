package com.destroyer.will.service.exmStufavoritewill;

import com.destroyer.core.entity.exmStufavoritewill.bo.ExmStufavoritewillQueryBO;
import com.destroyer.core.entity.exmStufavoritewill.vo.ExmStufavoritewillVO;
import java.util.Map;

 /**
 * 标题：学生收藏志愿(获取Map集合服务接口)
 * 说明：学生收藏志愿(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmStufavoritewillGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, ExmStufavoritewillVO> getMapGroupById(ExmStufavoritewillQueryBO req);
}