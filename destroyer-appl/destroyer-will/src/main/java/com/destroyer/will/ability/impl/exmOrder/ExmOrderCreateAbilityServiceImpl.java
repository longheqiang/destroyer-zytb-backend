package com.destroyer.will.ability.impl.exmOrder;

import cn.hutool.core.util.RandomUtil;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.common.util.DateUtils;
import com.destroyer.core.entity.exmOrder.bo.ExmOrderBO;
import com.destroyer.core.entity.wechat.bo.WechatPrepayBO;
import com.destroyer.core.entity.wechat.vo.WechatPrepayWithRequestPaymentVO;
import com.destroyer.core.service.wechat.IWechatFeignService;
import com.destroyer.core.util.UserUtils;
import com.destroyer.will.ability.exmOrder.IExmOrderCreateAbilityService;
import com.destroyer.will.service.exmOrder.IExmOrderCreateService;
import com.destroyer.will.service.exmOrder.IExmOrderEditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 标题：ExmOrderCreateAbilityServiceImpl
 * 说明：
 * 时间：2024/3/13
 * 作者：admin
 */
@Service
public class ExmOrderCreateAbilityServiceImpl implements IExmOrderCreateAbilityService {




    @Autowired
    private IExmOrderCreateService createService;

    @Autowired
    private IExmOrderEditService editService;



    @Autowired
    private IWechatFeignService wechatFeignService;
    /**
     * 新建订单
     * @param req
     * @return
     */
    @Override
    @Transactional
    public WechatPrepayWithRequestPaymentVO createAndRequestPayment(ExmOrderBO req) {
        WechatPrepayWithRequestPaymentVO rsp = new WechatPrepayWithRequestPaymentVO();
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        Long id = createService.create(req);

        //建立微信预支付订单
        WechatPrepayBO wechatPrepayBO = new WechatPrepayBO();
        wechatPrepayBO.setOrderNo(req.getOrderNo());
        wechatPrepayBO.setTotal(req.getTotal());
        wechatPrepayBO.setDescription(req.getDescription());
        String prepayId = wechatFeignService.prepay(wechatPrepayBO);
       //修改订单
        ExmOrderBO editBO = new ExmOrderBO();
        editBO.setId(id);
        editBO.setPrepayId(prepayId);
        boolean editState = editService.editById(editBO);
        if(!editState){
            throw new ServiceException("修改订单支付预订单ID失败!");
        }
        rsp = wechatFeignService.prepayWithRequestPaymentByPrepayId(prepayId);
        return rsp;
    }


    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(ExmOrderBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
            if(null == req.getTotal()){
                throw new ServiceException(ResultEnum.PARAM_MISS,"入参[金额（单位：分）]不能为空!");
            }
        }
    }

    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(ExmOrderBO req) {
        //执行前调用逻辑
        Long stuId = UserUtils.getUserId();
        req.setStuId(stuId);
        req.setOrderNo("100"+ DateUtils.getCurrentDateStrYYYYMMDDHHMMSS3()+ RandomUtil.randomInt(10, 100));
        req.setOrderStatus("0");
    }



}
