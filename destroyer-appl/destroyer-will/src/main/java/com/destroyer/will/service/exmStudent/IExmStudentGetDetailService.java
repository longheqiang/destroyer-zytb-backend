package com.destroyer.will.service.exmStudent;

import com.destroyer.core.entity.exmStudent.bo.ExmStudentQueryBO;
import com.destroyer.core.entity.exmStudent.vo.ExmStudentVO;
import java.util.List;

 /**
 * 标题：志愿学生(查询详情服务接口)
 * 说明：志愿学生(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStudentGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    ExmStudentVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    ExmStudentVO getDetailBy(ExmStudentQueryBO req);
   
}