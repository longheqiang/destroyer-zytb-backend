package com.destroyer.will.service.enlMajordata;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.enlMajordata.EnlMajordataPO;

 /**
 * 标题：专业录取数据表(基础通用服务接口)
 * 说明：专业录取数据表(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajordataBaseService  extends IService<EnlMajordataPO> {
}