package com.destroyer.will.service.enlMajordata;

import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataBO;

 /**
 * 标题：专业录取数据表(新增服务接口)
 * 说明：专业录取数据表(新增服务接口),自定义新增数据接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajordataCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(EnlMajordataBO req);
}