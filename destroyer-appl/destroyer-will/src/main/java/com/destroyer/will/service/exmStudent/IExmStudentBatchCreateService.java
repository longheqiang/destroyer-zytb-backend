package com.destroyer.will.service.exmStudent;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.exmStudent.bo.ExmStudentBO;
import java.util.List;

 /**
 * 标题：志愿学生(批量新增服务接口)
 * 说明：志愿学生(批量新增服务接口),自定义批量新增据服务接口
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStudentBatchCreateService {
    
    
    /**
    * 批量新增数据
    * @param req
    * @return 新增数据的组件ID集合
    */
    List<Long> batchCreate(BaseBatchBO<ExmStudentBO> req);
}