package com.destroyer.will.service.exmStudent;

import com.destroyer.core.entity.exmStudent.bo.ExmStudentBO;

 /**
 * 标题：志愿学生(新增服务接口)
 * 说明：志愿学生(新增服务接口),自定义新增数据接口
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStudentCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(ExmStudentBO req);
}