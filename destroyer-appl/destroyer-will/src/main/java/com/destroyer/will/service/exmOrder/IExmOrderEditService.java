package com.destroyer.will.service.exmOrder;

import com.destroyer.core.entity.exmOrder.bo.ExmOrderBO;

 /**
 * 标题：消费订单(编辑数据服务接口)
 * 说明：消费订单(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-13
 * 作者：admin
 */
public interface IExmOrderEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(ExmOrderBO req);
}