package com.destroyer.will.service.enlCollege;

import com.destroyer.core.entity.enlCollege.bo.EnlCollegeQueryBO;
import com.destroyer.core.entity.enlCollege.vo.EnlCollegeVO;
import java.util.Map;

 /**
 * 标题：招生院校(获取Map集合服务接口)
 * 说明：招生院校(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlCollegeGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, EnlCollegeVO> getMapGroupById(EnlCollegeQueryBO req);
}