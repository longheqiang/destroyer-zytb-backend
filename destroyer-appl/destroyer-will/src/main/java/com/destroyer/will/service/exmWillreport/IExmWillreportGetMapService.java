package com.destroyer.will.service.exmWillreport;

import com.destroyer.core.entity.exmWillreport.bo.ExmWillreportQueryBO;
import com.destroyer.core.entity.exmWillreport.vo.ExmWillreportVO;
import java.util.Map;

 /**
 * 标题：分析报告(获取Map集合服务接口)
 * 说明：分析报告(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillreportGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, ExmWillreportVO> getMapGroupById(ExmWillreportQueryBO req);
}