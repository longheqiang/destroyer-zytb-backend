package com.destroyer.will.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.bscMajors.bo.BscMajorsBO;
import com.destroyer.core.entity.bscMajors.bo.BscMajorsQueryBO;
import com.destroyer.core.entity.bscMajors.vo.BscMajorsVO;
import com.destroyer.will.service.bscMajors.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;

/**
 * 标题：专业库(控制层)
 * 说明：专业库(控制层),包括所有对外前端API接口
 * 时间：2024-3-7
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/will/bscMajors")
@Api(value = "BscMajorsController", tags = "专业库API")
public class BscMajorsController {


    /**
     * 基础服务
     */
    @Autowired
    private IBscMajorsBaseService baseService;


    /**
     * 新增数据服务
     */
    @Autowired
    private IBscMajorsCreateService createService;



    /**
     * 批量新增服务
     */
    @Autowired
    private IBscMajorsBatchCreateService batchCreateService;



    /**
     * 删除数据服务
     */
    @Autowired
    private IBscMajorsDeleteService deleteService;


    /**
     * 列表服务
     */
    @Autowired
    private IBscMajorsGetListService getListService;


    /**
     * map服务
     */
    @Autowired
    private IBscMajorsGetMapService getMapService;


    /**
     * 详情服务
     */
    @Autowired
    private IBscMajorsGetDetailService getDetailService;


    /**
     * 编辑服务
     */
    @Autowired
    private IBscMajorsEditService editService;


    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody BscMajorsBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }


    /**
     * Excel服务
     */
    @Autowired
    private IBscMajorsExcelService excelService;



    @PostMapping(value = "/batchCreate")
    @ApiOperation("批量新增API")
    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<BscMajorsBO> req) {
        List<Long> rsp = this.batchCreateService.batchCreate(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/deleteById")
    @ApiOperation("删除数据API(根据主键ID)")
    public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
        boolean rsp = this.deleteService.deleteById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }



    @PostMapping(value = "/deleteBy")
    @ApiOperation("删除数据API(根据查询条件删除)")
    public Result<Boolean> deleteBy(@RequestBody BscMajorsQueryBO req) {
        boolean rsp = this.deleteService.batchDeleteBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<BscMajorsVO>> list(@RequestBody BscMajorsQueryBO req) {
        List<BscMajorsVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/mapGroupById")
    @ApiOperation("查询map(ID分组)API")
    public Result<Map<Long, BscMajorsVO>> mapGroupById(@RequestBody BscMajorsQueryBO req) {
        Map<Long, BscMajorsVO> rsp = this.getMapService.getMapGroupById(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<BscMajorsVO>> page(@RequestBody BscMajorsQueryBO req) {
        Page<BscMajorsVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<BscMajorsVO> detailBy(@RequestBody BscMajorsQueryBO req) {
        BscMajorsVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailById")
    @ApiOperation("根据ID查询详情API")
    public Result<BscMajorsVO> detailById(@RequestBody BaseIdBO req) {
        BscMajorsVO rsp = this.getDetailService.getDetailById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody BscMajorsBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }


    @GetMapping("/exportExcelModule")
    @ApiOperation("导出Excel模板API")
    @ResponseBody
    public void exportExcelModule(HttpServletResponse response) throws IOException {
        excelService.exportExcelModule(response);
    }


    @PostMapping("/importExcelData")
    @ApiOperation("导入Excel数据API")
    public Result<Boolean> importExcelData(@RequestParam("file") MultipartFile req) throws IOException {
        boolean rsp = excelService.importExcelData(req);
        return Result.success(rsp);
    }
}