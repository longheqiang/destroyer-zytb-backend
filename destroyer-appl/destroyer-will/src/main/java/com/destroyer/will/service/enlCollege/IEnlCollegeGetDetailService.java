package com.destroyer.will.service.enlCollege;

import com.destroyer.core.entity.enlCollege.bo.EnlCollegeQueryBO;
import com.destroyer.core.entity.enlCollege.vo.EnlCollegeVO;
import java.util.List;

 /**
 * 标题：招生院校(查询详情服务接口)
 * 说明：招生院校(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlCollegeGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    EnlCollegeVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    EnlCollegeVO getDetailBy(EnlCollegeQueryBO req);
   
}