package com.destroyer.will.service.impl.enlMajordata;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.enlMajordata.EnlMajordataPO;
import com.destroyer.will.mapper.EnlMajordataMapper;
import com.destroyer.will.service.enlMajordata.IEnlMajordataBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：专业录取数据表(基础通用服务实现)
 * 说明：专业录取数据表(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class  EnlMajordataBaseServiceImpl extends ServiceImpl<EnlMajordataMapper, EnlMajordataPO> implements IEnlMajordataBaseService {
}