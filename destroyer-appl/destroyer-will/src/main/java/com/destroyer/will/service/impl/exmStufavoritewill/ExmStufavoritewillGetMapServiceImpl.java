package com.destroyer.will.service.impl.exmStufavoritewill;

import com.destroyer.core.entity.exmStufavoritewill.bo.ExmStufavoritewillQueryBO;
import com.destroyer.core.entity.exmStufavoritewill.vo.ExmStufavoritewillVO;
import com.destroyer.will.service.exmStufavoritewill.IExmStufavoritewillGetListService;
import com.destroyer.will.service.exmStufavoritewill.IExmStufavoritewillGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：学生收藏志愿(获取Map集合服务实现)
 * 说明：学生收藏志愿(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class ExmStufavoritewillGetMapServiceImpl implements IExmStufavoritewillGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IExmStufavoritewillGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, ExmStufavoritewillVO> getMapGroupById(ExmStufavoritewillQueryBO req) {
        Map<Long, ExmStufavoritewillVO> rsp = new HashMap<>();
        List<ExmStufavoritewillVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(ExmStufavoritewillVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}