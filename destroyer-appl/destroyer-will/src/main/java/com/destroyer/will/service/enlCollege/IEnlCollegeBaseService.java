package com.destroyer.will.service.enlCollege;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.enlCollege.EnlCollegePO;

 /**
 * 标题：招生院校(基础通用服务接口)
 * 说明：招生院校(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlCollegeBaseService  extends IService<EnlCollegePO> {
}