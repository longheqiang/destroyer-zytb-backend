package com.destroyer.will.service.impl.exmStudent;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.exmStudent.ExmStudentPO;
import com.destroyer.will.mapper.ExmStudentMapper;
import com.destroyer.will.service.exmStudent.IExmStudentBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：志愿学生(基础通用服务实现)
 * 说明：志愿学生(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-1
 * 作者：admin
 */
@Service
public class  ExmStudentBaseServiceImpl extends ServiceImpl<ExmStudentMapper, ExmStudentPO> implements IExmStudentBaseService {
}