package com.destroyer.will.service.exmWillcondition;

import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.exmWillcondition.bo.ExmWillconditionBO;
import java.util.List;

 /**
 * 标题：学生志愿条件(批量新增服务接口)
 * 说明：学生志愿条件(批量新增服务接口),自定义批量新增据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillconditionBatchCreateService {
    
    
    /**
    * 批量新增数据
    * @param req
    * @return 新增数据的组件ID集合
    */
    List<Long> batchCreate(BaseBatchBO<ExmWillconditionBO> req);
}