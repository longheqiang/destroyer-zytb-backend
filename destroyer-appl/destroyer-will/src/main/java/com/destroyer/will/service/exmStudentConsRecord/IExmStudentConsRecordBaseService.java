package com.destroyer.will.service.exmStudentConsRecord;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.exmStudentConsRecord.ExmStudentConsRecordPO;

 /**
 * 标题：用户消费记录(基础通用服务接口)
 * 说明：用户消费记录(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmStudentConsRecordBaseService  extends IService<ExmStudentConsRecordPO> {
}