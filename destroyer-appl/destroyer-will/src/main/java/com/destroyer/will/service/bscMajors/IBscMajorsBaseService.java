package com.destroyer.will.service.bscMajors;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.bscMajors.BscMajorsPO;

 /**
 * 标题：专业库(基础通用服务接口)
 * 说明：专业库(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscMajorsBaseService  extends IService<BscMajorsPO> {
}