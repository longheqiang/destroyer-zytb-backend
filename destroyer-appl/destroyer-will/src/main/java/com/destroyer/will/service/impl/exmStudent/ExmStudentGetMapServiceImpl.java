package com.destroyer.will.service.impl.exmStudent;

import com.destroyer.core.entity.exmStudent.bo.ExmStudentQueryBO;
import com.destroyer.core.entity.exmStudent.vo.ExmStudentVO;
import com.destroyer.will.service.exmStudent.IExmStudentGetListService;
import com.destroyer.will.service.exmStudent.IExmStudentGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：志愿学生(获取Map集合服务实现)
 * 说明：志愿学生(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-1
 * 作者：admin
 */
@Service
public class ExmStudentGetMapServiceImpl implements IExmStudentGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IExmStudentGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, ExmStudentVO> getMapGroupById(ExmStudentQueryBO req) {
        Map<Long, ExmStudentVO> rsp = new HashMap<>();
        List<ExmStudentVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(ExmStudentVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}