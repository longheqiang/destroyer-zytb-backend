package com.destroyer.will.service.impl.exmWillcondition;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.exmWillcondition.ExmWillconditionPO;
import com.destroyer.will.mapper.ExmWillconditionMapper;
import com.destroyer.will.service.exmWillcondition.IExmWillconditionBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：学生志愿条件(基础通用服务实现)
 * 说明：学生志愿条件(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class  ExmWillconditionBaseServiceImpl extends ServiceImpl<ExmWillconditionMapper, ExmWillconditionPO> implements IExmWillconditionBaseService {
}