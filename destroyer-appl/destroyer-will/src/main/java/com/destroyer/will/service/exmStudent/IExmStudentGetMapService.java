package com.destroyer.will.service.exmStudent;

import com.destroyer.core.entity.exmStudent.bo.ExmStudentQueryBO;
import com.destroyer.core.entity.exmStudent.vo.ExmStudentVO;
import java.util.Map;

 /**
 * 标题：志愿学生(获取Map集合服务接口)
 * 说明：志愿学生(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStudentGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, ExmStudentVO> getMapGroupById(ExmStudentQueryBO req);
}