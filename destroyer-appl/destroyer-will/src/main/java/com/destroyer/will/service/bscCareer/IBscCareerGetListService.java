package com.destroyer.will.service.bscCareer;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.bscCareer.bo.BscCareerQueryBO;
import com.destroyer.core.entity.bscCareer.vo.BscCareerVO;
import java.util.List;

 /**
 * 标题：职业库(获取列表服务接口)
 * 说明：职业库(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscCareerGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<BscCareerVO> getList(BscCareerQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<BscCareerVO> getPageList(BscCareerQueryBO req);
}