package com.destroyer.will.service.bscMajors;

import com.destroyer.core.entity.bscMajors.bo.BscMajorsBO;

 /**
 * 标题：专业库(新增服务接口)
 * 说明：专业库(新增服务接口),自定义新增数据接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscMajorsCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(BscMajorsBO req);
}