package com.destroyer.will.service.impl.enlMajorplan;

import com.destroyer.core.entity.enlMajorplan.bo.EnlMajorplanQueryBO;
import com.destroyer.core.entity.enlMajorplan.vo.EnlMajorplanVO;
import com.destroyer.will.service.enlMajorplan.IEnlMajorplanGetListService;
import com.destroyer.will.service.enlMajorplan.IEnlMajorplanGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：专业录取计划(获取Map集合服务实现)
 * 说明：专业录取计划(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class EnlMajorplanGetMapServiceImpl implements IEnlMajorplanGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IEnlMajorplanGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, EnlMajorplanVO> getMapGroupById(EnlMajorplanQueryBO req) {
        Map<Long, EnlMajorplanVO> rsp = new HashMap<>();
        List<EnlMajorplanVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(EnlMajorplanVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}