package com.destroyer.will.service.exmStuwill;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.exmStuwill.ExmStuwillPO;

 /**
 * 标题：学生志愿(基础通用服务接口)
 * 说明：学生志愿(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStuwillBaseService  extends IService<ExmStuwillPO> {
}