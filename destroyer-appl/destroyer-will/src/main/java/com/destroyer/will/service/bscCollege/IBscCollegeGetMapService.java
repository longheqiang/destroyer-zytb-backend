package com.destroyer.will.service.bscCollege;

import com.destroyer.core.entity.bscCollege.bo.BscCollegeQueryBO;
import com.destroyer.core.entity.bscCollege.vo.BscCollegeVO;
import java.util.Map;

 /**
 * 标题：院校库(获取Map集合服务接口)
 * 说明：院校库(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscCollegeGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, BscCollegeVO> getMapGroupById(BscCollegeQueryBO req);
}