package com.destroyer.will.service.impl.bscYear;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.bscYear.BscYearPO;
import com.destroyer.core.entity.bscYear.bo.BscYearBO;
import com.destroyer.will.service.bscYear.IBscYearBaseService;
import com.destroyer.will.service.bscYear.IBscYearBatchCreateService;
import com.destroyer.core.util.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;

 /**
 * 标题：年份(批量新增服务实现)
 * 说明：年份(批量新增服务实现),自定义批量新增据服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class BscYearBatchCreateServiceImpl implements IBscYearBatchCreateService {
    
    
    /**
     * 基础通用服务
     */
    @Autowired
    private IBscYearBaseService baseService;
    /**
     * 批量新增数据
     * @param req
     * @return 新增数据的组件ID集合
     */
    @Override
    public List<Long> batchCreate(BaseBatchBO<BscYearBO> req) {
        List<Long> rsp = new ArrayList<>();
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        List<BscYearPO> poList = req.getList().stream().map(k->{
            BscYearPO po = new BscYearPO(k);
            return po;
        }).collect(Collectors.toList());
        boolean state = baseService.saveBatch(poList);
        if(!state){
            throw new ServiceException(ResultEnum.FAILURE, "批量新增数据失败");
        }
        rsp = poList.stream().map(BscYearPO::getId).collect(Collectors.toList());
        return rsp;
    }
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(BaseBatchBO<BscYearBO> req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
            if(null == req || req.getList().size() == 0){
                throw new ServiceException(ResultEnum.PARAM_MISS, "批量新增入参不能为空");
            }
            List<BscYearBO> list = req.getList();
            for (BscYearBO i : list) {
                
                
                
                
                
                
                
                
                
                
                
            }
        }
    }
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(BaseBatchBO<BscYearBO> req) {
        // TODO 执行前调用逻辑
    }
}