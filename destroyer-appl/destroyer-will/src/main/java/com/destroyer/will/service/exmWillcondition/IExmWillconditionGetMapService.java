package com.destroyer.will.service.exmWillcondition;

import com.destroyer.core.entity.exmWillcondition.bo.ExmWillconditionQueryBO;
import com.destroyer.core.entity.exmWillcondition.vo.ExmWillconditionVO;
import java.util.Map;

 /**
 * 标题：学生志愿条件(获取Map集合服务接口)
 * 说明：学生志愿条件(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillconditionGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, ExmWillconditionVO> getMapGroupById(ExmWillconditionQueryBO req);
}