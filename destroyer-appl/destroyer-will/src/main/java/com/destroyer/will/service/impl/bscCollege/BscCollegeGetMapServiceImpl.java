package com.destroyer.will.service.impl.bscCollege;

import com.destroyer.core.entity.bscCollege.bo.BscCollegeQueryBO;
import com.destroyer.core.entity.bscCollege.vo.BscCollegeVO;
import com.destroyer.will.service.bscCollege.IBscCollegeGetListService;
import com.destroyer.will.service.bscCollege.IBscCollegeGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：院校库(获取Map集合服务实现)
 * 说明：院校库(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class BscCollegeGetMapServiceImpl implements IBscCollegeGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IBscCollegeGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, BscCollegeVO> getMapGroupById(BscCollegeQueryBO req) {
        Map<Long, BscCollegeVO> rsp = new HashMap<>();
        List<BscCollegeVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(BscCollegeVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}