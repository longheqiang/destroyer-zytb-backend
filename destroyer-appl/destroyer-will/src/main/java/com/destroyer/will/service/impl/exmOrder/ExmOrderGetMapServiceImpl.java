package com.destroyer.will.service.impl.exmOrder;

import com.destroyer.core.entity.exmOrder.bo.ExmOrderQueryBO;
import com.destroyer.core.entity.exmOrder.vo.ExmOrderVO;
import com.destroyer.will.service.exmOrder.IExmOrderGetListService;
import com.destroyer.will.service.exmOrder.IExmOrderGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：消费订单(获取Map集合服务实现)
 * 说明：消费订单(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-13
 * 作者：admin
 */
@Service
public class ExmOrderGetMapServiceImpl implements IExmOrderGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IExmOrderGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, ExmOrderVO> getMapGroupById(ExmOrderQueryBO req) {
        Map<Long, ExmOrderVO> rsp = new HashMap<>();
        List<ExmOrderVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(ExmOrderVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}