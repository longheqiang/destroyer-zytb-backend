package com.destroyer.will.service.enlMajorplan;

import com.destroyer.core.entity.enlMajorplan.bo.EnlMajorplanQueryBO;
import com.destroyer.core.entity.enlMajorplan.vo.EnlMajorplanVO;
import java.util.Map;

 /**
 * 标题：专业录取计划(获取Map集合服务接口)
 * 说明：专业录取计划(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajorplanGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, EnlMajorplanVO> getMapGroupById(EnlMajorplanQueryBO req);
}