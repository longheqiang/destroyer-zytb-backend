package com.destroyer.will.service.exmOrder;

import com.destroyer.core.entity.exmOrder.bo.ExmOrderQueryBO;
import com.destroyer.core.entity.exmOrder.vo.ExmOrderVO;
import java.util.Map;

 /**
 * 标题：消费订单(获取Map集合服务接口)
 * 说明：消费订单(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-13
 * 作者：admin
 */
public interface IExmOrderGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, ExmOrderVO> getMapGroupById(ExmOrderQueryBO req);
}