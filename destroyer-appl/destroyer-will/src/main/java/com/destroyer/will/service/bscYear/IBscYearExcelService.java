package com.destroyer.will.service.bscYear;

import com.destroyer.core.service.excel.IExcelBaseService;


/**
 * 标题：年份(Excel服务接口)
 * 说明：年份(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-7
 * 作者：admin
 */
public interface IBscYearExcelService extends IExcelBaseService {



}