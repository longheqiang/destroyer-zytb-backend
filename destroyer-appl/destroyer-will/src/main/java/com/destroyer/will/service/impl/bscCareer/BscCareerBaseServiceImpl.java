package com.destroyer.will.service.impl.bscCareer;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.bscCareer.BscCareerPO;
import com.destroyer.will.mapper.BscCareerMapper;
import com.destroyer.will.service.bscCareer.IBscCareerBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：职业库(基础通用服务实现)
 * 说明：职业库(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class  BscCareerBaseServiceImpl extends ServiceImpl<BscCareerMapper, BscCareerPO> implements IBscCareerBaseService {
}