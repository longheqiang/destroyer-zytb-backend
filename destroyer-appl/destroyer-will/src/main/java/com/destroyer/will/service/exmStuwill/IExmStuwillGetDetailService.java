package com.destroyer.will.service.exmStuwill;

import com.destroyer.core.entity.exmStuwill.bo.ExmStuwillQueryBO;
import com.destroyer.core.entity.exmStuwill.vo.ExmStuwillVO;
import java.util.List;

 /**
 * 标题：学生志愿(查询详情服务接口)
 * 说明：学生志愿(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStuwillGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    ExmStuwillVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    ExmStuwillVO getDetailBy(ExmStuwillQueryBO req);
   
}