package com.destroyer.will.service.exmWillcondition;

import com.destroyer.core.entity.exmWillcondition.bo.ExmWillconditionQueryBO;
import com.destroyer.core.entity.exmWillcondition.vo.ExmWillconditionVO;
import java.util.List;

 /**
 * 标题：学生志愿条件(查询详情服务接口)
 * 说明：学生志愿条件(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillconditionGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    ExmWillconditionVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    ExmWillconditionVO getDetailBy(ExmWillconditionQueryBO req);
   
}