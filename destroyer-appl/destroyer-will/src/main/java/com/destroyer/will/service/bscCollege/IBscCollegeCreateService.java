package com.destroyer.will.service.bscCollege;

import com.destroyer.core.entity.bscCollege.bo.BscCollegeBO;

 /**
 * 标题：院校库(新增服务接口)
 * 说明：院校库(新增服务接口),自定义新增数据接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscCollegeCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(BscCollegeBO req);
}