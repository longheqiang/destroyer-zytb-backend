package com.destroyer.will.service.exmWillreport;

import com.destroyer.core.entity.exmWillreport.bo.ExmWillreportBO;

 /**
 * 标题：分析报告(编辑数据服务接口)
 * 说明：分析报告(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillreportEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(ExmWillreportBO req);
}