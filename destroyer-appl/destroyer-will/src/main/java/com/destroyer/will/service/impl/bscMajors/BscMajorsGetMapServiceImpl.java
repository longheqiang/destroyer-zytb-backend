package com.destroyer.will.service.impl.bscMajors;

import com.destroyer.core.entity.bscMajors.bo.BscMajorsQueryBO;
import com.destroyer.core.entity.bscMajors.vo.BscMajorsVO;
import com.destroyer.will.service.bscMajors.IBscMajorsGetListService;
import com.destroyer.will.service.bscMajors.IBscMajorsGetMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

 /**
 * 标题：专业库(获取Map集合服务实现)
 * 说明：专业库(获取Map集合服务实现),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class BscMajorsGetMapServiceImpl implements IBscMajorsGetMapService {
    
    
    /**
     * 列表服务
     */
    @Autowired
    private IBscMajorsGetListService getListService;
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    @Override
    public Map<Long, BscMajorsVO> getMapGroupById(BscMajorsQueryBO req) {
        Map<Long, BscMajorsVO> rsp = new HashMap<>();
        List<BscMajorsVO> list = this.getListService.getList(req);
        rsp = list.stream().collect(Collectors.toMap(BscMajorsVO::getId, Function.identity(),(k1, k2)->k2));
        return rsp;
    }
}