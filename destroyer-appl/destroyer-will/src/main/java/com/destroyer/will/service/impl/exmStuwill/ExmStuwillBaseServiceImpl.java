package com.destroyer.will.service.impl.exmStuwill;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.exmStuwill.ExmStuwillPO;
import com.destroyer.will.mapper.ExmStuwillMapper;
import com.destroyer.will.service.exmStuwill.IExmStuwillBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：学生志愿(基础通用服务实现)
 * 说明：学生志愿(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-1
 * 作者：admin
 */
@Service
public class  ExmStuwillBaseServiceImpl extends ServiceImpl<ExmStuwillMapper, ExmStuwillPO> implements IExmStuwillBaseService {
}