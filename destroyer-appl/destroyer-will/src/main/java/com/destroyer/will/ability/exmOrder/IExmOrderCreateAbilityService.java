package com.destroyer.will.ability.exmOrder;

import com.destroyer.core.entity.exmOrder.bo.ExmOrderBO;
import com.destroyer.core.entity.wechat.vo.WechatPrepayWithRequestPaymentVO;

/**
 * 标题：IExmOrderCreateAbilityService
 * 说明：新建订单能力接口
 * 时间：2024/3/13
 * 作者：admin
 */
public interface IExmOrderCreateAbilityService {

    /**
     * 新建订单
     * @param req
     * @return
     */
    WechatPrepayWithRequestPaymentVO createAndRequestPayment(ExmOrderBO req);
}
