package com.destroyer.will.service.exmOrder;

import com.destroyer.core.entity.exmOrder.bo.ExmOrderBO;

 /**
 * 标题：消费订单(新增服务接口)
 * 说明：消费订单(新增服务接口),自定义新增数据接口
 * 时间：2024-3-13
 * 作者：admin
 */
public interface IExmOrderCreateService {
    /**
     * 新增数据
     * @param req
     * @return 主键ID
     */
    Long create(ExmOrderBO req);
}