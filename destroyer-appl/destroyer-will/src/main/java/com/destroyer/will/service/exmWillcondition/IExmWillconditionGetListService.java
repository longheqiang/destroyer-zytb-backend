package com.destroyer.will.service.exmWillcondition;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.exmWillcondition.bo.ExmWillconditionQueryBO;
import com.destroyer.core.entity.exmWillcondition.vo.ExmWillconditionVO;
import java.util.List;

 /**
 * 标题：学生志愿条件(获取列表服务接口)
 * 说明：学生志愿条件(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillconditionGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<ExmWillconditionVO> getList(ExmWillconditionQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<ExmWillconditionVO> getPageList(ExmWillconditionQueryBO req);
}