package com.destroyer.will.service.exmStudent;

import com.baomidou.mybatisplus.extension.service.IService;
import com.destroyer.core.entity.exmStudent.ExmStudentPO;

 /**
 * 标题：志愿学生(基础通用服务接口)
 * 说明：志愿学生(基础通用服务接口),MP自带的增删查改等基础服务接口
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStudentBaseService  extends IService<ExmStudentPO> {
}