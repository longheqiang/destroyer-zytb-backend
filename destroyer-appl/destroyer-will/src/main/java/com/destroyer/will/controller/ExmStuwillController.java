package com.destroyer.will.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.exmStuwill.bo.ExmStuwillBO;
import com.destroyer.core.entity.exmStuwill.bo.ExmStuwillQueryBO;
import com.destroyer.core.entity.exmStuwill.vo.ExmStuwillVO;
import com.destroyer.will.service.exmStuwill.*;

/**
 * 标题：学生志愿(控制层)
 * 说明：学生志愿(控制层),包括所有对外前端API接口
 * 时间：2024-3-7
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/will/exmStuwill")
@Api(value = "ExmStuwillController", tags = "学生志愿API")
public class ExmStuwillController {


    /**
     * 基础服务
     */
    @Autowired
    private IExmStuwillBaseService baseService;


    /**
     * 新增数据服务
     */
    @Autowired
    private IExmStuwillCreateService createService;



    /**
     * 批量新增服务
     */
    @Autowired
    private IExmStuwillBatchCreateService batchCreateService;



    /**
     * 删除数据服务
     */
    @Autowired
    private IExmStuwillDeleteService deleteService;


    /**
     * 列表服务
     */
    @Autowired
    private IExmStuwillGetListService getListService;


    /**
     * map服务
     */
    @Autowired
    private IExmStuwillGetMapService getMapService;


    /**
     * 详情服务
     */
    @Autowired
    private IExmStuwillGetDetailService getDetailService;


    /**
     * 编辑服务
     */
    @Autowired
    private IExmStuwillEditService editService;


    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody ExmStuwillBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }


    /**
     * Excel服务
     */
    @Autowired
    private IExmStuwillExcelService excelService;



    @PostMapping(value = "/batchCreate")
    @ApiOperation("批量新增API")
    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<ExmStuwillBO> req) {
        List<Long> rsp = this.batchCreateService.batchCreate(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/deleteById")
    @ApiOperation("删除数据API(根据主键ID)")
    public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
        boolean rsp = this.deleteService.deleteById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }



    @PostMapping(value = "/deleteBy")
    @ApiOperation("删除数据API(根据查询条件删除)")
    public Result<Boolean> deleteBy(@RequestBody ExmStuwillQueryBO req) {
        boolean rsp = this.deleteService.batchDeleteBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<ExmStuwillVO>> list(@RequestBody ExmStuwillQueryBO req) {
        List<ExmStuwillVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/mapGroupById")
    @ApiOperation("查询map(ID分组)API")
    public Result<Map<Long, ExmStuwillVO>> mapGroupById(@RequestBody ExmStuwillQueryBO req) {
        Map<Long, ExmStuwillVO> rsp = this.getMapService.getMapGroupById(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<ExmStuwillVO>> page(@RequestBody ExmStuwillQueryBO req) {
        Page<ExmStuwillVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<ExmStuwillVO> detailBy(@RequestBody ExmStuwillQueryBO req) {
        ExmStuwillVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailById")
    @ApiOperation("根据ID查询详情API")
    public Result<ExmStuwillVO> detailById(@RequestBody BaseIdBO req) {
        ExmStuwillVO rsp = this.getDetailService.getDetailById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody ExmStuwillBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }


    @GetMapping("/exportExcelModule")
    @ApiOperation("导出Excel模板API")
    @ResponseBody
    public void exportExcelModule(HttpServletResponse response) throws IOException {
        excelService.exportExcelModule(response);
    }


    @PostMapping("/importExcelData")
    @ApiOperation("导入Excel数据API")
    public Result<Boolean> importExcelData(@RequestParam("file") MultipartFile req) throws IOException {
        boolean rsp = excelService.importExcelData(req);
        return Result.success(rsp);
    }
}