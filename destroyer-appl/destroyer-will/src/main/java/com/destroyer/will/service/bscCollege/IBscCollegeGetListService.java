package com.destroyer.will.service.bscCollege;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.bscCollege.bo.BscCollegeQueryBO;
import com.destroyer.core.entity.bscCollege.vo.BscCollegeVO;
import java.util.List;

 /**
 * 标题：院校库(获取列表服务接口)
 * 说明：院校库(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IBscCollegeGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<BscCollegeVO> getList(BscCollegeQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<BscCollegeVO> getPageList(BscCollegeQueryBO req);
}