package com.destroyer.will.service.exmStudentConsRecord;

import com.destroyer.core.entity.exmStudentConsRecord.bo.ExmStudentConsRecordBO;

 /**
 * 标题：用户消费记录(编辑数据服务接口)
 * 说明：用户消费记录(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmStudentConsRecordEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(ExmStudentConsRecordBO req);
}