package com.destroyer.will.service.exmWillreport;

import com.destroyer.core.entity.exmWillreport.bo.ExmWillreportQueryBO;
import com.destroyer.core.entity.exmWillreport.vo.ExmWillreportVO;
import java.util.List;

 /**
 * 标题：分析报告(查询详情服务接口)
 * 说明：分析报告(查询详情服务接口),自定义基础详情查询接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmWillreportGetDetailService {
    
    
    /**
    * 根据主键ID查询详情
    * @param req
    * @return
    */
    ExmWillreportVO getDetailById(Long req);
    
    
    /**
    * 根据条件查询详情
    * @param req
    * @return
    */
    ExmWillreportVO getDetailBy(ExmWillreportQueryBO req);
   
}