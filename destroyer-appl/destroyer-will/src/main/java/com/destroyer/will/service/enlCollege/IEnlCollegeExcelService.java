package com.destroyer.will.service.enlCollege;

import com.destroyer.core.service.excel.IExcelBaseService;

/**
 * 标题：招生院校(Excel服务接口)
 * 说明：招生院校(Excel服务接口),自定义Excel服务接口
 * 时间：2024-3-7
 * 作者：admin
 */
public interface IEnlCollegeExcelService extends IExcelBaseService {



}