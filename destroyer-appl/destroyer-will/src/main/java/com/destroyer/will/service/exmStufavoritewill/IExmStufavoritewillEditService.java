package com.destroyer.will.service.exmStufavoritewill;

import com.destroyer.core.entity.exmStufavoritewill.bo.ExmStufavoritewillBO;

 /**
 * 标题：学生收藏志愿(编辑数据服务接口)
 * 说明：学生收藏志愿(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmStufavoritewillEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(ExmStufavoritewillBO req);
}