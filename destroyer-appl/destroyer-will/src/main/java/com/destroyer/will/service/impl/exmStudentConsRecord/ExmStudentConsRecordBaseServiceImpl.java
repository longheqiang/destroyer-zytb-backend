package com.destroyer.will.service.impl.exmStudentConsRecord;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.exmStudentConsRecord.ExmStudentConsRecordPO;
import com.destroyer.will.mapper.ExmStudentConsRecordMapper;
import com.destroyer.will.service.exmStudentConsRecord.IExmStudentConsRecordBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：用户消费记录(基础通用服务实现)
 * 说明：用户消费记录(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class  ExmStudentConsRecordBaseServiceImpl extends ServiceImpl<ExmStudentConsRecordMapper, ExmStudentConsRecordPO> implements IExmStudentConsRecordBaseService {
}