package com.destroyer.will.service.impl.bscYear;

import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.bscYear.BscYearPO;
import com.destroyer.core.entity.bscYear.bo.BscYearBO;
import com.destroyer.will.service.bscYear.IBscYearBaseService;
import com.destroyer.will.service.bscYear.IBscYearEditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 /**
 * 标题：年份(编辑数据服务实现)
 * 说明：年份(编辑数据服务实现),自定义编辑数据服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class BscYearEditServiceImpl implements IBscYearEditService {
    
    
     /**
     * 基础通用服务
     */
    @Autowired
    private IBscYearBaseService baseService;
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    @Override
    public boolean editById(BscYearBO req) {
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        //入参处理
        BscYearPO po = new BscYearPO(req);
        //更新数据
        boolean rsp = this.baseService.updateById(po);
        return rsp;
    }
    
    
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(BscYearBO req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
	
        if (null == req.getId()) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "更新数据入参ID不能为空");
        }
	
	
	
	
	
	
	
	
        }
    }
    
    
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(BscYearBO req) {
        // TODO 执行前调用逻辑
    }
}