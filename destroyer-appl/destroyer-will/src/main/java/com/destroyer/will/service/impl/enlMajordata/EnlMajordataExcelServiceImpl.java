package com.destroyer.will.service.impl.enlMajordata;

import com.alibaba.excel.EasyExcel;
import com.destroyer.common.entity.system.ResultEnum;
import com.destroyer.common.excel.EasyExcelUtil;
import com.destroyer.common.excel.MyReadListener;
import com.destroyer.common.excel.ReadBack;
import com.destroyer.common.exception.ServerDataException;
import com.destroyer.common.exception.ServiceException;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.util.ResponseUtil;
import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataBO;
import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataExcelBO;
import com.destroyer.common.util.DateUtils;
import com.destroyer.will.service.enlMajordata.IEnlMajordataBatchCreateService;
import com.destroyer.will.service.enlMajordata.IEnlMajordataExcelService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 标题：专业录取数据表(Excel服务实现)
 * 说明：专业录取数据表(Excel服务实现),Excel相关服务
 * 时间：2024-3-7
 * 作者：admin
 */
@Service
public class EnlMajordataExcelServiceImpl implements IEnlMajordataExcelService {


    /**
     * 批量新增服务
     */
    @Autowired
    private IEnlMajordataBatchCreateService batchCreateService;


    private final static String SHEET_NAME = "专业录取数据表";


    /**
     * 导出模板
     *
     * @param response
     * @return
     */
    @Override
    public boolean exportExcelModule(HttpServletResponse response) throws IOException {
        List<EnlMajordataExcelBO> rsp = new ArrayList<>();
        EnlMajordataExcelBO example = new EnlMajordataExcelBO().buildExportModule();
        rsp.add(example);
        String name = SHEET_NAME + "Excel模板";
        ResponseUtil.setExcelResponse(response, name);
        EasyExcelUtil.getExcelWriterBuilder(response.getOutputStream(), EnlMajordataExcelBO.class).sheet(SHEET_NAME).doWrite(rsp);
        return false;
    }


    /**
     * 导入数据
     * @param req
     * @return
     */
    @Override
    public boolean importExcelData(MultipartFile req) throws IOException {
        boolean rsp = false;
        //入参效验
        this.validParams(req);
        //执行核心逻辑前调用
        this.beforeToDo(req);
        final List<EnlMajordataExcelBO> excelList = new ArrayList<>();
        //读取Excel内容
        EasyExcel.read(req.getInputStream(), EnlMajordataExcelBO.class,
                new MyReadListener<>((ReadBack<EnlMajordataExcelBO>) excelList::add)).sheet(SHEET_NAME).doRead();
        if (excelList.size() == 0) {
            throw new ServerDataException("excel 解析失败");
        }
        //保存数据
        this.saveData(excelList);
        return rsp;
    }
    /**
     * 保存数据
     * @param req
     * @return
     */
    private boolean saveData(List<EnlMajordataExcelBO> req){
        boolean rsp = false;
        BaseBatchBO<EnlMajordataBO> baseBatchBO = new BaseBatchBO<>();
        List<EnlMajordataBO>  boList = new ArrayList<>();
        for (EnlMajordataExcelBO excelBO:req){
            EnlMajordataBO bo = new EnlMajordataBO();
            //新增逻辑控制，内容拷贝
            BeanUtils.copyProperties(excelBO,bo);
            boList.add(bo);
        }
        if (boList.size() > 0) {
            baseBatchBO.setList(boList);
            rsp = batchCreateService.batchCreate(baseBatchBO).size() > 0;
        }
        return rsp;
    }
    /**
     * 类通用入参校验
     *
     * @param req
     */
    private void validParams(MultipartFile req) {
        if (null == req) {
            throw new ServiceException(ResultEnum.PARAM_MISS, "入参不能为空");
        } else {
        }
    }
    /**
     * 类通用执行前调用逻辑
     *
     * @param req 入参
     */
    private void beforeToDo(MultipartFile req) {
        // TODO 执行前调用逻辑
    }
}