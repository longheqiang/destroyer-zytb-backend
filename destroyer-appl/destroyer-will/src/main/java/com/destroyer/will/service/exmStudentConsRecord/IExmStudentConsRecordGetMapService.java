package com.destroyer.will.service.exmStudentConsRecord;

import com.destroyer.core.entity.exmStudentConsRecord.bo.ExmStudentConsRecordQueryBO;
import com.destroyer.core.entity.exmStudentConsRecord.vo.ExmStudentConsRecordVO;
import java.util.Map;

 /**
 * 标题：用户消费记录(获取Map集合服务接口)
 * 说明：用户消费记录(获取Map集合服务接口),用于查询各种Map集合服务
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmStudentConsRecordGetMapService {
    
    
    /**
     * 根据ID分组查询map集合
     * @param req
     * @return
     */
    Map<Long, ExmStudentConsRecordVO> getMapGroupById(ExmStudentConsRecordQueryBO req);
}