package com.destroyer.will.service.impl.exmWillreport;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.destroyer.core.entity.exmWillreport.ExmWillreportPO;
import com.destroyer.will.mapper.ExmWillreportMapper;
import com.destroyer.will.service.exmWillreport.IExmWillreportBaseService;
import org.springframework.stereotype.Service;

 /**
 * 标题：分析报告(基础通用服务实现)
 * 说明：分析报告(基础通用服务实现),MP自带的增删查改等基础服务实现
 * 时间：2024-3-4
 * 作者：admin
 */
@Service
public class  ExmWillreportBaseServiceImpl extends ServiceImpl<ExmWillreportMapper, ExmWillreportPO> implements IExmWillreportBaseService {
}