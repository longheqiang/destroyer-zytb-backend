package com.destroyer.will.service.exmStuwill;

import com.destroyer.core.entity.exmStuwill.bo.ExmStuwillBO;

 /**
 * 标题：学生志愿(编辑数据服务接口)
 * 说明：学生志愿(编辑数据服务接口),自定义编辑数据服务接口
 * 时间：2024-3-1
 * 作者：admin
 */
public interface IExmStuwillEditService {
    
    
    /**
     * 更新数据（根据主键ID）
     * @param req
     * @return
     */
    boolean editById(ExmStuwillBO req);
}