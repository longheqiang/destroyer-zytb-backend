package com.destroyer.will.service.enlMajordata;

import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.enlMajordata.bo.EnlMajordataQueryBO;

/**
 * 标题：专业录取数据表(删除数据服务接口)
 * 说明：专业录取数据表(删除数据服务接口),自定义删除数据服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IEnlMajordataDeleteService {
    /**
     * 删除数据（根据主键ID）
     * @param req
     * @return
     */
    boolean deleteById(Long req);
    
    
    /**
     * 删除数据（根据主键ID集合批量删除）
     * @param req
     * @return
     */
    boolean batchDeleteByIds(BaseIdListBO req);
    
    
     /**
     * 删除数据（根据查询条件）
     * @param req
     * @return
     */
    boolean batchDeleteBy(EnlMajordataQueryBO req);
}