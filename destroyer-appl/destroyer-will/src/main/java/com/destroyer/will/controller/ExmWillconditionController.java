package com.destroyer.will.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import com.destroyer.common.entity.system.Result;
import com.destroyer.core.entity.base.BaseIdBO;
import com.destroyer.core.entity.base.BaseIdListBO;
import com.destroyer.core.entity.base.BaseBatchBO;
import com.destroyer.core.entity.exmWillcondition.bo.ExmWillconditionBO;
import com.destroyer.core.entity.exmWillcondition.bo.ExmWillconditionQueryBO;
import com.destroyer.core.entity.exmWillcondition.vo.ExmWillconditionVO;
import com.destroyer.will.service.exmWillcondition.*;

/**
 * 标题：学生志愿条件(控制层)
 * 说明：学生志愿条件(控制层),包括所有对外前端API接口
 * 时间：2024-3-7
 * 作者：admin
 */
@RestController
@CrossOrigin
@RequestMapping("/api/destroyer/will/exmWillcondition")
@Api(value = "ExmWillconditionController", tags = "学生志愿条件API")
public class ExmWillconditionController {


    /**
     * 基础服务
     */
    @Autowired
    private IExmWillconditionBaseService baseService;


    /**
     * 新增数据服务
     */
    @Autowired
    private IExmWillconditionCreateService createService;



    /**
     * 批量新增服务
     */
    @Autowired
    private IExmWillconditionBatchCreateService batchCreateService;



    /**
     * 删除数据服务
     */
    @Autowired
    private IExmWillconditionDeleteService deleteService;


    /**
     * 列表服务
     */
    @Autowired
    private IExmWillconditionGetListService getListService;


    /**
     * map服务
     */
    @Autowired
    private IExmWillconditionGetMapService getMapService;


    /**
     * 详情服务
     */
    @Autowired
    private IExmWillconditionGetDetailService getDetailService;


    /**
     * 编辑服务
     */
    @Autowired
    private IExmWillconditionEditService editService;


    @PostMapping(value = "/create")
    @ApiOperation("新增数据API")
    public Result<Long> create(@RequestBody ExmWillconditionBO req) {
        Long rsp = this.createService.create(req);
        return Result.success(rsp);
    }


    /**
     * Excel服务
     */
    @Autowired
    private IExmWillconditionExcelService excelService;



    @PostMapping(value = "/batchCreate")
    @ApiOperation("批量新增API")
    public Result<List<Long>> batchCreate(@RequestBody BaseBatchBO<ExmWillconditionBO> req) {
        List<Long> rsp = this.batchCreateService.batchCreate(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/deleteById")
    @ApiOperation("删除数据API(根据主键ID)")
    public Result<Boolean> deleteById(@RequestBody BaseIdBO req) {
        boolean rsp = this.deleteService.deleteById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/batchDeleteByIds")
    @ApiOperation("删除数据API(根据主键ID集合批量删除)")
    public Result<Boolean> batchDeleteByIds(@RequestBody BaseIdListBO req) {
        boolean rsp = this.deleteService.batchDeleteByIds(req);
        return Result.success(rsp);
    }



    @PostMapping(value = "/deleteBy")
    @ApiOperation("删除数据API(根据查询条件删除)")
    public Result<Boolean> deleteBy(@RequestBody ExmWillconditionQueryBO req) {
        boolean rsp = this.deleteService.batchDeleteBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/list")
    @ApiOperation("查询列表API")
    public Result<List<ExmWillconditionVO>> list(@RequestBody ExmWillconditionQueryBO req) {
        List<ExmWillconditionVO> rsp = this.getListService.getList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/mapGroupById")
    @ApiOperation("查询map(ID分组)API")
    public Result<Map<Long, ExmWillconditionVO>> mapGroupById(@RequestBody ExmWillconditionQueryBO req) {
        Map<Long, ExmWillconditionVO> rsp = this.getMapService.getMapGroupById(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/page")
    @ApiOperation("查询分页列表API")
    public Result<Page<ExmWillconditionVO>> page(@RequestBody ExmWillconditionQueryBO req) {
        Page<ExmWillconditionVO> rsp = this.getListService.getPageList(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailBy")
    @ApiOperation("根据查询条件查询详情API")
    public Result<ExmWillconditionVO> detailBy(@RequestBody ExmWillconditionQueryBO req) {
        ExmWillconditionVO rsp = this.getDetailService.getDetailBy(req);
        return Result.success(rsp);
    }


    @PostMapping(value = "/detailById")
    @ApiOperation("根据ID查询详情API")
    public Result<ExmWillconditionVO> detailById(@RequestBody BaseIdBO req) {
        ExmWillconditionVO rsp = this.getDetailService.getDetailById(req.getId());
        return Result.success(rsp);
    }


    @PostMapping(value = "/edit")
    @ApiOperation("修改数据API")
    public Result<Boolean> edit(@RequestBody ExmWillconditionBO req) {
        boolean rsp = this.editService.editById(req);
        return Result.success(rsp);
    }


    @GetMapping("/exportExcelModule")
    @ApiOperation("导出Excel模板API")
    @ResponseBody
    public void exportExcelModule(HttpServletResponse response) throws IOException {
        excelService.exportExcelModule(response);
    }


    @PostMapping("/importExcelData")
    @ApiOperation("导入Excel数据API")
    public Result<Boolean> importExcelData(@RequestParam("file") MultipartFile req) throws IOException {
        boolean rsp = excelService.importExcelData(req);
        return Result.success(rsp);
    }
}