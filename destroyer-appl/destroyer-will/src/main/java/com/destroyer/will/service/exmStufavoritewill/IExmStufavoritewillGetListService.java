package com.destroyer.will.service.exmStufavoritewill;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.destroyer.core.entity.exmStufavoritewill.bo.ExmStufavoritewillQueryBO;
import com.destroyer.core.entity.exmStufavoritewill.vo.ExmStufavoritewillVO;
import java.util.List;

 /**
 * 标题：学生收藏志愿(获取列表服务接口)
 * 说明：学生收藏志愿(获取列表服务接口),自定义基础列表查询,基础分页列表查询服务接口
 * 时间：2024-3-4
 * 作者：admin
 */
public interface IExmStufavoritewillGetListService {
    
    
    /**
     * 获取列表(根据QueryBO)
     * @param req
     * @return
     */
    List<ExmStufavoritewillVO> getList(ExmStufavoritewillQueryBO req);
    
    
    /**
     * 获取分页列表(根据QueryBO)
     * @param req
     * @return
     */
    Page<ExmStufavoritewillVO> getPageList(ExmStufavoritewillQueryBO req);
}