package com.destroyer.demo;

import com.destroyer.common.constant.PackageConstant;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@Slf4j
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan(PackageConstant.MAPPER_DESTROYER_DEMO)
@ComponentScan(basePackages = {PackageConstant.BASE_DESTROYER_CORE,PackageConstant.BASE_DESTROYER_DEMO})
@EnableFeignClients(PackageConstant.BASE_DESTROYER_CORE)
public class DestroyerDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DestroyerDemoApplication.class, args);
        log.info("=======================demo中心启动成功=======================");

    }

}
