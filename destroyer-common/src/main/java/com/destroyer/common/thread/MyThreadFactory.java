package com.destroyer.common.thread;
// Created by  on 2022/1/25.
//

import java.util.concurrent.ThreadFactory;

/**
 * 自定义线程名称。
 */
public class MyThreadFactory implements ThreadFactory {
    private String name = "MyThreadFactory";

    public MyThreadFactory(String name) {
        this.name = name;
    }

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(r, name + r.hashCode());
    }


}
