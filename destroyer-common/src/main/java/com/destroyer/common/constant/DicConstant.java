package com.destroyer.common.constant;


import java.io.Serializable;

/**
 * 标题：字典常量
 * 说明：字典常量,与数据库字典表数据对应，保证开发过程使用字典时的统一性，SysDicConstantService.getConstant生成字符串覆盖该类
 * 使用：DicConstant.SYS_BAN.BAN
 * 时间：2023/3/17
 * 作者：admin
 */
public class DicConstant implements Serializable {

    private static final long serialVersionUID = 2778758739260116843L;

    /**
     * 系统角色
     */
    public static final class SYS_ROLE {
        public static final String NAME = "SYS_ROLE";
        /**
         * 普通用户
         */
        public static final String NORMAL_USER = "normal";
        /**
         * 超级管理员
         */
        public static final String SUPER_ADMIN = "super_admin";
        /**
         * 学生用户
         */
        public static final String STU_USER = "stu_user";
    }

    /**
     * 基础用户状态
     */
    public static final class USER_STATE {
        public static final String NAME = "USER_STATE";
        /**
         * 正常
         */
        public static final String NORMAL = "normal";
        /**
         * 禁用
         */
        public static final String DISABLED = "disabled";
    }
}