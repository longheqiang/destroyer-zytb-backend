package com.destroyer.common.constant;

import java.io.Serializable;

/**
 * 标题：PackageConstant
 * 说明：包路径常量
 * 时间：2023/9/17
 * 作者：admin
 */
public class PackageConstant implements Serializable {
    private static final long serialVersionUID = -243573076448414260L;


    /**
     * 核心基础包路径
     */
    public static final String BASE_DESTROYER_CORE = "com.destroyer.core";


    /**
     * 核心mapper包路径
     */
    public static final String MAPPER_DESTROYER_CORE = "com.destroyer.core"+".mapper";

    /**
     * demo基础包路径
     */
    public static final String BASE_DESTROYER_DEMO = "com.destroyer.demo";


    /**
     * demo mapper包路径
     */
    public static final String MAPPER_DESTROYER_DEMO = BASE_DESTROYER_DEMO +".mapper";




    /**
     * 网关基础包路径
     */
    public static final String BASE_DESTROYER_GATEWAY = "com.destroyer.gateway";

    /**
     * 网关mapper包路径
     */
    public static final String MAPPER_SCZX_GATEWAY = BASE_DESTROYER_GATEWAY +".mapper";


    /**
     *系统中心基础包路径
     */
    public static final String BASE_DESTROYER_SYSTEM = "com.destroyer.system";

    /**
     *系统中心mapper包路径
     */
    public static final String MAPPER_DESTROYER_SYSTEM = BASE_DESTROYER_SYSTEM +".mapper";


    /**
     *用户中心基础包路径
     */
    public static final String BASE_DESTROYER_USER = "com.destroyer.user";


    /**
     *用户中心mapper包路径
     */
    public static final String MAPPER_DESTROYER_USER = BASE_DESTROYER_USER +".mapper";


    /**
     *志愿中心基础包路径
     */
    public static final String BASE_DESTROYER_WILL = "com.destroyer.will";


    /**
     *志愿中心mapper包路径
     */
    public static final String MAPPER_DESTROYER_WILL = BASE_DESTROYER_WILL +".mapper";




}
