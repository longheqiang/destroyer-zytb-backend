
package com.destroyer.common.exception;

import com.destroyer.common.entity.system.ResultEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 标题：ResultEnum
 * 说明：全局基础统一异常
 * 时间：2023/8/15
 * 作者：admin
 */
@Data
@Accessors(chain = true)
public class ServiceException extends RuntimeException {
    private ResultEnum result = ResultEnum.FAILURE;

    public ServiceException(String msg) {

        super(msg);
    }

    public ServiceException(ResultEnum result) {
        this.result = result;
    }

    public ServiceException(ResultEnum result, String msg) {
        super(msg);
        this.result = result;
    }


}
