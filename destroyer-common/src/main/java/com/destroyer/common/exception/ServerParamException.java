
package com.destroyer.common.exception;

import com.destroyer.common.entity.system.ResultEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 标题：ResultEnum
 * 说明：全局系业务统一异常
 * 时间：2023/8/15
 * 作者：admin
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ServerParamException extends RuntimeException {
    private ResultEnum result=ResultEnum.PARAM_MISS;

    public ServerParamException(String msg) {
        super(msg);
    }
    public ServerParamException(ResultEnum result) {
        this.result = result;
    }
    public ServerParamException(ResultEnum result, String msg) {
        super(msg);
        this.result = result;
    }



}
