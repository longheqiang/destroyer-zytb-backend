
package com.destroyer.common.exception;

import com.destroyer.common.entity.system.ResultEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 标题：ResultEnum
 * 说明：全局系业务统一异常
 * 时间：2023/8/15
 * 作者：admin
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ServerDataException extends RuntimeException {
    private ResultEnum result = ResultEnum.FAILURE;

    public ServerDataException() {
        super("记录未找到");
    }

    public ServerDataException(String msg) {
        super(msg);
    }

    public ServerDataException(ResultEnum result) {
        this.result = result;
    }

    public ServerDataException(ResultEnum result, String msg) {
        super(msg);
        this.result = result;
    }


}
