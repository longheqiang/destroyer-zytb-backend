
package com.destroyer.common.entity.system;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;


/**
 * 标题：统一返回响应类
 * 说明：统一返回响应类,全局统一的http请求返回格式
 * 时间：2023/8/15
 * 作者：admin
 */
@Data
@Accessors(chain = true)
@ApiModel("统一返回响应对象")
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 状态码
     */
    @ApiModelProperty(value = "状态码")
    private int code;


    /**
     * 返回消息
     */
    @ApiModelProperty(value = "返回消息")
    private String msg;

    /**
     * 是否成功
     */
    @ApiModelProperty(value = "是否成功")
    private boolean success;

    /**
     * 承载数据
     */
    @ApiModelProperty(value = "承载数据")
    private T data;


    public Result() {
    }

    public Result(int code, String msg, boolean success) {
        this.code = code;
        this.success = success;
        this.data = data;
        this.msg = msg;
    }

    public Result(int code, String msg, boolean success, T data) {
        this.code = code;
        this.success = success;
        this.data = data;
        this.msg = msg;
    }

    /**
     * 成功(默认)
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> success() {
        return new Result<T>(ResultEnum.SUCCESS.code, ResultEnum.SUCCESS.msg, true);
    }

    /**
     * 成功(自定义消息)
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(T data) {
        return new Result<T>(ResultEnum.SUCCESS.code, ResultEnum.SUCCESS.msg, true, data);
    }

    /**
     * 成功(自定义消息、自定义承载数据)
     *
     * @param msg
     * @param data
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(String msg, T data) {
        return new Result<T>(ResultEnum.SUCCESS.code, msg, true, data);
    }


    /**
     * 失败(默认)
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> fail() {
        return new Result<>(ResultEnum.FAILURE.code, ResultEnum.FAILURE.msg, false);
    }

    /**
     * 失败(自定义消息)
     *
     * @param msg
     * @param <T>
     * @return
     */
    public static <T> Result<T> fail(String msg) {
        return new Result<>(ResultEnum.FAILURE.code, msg, false);
    }

    /**
     * 失败(自定义枚举)
     *
     * @param result
     * @param <T>
     * @return
     */
    public static <T> Result<T> fail(ResultEnum result) {
        return new Result<>(result.code, result.msg, false);
    }

    /**
     * 失败(自定义枚举、自定义消息)
     *
     * @param result
     * @param msg
     * @param <T>
     * @return
     */
    public static <T> Result<T> fail(ResultEnum result, String msg) {
        return new Result<>(result.code, msg, false);
    }


}
