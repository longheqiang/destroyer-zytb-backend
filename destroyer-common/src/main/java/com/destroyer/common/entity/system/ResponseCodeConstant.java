package com.destroyer.common.entity.system;

/**
 * 标题：Http响应代码常量类
 * 说明：Http响应代码常量类
 * 时间：2023/8/15
 * 作者：admin
 */
public class ResponseCodeConstant {
    public static final int SC_OK = 200;
    public static final int SC_BAD_CRC = 201;
    public static final int SC_MODBUS_COMMAND_ERROR = 202;
    public static final int SC_BUSY = 203;
    public static final int SC_BAD_REQUEST = 400;
    public static final int SC_UNAUTHORIZED = 401;
    public static final int SC_BAD_SCOPE = 402;
    public static final int SC_OFFLINE = 800;
    public static final int SC_NOT_FOUND = 404;
    public static final int SC_METHOD_NOT_ALLOWED = 405;
    public static final int SC_UNSUPPORTED_MEDIA_TYPE = 406;
    public static final int SC_FORBIDDEN = 407;
    public static final int SC_INTERNAL_SERVER_ERROR = 500;
    public static final int TIME_OUT = 208;
}
