package com.destroyer.common.entity.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 标题：AuthUser
 * 说明：
 * 时间：2023/3/20
 * 作者：admin
 */
@Data
public class AuthBaseVO<T> implements Serializable {

    private static final long serialVersionUID = 5462242729899864991L;
    @ApiModelProperty(value = "token")
    private String token;
    @ApiModelProperty(value = "登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date loginTime;

    @ApiModelProperty(value = "过期时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date expireTime;

    /** 系统角色;超级管理员:admin,普通用户：normal */
    @ApiModelProperty(value = "系统角色;超级管理员:admin,普通用户：normal")
    private String sysRole ;

    @ApiModelProperty(value = "认证信息")
    private T auth;
}
