package com.destroyer.common.entity.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 标题：学生用户身份验证信息
 * 说明：学生用户身份验证信息
 * 时间：2023/3/20
 * 作者：admin
 */
@Data
public class AuthStudentVO implements Serializable {
    
    private static final long serialVersionUID = 5462242729899864991L;
    @ApiModelProperty(value = "用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String prono;


    /**
     * 高考年份
     */
    @ApiModelProperty(value = "高考年份")
    private Integer gkyear;


    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String stuname;


    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private String gender;


    /**
     * 高中
     */
    @ApiModelProperty(value = "高中")
    private String highschool;


    /**
     * 电话
     */
    @ApiModelProperty(value = "电话")
    private String phone;


    /**
     * 微信号
     */
    @ApiModelProperty(value = "微信号")
    private String weixin;


    /**
     * 微信uid
     */
    @ApiModelProperty(value = "微信uid")
    private String wxunid;


    /**
     * 余额
     */
    @ApiModelProperty(value = "余额")
    private BigDecimal balance;


    /**
     * 选科情况
     */
    @ApiModelProperty(value = "选科情况")
    private String subjects;


    /**
     * 职业兴趣
     */
    @ApiModelProperty(value = "职业兴趣")
    private String career;


    /**
     * 兴趣
     */
    @ApiModelProperty(value = "兴趣")
    private String interest;




    /**
     * 微信openid
     */
    @ApiModelProperty(value = "微信openid")
    private String wxopenid;

    /** 是否vip */
    @ApiModelProperty(value = "是否vip")
    private Boolean isvip ;


    /**
     * session_key
     */
    @ApiModelProperty(value = "session_key")
    private String sessionKey;

}
