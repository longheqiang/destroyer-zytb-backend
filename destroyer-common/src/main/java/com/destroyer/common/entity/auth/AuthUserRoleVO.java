package com.destroyer.common.entity.auth;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 标题：AuthRole
 * 说明：
 * 时间：2023/4/4
 * 作者：admin
 */
@Data
public class AuthUserRoleVO implements Serializable {
    
    private static final long serialVersionUID = 7117427568299819884L;

    /**主键ID */
    @ApiModelProperty(value = "角色ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;


    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    private String name;



    /**
     * 角色别名
     */
    @ApiModelProperty(value = "角色别名")
    private String alias;


    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long pid;


}
