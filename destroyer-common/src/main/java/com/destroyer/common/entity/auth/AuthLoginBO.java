package com.destroyer.common.entity.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 标题：UserLoginBO
 * 说明：
 * 时间：2023/2/23
 * 作者：admin
 */
@Data
public class AuthLoginBO implements Serializable {
    
    private static final long serialVersionUID = 279609616282505168L;

    /**
     * 账号
     */
    @ApiModelProperty(value = "账号")
    private String account;


    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String pwd;



}
