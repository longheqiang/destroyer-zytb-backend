package com.destroyer.common.entity.system;

/**
 * 标题：ResultEnum
 * 说明：统一请求响应枚举类
 * 时间：2023/8/15
 * 作者：admin
 */
public enum ResultEnum {

    /**
     * 操作成功
     */
    SUCCESS(ResponseCodeConstant.SC_OK, "操作成功"),

    /**
     * 业务异常
     */
    FAILURE(ResponseCodeConstant.SC_BAD_REQUEST, "业务异常"),

    /**
     * CRC业务异常
     */
    CRCFAILURE(ResponseCodeConstant.SC_BAD_CRC, "CRC检验出错"),

    /**
     * 请求未授权
     */
    UN_AUTHORIZED(ResponseCodeConstant.SC_UNAUTHORIZED, "请求未授权"),

    /**
     * 404 没找到请求
     */
    NOT_FOUND(ResponseCodeConstant.SC_NOT_FOUND, "404 没找到请求"),
    /**
     * 404 没找到记录
     */
    NOT_FOUND_RECORD(ResponseCodeConstant.SC_NOT_FOUND, "404 没找到相关记录"),

    /**
     * 消息不能读取
     */
    MSG_NOT_READABLE(ResponseCodeConstant.SC_BAD_REQUEST, "消息不能读取"),

    /**
     * 不支持当前请求方法
     */
    METHOD_NOT_SUPPORTED(ResponseCodeConstant.SC_METHOD_NOT_ALLOWED, "不支持当前请求方法"),

    /**
     * 不支持当前媒体类型
     */
    MEDIA_TYPE_NOT_SUPPORTED(ResponseCodeConstant.SC_UNSUPPORTED_MEDIA_TYPE, "不支持当前媒体类型"),

    /**
     * 请求被拒绝
     */
    REQ_REJECT(ResponseCodeConstant.SC_FORBIDDEN, "请求被拒绝"),

    /**
     * 服务器异常
     */
    INTERNAL_SERVER_ERROR(ResponseCodeConstant.SC_INTERNAL_SERVER_ERROR, "服务器异常"),

    /**
     * 缺少必要请求参数
     */
    PARAM_MISS(ResponseCodeConstant.SC_BAD_REQUEST, "缺少必要的请求参数"),

    /**
     * 请求参数类型错误
     */
    PARAM_TYPE_ERROR(ResponseCodeConstant.SC_BAD_REQUEST, "请求参数类型错误"),

    /**
     * 请求参数绑定错误
     */
    PARAM_BIND_ERROR(ResponseCodeConstant.SC_BAD_REQUEST, "请求参数绑定错误"),

    /**
     * 参数校验失败
     */
    PARAM_VALID_ERROR(ResponseCodeConstant.SC_BAD_REQUEST, "参数校验失败"),


    /**
     * 发送超时
     */
    SEND_TIME_OUT(ResponseCodeConstant.TIME_OUT, "超时"),
    ;

    /**
     * code编码
     */
    final int code;
    /**
     * 中文信息描述
     */
    final String msg;

    ResultEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}