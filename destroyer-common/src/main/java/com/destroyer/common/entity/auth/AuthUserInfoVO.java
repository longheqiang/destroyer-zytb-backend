package com.destroyer.common.entity.auth;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 标题：普通用户身份验证信息
 * 说明：普通用户身份验证信息
 * 时间：2023/3/20
 * 作者：admin
 */
@Data
public class AuthUserInfoVO implements Serializable {
    
    private static final long serialVersionUID = 5462242729899864991L;
    @ApiModelProperty(value = "用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /**
     * 用户代码
     */
    @ApiModelProperty(value = "用户代码")
    private String usercode;


    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String username;


    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称")
    private String nickname;


    /** 系统角色;超级管理员:admin,普通用户：normal */
    @ApiModelProperty(value = "系统角色;超级管理员:admin,普通用户：normal")
    private String sysRole ;



    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long roleId;


    /**
     * 拼音
     */
    @ApiModelProperty(value = "拼音")
    private String pinyin;


    /**
     * 用户类型
     */
    @ApiModelProperty(value = "用户类型")
    private String usertype;


    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;


    /**
     * 电话
     */
    @ApiModelProperty(value = "电话")
    private String mobile;


    /**
     * QQ号
     */
    @ApiModelProperty(value = "QQ号")
    private String qq;


    /**
     * 微信号
     */
    @ApiModelProperty(value = "微信号")
    private String weixin;


    /**
     * 主题
     */
    @ApiModelProperty(value = "主题")
    private String theme;


    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    private String avatar;


    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;


    /**
     * 最近登录时间
     */
    @ApiModelProperty(value = "最近登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastlogintime;


    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段")
    private String jsonExt;


    /**
     * 备用1
     */
    @ApiModelProperty(value = "备用1")
    private String opt1;


    /**
     * 备用2
     */
    @ApiModelProperty(value = "备用2")
    private String opt2;


    /**
     * 备用3
     */
    @ApiModelProperty(value = "备用3")
    private String opt3;


    /**
     * 省份代码
     */
    @ApiModelProperty(value = "省份代码")
    private String procode;


    /**
     * 用户状态;正常:normal,禁用:disabled
     */
    @ApiModelProperty(value = "用户状态;正常:normal,禁用:disabled")
    private String userState;


    /**
     * 用户状态;正常:normal,禁用:disabled
     */
    @ApiModelProperty(value = "用户状态;正常:normal,禁用:disabled")
    private String userStateStr;


    @ApiModelProperty(value = "角色")
    private AuthUserRoleVO role;

}
