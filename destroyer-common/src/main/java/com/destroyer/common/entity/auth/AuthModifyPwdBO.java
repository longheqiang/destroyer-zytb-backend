package com.destroyer.common.entity.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 标题：UserLoginBO
 * 说明：
 * 时间：2023/2/23
 * 作者：admin
 */
@Data
@ApiModel("修改密码(业务对象)")
public class AuthModifyPwdBO implements Serializable {
    
    private static final long serialVersionUID = 279609616282505168L;


    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String oldPwd;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String newPwd;

    /**
     * 密码
     */
    @ApiModelProperty(value = "重复密码")
    private String rePwd;



}
