package com.destroyer.common.util;

import com.destroyer.common.exception.ServiceException;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * @Author: admin
 * @date: 2022/06/14  11:41
 * @Description:Prop 配置参数工具类
 * @Version: 1.0
 */
public class PropertiesUtils {


    public static Properties properties;


    private PropertiesUtils() {}
    /**
     * 单例模式获取prop
     * @return
     */
    public static Properties getProperties(){
        if(null == properties){
            // 获取应用properties
            Properties applicationProperties = getConfig("application.properties");

            // 获取当前环境的properties
            String active = getPropValueByProp(applicationProperties,"spring.profiles.active");
            if(StringUtils.isNotBlank(active)){
                String propFileName = "application-"+active+".properties";
                properties = getConfig(propFileName);
            }

        }
        return properties;
    }

    /**
     * 读取 classpath 下 指定的properties配置文件，加载到Properties并返回Properties
     * @param name 配置文件名，如：mongo.properties
     * @return
     */
    private static Properties getConfig(String name){
        Properties props=null;
        try{
            props = new Properties();
            InputStream in = PropertiesUtils.class.getClassLoader().getResourceAsStream(name);
            BufferedReader bf = new BufferedReader(new InputStreamReader(in));
            props.load(bf);
            in.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return props;
    }

    /**
     * 获取当前环境Prop配置参数
     * @param key
     * @return
     */
    public static String getPropValue(String key){
        getProperties();
        return getPropValueByProp(properties,key);
    }
    /**
     * 根据Prop获取Prop配置参数
     * @param key
     * @return
     */
    public static String getPropValueByProp(Properties prop,String key){
        if(StringUtils.isBlank(key)){
            throw new ServiceException("获取应用配置信息时，属性名称不能为空！");
        }
        if(null == prop){
            throw new ServiceException("获取应用配置信息未获取到配置文件信息！");
        }

        String value = prop.getProperty(key);
        if(value == null){
            return null;
        }
        value = value.trim();
        //判断是否是环境变量配置属性,例如 server.env=${serverEnv:local}
        if(value.startsWith("${") && value.endsWith("}") && value.contains(":")){
            int indexOfColon = value.indexOf(":");
            String envName = value.substring(2,indexOfColon);
            //获取系统环境变量 envName 的内容，如果没有找到，则返回defaultValue
            String envValue = System.getenv(envName);
            if(envValue == null){
                //配置的默认值
                return value.substring(indexOfColon+1,value.length()-1);
            }
            return envValue;
        }
        return value;
    }
}
