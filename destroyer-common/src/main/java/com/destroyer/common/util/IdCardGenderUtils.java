package com.destroyer.common.util;

/**
 * 标题：IdCardGenderUtils
 * 说明：
 * 时间：2023/10/23
 * 作者：admin
 */
public class IdCardGenderUtils {
    /**
     * 根据身份证号码获取性别信息
     * @param idCard 身份证号码
     * @return 性别信息，M-男，F-女，N-未知
     */
    public static String getGenderByIdCard(String idCard) {
        if (idCard == null || idCard.length() < 17) {
            //无效
            return "N";
        }
        int genderDigit = Character.getNumericValue(idCard.charAt(16));
        return (genderDigit % 2 == 0) ? "F" : "M";
    }

    public static boolean isM(String idCard){
        return getGenderByIdCard(idCard) == "M";
    }

    public static boolean isF(String idCard){
        return getGenderByIdCard(idCard) == "F";
    }
}
