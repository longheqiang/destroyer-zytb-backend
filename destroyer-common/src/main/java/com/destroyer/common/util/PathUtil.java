package com.destroyer.common.util;

import java.net.URL;

/**
 * 标题：ClassLoaderUtil
 * 说明：
 * 时间：2024/3/18
 * 作者：admin
 */
public class PathUtil {

    /**
     * 从类资源路径中获取资源文件路径
     * @param classPath eg：classpath:/wechat/apiclient_key.pem
     * @return wechat/apiclient_key.pem
     */
    public static String getResourcesPathFromClassPath(String classPath){
        String rsp = null;
        if (classPath.startsWith("classpath:")) {
            int startIndex = "classpath:".length();
            rsp = classPath.substring(startIndex);
        }
        return rsp;
    }

    /**
     * 从类资源路径中获取资源文件路径
     * @param classPath eg：classpath:/wechat/apiclient_key.pem
     * @param c
     * @return wechat/apiclient_key.pem
     */
    public static String getResourcesPathFromClassPath(String classPath,Class c){
        String rsp = null;
        if (classPath.startsWith("classpath:")) {
            int startIndex = "classpath:".length();
            classPath = classPath.substring(startIndex);
        }
        ClassLoader classLoader = c.getClassLoader();
        URL resourceUrl = classLoader.getResource(classPath);
        if(null != resourceUrl){
            rsp = resourceUrl.getPath();
        }
        return rsp;
    }
}
