package com.destroyer.common.util;

import org.springframework.beans.factory.annotation.Value;

/**
 * 标题：SnowflakeIdUtil
 * 说明：单例模式获取ID
 * 时间：2021/2/22
 * 作者：nljlhq
 */
public class IdUtils {

    /** 工作机器ID(0~31) */
    @Value("${server.workerId:0}")
    private static long workerId;
    /** 数据中心ID(0~31) */
    @Value("${server.datacenterId:0}")
    private static long datacenterId;

    /**
     * 雪花序列工具类
     */
    private static SnowflakeIdUtils snowflakeIdUtils;

    /**
     * 获取一个雪花序列
     * @return
     */
    public static Long nextId(){
        if(snowflakeIdUtils == null){
            snowflakeIdUtils = new SnowflakeIdUtils(workerId,datacenterId);
        }
         return snowflakeIdUtils.nextId();
    }
}
