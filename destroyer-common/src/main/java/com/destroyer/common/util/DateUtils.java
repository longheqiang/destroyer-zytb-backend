package com.destroyer.common.util;


import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 标题：TimeUtil
 * 说明：
 * 时间：2021/4/7
 * 作者：nljlhq
 */
public class DateUtils {
    public static final String YYYYMMDDHHMMSS = "yyyy-MM-dd HH:mm:ss";
    public static final String YYYYMMDDHHMMSS2 = "yyyy/MM/dd HH:mm:ss";
    public static final String YYYYMMDDHHMMSS3 = "yyyyMMddHHmmss";
    public static final String YYYYMMDD = "yyyy-MM-dd";
    public static final String YYYYMMDD2 = "yyyy/MM/dd";
    public static final String YYYYMMDD3 = "yyyyMMdd";


    /**
     * yyyy/MM/dd HH:mm:ss
     * @param strDate
     * @return
     */
    public static Date strToDateyyyyMMddHHmmss(String strDate) {
        if (StringUtils.isBlank(strDate)) {
            return null;
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS);
            ParsePosition pos = new ParsePosition(0);
            Date strtodate = formatter.parse(strDate, pos);
            return strtodate;
        }
    }

    /**
     * yyyy/MM/dd HH:mm:ss
     * @param strDate
     * @return
     */
    public static Date strToDateyyyyMMddHHmmss2(String strDate) {
        if (StringUtils.isBlank(strDate)) {
            return null;
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS2);
            ParsePosition pos = new ParsePosition(0);
            Date strtodate = formatter.parse(strDate, pos);
            return strtodate;
        }
    }
    /**
     * 无间隔YYYYMMDDHHMMSS
     * @param strDate
     * @return
     */
    public static Date strToDateyyyyMMddHHmmss3(String strDate) {
        if (StringUtils.isBlank(strDate)) {
            return null;
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS3);
            ParsePosition pos = new ParsePosition(0);
            Date strtodate = formatter.parse(strDate, pos);
            return strtodate;
        }
    }

    /**yyy-MM-dd
     * @param strDate
     * @return
     */
    public static Date strToDateYYYYMMDD(String strDate) {
        if (StringUtils.isBlank(strDate)) {
            return null;
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD);
            ParsePosition pos = new ParsePosition(0);
            Date strtodate = formatter.parse(strDate, pos);
            return strtodate;
        }
    }
    /**yyy/MM/dd
     * @param strDate
     * @return
     */
    public static Date strToDateYYYYMMDD2(String strDate) {
        if (StringUtils.isBlank(strDate)) {
            return null;
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD2);
            ParsePosition pos = new ParsePosition(0);
            Date strtodate = formatter.parse(strDate, pos);
            return strtodate;
        }
    }
    /**yyyMMdd
     * @param strDate
     * @return
     */
    public static Date strToDateYYYYMMDD3(String strDate) {
        if (StringUtils.isBlank(strDate)) {
            return null;
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD3);
            ParsePosition pos = new ParsePosition(0);
            Date strtodate = formatter.parse(strDate, pos);
            return strtodate;
        }
    }




    /**yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static Date getCurrentDateYYYYMMDDHHMMSS() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS);
        return strToDateyyyyMMddHHmmss(formatter.format(date));
    }

    /**yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static Date getCurrentDateYYYYMMDDHHMMSS(Integer differ) {
        Calendar calendar = new GregorianCalendar();
        Date date = new Date();
        calendar.setTime(date);
        calendar.add(calendar.DATE,differ);
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS);
        return strToDateYYYYMMDD2(formatter.format(calendar.getTime()));
    }
    /**yyyy/MM/dd HH:mm:ss
     * @return
     */
    public static Date getCurrentDateYYYYMMDDHHMMSS2() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS2);
        return strToDateyyyyMMddHHmmss2(formatter.format(date));
    }
    /**yyyy/MM/dd HH:mm:ss
     * @return
     */
    public static Date getCurrentDateYYYYMMDDHHMMSS2(Integer differ) {
        Calendar calendar = new GregorianCalendar();
        Date date = new Date();
        calendar.setTime(date);
        calendar.add(calendar.DATE,differ);
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS2);
        return strToDateYYYYMMDD2(formatter.format(calendar.getTime()));
    }
    /**yyyy-MM-dd
     * @return
     */
    public static Date getCurrentDateYYYYMMDD() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD);
        return strToDateYYYYMMDD(formatter.format(date));
    }
    /**yyyy-MM-dd
     * @return
     */
    public static Date getCurrentDateYYYYMMDD(Integer differ) {
        Calendar calendar = new GregorianCalendar();
        Date date = new Date();
        calendar.setTime(date);
        calendar.add(calendar.DATE,differ);
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD);
        return strToDateYYYYMMDD2(formatter.format(calendar.getTime()));
    }
    /**yyyy/MM/dd
     * @return
     */
    public static Date getCurrentDateYYYYMMDD2() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD2);
        return strToDateYYYYMMDD2(formatter.format(date));
    }
    /**yyyy/MM/dd
     * @return
     */
    public static Date getCurrentDateYYYYMMDD2(Integer differ) {
        Calendar calendar = new GregorianCalendar();
        Date date = new Date();
        calendar.setTime(date);
        calendar.add(calendar.DATE,differ);
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD2);
        return strToDateYYYYMMDD2(formatter.format(calendar.getTime()));
    }
    /**yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getCurrentDateStrYYYYMMDDHHMMSS() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS);
        return formatter.format(date);
    }
    /**yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getCurrentDateStrYYYYMMDDHHMMSS(Integer differ) {
        Calendar calendar = new GregorianCalendar();
        Date date = new Date();
        calendar.setTime(date);
        calendar.add(calendar.DATE,differ);
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS);
        return formatter.format(calendar.getTime());
    }

    /**yyyy/MM/dd HH:mm:ss
     * @return
     */
    public static String getCurrentDateStrYYYYMMDDHHMMSS2() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS2);
        return formatter.format(date);
    }
    /**yyyy/MM/dd HH:mm:ss
     * @return
     */
    public static String getCurrentDateStrYYYYMMDDHHMMSS2(Integer differ) {
        Calendar calendar = new GregorianCalendar();
        Date date = new Date();
        calendar.setTime(date);
        calendar.add(calendar.DATE,differ);
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS2);
        return formatter.format(calendar.getTime());
    }
    /**yyyyMMddHHmmss
     * @return
     */
    public static String getCurrentDateStrYYYYMMDDHHMMSS3() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS3);
        return formatter.format(date);
    }
    /**
     * yyyy-MM-dd
     * @return
     */
    public static String getCurrentDateStrYYYYMMDD() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD);
        return formatter.format(date);
    }
    /**
     * yyyy-MM-dd
     * @return
     */
    public static String getCurrentDateStrYYYYMMDD(Integer differ) {
        Calendar calendar = new GregorianCalendar();
        Date date = new Date();
        calendar.setTime(date);
        calendar.add(calendar.DATE,differ);
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD);
        return formatter.format(calendar.getTime());
    }
    /**
     * yyyy/MM/dd
     * @return
     */
    public static String getCurrentDateStrYYYYMMDD2() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD2);
        return formatter.format(date);
    }
    /**
     * yyyyMMdd
     * @return
     */
    public static String getCurrentDateStrYYYYMMDD3() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD3);
        return formatter.format(date);
    }

    /**
     * yyyy-MM-dd HH:mm:ss
     * @param date
     * @return
     */
    public static String dateToStrYYYYMMDDHHMMSS(Date date) {
        if (date == null) {
            return null;
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS);
            String dateString = formatter.format(date);
            return dateString;
        }
    }
    /**
     * yyyy/MM/dd HH:mm:ss
     * @param date
     * @return
     */
    public static String dateToStrYYYYMMDDHHMMSS2(Date date) {
        if (date == null) {
            return "";
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS2);
            String dateString = formatter.format(date);
            return dateString;
        }
    }
    /**
     * yyyyMMddHHmmss
     * @param date
     * @return
     */
    public static String dateToStrYYYYMMDDHHMMSS3(Date date) {
        if (date == null) {
            return "";
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDDHHMMSS3);
            String dateString = formatter.format(date);
            return dateString;
        }
    }
    /**
     * yyyy-MM-dd
     * @param date
     * @return
     */
    public static String dateToStrYYYYMMDD(Date date) {
        if (date == null) {
            return "";
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD);
            String dateString = formatter.format(date);
            return dateString;
        }
    }

    /**
     * yyyy/MM/dd
     * @param date
     * @return
     */
    public static String dateToStrYYYYMMDD2(Date date) {
        if (date == null) {
            return "";
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD2);
            String dateString = formatter.format(date);
            return dateString;
        }
    }
    /**
     * yyyyMMdd
     * @param date
     * @return
     */
    public static String dateToStrYYYYMMDD3(Date date) {
        if (date == null) {
            return "";
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat(YYYYMMDD3);
            String dateString = formatter.format(date);
            return dateString;
        }
    }

    /**
     * 计算两个日期间相差秒数
     * @param startDate
     * @param endDate
     * @return
     * @throws ParseException
     */
    public static Long secondsBetween(Date startDate, Date endDate){
        Long start = startDate.getTime();
        Long end = endDate.getTime();
        Long rsp = (end - start)/1000;
        return rsp;
    }

    public static String utcToGM8(String timeStr) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(YYYYMMDDHHMMSS);

        // 解析时间字符串为 LocalDateTime
        LocalDateTime localDateTime = LocalDateTime.parse(timeStr, formatter);

        // 将 LocalDateTime 转换为 UTC 时间的 ZonedDateTime
        ZonedDateTime utcZonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.of("UTC"));

        // 将 UTC 时间转换为 GMT+8 时间
        ZonedDateTime gmtPlus8ZonedDateTime = utcZonedDateTime.withZoneSameInstant(ZoneId.of("Asia/Shanghai"));

        // 格式化为字符串输出
        return gmtPlus8ZonedDateTime.format(formatter);
    }
}
