package com.destroyer.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DestroyerCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(DestroyerCommonApplication.class, args);

    }

}
