package com.destroyer.common.excel.converter;

import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

/**
 * 数据型的转换器
 */
public class DoubleConverter implements Converter<Double> {

    @Override
    public Double convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        String cellValue = cellData.getStringValue();
        if (StrUtil.isEmpty(cellValue)) {
            return 0.0;
        }
        try {
            return Double.parseDouble(cellValue);
        }catch (Exception e){
            e.printStackTrace();
        }
       return 0.0;
    }
}
    