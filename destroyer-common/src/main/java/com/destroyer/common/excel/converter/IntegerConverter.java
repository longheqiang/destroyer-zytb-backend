package com.destroyer.common.excel.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

/**
 * 数据型的转换器
 */
public abstract class IntegerConverter implements Converter<Integer> {
    public abstract String[] getArrays();

    public  int defaultVal(){
        return 0;
    }
    @Override
    public Integer convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        String cellValue = cellData.getStringValue();
        String[] arrays = getArrays();
        for (int i = 0; i < arrays.length; i++) {
            if (arrays[i].equals(cellValue)) {
                return i;
            }
        }
        return defaultVal();
    }
}
    