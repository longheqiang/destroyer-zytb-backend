package com.destroyer.common.excel.ann;

import java.lang.annotation.*;

/**
 * 自己下拉定义注解
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface DropExcelProperty {
    /**
     * 下拉数据源
     *
     * @return
     */
    String[] options();

    /**
     * 所在列
     *
     * @return
     */
    int startCol();

    /**
     * 所在列结束
     *
     * @return
     */
    int endCol();


}
