package com.destroyer.common.excel.ann;

import org.apache.poi.ss.usermodel.IndexedColors;

import java.lang.annotation.*;

/**
 * 自定义颜色注解,默认红色
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface TitleColor {
    /**
     * 列
     *
     * @return
     */
    int col();

    /**
     * 颜色
     *
     * @return
     */
    IndexedColors color() default IndexedColors.RED;
}
