package com.destroyer.common.excel.converter;

import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

import java.util.Map;

/**
 * 数据型的转换器
 */
public abstract class BoolConverter implements Converter<Boolean> {
    public abstract Map<String,Boolean> getMap();

    public Boolean defaultVal(){
        return false;
    }
    @Override
    public Boolean convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        String cellValue = cellData.getStringValue();
        if(StrUtil.isEmpty(cellValue)){
            return defaultVal();
        }
        return getMap().getOrDefault(cellValue,defaultVal());
    }
}
    