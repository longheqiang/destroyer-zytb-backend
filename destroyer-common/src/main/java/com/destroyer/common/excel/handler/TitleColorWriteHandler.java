package com.destroyer.common.excel.handler;

import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.handler.context.CellWriteHandlerContext;
import org.apache.poi.ss.usermodel.*;

import java.util.Map;

public class TitleColorWriteHandler implements CellWriteHandler {
    /**
     * 列->颜色
     */

    private final Map<Integer,IndexedColors> map;

    public TitleColorWriteHandler(Map<Integer,IndexedColors> map) {
        this.map=map;
    }

    @Override
    public void afterCellDispose(CellWriteHandlerContext context) {
        // 当前单元格的坐标，可以通过context获取
        int rowIndex = context.getRowIndex();
        int columnIndex = context.getColumnIndex();
        // 这里我们只对头部单元格进行样式设置
        if (rowIndex == 0) {
            Cell cell = context.getCell();
            IndexedColors indexedColors = map.get(columnIndex);
            if (indexedColors!=null) {
                Workbook workbook = cell.getSheet().getWorkbook();
                CellStyle cellStyle = workbook.createCellStyle();
                Font font = workbook.createFont();
                font.setColor(indexedColors.getIndex());
                cellStyle.setFont(font);
                cell.setCellStyle(cellStyle);
            }
        }
    }
}
