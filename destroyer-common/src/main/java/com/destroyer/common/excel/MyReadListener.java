package com.destroyer.common.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;

public class MyReadListener<T> implements ReadListener<T> {
    ReadBack<T> readBack;
    public MyReadListener(ReadBack<T> readBack){
        this.readBack=readBack;
    }
    @Override
    public void invoke(T t, AnalysisContext analysisContext) {
        this.readBack.onRead(t);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
