package com.destroyer.common.excel;

/**
 * 读取实体回调
 * @param <T>
 */
public interface ReadBack<T> {
    void onRead(T t);
}
