package com.destroyer.common.excel.converter;

import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import org.apache.poi.ss.formula.functions.T;

import java.util.Map;

/**
 * 数据型的转换器
 */
public abstract class MapConverter implements Converter<T> {
    public abstract Map<String, T> getMap();

    public T defaultVal() {
        return new T();
    }

    @Override
    public T convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        String cellValue = cellData.getStringValue();
        if (StrUtil.isEmpty(cellValue)) {
            return defaultVal();
        }
        return getMap().getOrDefault(cellValue, defaultVal());
    }
}
    